This is the readme for generating a program file from the provided source code.

Open Quartus Prime 18.1 and select 'Open Project' then navigate to wherever the project 
was saved to. Select the .qpf file and press 'Open'

------
Below should be done if opening the project for the first time:
------

Go to the Generate dropdown and select 'Generate HDL..'.
At the first dropdown from the popup, change the HDL from 'Verilog' to 'VHDL'.
Then press generate.

A popup should appear to generate the files, which might take a few minutes.
When it completes, press Close, then exit Qsys.

Return to Quartus Prime, which might display a popup saying:
"You have created an IP Variation in the file"
Press OK

-----
--Generating .rbf file (aka the file to load onto the board)
-----
After a compilation is finished, a .rbf file should be in the root fpga directory titled 'av_soc_adc.rbf'.
This generally works to be used but another method of generating the .rbf file is outlined below.

First, go to file>convert programming files

A new window should appear.
Change the programming file type to read 'Raw Binary File (rbf)'.
Set the file name to read 'output_files/av_soc_adc.rbf'

Then, under input files to convert, click on the SOF data and click 'add file' to the right
Navigate to the 'output_files' directory and select 'av_soc_adc.sof'.
Then select generate, and the rbf file should be created.

This file will be located in the 'output_files' directory

If a warning about overwriting files appears, it's fine to just ignore it and continue.

-----
--Other Notes
-----

Additionally, do NOT upgrade IP files if prompted. An IP upgrade can potentially create system mismatches.

The program should be ready for modification of Compilation
To open the project in the future, open the '.qpf' file
