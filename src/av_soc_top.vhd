--===========================================================================
--
-- Project:    Zyvex av_soc_top FPGA
--
-- Date:       2015-08-21
-- Engineer:   Jeff Short
--             Micro Technology Services, Inc
--             1819 Firman Drive, Suite #137
--             Richardson, TX
--             972-231-6874, x103
--             jeff@mitsi.com
--
-- Target Board:  Altera Arria V SOC Board w/ ghrd_5astfd5k3
--
--
-- Target FPGA:   Altera Arria V - 5astfd5k3
--
--
--===========================================================================
-- Revisions:
-- 2015-08-21, Jeff Short
--    Initial creation.  Converted Altera Golden Reference Design Top Level
--    from verilog to vhdl.
--
-- 2016-01-27, Jeff Short
--    Updated input/output signals from final schematics.
--
-- 2016-10-07, Jeff Short
--    Updates for Rev B Analog Board - Note that Rev A boards will work fine
--    with this updated FPGA build, but the new features will not be available.
--    Changed System ID in Qsys from 0 to 1.
--    Added ADC_EN[3] output to J10.D11 (FPGA Pin E27), 2.5V for Analog Board Rev B
--    Added EEPROM WP output to J10.D12 (FPGA Pin F27), 2.5V for Analog Board Rev B
--
--===========================================================================

library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use ieee.math_real.all;
library altera_mf;
use altera_mf.altera_mf_components.all;
--use altera.altera_syn_attributes.all;


entity av_soc_top is
--   generic (
--   );
   port (
      --FPGA clock and reset
      fpga_clk_50             : in    std_logic;   --50MHz, 2.5V
      fpga_clk_100            : in    std_logic;   --100MHz, 2.5V
      clk_top1                : in    std_logic;   --156.25MHz
      clk_bot1                : in    std_logic;   --100MHz adjustable, 1.5V
      clkin_125               : in    std_logic;   --125MHz, clk_enet1_fpga_p

      cpu_reset_n             : in    std_logic;   --cpu_resetn

      --ADC Interface
      adc_ref_clk_out         : out   std_logic;                     --ADC reference clock.  Requires external fanout
      adc_conv_out            : out   std_logic;                     --ADC conversion start strobe.  Requires external fanout
      adc_clk_in              : in    std_logic_vector(7 downto 0);  --Return clock from each adc
      adc_serial_din          : in    std_logic_vector(7 downto 0);  --Serial data from each adc
      adc_en                  : out   std_logic_vector(3 downto 0);  --ADC mode select pins
      
      --DAC Interface
      dac_reset_out_n         : out   std_logic;                     --Reset output to DAC's, active low
      dac_clr_out_n           : out   std_logic;                     --Clear output to DAC's, active low
      dac_sclk_out            : out   std_logic;                     --Clock output to DAC's
      dac_sync_out_n          : out   std_logic;                     --Sync strobe to DAC's, active low
      dac_ldac_out_n          : out   std_logic;                     --Load DAC output, active low
      dac_sdin_out            : out   std_logic_vector(7 downto 0);  --Serial data sent to DAC's
      dac_sdo_in              : in    std_logic_vector(7 downto 0);  --Serial data received from DAC's

      --LCD Display
      display_sclk_out        : out   std_logic;                     --SPI Clock to display module
      display_cs_out_n        : out   std_logic;                     --SPI chip select to display module, active low
      display_miso            : in    std_logic;                     --SPI miso from display module
      display_mosi            : out   std_logic;                     --SPI mosi to display module
      
      --Digital Board Shift Chain
      dig_sclk_out            : out   std_logic;                     --Shift clock to digital board shift chain
      dig_latch_out           : out   std_logic;                     --Latch output to digital board shift chain
      dig_load_out_n          : out   std_logic;                     --Load output to digital board shift chain
      dig_miso                : in    std_logic;                     --Master in, slave out from digital board shift chain
      dig_mosi                : out   std_logic;                     --Master out, slave in to digital board shift chain
      
      --I2C Bus
      i2c_scl                 : inout std_logic;
      i2c_sda                 : inout std_logic;
      eeprom_wp               : out   std_logic;                     --EEPROM write protect (active high - 0=enable writes, 1=protected)
      power_adc_alert_n       : in    std_logic;                     --Alert output from power supply ADC monitor chip.  Needs FPGA pullup.
      
      enet_reset_n            : out   std_logic;

      --FPGA peripherals ports
      fpga_dipsw_pio          : in    std_logic_vector(3 downto 0);
      fpga_led_pio            : out   std_logic_vector(3 downto 0);
      fpga_button_pio         : in    std_logic_vector(3 downto 0);

      --HPS memory controller ports
      hps_memory_mem_a        : out   std_logic_vector(14 downto 0);
      hps_memory_mem_ba       : out   std_logic_vector(2 downto 0);
      hps_memory_mem_ck       : out   std_logic;
      hps_memory_mem_ck_n     : out   std_logic;
      hps_memory_mem_cke      : out   std_logic;
      hps_memory_mem_cs_n     : out   std_logic;
      hps_memory_mem_ras_n    : out   std_logic;
      hps_memory_mem_cas_n    : out   std_logic;
      hps_memory_mem_we_n     : out   std_logic;
      hps_memory_mem_reset_n  : out   std_logic;
      hps_memory_mem_dq       : inout std_logic_vector(39 downto 0);
      hps_memory_mem_dqs      : inout std_logic_vector(4 downto 0);
      hps_memory_mem_dqs_n    : inout std_logic_vector(4 downto 0);
      hps_memory_mem_odt      : out   std_logic;
      hps_memory_mem_dm       : out   std_logic_vector(4 downto 0);
      hps_memory_oct_rzqin    : in    std_logic;

      --HPS peripherals
      hps_emac1_TX_CLK        : out   std_logic;
      hps_emac1_TXD0          : out   std_logic;
      hps_emac1_TXD1          : out   std_logic;
      hps_emac1_TXD2          : out   std_logic;
      hps_emac1_TXD3          : out   std_logic;
      hps_emac1_RXD0          : in    std_logic;
      hps_emac1_MDIO          : inout std_logic;
      hps_emac1_MDC           : out   std_logic;
      hps_emac1_RX_CTL        : in    std_logic;
      hps_emac1_TX_CTL        : out   std_logic;
      hps_emac1_RX_CLK        : in    std_logic;
      hps_emac1_RXD1          : in    std_logic;
      hps_emac1_RXD2          : in    std_logic;
      hps_emac1_RXD3          : in    std_logic;
      hps_qspi_IO0            : inout std_logic;
      hps_qspi_IO1            : inout std_logic;
      hps_qspi_IO2            : inout std_logic;
      hps_qspi_IO3            : inout std_logic;
      hps_qspi_SS0            : out   std_logic;
      hps_qspi_CLK            : out   std_logic;
      hps_sdio_CMD            : inout std_logic;
      hps_sdio_D0             : inout std_logic;
      hps_sdio_D1             : inout std_logic;
      hps_sdio_CLK            : out   std_logic;
      hps_sdio_D2             : inout std_logic;
      hps_sdio_D3             : inout std_logic;
      hps_usb1_D0             : inout std_logic;
      hps_usb1_D1             : inout std_logic;
      hps_usb1_D2             : inout std_logic;
      hps_usb1_D3             : inout std_logic;
      hps_usb1_D4             : inout std_logic;
      hps_usb1_D5             : inout std_logic;
      hps_usb1_D6             : inout std_logic;
      hps_usb1_D7             : inout std_logic;
      hps_usb1_CLK            : in    std_logic;
      hps_usb1_STP            : out   std_logic;
      hps_usb1_DIR            : in    std_logic;
      hps_usb1_NXT            : in    std_logic;
      hps_uart0_RX            : in    std_logic;
      hps_uart0_TX            : out   std_logic;
      hps_uart1_RX            : in    std_logic;
      hps_uart1_TX            : out   std_logic;
      hps_i2c0_SDA            : inout std_logic;
      hps_i2c0_SCL            : inout std_logic;
      hps_trace_CLK           : out   std_logic;
      hps_trace_D0            : out   std_logic;
      hps_trace_D1            : out   std_logic;
      hps_trace_D2            : out   std_logic;
      hps_trace_D3            : out   std_logic;
      hps_trace_D4            : out   std_logic;
      hps_trace_D5            : out   std_logic;
      hps_trace_D6            : out   std_logic;
      hps_trace_D7            : out   std_logic;
      hps_gpio_GPIO00         : inout std_logic;
      hps_gpio_GPIO17         : inout std_logic;
      hps_gpio_GPIO18         : inout std_logic;
      hps_gpio_GPIO22         : inout std_logic;
      hps_gpio_GPIO24         : inout std_logic;
      hps_gpio_GPIO26         : inout std_logic;
      hps_gpio_GPIO27         : inout std_logic;
      hps_gpio_GPIO35         : inout std_logic;
      hps_gpio_GPIO40         : inout std_logic;
      hps_gpio_GPIO41         : inout std_logic;
      hps_gpio_GPIO42         : inout std_logic;
      hps_gpio_GPIO43         : inout std_logic
   );
end av_soc_top;

architecture av_soc_top_arch of av_soc_top is

   --------------------------------------------------------------------------
   -- Constants
   --------------------------------------------------------------------------
   constant NUM_ADC_CHANNELS           : natural := 8;   --Number of A/D converter channels
   constant NUM_ADC_BITS               : natural := 18;  --Number of bits per symbol
   constant NUM_ADC_LANES_PER_CHANNEL  : natural := 1;   --Number of incoming data lanes

   --------------------------------------------------------------------------
   -- Signal Definitions
   --------------------------------------------------------------------------
   signal clk_pll_0_outclk0         : std_logic;
   signal clk_pll_0_outclk0_reset   : std_logic;
   
   
   -- Internal wires and registers declaration
   signal clk_buf                   : std_logic;
   signal fpga_debounced_buttons    : std_logic_vector(3 downto 0);
   signal fpga_led_internal         : std_logic_vector(3 downto 0);
   signal hps_fpga_reset_n          : std_logic;
   signal stm_hw_events             : std_logic_vector(27 downto 0);
   signal hps_reset_req             : std_logic_vector(2 downto 0);
   signal hps_cold_reset            : std_logic;
   signal hps_warm_reset            : std_logic;
   signal hps_debug_reset           : std_logic;
   

   --ADC Signals
   type ADC_DATA_ARRAY is array (0 to NUM_ADC_CHANNELS-1) of std_logic_vector(NUM_ADC_BITS-1 downto 0);

   signal s_adc_en                  : std_logic_vector(3 downto 0);
   signal s_adc_reset               : std_logic;
   signal s_adc_data                : ADC_DATA_ARRAY;
   signal s_adc_data_out            : ADC_DATA_ARRAY;
   signal s_adc_wr_clk              : std_logic_vector(NUM_ADC_CHANNELS-1 downto 0);
   signal s_adc_clk                 : std_logic;
   signal s_temp_adc_data           : std_logic_vector(NUM_ADC_CHANNELS-1 downto 0);
   signal n_counter                 : natural range 0 to NUM_ADC_CHANNELS-1 := 0;

   --I2C Interface Signals
   signal s_i2c_sda_i               : std_logic;
   signal s_i2c_sda_o               : std_logic;
   signal s_i2c_sda_oen             : std_logic;
   signal s_i2c_scl_i               : std_logic;
   signal s_i2c_scl_o               : std_logic;
   signal s_i2c_scl_oen             : std_logic;
   
   --GPIO Signals
   signal s_gpio_in_0               : std_logic_vector(31 downto 0);
   signal s_gpio_out_0              : std_logic_vector(31 downto 0);
   signal s_gpio_shifter_in         : std_logic_vector(31 downto 0);
   signal s_gpio_shifter_out        : std_logic_vector(31 downto 0);
   
   --------------------------------------------------------------------------
   -- Component Definitions
   --------------------------------------------------------------------------
   component ALTCLKCTRa
   port (
      inclk             : in  std_logic;
      outclk            : out std_logic
   );
   end component;

   
   component hps_reset
   port (
      source_clk        : in  std_logic;
      source            : out std_logic_vector(2 downto 0)
   );
   end component;


   component debounce
   generic (
      WIDTH             : integer := 4;
      POLARITY          : string := "LOW";  --"LOW";
      TIMEOUT           : integer := 50000;  -- at 50Mhz this is a debounce time of 1ms
      TIMEOUT_WIDTH     : integer := 16      -- ceil(log2(TIMEOUT))
   );
   port (
      clk               : in  std_logic;
      reset_n           : in  std_logic;
      data_in           : in  std_logic_vector(WIDTH-1 downto 0);
      data_out          : out std_logic_vector(WIDTH-1 downto 0)
   );
   end component;


   component altera_edge_detector
   generic (
      PULSE_EXT               : integer := 32;
      EDGE_TYPE               : integer := 1;
      IGNORE_RST_WHILE_BUSY   : integer := 1
   );
   port (
      clk               : in  std_logic;
      rst_n             : in  std_logic;
      signal_in         : in  std_logic;
      pulse_out         : out std_logic
   );
   end component;

  
   --------------------------------------------------------------------------
   -- LVDS Deserializer
   --------------------------------------------------------------------------
   component lvds_deserializer
   generic (
      BITS_PER_SYMBOL      : natural range 4 to 24 := 18;   --Number of bits per symbol
      NUM_LANES            : natural range 1 to 10  := 1;   --Number of incoming data lanes
      MSB_FIRST            : boolean := false;              --Set high for MSB first operation, low for LSB first
      RISING_EDGE_FIRST    : boolean := true                --True=Capture rising edge data first,False=capture on falling edge first 
   );
   port (
      reset                : in std_logic;
      rx_clk_in            : in std_logic;
      rx_data_in           : in std_logic_vector (NUM_LANES-1 downto 0);
      rx_bit_slip_req      : in std_logic;
      rx_data_out          : out std_logic_vector (NUM_LANES * BITS_PER_SYMBOL-1 downto 0);
      rx_clk_out           : out std_logic 
   );
   end component;

   --------------------------------------------------------------------------
   --------------------------------------------------------------------------
   component PIO_MASTER_SHIFTER is
      generic (
         cSHIFT_REG_SIZE         : integer   := 16;         --Total size of shift register (read + write)
         cREF_CLOCK_FREQUENCY    : integer   := 60000000;   --60MHz Reference clock frequency (in Hz)
         cSHIFT_CLOCK_FREQUENCY  : integer   := 1000000;    --1MHz  Desired shift clock frequency (in Hz)
         cSHIFT_RATE             : integer   := 500;        --2ms   Desired shift rate (in Hz)
         cCLK_POLARITY           : boolean := true;         --False is same as CPOL=0:  Data changes on rising edge, sampled on falling edge
                                                            --True is same as CPOL=1:  Data changes on falling edge, sampled on rising edge
         cMSB_FIRST              : boolean   := true;       --MSB first (true) or LSB first (false)
         cSTROBE_ACTIVE_ENTIRE_TRANSFER : boolean := true;  --Strobe is asserted during entire transfer or pulsed at end of transfer
         cPULSE_OUTPUT_EN        : boolean   := true        --Set to allow SR_STROBE_EN_n to pulse during idle periods (for G1 EXP Board)
      );
      port (
         --Global Signals
         RESET                   : in  std_logic;           --Global reset
         REF_CLK                 : in  std_logic;           --Reference clock (usually OPB_CLK)

         --Data Inputs
         PIO_DATA_TO_SEND        : in  std_logic_vector(cSHIFT_REG_SIZE-1 downto 0);     --Parallel input data to send
         PIO_DATA_RECEIVED       : out std_logic_vector(cSHIFT_REG_SIZE-1 downto 0);     --Parallel output data read from shift register

         --Shift Register Control and Data Output
         SR_CLK_OUT              : out std_logic;           --Serial clock output
         SR_DATA_IN              : in  std_logic;           --Serial data input
         SR_DATA_OUT             : out std_logic;           --Serial data output
         SR_STROBE_OUT_n         : out std_logic;           --Serial strobe output, active low
         SR_OUTPUT_EN_n          : out std_logic            --Serial data output enable, active low
      );      
   end component PIO_MASTER_SHIFTER;  
   
   
   --SoC sub-system module
   component av_soc_adc is
   port (
      reset_50_reset_n                          : in    std_logic                     := 'X';             -- reset_n
      reset_reset_n                             : in    std_logic                     := 'X';             -- reset_n
      refclk_clk                                : in    std_logic                     := 'X';             -- clk
      clk_50_clk                                : in    std_logic                     := 'X';             -- clk
      clk_clk                                   : in    std_logic                     := 'X';             -- clk
      clk_pll_0_outclk0_clk                     : out   std_logic;                                        -- clk
      clk_pll_0_outclk0_reset_reset             : out   std_logic;                                        -- reset
      
      button_pio_external_connection_export     : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- export
      dipsw_pio_external_connection_export      : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- export
      
      hps_0_f2h_cold_reset_req_reset_n          : in    std_logic                     := 'X';             -- reset_n
      hps_0_f2h_debug_reset_req_reset_n         : in    std_logic                     := 'X';             -- reset_n
      hps_0_f2h_stm_hw_events_stm_hwevents      : in    std_logic_vector(27 downto 0) := (others => 'X'); -- stm_hwevents
      hps_0_f2h_warm_reset_req_reset_n          : in    std_logic                     := 'X';             -- reset_n
      hps_0_h2f_reset_reset_n                   : out   std_logic;                                        -- reset_n
      hps_io_hps_io_emac1_inst_TX_CLK           : out   std_logic;                                        -- hps_io_emac1_inst_TX_CLK
      hps_io_hps_io_emac1_inst_TXD0             : out   std_logic;                                        -- hps_io_emac1_inst_TXD0
      hps_io_hps_io_emac1_inst_TXD1             : out   std_logic;                                        -- hps_io_emac1_inst_TXD1
      hps_io_hps_io_emac1_inst_TX_CTL           : out   std_logic;                                        -- hps_io_emac1_inst_TX_CTL
      hps_io_hps_io_emac1_inst_RXD0             : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD0
      hps_io_hps_io_emac1_inst_RXD1             : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD1
      hps_io_hps_io_emac1_inst_TXD2             : out   std_logic;                                        -- hps_io_emac1_inst_TXD2
      hps_io_hps_io_emac1_inst_TXD3             : out   std_logic;                                        -- hps_io_emac1_inst_TXD3
      hps_io_hps_io_emac1_inst_MDIO             : inout std_logic                     := 'X';             -- hps_io_emac1_inst_MDIO
      hps_io_hps_io_emac1_inst_MDC              : out   std_logic;                                        -- hps_io_emac1_inst_MDC
      hps_io_hps_io_emac1_inst_RX_CTL           : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RX_CTL
      hps_io_hps_io_emac1_inst_RX_CLK           : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RX_CLK
      hps_io_hps_io_emac1_inst_RXD2             : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD2
      hps_io_hps_io_emac1_inst_RXD3             : in    std_logic                     := 'X';             -- hps_io_emac1_inst_RXD3
      hps_io_hps_io_qspi_inst_IO0               : inout std_logic                     := 'X';             -- hps_io_qspi_inst_IO0
      hps_io_hps_io_qspi_inst_IO1               : inout std_logic                     := 'X';             -- hps_io_qspi_inst_IO1
      hps_io_hps_io_qspi_inst_IO2               : inout std_logic                     := 'X';             -- hps_io_qspi_inst_IO2
      hps_io_hps_io_qspi_inst_IO3               : inout std_logic                     := 'X';             -- hps_io_qspi_inst_IO3
      hps_io_hps_io_qspi_inst_SS0               : out   std_logic;                                        -- hps_io_qspi_inst_SS0
      hps_io_hps_io_qspi_inst_CLK               : out   std_logic;                                        -- hps_io_qspi_inst_CLK
      hps_io_hps_io_sdio_inst_CMD               : inout std_logic                     := 'X';             -- hps_io_sdio_inst_CMD
      hps_io_hps_io_sdio_inst_D0                : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D0
      hps_io_hps_io_sdio_inst_D1                : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D1
      hps_io_hps_io_sdio_inst_CLK               : out   std_logic;                                        -- hps_io_sdio_inst_CLK
      hps_io_hps_io_sdio_inst_D2                : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D2
      hps_io_hps_io_sdio_inst_D3                : inout std_logic                     := 'X';             -- hps_io_sdio_inst_D3
      hps_io_hps_io_usb1_inst_D0                : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D0
      hps_io_hps_io_usb1_inst_D1                : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D1
      hps_io_hps_io_usb1_inst_D2                : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D2
      hps_io_hps_io_usb1_inst_D3                : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D3
      hps_io_hps_io_usb1_inst_D4                : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D4
      hps_io_hps_io_usb1_inst_D5                : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D5
      hps_io_hps_io_usb1_inst_D6                : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D6
      hps_io_hps_io_usb1_inst_D7                : inout std_logic                     := 'X';             -- hps_io_usb1_inst_D7
      hps_io_hps_io_usb1_inst_CLK               : in    std_logic                     := 'X';             -- hps_io_usb1_inst_CLK
      hps_io_hps_io_usb1_inst_STP               : out   std_logic;                                        -- hps_io_usb1_inst_STP
      hps_io_hps_io_usb1_inst_DIR               : in    std_logic                     := 'X';             -- hps_io_usb1_inst_DIR
      hps_io_hps_io_usb1_inst_NXT               : in    std_logic                     := 'X';             -- hps_io_usb1_inst_NXT
      hps_io_hps_io_uart0_inst_RX               : in    std_logic                     := 'X';             -- hps_io_uart0_inst_RX
      hps_io_hps_io_uart0_inst_TX               : out   std_logic;                                        -- hps_io_uart0_inst_TX
      hps_io_hps_io_uart1_inst_RX               : in    std_logic                     := 'X';             -- hps_io_uart1_inst_RX
      hps_io_hps_io_uart1_inst_TX               : out   std_logic;                                        -- hps_io_uart1_inst_TX
      hps_io_hps_io_i2c0_inst_SDA               : inout std_logic                     := 'X';             -- hps_io_i2c0_inst_SDA
      hps_io_hps_io_i2c0_inst_SCL               : inout std_logic                     := 'X';             -- hps_io_i2c0_inst_SCL
      hps_io_hps_io_trace_inst_CLK              : out   std_logic;                                        -- hps_io_trace_inst_CLK
      hps_io_hps_io_trace_inst_D0               : out   std_logic;                                        -- hps_io_trace_inst_D0
      hps_io_hps_io_trace_inst_D1               : out   std_logic;                                        -- hps_io_trace_inst_D1
      hps_io_hps_io_trace_inst_D2               : out   std_logic;                                        -- hps_io_trace_inst_D2
      hps_io_hps_io_trace_inst_D3               : out   std_logic;                                        -- hps_io_trace_inst_D3
      hps_io_hps_io_trace_inst_D4               : out   std_logic;                                        -- hps_io_trace_inst_D4
      hps_io_hps_io_trace_inst_D5               : out   std_logic;                                        -- hps_io_trace_inst_D5
      hps_io_hps_io_trace_inst_D6               : out   std_logic;                                        -- hps_io_trace_inst_D6
      hps_io_hps_io_trace_inst_D7               : out   std_logic;                                        -- hps_io_trace_inst_D7
      hps_io_hps_io_gpio_inst_GPIO00            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO00
      hps_io_hps_io_gpio_inst_GPIO17            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO17
      hps_io_hps_io_gpio_inst_GPIO18            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO18
      hps_io_hps_io_gpio_inst_GPIO22            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO22
      hps_io_hps_io_gpio_inst_GPIO24            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO24
      hps_io_hps_io_gpio_inst_GPIO26            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO26
      hps_io_hps_io_gpio_inst_GPIO27            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO27
      hps_io_hps_io_gpio_inst_GPIO35            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO35
      hps_io_hps_io_gpio_inst_GPIO40            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO40
      hps_io_hps_io_gpio_inst_GPIO41            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO41
      hps_io_hps_io_gpio_inst_GPIO42            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO42
      hps_io_hps_io_gpio_inst_GPIO43            : inout std_logic                     := 'X';             -- hps_io_gpio_inst_GPIO43
      
      led_pio_external_connection_in_port       : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- in_port
      led_pio_external_connection_out_port      : out   std_logic_vector(3 downto 0);                     -- out_port
      
      memory_mem_a                              : out   std_logic_vector(14 downto 0);                    -- mem_a
      memory_mem_ba                             : out   std_logic_vector(2 downto 0);                     -- mem_ba
      memory_mem_ck                             : out   std_logic;                                        -- mem_ck
      memory_mem_ck_n                           : out   std_logic;                                        -- mem_ck_n
      memory_mem_cke                            : out   std_logic;                                        -- mem_cke
      memory_mem_cs_n                           : out   std_logic;                                        -- mem_cs_n
      memory_mem_ras_n                          : out   std_logic;                                        -- mem_ras_n
      memory_mem_cas_n                          : out   std_logic;                                        -- mem_cas_n
      memory_mem_we_n                           : out   std_logic;                                        -- mem_we_n
      memory_mem_reset_n                        : out   std_logic;                                        -- mem_reset_n
      memory_mem_dq                             : inout std_logic_vector(39 downto 0) := (others => 'X'); -- mem_dq
      memory_mem_dqs                            : inout std_logic_vector(4 downto 0)  := (others => 'X'); -- mem_dqs
      memory_mem_dqs_n                          : inout std_logic_vector(4 downto 0)  := (others => 'X'); -- mem_dqs_n
      memory_mem_odt                            : out   std_logic;                                        -- mem_odt
      memory_mem_dm                             : out   std_logic_vector(4 downto 0);                     -- mem_dm
      memory_oct_rzqin                          : in    std_logic                     := 'X';             -- oct_rzqin
      
      i2c_master_ts_0_scl_pad_i                          : in    std_logic                     := 'X';             -- scl_pad_i
      i2c_master_ts_0_scl_pad_o                          : out   std_logic;                                        -- scl_pad_o
      i2c_master_ts_0_scl_padoen_o                       : out   std_logic;                                        -- scl_padoen_o
      i2c_master_ts_0_sda_pad_i                          : in    std_logic                     := 'X';             -- sda_pad_i
      i2c_master_ts_0_sda_pad_o                          : out   std_logic;                                        -- sda_pad_o
      i2c_master_ts_0_sda_padoen_o                       : out   std_logic;                                        -- sda_padoen_o

      ad5791_dac_clr_out_n                               : out   std_logic;                                        -- clr_out_n
      ad5791_dac_reset_out_n                             : out   std_logic;                                        -- reset_out_n
      ad5791_dac_sclk_out                                : out   std_logic;                                        -- sclk_out
      ad5791_dac_sync_out_n                              : out   std_logic;                                        -- sync_out_n
      ad5791_dac_ldac_out_n                              : out   std_logic;                                        -- ldac_out_n
      ad5791_dac_sdin_to_dac                             : out   std_logic_vector(7 downto 0);                     -- sdin_to_dac
      ad5791_dac_sdo_from_dac                            : in    std_logic_vector(7 downto 0)  := (others => 'X'); -- sdo_from_dac

      ad7960_ext_adc_clk_out                             : out   std_logic;                                        -- ext_clk_out
      ad7960_ext_adc_conv_start_out                      : out   std_logic;                                        -- adc_conv_start_out
      ad7960_ext_adc_en_bits                             : out   std_logic_vector(3 downto 0);                     -- adc_en_bits
      ad7960_ext_adc_rx_clk_in                           : in    std_logic_vector(7 downto 0)  := (others => 'X'); -- adc_rx_clk_in
      ad7960_ext_adc_rx_data_in                          : in    std_logic_vector(7 downto 0)  := (others => 'X'); -- adc_rx_data_in

      ad7960_dac_ref_ext_reset_out                       : out   std_logic;                                        -- reset_out
      ad7960_dac_ref_ext_ref_clk_out                     : out   std_logic;                                        -- ref_clk_out
      ad7960_dac_ref_ext_sync_out                        : out   std_logic;                                        -- sync_out
      
      gpio_0_in_port                                     : in    std_logic_vector(31 downto 0) := (others => 'X');
      gpio_0_out_port                                    : out   std_logic_vector(31 downto 0);

      gpio_shifter_in_port                               : in    std_logic_vector(31 downto 0) := (others => 'X');
      gpio_shifter_out_port                              : out   std_logic_vector(31 downto 0)
      
   );
   end component av_soc_adc;


  
--===========================================================================
--
--===========================================================================
begin

   --------------------------------------------------------------------------
   -- External Pin Assignments
   --------------------------------------------------------------------------
   --Unused Pins

   --Connection of internal logics
   s_adc_reset <= not cpu_reset_n;
   
-----FIXME - JCS - Need to create internal reset for this   
   enet_reset_n <= cpu_reset_n;


   fpga_led_pio     <= not fpga_led_internal;
   stm_hw_events    <= X"0000" & fpga_dipsw_pio & fpga_led_internal & fpga_debounced_buttons;

   adc_en(0) <= '0' when s_adc_en(0)='0' else 'Z';
   adc_en(1) <= '0' when s_adc_en(1)='0' else 'Z';
   adc_en(2) <= '0' when s_adc_en(2)='0' else 'Z';
   adc_en(3) <= '0' when s_adc_en(3)='0' else 'Z';
   
   --===========================================================================
   --GPIO Signals
   --===========================================================================
   --Inputs
   s_gpio_in_0(0)            <= power_adc_alert_n; --Power alert interrupt inputs
   s_gpio_in_0(18 downto 1)  <= s_gpio_out_0(18 downto 1); --Readback
   s_gpio_in_0(19)           <= display_miso;      --SPI miso from display module
   s_gpio_in_0(31 downto 20) <= s_gpio_out_0(31 downto 20); --Readback
   
   --Outputs
   eeprom_wp         <= s_gpio_out_0(1);           --EEPROM write protect (0=write enable, 1=protected)
   display_sclk_out  <= s_gpio_out_0(16);          --SPI Clock to display module
   display_cs_out_n  <= s_gpio_out_0(17);          --SPI chip select to display module, active low
   display_mosi      <= s_gpio_out_0(18);          --SPI mosi to display module
   
   --===========================================================================
   --Merge two external I2C busses into a single internal bus
   --===========================================================================
   s_i2c_scl_i      <= i2c_scl;
   i2c_scl          <= 'Z' when s_i2c_scl_oen='1' else s_i2c_scl_o;

   s_i2c_sda_i      <= i2c_sda;
   i2c_sda          <= 'Z' when s_i2c_sda_oen='1' else s_i2c_sda_o;
   
   --------------------------------------------------------------------------
   -- Misc Modules
   --------------------------------------------------------------------------
   
   -- Source/Probe megawizard instance
   hps_reset_inst : component hps_reset
   port map (
      source_clk     => clk_pll_0_outclk0,
      source         => hps_reset_req
   );

   -- Debounce logic to clean out glitches within 1ms
   debounce_inst : component debounce
   generic map (
      WIDTH          => 4,
      POLARITY       => "LOW",
      TIMEOUT        => 50000, -- at 50Mhz this is a debounce time of 1ms
      TIMEOUT_WIDTH  => 16     -- ceil(log2(TIMEOUT))
   ) 
   port map (
      clk            => clk_pll_0_outclk0,
      reset_n        => hps_fpga_reset_n,
      data_in        => fpga_button_pio,
      data_out       => fpga_debounced_buttons
   );

   pulse_cold_reset : component altera_edge_detector
   generic map (
      PULSE_EXT      => 6,
      EDGE_TYPE      => 1,
      IGNORE_RST_WHILE_BUSY => 1
   )
   port map (
      clk            => clk_pll_0_outclk0,
      rst_n          => hps_fpga_reset_n,
      signal_in      => hps_reset_req(0),
      pulse_out      => hps_cold_reset
   );

   pulse_warm_reset : component altera_edge_detector
   generic map (
      PULSE_EXT      => 2,
      EDGE_TYPE      => 1,
      IGNORE_RST_WHILE_BUSY => 1
   )
   port map (
      clk            => clk_pll_0_outclk0,
      rst_n          => hps_fpga_reset_n,
      signal_in      => hps_reset_req(1),
      pulse_out      => hps_warm_reset
   );

   pulse_debug_reset : component altera_edge_detector
   generic map (
      PULSE_EXT      => 32,
      EDGE_TYPE      => 1,
      IGNORE_RST_WHILE_BUSY => 1
   )
   port map (
      clk            => clk_pll_0_outclk0,
      rst_n          => hps_fpga_reset_n,
      signal_in      => hps_reset_req(2),
      pulse_out      => hps_debug_reset
   );

   --===========================================================================
   --CPLD-to-Nios Shift Register Communications Module
   --===========================================================================
   PIO_MASTER_SHIFTER_inst : PIO_MASTER_SHIFTER
      generic map (
         cSHIFT_REG_SIZE         => 32,               --Total size of shift register (read + write)
         cREF_CLOCK_FREQUENCY    => 50000000,         --Reference clock frequency (in Hz)
         cSHIFT_CLOCK_FREQUENCY  => 1000000,          --Desired shift clock frequency (in Hz)
         cSHIFT_RATE             => 1000,             --Desired shift rate (in Hz)
         cCLK_POLARITY           => true,             --False is same as CPOL=0:  Data changes on rising edge, sampled on falling edge
                                                      --True is same as CPOL=1:  Data changes on falling edge, sampled on rising edge
         cMSB_FIRST              => false,            --MSB first (true) or LSB first (false)
         cSTROBE_ACTIVE_ENTIRE_TRANSFER => false,     --Strobe is asserted during entire transfer or pulsed at end of transfer
         cPULSE_OUTPUT_EN        => false             --Set to allow SR_STROBE_EN_n to pulse during idle periods (for G1 EXP Board)
      )
      port map(
         --Global Input Signals
         RESET                   => not cpu_reset_n,  --Global reset
         REF_CLK                 => fpga_clk_50,      --Reference clock (NOTE:  Must be the same clock as the attached GPIO logic to avoid data glitches!!!)

         --Data Input
         PIO_DATA_TO_SEND        => s_gpio_shifter_out, --in  std_logic_vector(cSHIFT_REG_SIZE-1 downto 0);     --Parallel input data to send
         --Data Output
         PIO_DATA_RECEIVED       => s_gpio_shifter_in,  --out std_logic_vector(cSHIFT_REG_SIZE-1 downto 0);     --Parallel output data read from shift register

         --Shift Register Control and Data Output
         SR_CLK_OUT              => dig_sclk_out,     --Serial clock output
         SR_DATA_IN              => dig_miso,         --Serial data input
         SR_DATA_OUT             => dig_mosi,         --Serial data output
--         SR_STROBE_OUT_n         => dig_load_out_n,   --Serial strobe output, active low
--         SR_OUTPUT_EN_n          => dig_latch_out     --Serial data output enable (if needed)
         SR_STROBE_OUT_n         => dig_latch_out,    --Serial strobe output, active low
         SR_OUTPUT_EN_n          => dig_load_out_n    --Serial data output enable (if needed)
      ); --end of PIO_MASTER_SHIFTER;  

      

--   s_adc_clk <= s_adc_wr_clk(0);
--20160202, JCS - Temporary test clock
   s_adc_clk <= fpga_clk_50;
   
   
   
   --------------------------------------------------------------------------
   -- SoC sub-system module
   --------------------------------------------------------------------------
--    u0 : component av_nios_soc
--    port map (
--       reset_reset_n                             => hps_fpga_reset_n,
--       reset_50_reset_n                          => cpu_reset_n,
--       
--       refclk_clk                                => fpga_clk_100,
--       clk_50_clk                                => fpga_clk_50,      --50MHz
--       clk_clk                                   => clk_top1,         --156.25MHz
--       clk_pll_0_outclk0_clk                     => clk_pll_0_outclk0,       -- clk, 150MHz
--       clk_pll_0_outclk0_reset_reset             => clk_pll_0_outclk0_reset, -- reset
--       
--       button_pio_external_connection_export     => fpga_debounced_buttons,
--       dipsw_pio_external_connection_export      => fpga_dipsw_pio,
-- 
--       hps_0_f2h_cold_reset_req_reset_n          => not hps_cold_reset,
--       hps_0_f2h_debug_reset_req_reset_n         => not hps_debug_reset,
--       hps_0_f2h_stm_hw_events_stm_hwevents      => stm_hw_events,
--       hps_0_f2h_warm_reset_req_reset_n          => not hps_warm_reset,
--       hps_0_h2f_reset_reset_n                   => hps_fpga_reset_n,
-- 
--       hps_io_hps_io_emac1_inst_TX_CLK           => hps_emac1_TX_CLK,
--       hps_io_hps_io_emac1_inst_TXD0             => hps_emac1_TXD0,
--       hps_io_hps_io_emac1_inst_TXD1             => hps_emac1_TXD1,
--       hps_io_hps_io_emac1_inst_TXD2             => hps_emac1_TXD2,
--       hps_io_hps_io_emac1_inst_TXD3             => hps_emac1_TXD3,
--       hps_io_hps_io_emac1_inst_MDIO             => hps_emac1_MDIO,
--       hps_io_hps_io_emac1_inst_MDC              => hps_emac1_MDC,
--       hps_io_hps_io_emac1_inst_RX_CTL           => hps_emac1_RX_CTL,
--       hps_io_hps_io_emac1_inst_TX_CTL           => hps_emac1_TX_CTL,
--       hps_io_hps_io_emac1_inst_RX_CLK           => hps_emac1_RX_CLK,
--       hps_io_hps_io_emac1_inst_RXD0             => hps_emac1_RXD0,
--       hps_io_hps_io_emac1_inst_RXD1             => hps_emac1_RXD1,
--       hps_io_hps_io_emac1_inst_RXD2             => hps_emac1_RXD2,
--       hps_io_hps_io_emac1_inst_RXD3             => hps_emac1_RXD3,
--       hps_io_hps_io_qspi_inst_IO0               => hps_qspi_IO0,
--       hps_io_hps_io_qspi_inst_IO1               => hps_qspi_IO1,
--       hps_io_hps_io_qspi_inst_IO2               => hps_qspi_IO2,
--       hps_io_hps_io_qspi_inst_IO3               => hps_qspi_IO3,
--       hps_io_hps_io_qspi_inst_SS0               => hps_qspi_SS0,
--       hps_io_hps_io_qspi_inst_CLK               => hps_qspi_CLK,
--       hps_io_hps_io_sdio_inst_CMD               => hps_sdio_CMD,
--       hps_io_hps_io_sdio_inst_D0                => hps_sdio_D0,
--       hps_io_hps_io_sdio_inst_D1                => hps_sdio_D1,
--       hps_io_hps_io_sdio_inst_CLK               => hps_sdio_CLK,
--       hps_io_hps_io_sdio_inst_D2                => hps_sdio_D2,
--       hps_io_hps_io_sdio_inst_D3                => hps_sdio_D3,
--       hps_io_hps_io_usb1_inst_D0                => hps_usb1_D0,
--       hps_io_hps_io_usb1_inst_D1                => hps_usb1_D1,
--       hps_io_hps_io_usb1_inst_D2                => hps_usb1_D2,
--       hps_io_hps_io_usb1_inst_D3                => hps_usb1_D3,
--       hps_io_hps_io_usb1_inst_D4                => hps_usb1_D4,
--       hps_io_hps_io_usb1_inst_D5                => hps_usb1_D5,
--       hps_io_hps_io_usb1_inst_D6                => hps_usb1_D6,
--       hps_io_hps_io_usb1_inst_D7                => hps_usb1_D7,
--       hps_io_hps_io_usb1_inst_CLK               => hps_usb1_CLK,
--       hps_io_hps_io_usb1_inst_STP               => hps_usb1_STP,
--       hps_io_hps_io_usb1_inst_DIR               => hps_usb1_DIR,
--       hps_io_hps_io_usb1_inst_NXT               => hps_usb1_NXT,
--       hps_io_hps_io_uart0_inst_RX               => hps_uart0_RX,
--       hps_io_hps_io_uart0_inst_TX               => hps_uart0_TX,
--       hps_io_hps_io_uart1_inst_RX               => hps_uart1_RX,
--       hps_io_hps_io_uart1_inst_TX               => hps_uart1_TX,
--       hps_io_hps_io_i2c0_inst_SDA               => hps_i2c0_SDA,
--       hps_io_hps_io_i2c0_inst_SCL               => hps_i2c0_SCL,
--       hps_io_hps_io_trace_inst_CLK              => hps_trace_CLK,
--       hps_io_hps_io_trace_inst_D0               => hps_trace_D0,
--       hps_io_hps_io_trace_inst_D1               => hps_trace_D1,
--       hps_io_hps_io_trace_inst_D2               => hps_trace_D2,
--       hps_io_hps_io_trace_inst_D3               => hps_trace_D3,
--       hps_io_hps_io_trace_inst_D4               => hps_trace_D4,
--       hps_io_hps_io_trace_inst_D5               => hps_trace_D5,
--       hps_io_hps_io_trace_inst_D6               => hps_trace_D6,
--       hps_io_hps_io_trace_inst_D7               => hps_trace_D7,
--       hps_io_hps_io_gpio_inst_GPIO00            => hps_gpio_GPIO00,
--       hps_io_hps_io_gpio_inst_GPIO17            => hps_gpio_GPIO17,
--       hps_io_hps_io_gpio_inst_GPIO18            => hps_gpio_GPIO18,
--       hps_io_hps_io_gpio_inst_GPIO22            => hps_gpio_GPIO22,
--       hps_io_hps_io_gpio_inst_GPIO24            => hps_gpio_GPIO24,
--       hps_io_hps_io_gpio_inst_GPIO26            => hps_gpio_GPIO26,
--       hps_io_hps_io_gpio_inst_GPIO27            => hps_gpio_GPIO27,
--       hps_io_hps_io_gpio_inst_GPIO35            => hps_gpio_GPIO35,
--       hps_io_hps_io_gpio_inst_GPIO40            => hps_gpio_GPIO40,
--       hps_io_hps_io_gpio_inst_GPIO41            => hps_gpio_GPIO41,
--       hps_io_hps_io_gpio_inst_GPIO42            => hps_gpio_GPIO42,
--       hps_io_hps_io_gpio_inst_GPIO43            => hps_gpio_GPIO43,
-- 
--       led_pio_external_connection_in_port       => fpga_led_internal,
--       led_pio_external_connection_out_port      => fpga_led_internal,
-- 
--       --HPS Memory Controller
--       memory_mem_a                              => hps_memory_mem_a,
--       memory_mem_ba                             => hps_memory_mem_ba,
--       memory_mem_ck                             => hps_memory_mem_ck,
--       memory_mem_ck_n                           => hps_memory_mem_ck_n,
--       memory_mem_cke                            => hps_memory_mem_cke,
--       memory_mem_cs_n                           => hps_memory_mem_cs_n,
--       memory_mem_ras_n                          => hps_memory_mem_ras_n,
--       memory_mem_cas_n                          => hps_memory_mem_cas_n,
--       memory_mem_we_n                           => hps_memory_mem_we_n,
--       memory_mem_reset_n                        => hps_memory_mem_reset_n,
--       memory_mem_dq                             => hps_memory_mem_dq,
--       memory_mem_dqs                            => hps_memory_mem_dqs,
--       memory_mem_dqs_n                          => hps_memory_mem_dqs_n,
--       memory_mem_odt                            => hps_memory_mem_odt,
--       memory_mem_dm                             => hps_memory_mem_dm,
--       memory_oct_rzqin                          => hps_memory_oct_rzqin,
-- 
-- ---      tse_mac_pcs_mac_rx_clock_connection_clk   => enet_rx_clk,
-- ---      tse_mac_pcs_mac_tx_clock_connection_clk   => enet_tx_clk,
-- ---      tse_mac_mac_mdio_connection_mdc           => mdc,
-- ---      tse_mac_mac_mdio_connection_mdio_in       => mdio_in,
-- ---      tse_mac_mac_mdio_connection_mdio_out      => mdio_out,
-- ---      tse_mac_mac_mdio_connection_mdio_oen      => mdio_oen,
-- ---      tse_mac_mac_misc_connection_ff_tx_crc_fwd => open,
-- ---      tse_mac_mac_misc_connection_ff_tx_septy   => open,
-- ---      tse_mac_mac_misc_connection_tx_ff_uflow   => open,
-- ---      tse_mac_mac_misc_connection_ff_tx_a_full  => open,
-- ---      tse_mac_mac_misc_connection_ff_tx_a_empty => open,
-- ---      tse_mac_mac_misc_connection_rx_err_stat   => open,
-- ---      tse_mac_mac_misc_connection_rx_frm_type   => open,
-- ---      tse_mac_mac_misc_connection_ff_rx_dsav    => open,
-- ---      tse_mac_mac_misc_connection_ff_rx_a_full  => open,
-- ---      tse_mac_mac_misc_connection_ff_rx_a_empty => open,
-- ---      tse_mac_mac_mii_connection_mii_rx_d       => enet_rx_d(3 downto 0),
-- ---      tse_mac_mac_mii_connection_mii_rx_dv      => enet_rx_dv,
-- ---      tse_mac_mac_mii_connection_mii_rx_err     => enet_rx_er,
-- ---      tse_mac_mac_mii_connection_mii_tx_d       => enet_tx_d(3 downto 0),
-- ---      tse_mac_mac_mii_connection_mii_tx_en      => enet_tx_en,
-- -----      tse_mac_mac_mii_connection_mii_tx_err     => enet_tx_er,
-- ---      tse_mac_mac_mii_connection_mii_crs        => open,
-- ---      tse_mac_mac_mii_connection_mii_col        => open,
-- ---      tse_mac_mac_status_connection_set_10      => '0',
-- ---      tse_mac_mac_status_connection_set_1000    => '0',
-- ---      tse_mac_mac_status_connection_eth_mode    => eth_mode_from_the_tse_mac,
-- ---      tse_mac_mac_status_connection_ena_10      => open, --ena_10_from_the_tse_mac,
-- 
--       --DDR3 Controllers
--       fpga_memory_oct_rzqin                     => ddr3a_rzq,
--       
--       fpga_memory_a_mem_a                       => ddr3a_a,    
--       fpga_memory_a_mem_ba                      => ddr3a_ba,   
--       fpga_memory_a_mem_ck(0)                   => ddr3a_clk_p,
--       fpga_memory_a_mem_ck_n(0)                 => ddr3a_clk_n,
--       fpga_memory_a_mem_cke(0)                  => ddr3a_cke,  
--       fpga_memory_a_mem_cs_n(0)                 => ddr3a_csn,  
--       fpga_memory_a_mem_dm                      => ddr3a_dm,   
--       fpga_memory_a_mem_ras_n(0)                => ddr3a_rasn, 
--       fpga_memory_a_mem_cas_n(0)                => ddr3a_casn, 
--       fpga_memory_a_mem_we_n(0)                 => ddr3a_wen,  
--       fpga_memory_a_mem_reset_n                 => ddr3a_resetn,
--       fpga_memory_a_mem_dq                      => ddr3a_dq,   
--       fpga_memory_a_mem_dqs                     => ddr3a_dqs_p,
--       fpga_memory_a_mem_dqs_n                   => ddr3a_dqs_n,
--       fpga_memory_a_mem_odt(0)                  => ddr3a_odt,  
--       
-- --      fpga_memory_b_mem_a                       => ddr3b_a,     
-- --      fpga_memory_b_mem_ba                      => ddr3b_ba,    
-- --      fpga_memory_b_mem_ck(0)                   => ddr3b_clk_p,  
-- --      fpga_memory_b_mem_ck_n(0)                 => ddr3b_clk_n,    
-- --      fpga_memory_b_mem_cke(0)                  => ddr3b_cke, 
-- --      fpga_memory_b_mem_cs_n(0)                 => ddr3b_csn,  
-- --      fpga_memory_b_mem_dm                      => ddr3b_dm,    
-- --      fpga_memory_b_mem_ras_n(0)                => ddr3b_rasn,     
-- --      fpga_memory_b_mem_cas_n(0)                => ddr3b_casn,     
-- --      fpga_memory_b_mem_we_n(0)                 => ddr3b_wen,  
-- --      fpga_memory_b_mem_reset_n                 => ddr3b_resetn,  
-- --      fpga_memory_b_mem_dq                      => ddr3b_dq,    
-- --      fpga_memory_b_mem_dqs                     => ddr3b_dqs_p,   
-- --      fpga_memory_b_mem_dqs_n                   => ddr3b_dqs_n, 
-- --      fpga_memory_b_mem_odt(0)                  => ddr3b_odt,    
-- 
--       fpga_sdram_a_afi_reset_export_reset_n     => open,
--       fpga_sdram_a_status_local_init_done       => open,
--       fpga_sdram_a_status_local_cal_success     => open,
--       fpga_sdram_a_status_local_cal_fail        => open,
-- --      fpga_sdram_b_status_local_init_done       => open,
-- --      fpga_sdram_b_status_local_cal_success     => open,
-- --      fpga_sdram_b_status_local_cal_fail        => open,
-- 
--       fpga_sdram_a_pll_sharing_pll_mem_clk               => open, --            fpga_sdram_a_pll_sharing.pll_mem_clk
--       fpga_sdram_a_pll_sharing_pll_write_clk             => open, --                                    .pll_write_clk
--       fpga_sdram_a_pll_sharing_pll_locked                => open, --                                    .pll_locked
--       fpga_sdram_a_pll_sharing_pll_write_clk_pre_phy_clk => open, --                                    .pll_write_clk_pre_phy_clk
--       fpga_sdram_a_pll_sharing_pll_addr_cmd_clk          => open, --                                    .pll_addr_cmd_clk
--       fpga_sdram_a_pll_sharing_pll_avl_clk               => open, --                                    .pll_avl_clk
--       fpga_sdram_a_pll_sharing_pll_config_clk            => open, --                                    .pll_config_clk
--       fpga_sdram_a_pll_sharing_pll_mem_phy_clk           => open, --                                    .pll_mem_phy_clk
--       fpga_sdram_a_pll_sharing_afi_phy_clk               => open, --                                    .afi_phy_clk
--       fpga_sdram_a_pll_sharing_pll_avl_phy_clk           => open, --                                    .pll_avl_phy_clk
-- 
--       --UART
--       uart_rxd                                           => uart_rxd,
--       uart_txd                                           => uart_txd,
--       
--       --I2C
--       i2c_master_ts_0_scl_pad_i                          => s_i2c_scl_i,
--       i2c_master_ts_0_scl_pad_o                          => s_i2c_scl_o,
--       i2c_master_ts_0_scl_padoen_o                       => s_i2c_scl_oen,
--       i2c_master_ts_0_sda_pad_i                          => s_i2c_sda_i,
--       i2c_master_ts_0_sda_pad_o                          => s_i2c_sda_o,
--       i2c_master_ts_0_sda_padoen_o                       => s_i2c_sda_oen,
--       
-- --      display_spi_MISO                                   => display_miso,
-- --      display_spi_MOSI                                   => display_mosi,
-- --      display_spi_SCLK                                   => display_sclk_out,
-- --      display_spi_SS_n                                   => display_cs_out_n,
-- 
--       ad5791_dac_clr_out_n                               => dac_clr_out_n,
--       ad5791_dac_reset_out_n                             => dac_reset_out_n,
--       ad5791_dac_sclk_out                                => dac_sclk_out,
--       ad5791_dac_sync_out_n                              => dac_sync_out_n,
--       ad5791_dac_ldac_out_n                              => dac_ldac_out_n,
--       ad5791_dac_sdin_to_dac                             => dac_sdin_out,
--       ad5791_dac_sdo_from_dac                            => dac_sdo_in,
--       
--       ad7960_ext_adc_clk_out                             => adc_ref_clk_out,
--       ad7960_ext_adc_conv_start_out                      => adc_conv_out,
--       ad7960_ext_adc_en_bits                             => s_adc_en,
--       ad7960_ext_adc_rx_clk_in                           => adc_clk_in,
--       ad7960_ext_adc_rx_data_in                          => adc_serial_din,
-- 
--  --20160316, JCS: Currently not used by DAC module
--       ad7960_dac_ref_ext_reset_out                       => open,
--       ad7960_dac_ref_ext_ref_clk_out                     => open,
--       ad7960_dac_ref_ext_sync_out                        => open,
--       
--       gpio_0_in_port                                     => s_gpio_in_0,
--       gpio_0_out_port                                    => s_gpio_out_0,
-- 
--       gpio_shifter_in_port                               => s_gpio_shifter_in,
--       gpio_shifter_out_port                              => s_gpio_shifter_out
--       
--    );

   --------------------------------------------------------------------------
   -- SoC sub-system module
   --------------------------------------------------------------------------
   u0 : component av_soc_adc
   port map (
      reset_reset_n                             => hps_fpga_reset_n,
      reset_50_reset_n                          => cpu_reset_n,
      
      refclk_clk                                => fpga_clk_100,
      clk_50_clk                                => fpga_clk_50,      --50MHz
      clk_clk                                   => clk_top1,         --156.25MHz
      clk_pll_0_outclk0_clk                     => clk_pll_0_outclk0,       -- clk, 150MHz
      clk_pll_0_outclk0_reset_reset             => clk_pll_0_outclk0_reset, -- reset
      
      button_pio_external_connection_export     => fpga_debounced_buttons,
      dipsw_pio_external_connection_export      => fpga_dipsw_pio,

      hps_0_f2h_cold_reset_req_reset_n          => not hps_cold_reset,
      hps_0_f2h_debug_reset_req_reset_n         => not hps_debug_reset,
      hps_0_f2h_stm_hw_events_stm_hwevents      => stm_hw_events,
      hps_0_f2h_warm_reset_req_reset_n          => not hps_warm_reset,
      hps_0_h2f_reset_reset_n                   => hps_fpga_reset_n,

      hps_io_hps_io_emac1_inst_TX_CLK           => hps_emac1_TX_CLK,
      hps_io_hps_io_emac1_inst_TXD0             => hps_emac1_TXD0,
      hps_io_hps_io_emac1_inst_TXD1             => hps_emac1_TXD1,
      hps_io_hps_io_emac1_inst_TXD2             => hps_emac1_TXD2,
      hps_io_hps_io_emac1_inst_TXD3             => hps_emac1_TXD3,
      hps_io_hps_io_emac1_inst_MDIO             => hps_emac1_MDIO,
      hps_io_hps_io_emac1_inst_MDC              => hps_emac1_MDC,
      hps_io_hps_io_emac1_inst_RX_CTL           => hps_emac1_RX_CTL,
      hps_io_hps_io_emac1_inst_TX_CTL           => hps_emac1_TX_CTL,
      hps_io_hps_io_emac1_inst_RX_CLK           => hps_emac1_RX_CLK,
      hps_io_hps_io_emac1_inst_RXD0             => hps_emac1_RXD0,
      hps_io_hps_io_emac1_inst_RXD1             => hps_emac1_RXD1,
      hps_io_hps_io_emac1_inst_RXD2             => hps_emac1_RXD2,
      hps_io_hps_io_emac1_inst_RXD3             => hps_emac1_RXD3,
      hps_io_hps_io_qspi_inst_IO0               => hps_qspi_IO0,
      hps_io_hps_io_qspi_inst_IO1               => hps_qspi_IO1,
      hps_io_hps_io_qspi_inst_IO2               => hps_qspi_IO2,
      hps_io_hps_io_qspi_inst_IO3               => hps_qspi_IO3,
      hps_io_hps_io_qspi_inst_SS0               => hps_qspi_SS0,
      hps_io_hps_io_qspi_inst_CLK               => hps_qspi_CLK,
      hps_io_hps_io_sdio_inst_CMD               => hps_sdio_CMD,
      hps_io_hps_io_sdio_inst_D0                => hps_sdio_D0,
      hps_io_hps_io_sdio_inst_D1                => hps_sdio_D1,
      hps_io_hps_io_sdio_inst_CLK               => hps_sdio_CLK,
      hps_io_hps_io_sdio_inst_D2                => hps_sdio_D2,
      hps_io_hps_io_sdio_inst_D3                => hps_sdio_D3,
      hps_io_hps_io_usb1_inst_D0                => hps_usb1_D0,
      hps_io_hps_io_usb1_inst_D1                => hps_usb1_D1,
      hps_io_hps_io_usb1_inst_D2                => hps_usb1_D2,
      hps_io_hps_io_usb1_inst_D3                => hps_usb1_D3,
      hps_io_hps_io_usb1_inst_D4                => hps_usb1_D4,
      hps_io_hps_io_usb1_inst_D5                => hps_usb1_D5,
      hps_io_hps_io_usb1_inst_D6                => hps_usb1_D6,
      hps_io_hps_io_usb1_inst_D7                => hps_usb1_D7,
      hps_io_hps_io_usb1_inst_CLK               => hps_usb1_CLK,
      hps_io_hps_io_usb1_inst_STP               => hps_usb1_STP,
      hps_io_hps_io_usb1_inst_DIR               => hps_usb1_DIR,
      hps_io_hps_io_usb1_inst_NXT               => hps_usb1_NXT,
      hps_io_hps_io_uart0_inst_RX               => hps_uart0_RX,
      hps_io_hps_io_uart0_inst_TX               => hps_uart0_TX,
      hps_io_hps_io_uart1_inst_RX               => hps_uart1_RX,
      hps_io_hps_io_uart1_inst_TX               => hps_uart1_TX,
      hps_io_hps_io_i2c0_inst_SDA               => hps_i2c0_SDA,
      hps_io_hps_io_i2c0_inst_SCL               => hps_i2c0_SCL,
      hps_io_hps_io_trace_inst_CLK              => hps_trace_CLK,
      hps_io_hps_io_trace_inst_D0               => hps_trace_D0,
      hps_io_hps_io_trace_inst_D1               => hps_trace_D1,
      hps_io_hps_io_trace_inst_D2               => hps_trace_D2,
      hps_io_hps_io_trace_inst_D3               => hps_trace_D3,
      hps_io_hps_io_trace_inst_D4               => hps_trace_D4,
      hps_io_hps_io_trace_inst_D5               => hps_trace_D5,
      hps_io_hps_io_trace_inst_D6               => hps_trace_D6,
      hps_io_hps_io_trace_inst_D7               => hps_trace_D7,
      hps_io_hps_io_gpio_inst_GPIO00            => hps_gpio_GPIO00,
      hps_io_hps_io_gpio_inst_GPIO17            => hps_gpio_GPIO17,
      hps_io_hps_io_gpio_inst_GPIO18            => hps_gpio_GPIO18,
      hps_io_hps_io_gpio_inst_GPIO22            => hps_gpio_GPIO22,
      hps_io_hps_io_gpio_inst_GPIO24            => hps_gpio_GPIO24,
      hps_io_hps_io_gpio_inst_GPIO26            => hps_gpio_GPIO26,
      hps_io_hps_io_gpio_inst_GPIO27            => hps_gpio_GPIO27,
      hps_io_hps_io_gpio_inst_GPIO35            => hps_gpio_GPIO35,
      hps_io_hps_io_gpio_inst_GPIO40            => hps_gpio_GPIO40,
      hps_io_hps_io_gpio_inst_GPIO41            => hps_gpio_GPIO41,
      hps_io_hps_io_gpio_inst_GPIO42            => hps_gpio_GPIO42,
      hps_io_hps_io_gpio_inst_GPIO43            => hps_gpio_GPIO43,

      led_pio_external_connection_in_port       => fpga_led_internal,
      led_pio_external_connection_out_port      => fpga_led_internal,

      --HPS Memory Controller
      memory_mem_a                              => hps_memory_mem_a,
      memory_mem_ba                             => hps_memory_mem_ba,
      memory_mem_ck                             => hps_memory_mem_ck,
      memory_mem_ck_n                           => hps_memory_mem_ck_n,
      memory_mem_cke                            => hps_memory_mem_cke,
      memory_mem_cs_n                           => hps_memory_mem_cs_n,
      memory_mem_ras_n                          => hps_memory_mem_ras_n,
      memory_mem_cas_n                          => hps_memory_mem_cas_n,
      memory_mem_we_n                           => hps_memory_mem_we_n,
      memory_mem_reset_n                        => hps_memory_mem_reset_n,
      memory_mem_dq                             => hps_memory_mem_dq,
      memory_mem_dqs                            => hps_memory_mem_dqs,
      memory_mem_dqs_n                          => hps_memory_mem_dqs_n,
      memory_mem_odt                            => hps_memory_mem_odt,
      memory_mem_dm                             => hps_memory_mem_dm,
      memory_oct_rzqin                          => hps_memory_oct_rzqin,
      
      --I2C
      i2c_master_ts_0_scl_pad_i                          => s_i2c_scl_i,
      i2c_master_ts_0_scl_pad_o                          => s_i2c_scl_o,
      i2c_master_ts_0_scl_padoen_o                       => s_i2c_scl_oen,
      i2c_master_ts_0_sda_pad_i                          => s_i2c_sda_i,
      i2c_master_ts_0_sda_pad_o                          => s_i2c_sda_o,
      i2c_master_ts_0_sda_padoen_o                       => s_i2c_sda_oen,
      
      ad5791_dac_clr_out_n                               => dac_clr_out_n,
      ad5791_dac_reset_out_n                             => dac_reset_out_n,
      ad5791_dac_sclk_out                                => dac_sclk_out,
      ad5791_dac_sync_out_n                              => dac_sync_out_n,
      ad5791_dac_ldac_out_n                              => dac_ldac_out_n,
      ad5791_dac_sdin_to_dac                             => dac_sdin_out,
      ad5791_dac_sdo_from_dac                            => dac_sdo_in,
      
      ad7960_ext_adc_clk_out                             => adc_ref_clk_out,
      ad7960_ext_adc_conv_start_out                      => adc_conv_out,
      ad7960_ext_adc_en_bits                             => s_adc_en,
      ad7960_ext_adc_rx_clk_in                           => adc_clk_in,
      ad7960_ext_adc_rx_data_in                          => adc_serial_din,

      --Currently not used by DAC module
      ad7960_dac_ref_ext_reset_out                       => open,
      ad7960_dac_ref_ext_ref_clk_out                     => open,
      ad7960_dac_ref_ext_sync_out                        => open,
      
      gpio_0_in_port                                     => s_gpio_in_0,
      gpio_0_out_port                                    => s_gpio_out_0,

      gpio_shifter_in_port                               => s_gpio_shifter_in,
      gpio_shifter_out_port                              => s_gpio_shifter_out
   );
   
   
end;  --end of file
