#############################################################################
#
# av_soc.sdc constraints
#
#############################################################################
derive_pll_clocks
derive_clock_uncertainty
set_time_format -unit ns -decimal_places 6

# 50MHz board input clock, 25MHz ENET TX/RX clock
create_clock -period 20  [get_ports fpga_clk_50]
create_clock -period 10  [get_ports fpga_clk_100]
create_clock -period 6.4 [get_ports clk_top1]
create_clock -period 10  [get_ports clk_bot1]
create_clock -period 40  [get_ports fpgaio_emac0_tx_clk]
create_clock -period 40  [get_ports fpgaio_emac0_rx_clk]
create_clock -period 400 [get_ports fpgaio_emac_mdc]


#-------------------------------------------------------------------------------
# ADC Inputs
#-------------------------------------------------------------------------------
### ADC Sensor Clock
set adc_clk_in_period 3.333
### AD7960 max data shift relative to DCO clock
set adc_td_max 1.000
set adc_td_min -0.000

###Note that clock is shifted by 180 degrees since AD7960 outputs data on falling edge
create_clock -name adc_clk_in_0 -period "300 MHz" -waveform {1.667 3.333} [get_ports {adc_clk_in[0]}]
create_clock -name adc_clk_in_1 -period "300 MHz" -waveform {1.667 3.333} [get_ports {adc_clk_in[1]}]
create_clock -name adc_clk_in_2 -period "300 MHz" -waveform {1.667 3.333} [get_ports {adc_clk_in[2]}]
create_clock -name adc_clk_in_3 -period "300 MHz" -waveform {1.667 3.333} [get_ports {adc_clk_in[3]}]
create_clock -name adc_clk_in_4 -period "300 MHz" -waveform {1.667 3.333} [get_ports {adc_clk_in[4]}]
create_clock -name adc_clk_in_5 -period "300 MHz" -waveform {1.667 3.333} [get_ports {adc_clk_in[5]}]
create_clock -name adc_clk_in_6 -period "300 MHz" -waveform {1.667 3.333} [get_ports {adc_clk_in[6]}]
create_clock -name adc_clk_in_7 -period "300 MHz" -waveform {1.667 3.333} [get_ports {adc_clk_in[7]}]

##create_generated_clock \
##   -name adc_div_clk_0 \
##   -source [get_pins {\adc_chan:0:adc_chan_inst|s_clk_out|q}] \
##   -master_clock [get_clocks {adc_clk_in_0}] \
##   -divide_by 18 \
##   [get_registers {lvds_deserializer:\adc_chan:0:adc_chan_inst|*}]

###create_generated_clock \
###   -name adc_div_clk_0 \
###   -source [get_registers {lvds_deserializer:\adc_chan:0:adc_chan_inst|s_clk_out}] \
###   -master_clock [get_ports {adc_clk_in[0]}] \
###   -divide_by 18 \
###   [get_registers {lvds_deserializer:\adc_chan:0:adc_chan_inst|s_clk_out}]   

#Internal deserializer divided clock
create_clock -name adc_div_clk_0 -period "20 MHz" [get_registers {lvds_deserializer:\adc_chan:0:adc_chan_inst|s_clk_out}]
create_clock -name adc_div_clk_1 -period "20 MHz" [get_registers {lvds_deserializer:\adc_chan:1:adc_chan_inst|s_clk_out}]
create_clock -name adc_div_clk_2 -period "20 MHz" [get_registers {lvds_deserializer:\adc_chan:2:adc_chan_inst|s_clk_out}]
create_clock -name adc_div_clk_3 -period "20 MHz" [get_registers {lvds_deserializer:\adc_chan:3:adc_chan_inst|s_clk_out}]
create_clock -name adc_div_clk_4 -period "20 MHz" [get_registers {lvds_deserializer:\adc_chan:4:adc_chan_inst|s_clk_out}]
create_clock -name adc_div_clk_5 -period "20 MHz" [get_registers {lvds_deserializer:\adc_chan:5:adc_chan_inst|s_clk_out}]
create_clock -name adc_div_clk_6 -period "20 MHz" [get_registers {lvds_deserializer:\adc_chan:6:adc_chan_inst|s_clk_out}]
create_clock -name adc_div_clk_7 -period "20 MHz" [get_registers {lvds_deserializer:\adc_chan:7:adc_chan_inst|s_clk_out}]

##create_generated_clock \
##   -name adc_div_clk_0 \
##   -source [get_registers {lvds_deserializer:\adc_chan:0:adc_chan_inst|s_clk_out}] \
###   -source [get_pins {\adc_chan:0:adc_chan_inst|s_clk_out|q}] \
##   -master_clock [get_clocks {adc_clk_in[0]}] \
##   -divide_by 18 \
##   [get_keepers {lvds_deserializer:\adc_chan:0:adc_chan_inst|s_clk_out}]   

###create_generated_clock \
###   -name adc_div_clk_0 \
###   -source [get_pins {\adc_chan:0:adc_chan_inst|s_clk_out|q}] \
###   -divide_by 18 \
###   [get_keepers {lvds_deserializer:\adc_chan:0:adc_chan_inst|*}]
###   
###create_generated_clock \
###   -name adc_div_clk_1 \
###   -source [get_pins {\adc_chan:1:adc_chan_inst|s_clk_out|q}] \
###   -divide_by 18 \
###   [get_keepers {lvds_deserializer:\adc_chan:1:adc_chan_inst|*}]
###
###create_generated_clock \
###   -name adc_div_clk_2 \
###   -source [get_pins {\adc_chan:2:adc_chan_inst|s_clk_out|q}] \
###   -divide_by 18 \
###   [get_keepers {lvds_deserializer:\adc_chan:2:adc_chan_inst|*}]
###
###create_generated_clock \
###   -name adc_div_clk_3 \
###   -source [get_pins {\adc_chan:3:adc_chan_inst|s_clk_out|q}] \
###   -divide_by 18 \
###   [get_keepers {lvds_deserializer:\adc_chan:3:adc_chan_inst|*}]
###
###create_generated_clock \
###   -name adc_div_clk_4 \
###   -source [get_pins {\adc_chan:4:adc_chan_inst|s_clk_out|q}] \
###   -divide_by 18 \
###   [get_keepers {lvds_deserializer:\adc_chan:4:adc_chan_inst|*}]
###
###create_generated_clock \
###   -name adc_div_clk_5 \
###   -source [get_pins {\adc_chan:5:adc_chan_inst|s_clk_out|q}] \
###   -divide_by 18 \
###   [get_keepers {lvds_deserializer:\adc_chan:5:adc_chan_inst|*}]
###
###create_generated_clock \
###   -name adc_div_clk_6 \
###   -source [get_pins {\adc_chan:6:adc_chan_inst|s_clk_out|q}] \
###   -divide_by 18 \
###   [get_keepers {lvds_deserializer:\adc_chan:6:adc_chan_inst|*}]
###
###create_generated_clock \
###   -name adc_div_clk_7 \
###   -source [get_pins {\adc_chan:7:adc_chan_inst|s_clk_out|q}] \
###   -divide_by 18 \
###   [get_keepers {lvds_deserializer:\adc_chan:7:adc_chan_inst|*}]

### adc_clk_in[0]
#Create virtual clock and set clock to have a 90 degree phase shift with: -waveform { 0.25*period 0.75*period }
create_clock -name adc_clk_in_0_virtual -period $adc_clk_in_period
set_input_delay -clock adc_clk_in_0_virtual -max $adc_td_max [get_ports {adc_serial_din[0]}]
set_input_delay -clock adc_clk_in_0_virtual -min $adc_td_min [get_ports {adc_serial_din[0]}] -add_delay
#set_input_delay -clock adc_clk_in_0_virtual -clock_fall -max $adc_td_max [get_ports {adc_serial_din[0]}] -add_delay
#set_input_delay -clock adc_clk_in_0_virtual -clock_fall -min $adc_td_min [get_ports {adc_serial_din[0]}] -add_delay

### adc_clk_in[1]
#Create virtual clock and set clock to have a 90 degree phase shift with: -waveform { 0.25*period 0.75*period }
create_clock -name adc_clk_in_1_virtual -period $adc_clk_in_period
set_input_delay -clock adc_clk_in_1_virtual -max $adc_td_max [get_ports {adc_serial_din[1]}]
set_input_delay -clock adc_clk_in_1_virtual -min $adc_td_min [get_ports {adc_serial_din[1]}] -add_delay
#set_input_delay -clock adc_clk_in_1_virtual -clock_fall -max $adc_td_max [get_ports {adc_serial_din[1]}] -add_delay
#set_input_delay -clock adc_clk_in_1_virtual -clock_fall -min $adc_td_min [get_ports {adc_serial_din[1]}] -add_delay

### adc_clk_in[2]
#Create virtual clock and set clock to have a 90 degree phase shift with: -waveform { 0.25*period 0.75*period }
create_clock -name adc_clk_in_2_virtual -period $adc_clk_in_period
set_input_delay -clock adc_clk_in_2_virtual -max $adc_td_max [get_ports {adc_serial_din[2]}]
set_input_delay -clock adc_clk_in_2_virtual -min $adc_td_min [get_ports {adc_serial_din[2]}] -add_delay
#set_input_delay -clock adc_clk_in_2_virtual -clock_fall -max $adc_td_max [get_ports {adc_serial_din[2]}] -add_delay
#set_input_delay -clock adc_clk_in_2_virtual -clock_fall -min $adc_td_min [get_ports {adc_serial_din[2]}] -add_delay

### adc_clk_in[3]
#Create virtual clock and set clock to have a 90 degree phase shift with: -waveform { 0.25*period 0.75*period }
create_clock -name adc_clk_in_3_virtual -period $adc_clk_in_period
set_input_delay -clock adc_clk_in_3_virtual -max $adc_td_max [get_ports {adc_serial_din[3]}]
set_input_delay -clock adc_clk_in_3_virtual -min $adc_td_min [get_ports {adc_serial_din[3]}] -add_delay
#set_input_delay -clock adc_clk_in_3_virtual -clock_fall -max $adc_td_max [get_ports {adc_serial_din[3]}] -add_delay
#set_input_delay -clock adc_clk_in_3_virtual -clock_fall -min $adc_td_min [get_ports {adc_serial_din[3]}] -add_delay

### adc_clk_in[4]
#Create virtual clock and set clock to have a 90 degree phase shift with: -waveform { 0.25*period 0.75*period }
create_clock -name adc_clk_in_4_virtual -period $adc_clk_in_period
set_input_delay -clock adc_clk_in_4_virtual -max $adc_td_max [get_ports {adc_serial_din[4]}]
set_input_delay -clock adc_clk_in_4_virtual -min $adc_td_min [get_ports {adc_serial_din[4]}] -add_delay
#set_input_delay -clock adc_clk_in_4_virtual -clock_fall -max $adc_td_max [get_ports {adc_serial_din[4]}] -add_delay
#set_input_delay -clock adc_clk_in_4_virtual -clock_fall -min $adc_td_min [get_ports {adc_serial_din[4]}] -add_delay

### adc_clk_in[5]
#Create virtual clock and set clock to have a 90 degree phase shift with: -waveform { 0.25*period 0.75*period }
create_clock -name adc_clk_in_5_virtual -period $adc_clk_in_period
set_input_delay -clock adc_clk_in_5_virtual -max $adc_td_max [get_ports {adc_serial_din[5]}]
set_input_delay -clock adc_clk_in_5_virtual -min $adc_td_min [get_ports {adc_serial_din[5]}] -add_delay
#set_input_delay -clock adc_clk_in_5_virtual -clock_fall -max $adc_td_max [get_ports {adc_serial_din[5]}] -add_delay
#set_input_delay -clock adc_clk_in_5_virtual -clock_fall -min $adc_td_min [get_ports {adc_serial_din[5]}] -add_delay

### adc_clk_in[6]
#Create virtual clock and set clock to have a 90 degree phase shift with: -waveform { 0.25*period 0.75*period }
create_clock -name adc_clk_in_6_virtual -period $adc_clk_in_period
set_input_delay -clock adc_clk_in_6_virtual -max $adc_td_max [get_ports {adc_serial_din[6]}]
set_input_delay -clock adc_clk_in_6_virtual -min $adc_td_min [get_ports {adc_serial_din[6]}] -add_delay
#set_input_delay -clock adc_clk_in_6_virtual -clock_fall -max $adc_td_max [get_ports {adc_serial_din[6]}] -add_delay
#set_input_delay -clock adc_clk_in_6_virtual -clock_fall -min $adc_td_min [get_ports {adc_serial_din[6]}] -add_delay

### adc_clk_in[7]
#Create virtual clock and set clock to have a 90 degree phase shift with: -waveform { 0.25*period 0.75*period }
create_clock -name adc_clk_in_7_virtual -period $adc_clk_in_period
set_input_delay -clock adc_clk_in_7_virtual -max $adc_td_max [get_ports {adc_serial_din[7]}]
set_input_delay -clock adc_clk_in_7_virtual -min $adc_td_min [get_ports {adc_serial_din[7]}] -add_delay
#set_input_delay -clock adc_clk_in_7_virtual -clock_fall -max $adc_td_max [get_ports {adc_serial_din[7]}] -add_delay
#set_input_delay -clock adc_clk_in_7_virtual -clock_fall -min $adc_td_min [get_ports {adc_serial_din[7]}] -add_delay
    
set_clock_groups \
    -exclusive \
    -group [get_clocks {adc_clk_in_0}] \
    -group [get_clocks {adc_clk_in_1}] \
    -group [get_clocks {adc_clk_in_2}] \
    -group [get_clocks {adc_clk_in_3}] \
    -group [get_clocks {adc_clk_in_4}] \
    -group [get_clocks {adc_clk_in_5}] \
    -group [get_clocks {adc_clk_in_6}] \
    -group [get_clocks {adc_clk_in_7}] \
    -group [get_clocks {adc_div_clk_0}] \
    -group [get_clocks {adc_div_clk_1}] \
    -group [get_clocks {adc_div_clk_2}] \
    -group [get_clocks {adc_div_clk_3}] \
    -group [get_clocks {adc_div_clk_4}] \
    -group [get_clocks {adc_div_clk_5}] \
    -group [get_clocks {adc_div_clk_6}] \
    -group [get_clocks {adc_div_clk_7}]


##create_generated_clock -name s_clk_out_0 -source [get_ports {adc_clk_in[0]}] -divide_by 18 -phase 90 
##create_generated_clock -name s_clk_out_1 -source [get_ports {adc_clk_in[1]}] -divide_by 18 -phase 90 
##create_generated_clock -name s_clk_out_2 -source [get_ports {adc_clk_in[2]}] -divide_by 18 -phase 90 
##create_generated_clock -name s_clk_out_3 -source [get_ports {adc_clk_in[3]}] -divide_by 18 -phase 90 
##create_generated_clock -name s_clk_out_4 -source [get_ports {adc_clk_in[4]}] -divide_by 18 -phase 90 
##create_generated_clock -name s_clk_out_5 -source [get_ports {adc_clk_in[5]}] -divide_by 18 -phase 90 
##create_generated_clock -name s_clk_out_6 -source [get_ports {adc_clk_in[6]}] -divide_by 18 -phase 90     
##create_generated_clock -name s_clk_out_7 -source [get_ports {adc_clk_in[7]}] -divide_by 18 -phase 90     

#set_false_path -from [get_clocks {adc_clk_in_0}] -to [get_registers {lvds_deserializer:\adc_chan:0:adc_chan_inst|s_clk_out}]
#set_false_path -from [get_clocks {adc_clk_in_1}] -to [get_registers {lvds_deserializer:\adc_chan:1:adc_chan_inst|s_clk_out}]
#set_false_path -from [get_clocks {adc_clk_in_2}] -to [get_registers {lvds_deserializer:\adc_chan:2:adc_chan_inst|s_clk_out}]
#set_false_path -from [get_clocks {adc_clk_in_3}] -to [get_registers {lvds_deserializer:\adc_chan:3:adc_chan_inst|s_clk_out}]
#set_false_path -from [get_clocks {adc_clk_in_4}] -to [get_registers {lvds_deserializer:\adc_chan:4:adc_chan_inst|s_clk_out}]
#set_false_path -from [get_clocks {adc_clk_in_5}] -to [get_registers {lvds_deserializer:\adc_chan:5:adc_chan_inst|s_clk_out}]
#set_false_path -from [get_clocks {adc_clk_in_6}] -to [get_registers {lvds_deserializer:\adc_chan:6:adc_chan_inst|s_clk_out}]
#set_false_path -from [get_clocks {adc_clk_in_7}] -to [get_registers {lvds_deserializer:\adc_chan:7:adc_chan_inst|s_clk_out}]
#
#set_false_path -from [get_clocks {adc_clk_in_0}] -to [get_clocks {adc_div_clk_0}]
#set_false_path -from [get_clocks {adc_clk_in_1}] -to [get_clocks {adc_div_clk_0}]
#set_false_path -from [get_clocks {adc_clk_in_2}] -to [get_clocks {adc_div_clk_0}]
#set_false_path -from [get_clocks {adc_clk_in_3}] -to [get_clocks {adc_div_clk_0}]
#set_false_path -from [get_clocks {adc_clk_in_4}] -to [get_clocks {adc_div_clk_0}]
#set_false_path -from [get_clocks {adc_clk_in_5}] -to [get_clocks {adc_div_clk_0}]
#set_false_path -from [get_clocks {adc_clk_in_6}] -to [get_clocks {adc_div_clk_0}]
#set_false_path -from [get_clocks {adc_clk_in_7}] -to [get_clocks {adc_div_clk_0}]

#set_false_path -from [get_clocks {adc_clk_in_0}] -to [get_clocks {fpga_clk_50}]
#set_false_path -from [get_clocks {adc_clk_in_1}] -to [get_clocks {fpga_clk_50}]
#set_false_path -from [get_clocks {adc_clk_in_2}] -to [get_clocks {fpga_clk_50}]
#set_false_path -from [get_clocks {adc_clk_in_3}] -to [get_clocks {fpga_clk_50}]
#set_false_path -from [get_clocks {adc_clk_in_4}] -to [get_clocks {fpga_clk_50}]
#set_false_path -from [get_clocks {adc_clk_in_5}] -to [get_clocks {fpga_clk_50}]
#set_false_path -from [get_clocks {adc_clk_in_6}] -to [get_clocks {fpga_clk_50}]
#set_false_path -from [get_clocks {adc_clk_in_7}] -to [get_clocks {fpga_clk_50}]

#AD7960 ADC Clocks
set_false_path -from [get_clocks {adc_clk_in_0}] -to [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]
set_false_path -from [get_clocks {adc_clk_in_1}] -to [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]
set_false_path -from [get_clocks {adc_clk_in_2}] -to [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]
set_false_path -from [get_clocks {adc_clk_in_3}] -to [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]
set_false_path -from [get_clocks {adc_clk_in_4}] -to [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]
set_false_path -from [get_clocks {adc_clk_in_5}] -to [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]
set_false_path -from [get_clocks {adc_clk_in_6}] -to [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]
set_false_path -from [get_clocks {adc_clk_in_7}] -to [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]

#GPIO Shifter
set_false_path -from [get_clocks {fpga_clk_50}] -to [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]
set_false_path -from [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] -to [get_clocks {fpga_clk_50}]

#QSys clocks (Bridges, etc)
set_false_path -from [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] -to [get_clocks {u0|pll_0|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk}]
set_false_path -from [get_clocks {u0|pll_0|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk}] -to [get_clocks {u0|pll_0|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}]

#Sensor Interface (Serial to Parallel conversion)
set_false_path -from [get_clocks {adc_clk_in_1}] -to [get_clocks {u0|pll_0|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk}]

#ad7960_deserializer
set_false_path -from {av_soc_adc:u0|ad7960_deserializer:ad7960_deserializer_0|s_adc_clk_en} -to {av_soc_adc:u0|ad7960_deserializer:ad7960_deserializer_0|s_resync_pipe[0]}
set_false_path -from {av_soc_adc:u0|ad7960_deserializer:ad7960_deserializer_0|n_cycle_counter*} -to {sld_signaltap:ad7960_adc_fifo_stp|acq_data_in_reg*}

#-------------------------------------------------------------------------------
# constrain the Ethernet RGMII interface
#-------------------------------------------------------------------------------
#source src/enet_fpga_mii.sdc

#set_false_path -from [get_clocks {fpgaio_emac0_tx_clk}] -to [get_clocks {fpga_clk_50}]
#set_false_path -from [get_clocks {fpgaio_emac0_rx_clk}] -to [get_clocks {fpga_clk_50}]
#set_false_path -from [get_clocks {fpgaio_emac0_tx_clk}] -to [get_clocks {fpga_clk_100}]
#set_false_path -from [get_clocks {fpgaio_emac0_rx_clk}] -to [get_clocks {fpga_clk_100}]
#set_false_path -from [get_clocks {fpgaio_emac0_tx_clk}] -to [get_clocks {clk_top1}]
#set_false_path -from [get_clocks {fpgaio_emac0_rx_clk}] -to [get_clocks {clk_top1}]

# for enhancing USB BlasterII to be reliable, 25MHz
create_clock -name {altera_reserved_tck} -period 40 {altera_reserved_tck}
set_input_delay -clock altera_reserved_tck -clock_fall 3 [get_ports altera_reserved_tdi]
set_input_delay -clock altera_reserved_tck -clock_fall 3 [get_ports altera_reserved_tms]
set_output_delay -clock altera_reserved_tck 3 [get_ports altera_reserved_tdo]

# IO path delay for FPGA IO EMAC, 50% delay period assumed, mdio path has 30/400 delay
set_input_delay -clock fpgaio_emac0_rx_clk -min 10 [ get_ports fpgaio_emac0_rx* ]
set_input_delay -clock fpgaio_emac0_rx_clk -max 29 [ get_ports fpgaio_emac0_rx* ]
set_output_delay -clock fpgaio_emac0_tx_clk -min -2 [ get_ports fpgaio_emac0_tx* ]
set_output_delay -clock fpgaio_emac0_tx_clk -max 10 [ get_ports fpgaio_emac0_tx* ]
set_input_delay -clock fpgaio_emac_mdc 30 [ get_ports fpgaio_emac_mdio ]
set_output_delay -clock fpgaio_emac_mdc 30 [ get_ports fpgaio_emac_mdio ]
set_false_path -from * -to [ get_ports fpgaio_emac_dual_resetn ]
set_false_path -from * -to [ get_ports fpgaio_emac_mdc ]

# FPGA IO port constraints
set_false_path -from [get_ports {fpga_button_pio[0]}] -to *
set_false_path -from [get_ports {fpga_button_pio[1]}] -to *
set_false_path -from [get_ports {fpga_button_pio[2]}] -to *
set_false_path -from [get_ports {fpga_button_pio[3]}] -to *
set_false_path -from [get_ports {fpga_dipsw_pio[0]}] -to *
set_false_path -from [get_ports {fpga_dipsw_pio[1]}] -to *
set_false_path -from [get_ports {fpga_dipsw_pio[2]}] -to *
set_false_path -from [get_ports {fpga_dipsw_pio[3]}] -to *
set_false_path -from * -to [get_ports {fpga_led_pio[0]}]
set_false_path -from * -to [get_ports {fpga_led_pio[1]}]
set_false_path -from * -to [get_ports {fpga_led_pio[2]}]
set_false_path -from * -to [get_ports {fpga_led_pio[3]}]

# HPS peripherals port false path setting to workaround the unconstraint path (setting false_path for hps_0 ports will not affect the routing as it is hard silicon)
set_false_path -from * -to [get_ports {hps_emac1_TX_CLK}] 
set_false_path -from * -to [get_ports {hps_emac1_TXD0}] 
set_false_path -from * -to [get_ports {hps_emac1_TXD1}] 
set_false_path -from * -to [get_ports {hps_emac1_TXD2}] 
set_false_path -from * -to [get_ports {hps_emac1_TXD3}] 
set_false_path -from * -to [get_ports {hps_emac1_MDC}] 
set_false_path -from * -to [get_ports {hps_emac1_TX_CTL}] 
set_false_path -from * -to [get_ports {hps_qspi_SS0}] 
set_false_path -from * -to [get_ports {hps_qspi_CLK}] 
set_false_path -from * -to [get_ports {hps_sdio_CLK}] 
set_false_path -from * -to [get_ports {hps_usb1_STP}] 
set_false_path -from * -to [get_ports {hps_uart0_TX}] 
set_false_path -from * -to [get_ports {hps_uart1_TX}] 
set_false_path -from * -to [get_ports {hps_trace_CLK}] 
set_false_path -from * -to [get_ports {hps_trace_D0}] 
set_false_path -from * -to [get_ports {hps_trace_D1}] 
set_false_path -from * -to [get_ports {hps_trace_D2}] 
set_false_path -from * -to [get_ports {hps_trace_D3}] 
set_false_path -from * -to [get_ports {hps_trace_D4}] 
set_false_path -from * -to [get_ports {hps_trace_D5}] 
set_false_path -from * -to [get_ports {hps_trace_D6}] 
set_false_path -from * -to [get_ports {hps_trace_D7}] 

set_false_path -from * -to [get_ports {hps_emac1_MDIO}] 
set_false_path -from * -to [get_ports {hps_qspi_IO0}] 
set_false_path -from * -to [get_ports {hps_qspi_IO1}] 
set_false_path -from * -to [get_ports {hps_qspi_IO2}] 
set_false_path -from * -to [get_ports {hps_qspi_IO3}] 
set_false_path -from * -to [get_ports {hps_sdio_CMD}] 
set_false_path -from * -to [get_ports {hps_sdio_D0}] 
set_false_path -from * -to [get_ports {hps_sdio_D1}] 
set_false_path -from * -to [get_ports {hps_sdio_D2}] 
set_false_path -from * -to [get_ports {hps_sdio_D3}] 
set_false_path -from * -to [get_ports {hps_usb1_D0}] 
set_false_path -from * -to [get_ports {hps_usb1_D1}] 
set_false_path -from * -to [get_ports {hps_usb1_D2}] 
set_false_path -from * -to [get_ports {hps_usb1_D3}] 
set_false_path -from * -to [get_ports {hps_usb1_D4}] 
set_false_path -from * -to [get_ports {hps_usb1_D5}] 
set_false_path -from * -to [get_ports {hps_usb1_D6}] 
set_false_path -from * -to [get_ports {hps_usb1_D7}] 
set_false_path -from * -to [get_ports {hps_i2c0_SDA}] 
set_false_path -from * -to [get_ports {hps_i2c0_SCL}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO00}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO17}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO18}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO22}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO24}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO26}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO27}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO35}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO40}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO41}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO42}] 
set_false_path -from * -to [get_ports {hps_gpio_GPIO43}] 

set_false_path -from [get_ports {hps_emac1_MDIO}] -to *
set_false_path -from [get_ports {hps_qspi_IO0}] -to *
set_false_path -from [get_ports {hps_qspi_IO1}] -to *
set_false_path -from [get_ports {hps_qspi_IO2}] -to *
set_false_path -from [get_ports {hps_qspi_IO3}] -to *
set_false_path -from [get_ports {hps_sdio_CMD}] -to *
set_false_path -from [get_ports {hps_sdio_D0}] -to *
set_false_path -from [get_ports {hps_sdio_D1}] -to *
set_false_path -from [get_ports {hps_sdio_D2}] -to *
set_false_path -from [get_ports {hps_sdio_D3}] -to *
set_false_path -from [get_ports {hps_usb1_D0}] -to *
set_false_path -from [get_ports {hps_usb1_D1}] -to *
set_false_path -from [get_ports {hps_usb1_D2}] -to *
set_false_path -from [get_ports {hps_usb1_D3}] -to *
set_false_path -from [get_ports {hps_usb1_D4}] -to *
set_false_path -from [get_ports {hps_usb1_D5}] -to *
set_false_path -from [get_ports {hps_usb1_D6}] -to *
set_false_path -from [get_ports {hps_usb1_D7}] -to *
set_false_path -from [get_ports {hps_i2c0_SDA}] -to *
set_false_path -from [get_ports {hps_i2c0_SCL}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO00}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO17}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO18}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO22}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO24}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO26}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO27}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO35}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO40}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO41}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO42}] -to *
set_false_path -from [get_ports {hps_gpio_GPIO43}] -to *

set_false_path -from [get_ports {hps_emac1_RX_CTL}] -to *
set_false_path -from [get_ports {hps_emac1_RX_CLK}] -to *
set_false_path -from [get_ports {hps_emac1_RXD0}] -to *
set_false_path -from [get_ports {hps_emac1_RXD1}] -to *
set_false_path -from [get_ports {hps_emac1_RXD2}] -to *
set_false_path -from [get_ports {hps_emac1_RXD3}] -to *
set_false_path -from [get_ports {hps_usb1_CLK}] -to *
set_false_path -from [get_ports {hps_usb1_DIR}] -to *
set_false_path -from [get_ports {hps_usb1_NXT}] -to *
set_false_path -from [get_ports {hps_uart0_RX}] -to *
set_false_path -from [get_ports {hps_uart1_RX}] -to *

# create unused clock constraint for HPS I2C and usb1 to avoid misleading unconstraint clock reporting in TimeQuest
create_clock -period "1 MHz" [get_ports hps_i2c0_SCL]
create_clock -period "48 MHz" [get_ports hps_usb1_CLK]


