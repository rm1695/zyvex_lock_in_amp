----------------------------------------------------------------------------
--Date:	10/22/2008
--File:	pio_master_shifter.vhd
--By:	   Jeff Short
--
--Design:   DCI-V3 - L3 Electro-Optical Systems
--Target:   Altera Cyclone III FPGA
--Desc:     FPGA-to-CPLD Serial Shift Register Master Controller
--
--Algorithm:
--
-- Implements a master register that is meant to interface to 'X'-bits of GPIO and perform
-- an SPI-type shift operation to another device on a periodic basis.
-- This particular implementation is meant to shift 16-bits between an FPGA and a CPLD for
-- basic keypress input and misc control signal outputs.
--
-- Implemented as an N-bit write register and a 16-bit read register with button input debouncing
--
--Revisions:
-- 2012-11-27, Jeff Short
--    Added extra wait states to allow SR_OUTPUT_EN_n signal to be used to 
--    externally latch and sample the serial port CTS/RST signals on the
--    G1 Expansion Board.  The mode here only provides the ability to clock
--    the SR_OUTPUT_EN_n signal during non-shifting idle periods.
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

--use work.UTIL_pkg.all;

--Library UNISIM;
--use UNISIM.vcomponents.all;

entity PIO_MASTER_SHIFTER is
   generic (
      cSHIFT_REG_SIZE         : integer := 16;           --Total size of shift register (read + write)
      cREF_CLOCK_FREQUENCY    : integer := 60000000;     --Reference clock frequency (in Hz)
      cSHIFT_CLOCK_FREQUENCY  : integer := 1000000;      --Desired shift clock frequency (in Hz)
      cSHIFT_RATE             : integer := 500;          --Desired shift rate (in Hz)
      cCLK_POLARITY           : boolean := true;         --False is same as CPOL=0:  Data changes on rising edge, sampled on falling edge
                                                         --True is same as CPOL=1:  Data changes on falling edge, sampled on rising edge
      cMSB_FIRST              : boolean := true;         --MSB first (true) or LSB first (false)
      cSTROBE_ACTIVE_ENTIRE_TRANSFER : boolean := true;  --Strobe is asserted during entire transfer or pulsed at end of transfer
      cPULSE_OUTPUT_EN        : boolean := false         --Set to allow SR_STROBE_EN_n to pulse during idle periods (for G1 EXP Board)
   );
   port (
      --Global Signals
      RESET                   : in  std_logic;           --Global reset
      REF_CLK                 : in  std_logic;           --Reference clock (usually OPB_CLK)

      --Data Inputs
      PIO_DATA_TO_SEND        : in  std_logic_vector(cSHIFT_REG_SIZE-1 downto 0);     --Parallel input data to send
      PIO_DATA_RECEIVED       : out std_logic_vector(cSHIFT_REG_SIZE-1 downto 0);     --Parallel output data read from shift register

      --Shift Register Control and Data Output
      SR_CLK_OUT              : out std_logic;           --Serial data output
      SR_DATA_IN              : in  std_logic;           --Serial data input
      SR_DATA_OUT             : out std_logic;           --Serial data output
      SR_STROBE_OUT_n         : out std_logic;           --Serial data output
      SR_OUTPUT_EN_n          : out std_logic            --Serial data output
      
   );      
end PIO_MASTER_SHIFTER;  


architecture PIO_MASTER_SHIFTER_arch of PIO_MASTER_SHIFTER is

   --------------------------------------------------------------------------
   -- Component Declarations
   --------------------------------------------------------------------------


   --------------------------------------------------------------------------
   -- State Machine Definitions
   --------------------------------------------------------------------------
   --Sample Enable State Type
   type STATE_TYPE is
   (  STATE_INIT,
      STATE_IDLE,
      STATE_PULSE_OE_ON,
      STATE_PULSE_OE_OFF,
      STATE_BEGIN_TRANSFER_WAIT,
      STATE_BEGIN_TRANSFER,
      STATE_SHIFTING,
      STATE_PULSE_CLOCK,
      STATE_SHIFT_DONE
   );
   signal sSR_Current_State: STATE_TYPE := STATE_INIT;      --Current state	  
--   attribute FSM_ENCODING : string;
--   attribute FSM_ENCODING of sSR_Current_State: signal is "ONE-HOT";
--   attribute FSM_ENCODING of sSR_Next_State: signal is "ONE-HOT";

   --------------------------------------------------------------------------
   -- Signal Declarations
   --------------------------------------------------------------------------
   --constants
   constant cDEFAULT_DATA        : std_logic_vector(cSHIFT_REG_SIZE-1 downto 0) := (others=>'1');   --Default data to shift out during reset/init

   --connections
   
   --internal signals
   signal sSR_CLK_PULSE          : std_logic := '0';
   signal sSHIFT_TRIGGER         : std_logic := '0';

   --Internal versions of external signals
   signal sSR_OUTPUT_EN_n        : std_logic := '1';           --Leave shift reg OE deasserted until state machine inits contents of ext sr
   signal sSR_CLK_OUT            : std_logic := '1';
   signal sSR_STROBE_OUT_n       : std_logic := '1';
   signal sSR_DATA_OUT           : std_logic := '1';

   signal sSHIFT_REG_OUT         : std_logic_vector(cSHIFT_REG_SIZE-1 downto 0);      --Output data shift register


begin

   --------------------------------------------------------------------------
   -- Map internal signals to external ports
   --------------------------------------------------------------------------
   SR_OUTPUT_EN_n    <= sSR_OUTPUT_EN_n;
   SR_CLK_OUT        <= sSR_CLK_OUT when cCLK_POLARITY=false else not sSR_CLK_OUT;  --Invert clock output if needed
   SR_STROBE_OUT_n   <= sSR_STROBE_OUT_n;
   SR_DATA_OUT       <= sSR_DATA_OUT;


   --------------------------------------------------------------------------
   -- Generate Serial Shift Output Clock
   --
   -- Use input clock divided by cREF_CLOCK_FREQUENCY / cSHIFT_CLOCK_FREQUENCY
   -- Output clock is MS bit
   --------------------------------------------------------------------------
   CLK_DIV_proc : process (RESET, REF_CLK)
      variable i_CLK_DIVIDER : integer range 0 to ((cREF_CLOCK_FREQUENCY / cSHIFT_CLOCK_FREQUENCY)-1) := 0;

   begin
      if RESET = '1' then 
         sSR_CLK_PULSE <= '0';
      elsif rising_edge(REF_CLK) then
         --Generate a single clock wide pulse to run the state machine with
         if (i_CLK_DIVIDER = (cREF_CLOCK_FREQUENCY / cSHIFT_CLOCK_FREQUENCY)-1) then
            i_CLK_DIVIDER := 0;
            sSR_CLK_PULSE <= '1';
         else
            i_CLK_DIVIDER := i_CLK_DIVIDER + 1;
            sSR_CLK_PULSE <= '0';
         end if;
      end if;
   end process;

   --------------------------------------------------------------------------
   -- Generate Start of Shift Trigger
   --
   -- Use input clock divided by cSHIFT_CLOCK_FREQUENCY / cSHIFT_RATE
   -- Output clock is MS bit
   --------------------------------------------------------------------------
   CLK_TRIG_DIV_proc : process (RESET, REF_CLK)
      variable i_SHIFT_TRIG_DIVIDER : integer range 0 to ((cSHIFT_CLOCK_FREQUENCY / cSHIFT_RATE)-1) := 0;

   begin
      if RESET = '1' then 
         sSHIFT_TRIGGER <= '0';
      elsif rising_edge(REF_CLK) then
         --Only run this counter when sSR_CLK_PULSE pulses high
         if (sSR_CLK_PULSE='1') then
            
            --Generate a single clock wide pulse to trigger the state machine with
            if (i_SHIFT_TRIG_DIVIDER = (cSHIFT_CLOCK_FREQUENCY / cSHIFT_RATE)-1) then
               i_SHIFT_TRIG_DIVIDER := 0; --Terminal count reached, so reset counter
               sSHIFT_TRIGGER <= '1';
            else
               i_SHIFT_TRIG_DIVIDER := i_SHIFT_TRIG_DIVIDER + 1;
               sSHIFT_TRIGGER <= '0';
            end if;
         end if;
      end if;
   end process;


   --------------------------------------------------------------------------
   -- Serial Shift State Machine
   --
   -- Stores most recent shifted data.  When the input data is changed,
   -- the new data is copied into a shift register and is shifted out to
   -- the external shift register, followed by a strobe.
   --
   -- Default data is shifted out during reset and the OEn of the external
   -- shift register is asserted.
   --------------------------------------------------------------------------
   sr_main_state : process (RESET, REF_CLK)
      variable i_SHIFT_CTR : integer range 0 to (cSHIFT_REG_SIZE) := cSHIFT_REG_SIZE;

   begin
      
      if RESET = '1' then
         sSR_OUTPUT_EN_n      <= '1';                       --Deassert output enable on external shift register
         sSR_STROBE_OUT_n     <= '1';                       --Deassert strobe output
         sSR_Current_State    <= STATE_INIT;                --Start at beginning of state machine
         sSHIFT_REG_OUT       <= cDEFAULT_DATA;
         sSR_CLK_OUT          <= '0';
         PIO_DATA_RECEIVED    <= cDEFAULT_DATA;
         
      elsif rising_edge(REF_CLK) then
         --Only run state machine once every N reference clock cycles
         if sSR_CLK_PULSE='1' then

            --State machine
            case sSR_Current_State is
               
               when STATE_INIT =>
                  sSR_CLK_OUT <= '0';
                  sSR_OUTPUT_EN_n <= '1';                   --Tristate shift register so external pullups/pulldowns are active
                  sSR_STROBE_OUT_n <= '1';                  --Deassert strobe
                  sSR_Current_State <= STATE_IDLE;          --Wait for shift trigger
                  
               --Monitor trigger to start shifter
               when STATE_IDLE =>
                  sSR_CLK_OUT <= '0';
                  sSR_OUTPUT_EN_n <= '1';                   --Tristate shift register so external pullups/pulldowns are active
                  sSR_STROBE_OUT_n <= '1';                  --Deassert strobe
                  if sSHIFT_TRIGGER = '1' then              --Check if incoming data has changed
                     sSR_Current_State <= STATE_BEGIN_TRANSFER_WAIT;--and go shift the new data out
                  else
                     --Check if need to pulse sSR_OUTPUT_EN_n during idle periods
                     if cPULSE_OUTPUT_EN = true then
                        sSR_Current_State <= STATE_PULSE_OE_ON;--and go shift the new data out
                     end if;
                  end if;
                  
               --Pulse sSR_OUTPUT_EN_n low then high to load data into shift input shift register(s)
               when STATE_PULSE_OE_ON =>
                  sSR_OUTPUT_EN_n <= '0';                   --Set strobe enable low
                  sSR_Current_State <= STATE_PULSE_OE_OFF;  --Wait for shift trigger

               when STATE_PULSE_OE_OFF =>
                  sSR_OUTPUT_EN_n <= '1';                   --Set strobe enable high
                  sSR_Current_State <= STATE_IDLE;          --Back to idle
               
               --Transfer 
               when STATE_BEGIN_TRANSFER_WAIT =>
                  if cPULSE_OUTPUT_EN = false then          --Only do this if NOT pulsing it
                     sSR_OUTPUT_EN_n <= '0';                --Set output enable low during shifting operation
                  end if;
                  sSR_Current_State <= STATE_BEGIN_TRANSFER;--and go shift the new data out
   
               --Transfer 
               when STATE_BEGIN_TRANSFER =>
                  if cPULSE_OUTPUT_EN = false then          --Only do this if NOT pulsing it
                     sSR_OUTPUT_EN_n <= '1';                --Set output enable high to load readback data
                  end if;
                  sSHIFT_REG_OUT <= PIO_DATA_TO_SEND;       --Copy new data into shift register
                  i_SHIFT_CTR := cSHIFT_REG_SIZE;           --Load and enable shift counter
                  if cSTROBE_ACTIVE_ENTIRE_TRANSFER = true then
                     sSR_STROBE_OUT_n <= '0';               --Assert strobe furing entire transfer
                  end if;
                  sSR_Current_State <= STATE_SHIFTING;
               
               --Shift data out until counter reaches 0
               when STATE_SHIFTING =>
                  sSR_CLK_OUT <= '1';                       --Set shift clock low
                  if i_SHIFT_CTR /= 0 then
                     i_SHIFT_CTR := i_SHIFT_CTR - 1;
                     if cMSB_FIRST = true then
                        --MSB First
--                        sSHIFT_REG_OUT <= sSHIFT_REG_OUT(cSHIFT_REG_SIZE-2 downto 0) & SR_DATA_IN;   --Shift MSB out first
                        sSHIFT_REG_OUT <= sSHIFT_REG_OUT(cSHIFT_REG_SIZE-2 downto 0) & '0';   --Shift MSB out first
                        sSR_DATA_OUT <= sSHIFT_REG_OUT(cSHIFT_REG_SIZE-1);    --Output MSB of internal shift reg;                     sSR_Current_State <= STATE_PULSE_CLOCK;
                     else
                        --LSB First
--                        sSHIFT_REG_OUT <= SR_DATA_IN & sSHIFT_REG_OUT(cSHIFT_REG_SIZE-1 downto 1);   --Shift LSB out first
                        sSHIFT_REG_OUT <= '0' & sSHIFT_REG_OUT(cSHIFT_REG_SIZE-1 downto 1);   --Shift LSB out first
                        sSR_DATA_OUT <= sSHIFT_REG_OUT(0);  --Output LSB of internal shift reg;                     sSR_Current_State <= STATE_PULSE_CLOCK;
                     end if;
                     sSR_Current_State <= STATE_PULSE_CLOCK;
                  else
                     sSR_Current_State <= STATE_SHIFT_DONE;
                  end if;
                  
               --Pulse clock out to external shift register
               when STATE_PULSE_CLOCK =>
                  sSR_CLK_OUT <= '0';
                  if cMSB_FIRST = true then
                     --MSB First
                     sSHIFT_REG_OUT(0) <= SR_DATA_IN;    --Shift incoming MSB-first data into LSB
                  else
                     --LSB First
                     sSHIFT_REG_OUT(cSHIFT_REG_SIZE-1) <= SR_DATA_IN;
                  end if;
                  sSR_Current_State <= STATE_SHIFTING;   --Shift incoming LSB-first data into MSB
               
               --Complete strobe and assert output enable
               when STATE_SHIFT_DONE =>
                  if cSTROBE_ACTIVE_ENTIRE_TRANSFER = false then
                     sSR_STROBE_OUT_n <= '0';               --Assert strobe for 1 cycle at end of transfer
                  end if;
--                  sSR_STROBE_OUT_n <= '1';                  --Deassert strobe
                  PIO_DATA_RECEIVED <= sSHIFT_REG_OUT;      --Transfer incoming shift data to recived output reg
                  sSR_Current_State <= STATE_IDLE;          --Back to whence we have come
               
               when others =>
                  sSR_Current_State <= STATE_INIT;
                  
            end case;

         end if;
      end if;
   end process;

end PIO_MASTER_SHIFTER_arch;
   
