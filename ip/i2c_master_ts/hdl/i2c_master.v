/* FPS-Tech I2C Wrapper
 * 
 * - Verilog wrapper for OpenCores (www.opencores.org) I2C Master Core
 * - Wrapper interfaces with Alteras Avalon Bus as a SOPC Builder component
 * 
 * - Ver: 1.0
 */

module i2c_master(clk, reset_n, address,
writedata, readdata, write, chipselect, 
//irq, waitrequest_n, i2c_scl, i2c_sda
irq, waitrequest_n,
scl_pad_i, scl_pad_o, scl_padoen_o, sda_pad_i, sda_pad_o, sda_padoen_o 
);

input clk, reset_n;
input [2:0] address;
input [7:0] writedata;
output [7:0] readdata;
input write, chipselect;

output irq, waitrequest_n;

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//Begin Changes:
//20090617, Jeff Short - Modified to bring I2C input, output, and output enable signals out for external use
// I2C signals
// i2c clock line
input  scl_pad_i;       // SCL-line input
output scl_pad_o;       // SCL-line output (always 1'b0)
output scl_padoen_o;    // SCL-line output enable (active low)

// i2c data line
input  sda_pad_i;       // SDA-line input
output sda_pad_o;       // SDA-line output (always 1'b0)
output sda_padoen_o;    // SDA-line output enable (active low)

//inout i2c_scl;
//inout i2c_sda;
//End Changes
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

wire rst;
assign rst = 1'b0;

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//Begin Changes:
//20090617, Jeff Short - Modified to bring I2C input, output, and output enable signals out for external use
//
//wire scl_pad_o;
//wire scl_padoen_o;
//wire scl_pad_i;
//
//wire sda_pad_o;
//wire sda_padoen_o;
//wire sda_pad_i;
//
//assign i2c_sda = sda_padoen_o ? 1'bZ : sda_pad_o;
//assign i2c_scl = scl_padoen_o ? 1'bZ : scl_pad_o;
//assign sda_pad_i = i2c_sda;
//assign scl_pad_i = i2c_scl;
//End Changes
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

i2c_master_top i2c_inst(
	clk, rst, reset_n, address, writedata, readdata,
	write, chipselect, chipselect, waitrequest_n, irq,
	scl_pad_i, scl_pad_o, scl_padoen_o, sda_pad_i, sda_pad_o, sda_padoen_o );



endmodule

