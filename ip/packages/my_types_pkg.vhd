----------------------------------------------------------------------------
-- Date:   	2017-08-07
-- File:    channel_control.vhd
-- By:      Robert Mason
--
-- Desc:   Input package design
--
--Revisions:
--
--	20170807 - Robert Mason
--		Initial project design
--
-- 20170817 - Robert Mason
--		Addition of generic WIDTH for hysteresis module
--
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

package my_types_pkg is
	type array1024 is array (0 to 1024) of std_logic_vector(63 downto 0);
	type array512 is array (0 to 511) of std_logic_vector(63 downto 0);
	type array256 is array (0 to 255) of std_logic_vector(63 downto 0);
	type array128 is array (0 to 127) of std_logic_vector(63 downto 0);
	type array64 is array (0 to 63) of std_logic_vector(63 downto 0);
	type array32 is array (0 to 31) of std_logic_vector(63 downto 0);
	type array16 is array (0 to 15) of std_logic_vector(63 downto 0);
	type array8	is array (0 to 7) of std_logic_vector(63 downto 0);
	type array4	is array (0 to 3) of std_logic_vector(63 downto 0);
	type array3 is array (0 to 2) of std_logic_vector(63 downto 0);
	type array3int is array (0 to 2) of std_logic_vector(31 downto 0);
	type array2 is array (0 to 1) of std_logic_vector(63 downto 0);
	type arrayWidth is array (0 to 7) of std_logic_vector(63 downto 0);
	
	type int_16_array is array (0 to 15) of std_logic_vector(31 downto 0);
	type int_8_array is array (0 to 7) of std_logic_vector(31 downto 0);
	type int_4_array is array (0 to 3) of std_logic_vector(31 downto 0);
	type int_16_extended_array is array (0 to 15) of std_logic_vector(32 downto 0);
	type int_8_extended_array is array (0 to 7) of std_logic_vector(32 downto 0);
	type int_4_extended_array is array (0 to 3) of std_logic_vector(32 downto 0);
	type sel_16_array	is array (0 to 15) of std_logic_vector(5 downto 0);
	type 	 shift_array		is array (0 to 15) of std_logic_vector(4 downto 0);
end package;