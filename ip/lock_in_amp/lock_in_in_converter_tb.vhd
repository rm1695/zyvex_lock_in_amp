LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY lock_in_in_converter_tb IS 
END lock_in_in_converter_tb;

ARCHITECTURE behavior OF lock_in_in_converter_tb IS

   component Lock_in_in_converter is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		input_data				  : in std_logic_vector(31 downto 0)  := (others => '0');
		input_addr				  : in std_logic_vector(7 downto 0)   := (others => '0');
		
		output_data		   	  : out std_logic_vector(63 downto 0) := (others => '0');
		output_addr				  : out std_logic_vector(7 downto 0)  := (others => '0');
		output_ready			  : out std_logic	:= '0'
	);
	end component;

   signal clk : std_logic := '0';
   constant clk_period : time := 1.176 ns;
	signal outR : std_logic := '0';
	signal strt : std_logic := '0';
	signal rst : std_logic := '0';
	signal inn : std_logic_vector(31 downto 0) := (others => '0');
	signal in_adr : std_logic_vector(7 downto 0) := (others => '0');
	signal out_adr : std_logic_vector(7 downto 0) := (others => '0');
	signal out_data : std_logic_vector(63 downto 0) := (others => '0');


BEGIN

   uut : Lock_in_in_converter
	port map (
		clock_sink_clk         => clk,           --   clock_sink.clk
		reset_sink_reset       => rst,           --   reset_sink.reset

		start_calc     		  => strt,
		input_data				  => inn,
		input_addr				  => in_adr,
		
		output_data		   	  => out_data,
		output_addr				  => out_adr,
		output_ready			  => outR
	);      

   -- Clock process definitions( clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;  --for 0.5 ns signal is '0'.
        clk <= '1';
        wait for clk_period/2;  --for next 0.5 ns signal is '1'.
   end process;
	
	test_proc : process
	begin
		wait for clk_period * 5;
		wait for clk_period * 0.5;
		strt <= '1';
		inn <= x"00000100";
		in_adr <= "00010000";
		wait for clk_period;
		inn <= x"01000000";
		strt <= '0';
		wait for clk_period;
		inn <= x"00000000";
		wait for clk_period * 100;
	end process;

END;