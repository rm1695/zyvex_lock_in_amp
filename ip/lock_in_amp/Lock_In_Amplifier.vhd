----------------------------------------------------------------------------
-- Date:    2019-07-28
-- File:    Lock_In_Amplifier.vhd
-- By:      Robert Mason
--
-- Desc:    Lock in Amplifier control module
--
-- Notes:   
--
-- Issues:
--
-- Need to handle Double inputs from CPU
-- Need to worry about timing adaptors
-- Need to worry about read operations (do they have enough time for the CPU to pick them up?)
-- Output_data_ready needs to be properly handled
--
--
------------------------------------------------------------------------------
--
--Revisions:
--
-- 20190728 - Robert Mason
--   Initial design creation, set up initial registers for passthrough
--
-- 20190801 - Robert Mason
--   Addition of sampling registers, as well as data input handling
--
-- 20190805 - Robert Mason
--   Addition of data output, addition of data saving model
--
-- 20190806 - Robert Mason
--   Changed sampling rate to be number of points
--   Changed memory read addresses to be based off of 0x90408 instead
--   Setup to limit to 1MHz signal
--
-- 20190826 - Robert Mason
--   Set up to cycle through a counting loop for calculations
--
-- 20190831 - Robert Mason
--	  Set up memory component as well as a testing output
--
-- 20200617 - Robert Mason
--   Proper rollback for revision control. Baseline of finished build
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Lock_In_Amplifier is
	port (
		avalon_slave_address   : in  std_logic_vector(7 downto 0)  := (others => '0'); -- avalon_slave.address
		avalon_slave_read      : in  std_logic                     := '0';             --             .read
		avalon_slave_write     : in  std_logic                     := '0';             --             .write
		avalon_slave_writedata : in  std_logic_vector(31 downto 0) := (others => '0'); --             .writedata
		avalon_slave_readdata  : out std_logic_vector(31 downto 0);                   --             .readdata
		
		
		--for dac
		lock_in_enable		      : out std_logic:='0';
		lock_in_address		   : out std_logic_vector(5 downto 0);
		lock_in_write		      : out std_logic;
		lock_in_writedata	      : out std_logic_vector(31 downto 0);
		
		--For avs_fifo
		--Avalon Streaming data in
      asi_reset               : in  std_logic;  --Reset for all Avalon buses
      asi_clk                 : in  std_logic;  --Clock for all Avalon buses
		asi_sop_in              : in  std_logic;
      asi_eop_in              : in  std_logic;
      asi_valid_in            : in  std_logic;
      asi_data_in             : in  std_logic_vector(27 downto 0);  --Parallel shift register data output
      asi_channel_in          : in  std_logic_vector(2 downto 0) --Channel number output
	);
end entity Lock_In_Amplifier;

architecture rtl of Lock_In_Amplifier is

   --========================================================================
   -- Constant Definitions
   --========================================================================

	
	--Constants
	constant INT32_T_SIZE					: natural 								:= 32;
	
	--Data Registers
	constant cADDR_REG_ENABLE				: std_logic_vector(7 downto 0)	:= "00000000"; --Control register
	constant cADDR_REG_SAMPLE_FREQ      : std_logic_vector(7 downto 0)   := "00000001"; --Sample frequency (750m/data)
	constant cADDR_REG_CONST_ADDR			: std_logic_vector(7 downto 0)	:= "00000011"; --Address to write constant to
	constant cADDR_REG_CONST_DATA			: std_logic_vector(7 downto 0)	:= "00000100"; --Data to write constant to
	constant cADDR_REG_BUF_SIZE			: std_logic_Vector(7 downto 0)	:= "00000101"; --Size of data buffers (might be a const)
	
	--Read Registers
	constant cADDR_REG_OUTPUT_0			: std_logic_vector(7 downto 0)	:= "00001000"; --Averaged data CH:0
	constant cADDR_REG_OUTPUT_1			: std_logic_vector(7 downto 0)	:= "00001001"; --Averaged data CH:1
	constant cADDR_REG_OUTPUT_2			: std_logic_vector(7 downto 0)	:= "00001010"; --Averaged data CH:2
	constant cADDR_REG_OUTPUT_3			: std_logic_vector(7 downto 0)	:= "00001011"; --Averaged data CH:3
	constant cADDR_REG_OUTPUT_4			: std_logic_vector(7 downto 0)	:= "00001100"; --Averaged data CH:4
	constant cADDR_REG_OUTPUT_5			: std_logic_vector(7 downto 0)	:= "00001101"; --Averaged data CH:5
	constant cADDR_REG_OUTPUT_6			: std_logic_vector(7 downto 0)	:= "00001110"; --Averaged data CH:6
	constant cADDR_REG_OUTPUT_7			: std_logic_vector(7 downto 0)	:= "00001111"; --Averaged data CH:7
	constant cADDR_REG_OUTPUT_1_0		   : std_logic_vector(7 downto 0)	:= "00010000"; --Averaged 1 data CH:0
	constant cADDR_REG_OUTPUT_1_1		   : std_logic_vector(7 downto 0)	:= "00010001"; --Averaged 1 data CH:1
	constant cADDR_REG_OUTPUT_1_2		   : std_logic_vector(7 downto 0)	:= "00010010"; --Averaged 1 data CH:2
	constant cADDR_REG_OUTPUT_1_3		   : std_logic_vector(7 downto 0)	:= "00010011"; --Averaged 1 data CH:3
	constant cADDR_REG_OUTPUT_1_4		   : std_logic_vector(7 downto 0)	:= "00010100"; --Averaged 1 data CH:4
	constant cADDR_REG_OUTPUT_1_5		   : std_logic_vector(7 downto 0)	:= "00010101"; --Averaged 1 data CH:5
	constant cADDR_REG_OUTPUT_1_6		   : std_logic_vector(7 downto 0)	:= "00010110"; --Averaged 1 data CH:6
	constant cADDR_REG_OUTPUT_1_7		   : std_logic_vector(7 downto 0)	:= "00010111"; --Averaged 1 data CH:7
	
	constant cADDR_REG_SIN_0				: std_logic_vector(7 downto 0)	:= "00100000";
	constant cADDR_REG_COS_0				: std_logic_vector(7 downto 0)	:= "00100001";
	constant cADDR_REG_SIN_1				: std_logic_vector(7 downto 0)	:= "00100010";
	constant cADDR_REG_COS_1				: std_logic_vector(7 downto 0)	:= "00100011";
	constant cADDR_REG_SIN_2				: std_logic_vector(7 downto 0)	:= "00100100";
	constant cADDR_REG_COS_2				: std_logic_vector(7 downto 0)	:= "00100101";
	constant cADDR_REG_SIN_3				: std_logic_vector(7 downto 0)	:= "00100110";
	constant cADDR_REG_COS_3				: std_logic_vector(7 downto 0)	:= "00100111";
	constant cADDR_REG_SIN_4				: std_logic_vector(7 downto 0)	:= "00101000";
	constant cADDR_REG_COS_4				: std_logic_vector(7 downto 0)	:= "00101001";
	constant cADDR_REG_SIN_5				: std_logic_vector(7 downto 0)	:= "00101010";
	constant cADDR_REG_COS_5				: std_logic_vector(7 downto 0)	:= "00101011";
	constant cADDR_REG_SIN_6				: std_logic_vector(7 downto 0)	:= "00101100";
	constant cADDR_REG_COS_6				: std_logic_vector(7 downto 0)	:= "00101101";
	constant cADDR_REG_SIN_7				: std_logic_vector(7 downto 0)	:= "00101110";
	constant cADDR_REG_COS_7				: std_logic_vector(7 downto 0)	:= "00101111";
	
	constant cADDR_REG_NORM_0				: std_logic_vector(7 downto 0)	:= "00100000";
	constant cADDR_REG_NORM_1				: std_logic_vector(7 downto 0)	:= "00100001";
	constant cADDR_REG_NORM_2				: std_logic_vector(7 downto 0)	:= "00100010";
	constant cADDR_REG_NORM_3				: std_logic_vector(7 downto 0)	:= "00100011";
	constant cADDR_REG_NORM_4				: std_logic_vector(7 downto 0)	:= "00100100";
	constant cADDR_REG_NORM_5				: std_logic_vector(7 downto 0)	:= "00100101";
	constant cADDR_REG_NORM_6				: std_logic_vector(7 downto 0)	:= "00100110";
	constant cADDR_REG_NORM_7				: std_logic_vector(7 downto 0)	:= "00100111";
	
	constant cADDR_REG_BANDPASS_A0		: std_logic_vector(7 downto 0)	:= "00110000";
	constant cADDR_REG_BANDPASS_A1		: std_logic_vector(7 downto 0)	:= "00111000";
	constant cADDR_REG_BANDPASS_A2		: std_logic_vector(7 downto 0)	:= "01000000";
	constant cADDR_REG_BANDPASS_A3		: std_logic_vector(7 downto 0)	:= "01001000";
	
	constant cADDR_REG_BANDPASS_B0		: std_logic_vector(7 downto 0)	:= "01010000";
	constant cADDR_REG_BANDPASS_B1		: std_logic_vector(7 downto 0)	:= "01011000";
	constant cADDR_REG_BANDPASS_B2		: std_logic_vector(7 downto 0)	:= "01100000";
	constant cADDR_REG_BANDPASS_B3		: std_logic_vector(7 downto 0)	:= "01101000";
	constant cADDR_REG_BANDPASS_B4		: std_logic_vector(7 downto 0)	:= "01110000";
	
	constant cADDR_REG_LPF					: std_logic_vector(7 downto 0)	:= "01111111";
	
	--========================================================================
	--Aliases
	--========================================================================

	---------
	--Types
	---------
	type int_8_array is array (0 to 7) of std_logic_vector(31 downto 0);
	type double_8_array is array (0 to 7) of std_logic_vector(63 downto 0);

	---------
	--General
	---------
	alias clock is asi_clk;
	alias reset is asi_reset;
	
   --========================================================================
   -- Register Declarations
   --========================================================================
	
	signal enable		: std_logic	                    := '0'; --Operational Control
	signal sample_freq: std_logic_vector(31 downto 0) := "00000000000000000000000000000001"; --Frequency to save samples
	
	signal const_addr	: std_logic_vector(31 downto 0) := (others => '0');
	signal const_data	: std_logic_vector(63 downto 0) := (others => '0');
	
   --========================================================================
   -- Signal Declarations
   --========================================================================
	signal sampled_data           : int_8_array                  := (others => (others => '0'));
	signal sampled_data_1			: int_8_array						 := (others => (others => '0'));
	signal output_data            : int_8_array 						 := (others => (others => '0'));
	signal output_data1           : int_8_array 						 := (others => (others => '0'));
	
	signal output_data_ready		: std_logic_vector(7 downto 0) := (others => '0'); --Need to be manually set eventually
	signal lock_in_out0_dat			: std_logic_vector(63 downto 0):= (others => '0');
	
	signal sample_counter		   : integer	                   := 0;
	signal input_counter          : integer                      := 0;
	signal output_max_counter     : integer                      := 16;
	signal output_channel_counter : integer                      := 16;
	
	signal const_write_data			: std_logic							 := '0';
	signal lie_start					: std_logic							 := '0';
	signal lie_done               : std_logic							 := '0';
	signal lock_in_conv_done		: std_logic							 := '0';
	
	--Lock in input conversion
	signal liic_output				: double_8_array					 := (others => (others => '0'));
	signal liic_out					: std_logic_Vector(63 downto 0):= (others => '0');
	signal liic_addr					: std_logic_vector(7 downto 0) := (others => '0');
	signal liic_done					: std_logic						    := '0';
	
	--Lock in input state handling;
	signal input_data             : int_8_array                  := (others => (others => '0'));
	signal liic_input_queued		: std_logic_vector(7 downto 0) := (others => '0');
	signal liic_input					: std_logic_Vector(31 downto 0):= (others => '0');
	signal liic_input_addr			: std_logic_vector(7 downto 0) := (others => '0');
	signal liic_start					: std_logic							 := '0';
	
	signal liic_counter				: integer							 := 0;
	
	--Control signals
	signal index						: integer							 := 0;
	signal buf_size				   : integer							 := -1;
	
	--lock_in_norm
	signal lock_in_norm             : int_8_array                  := (others => (others => '0'));
	signal lock_in_norm_d				: double_8_array					 := (others => (others => '0'));
	
	--Creating Sin/Cos Values buffers
	signal sin0	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos0	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin1	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos1	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin2	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos2	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin3	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos3	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin4	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos4	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin5	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos5	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin6	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos6	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin7	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos7	: std_logic_vector(63 downto 0)	:= (others => '0');
	
	signal sin0_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos0_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin1_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos1_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin2_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos2_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin3_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos3_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin4_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos4_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin5_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos5_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin6_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos6_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal sin7_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	signal cos7_lpf	: std_logic_vector(63 downto 0)	:= (others => '0');
	
	
	--Sin/Cos write trigger
	signal lim_write 	  : std_logic	:= '0'; 
	signal lim_read_rdy : std_logic	:= '0';
	
	--Bandpass Signals
	signal bandpass_a0	: int_8_array	:= (others => (others => '0'));
	signal bandpass_a1	: int_8_array	:= (others => (others => '0'));
	signal bandpass_a2	: int_8_array	:= (others => (others => '0'));
	signal bandpass_a3	: int_8_array	:= (others => (others => '0'));
	
	signal bandpass_b0	: int_8_array	:= (others => (others => '0'));
	signal bandpass_b1	: int_8_array	:= (others => (others => '0'));
	signal bandpass_b2	: int_8_array	:= (others => (others => '0'));
	signal bandpass_b3	: int_8_array	:= (others => (others => '0'));
	signal bandpass_b4	: int_8_array	:= (others => (others => '0'));
	
	
	--converted signals
	signal bandpass_a0f	: double_8_array	:= (others => (others => '0'));
	signal bandpass_a1f	: double_8_array	:= (others => (others => '0'));
	signal bandpass_a2f	: double_8_array	:= (others => (others => '0'));
	signal bandpass_a3f	: double_8_array	:= (others => (others => '0'));
	
	signal bandpass_b0f	: double_8_array	:= (others => (others => '0'));
	signal bandpass_b1f	: double_8_array	:= (others => (others => '0'));
	signal bandpass_b2f	: double_8_array	:= (others => (others => '0'));
	signal bandpass_b3f	: double_8_array	:= (others => (others => '0'));
	signal bandpass_b4f	: double_8_array	:= (others => (others => '0'));
	
	signal bandpass_a0_in : std_logic_Vector(63 downto 0) := (others => '0');
	signal bandpass_a1_in : std_logic_Vector(63 downto 0) := (others => '0');
	signal bandpass_a2_in : std_logic_Vector(63 downto 0) := (others => '0');
	signal bandpass_a3_in : std_logic_Vector(63 downto 0) := (others => '0');
	
	signal bandpass_b0_in : std_logic_Vector(63 downto 0) := (others => '0');
	signal bandpass_b1_in : std_logic_Vector(63 downto 0) := (others => '0');
	signal bandpass_b2_in : std_logic_Vector(63 downto 0) := (others => '0');
	signal bandpass_b3_in : std_logic_Vector(63 downto 0) := (others => '0');
	signal bandpass_b4_in : std_logic_Vector(63 downto 0) := (others => '0');
	
	signal lock_in_lpf_f	: std_logic_vector(31 downto 0)	:= (others => '0');
	signal lock_in_lpf_d	: std_logic_vector(63 downto 0)	:= (others => '0');
	
	signal lic_out : std_logic_vector(63 downto 0)	:= (others => '0');
	
	signal lic_done : std_logic	:= '0';
	
	signal lie_sin_input : std_logic_vector(63 downto 0)	:= (others => '0');
	signal lie_cos_input : std_logic_vector(63 downto 0)	:= (others => '0');
	
	signal lic_start : std_logic := '0';
	
	signal lic_input : std_logic_vector(63 downto 0)	:= (others => '0');
	
	--Outputs to write
	signal sin0_int : std_logic_vector(31 downto 0) := (others => '0');
	signal sin1_int : std_logic_vector(31 downto 0) := (others => '0');
	signal sin2_int : std_logic_vector(31 downto 0) := (others => '0');
	signal sin3_int : std_logic_vector(31 downto 0) := (others => '0');
	signal sin4_int : std_logic_vector(31 downto 0) := (others => '0');
	signal sin5_int : std_logic_vector(31 downto 0) := (others => '0');
	signal sin6_int : std_logic_vector(31 downto 0) := (others => '0');
	signal sin7_int : std_logic_vector(31 downto 0) := (others => '0');
	
	--multiplication signals
	signal m0_0	: std_logic_vector(63 downto 0) := (others => '0');
	signal m0_1	: std_logic_vector(63 downto 0) := (others => '0');
	signal m0_out	: std_logic_vector(63 downto 0) := (others => '0');
	
	signal lock_in_signal0_int : int_8_array := (others => (others => '0'));
	signal lock_in_input0_int : int_8_array := (others => (others => '0'));
	
	signal lock_in_input0_d : double_8_array := (others => (others => '0'));
	
	signal num_cycles_set : integer := 0;
	
	signal d_in : int_8_array := (others => (others => '0'));
	signal i_data : int_8_array := (others => (others => '0'));
	signal write_fifo : std_logic_vector(7 downto 0) := (others => '0');
	signal read_fifo : std_logic_vector(7 downto 0) := (others => '0');
	
	signal lie_r : std_logic := '0';
	
	--===========================
	--Temporary Debug Signals
	--===========================
	signal bp_a_0_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_a_1_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_a_2_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_a_3_out : std_logic_vector(63 downto 0) := (others => '0');
	
	signal bp_b_0_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_b_1_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_b_2_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_b_3_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_b_4_out : std_logic_vector(63 downto 0) := (others => '0');
	
	signal s2Aout0 : std_logic_vector(63 downto 0) := (others => '0');
	signal s2Aout1 : std_logic_vector(63 downto 0) := (others => '0');
	signal s2Aout2 : std_logic_vector(63 downto 0) := (others => '0');
	
	signal bpAout0 : std_logic_vector(63 downto 0) := (others => '0');
	signal bpAout1 : std_logic_vector(63 downto 0) := (others => '0');
	signal bpAout2 : std_logic_vector(63 downto 0) := (others => '0');
	signal bpAout3 : std_logic_vector(63 downto 0) := (others => '0');
	
	signal bpAout10 : std_logic_vector(63 downto 0) := (others => '0');
	signal bpAout11 : std_logic_vector(63 downto 0) := (others => '0');
	
	signal bpAout20 : std_logic_vector(63 downto 0) := (others => '0');
	
	--Float values
	signal bp_a_0_outf : std_logic_vector(31 downto 0) := (others => '0');
	signal bp_a_1_outf : std_logic_vector(31 downto 0) := (others => '0');
	signal bp_a_2_outf : std_logic_vector(31 downto 0) := (others => '0');
	signal bp_a_3_outf : std_logic_vector(31 downto 0) := (others => '0');
	
	signal bp_b_0_outf : std_logic_vector(31 downto 0) := (others => '0');
	signal bp_b_1_outf : std_logic_vector(31 downto 0) := (others => '0');
	signal bp_b_2_outf : std_logic_vector(31 downto 0) := (others => '0');
	signal bp_b_3_outf : std_logic_vector(31 downto 0) := (others => '0');
	signal bp_b_4_outf : std_logic_vector(31 downto 0) := (others => '0');
	
	signal s2Aout0f : std_logic_vector(31 downto 0) := (others => '0');
	signal s2Aout1f : std_logic_vector(31 downto 0) := (others => '0');
	signal s2Aout2f : std_logic_vector(31 downto 0) := (others => '0');
	
	signal bpAout0f : std_logic_vector(31 downto 0) := (others => '0');
	signal bpAout1f : std_logic_vector(31 downto 0) := (others => '0');
	signal bpAout2f : std_logic_vector(31 downto 0) := (others => '0');
	signal bpAout3f : std_logic_vector(31 downto 0) := (others => '0');
	
	signal bpAout10f : std_logic_vector(31 downto 0) := (others => '0');
	signal bpAout11f : std_logic_vector(31 downto 0) := (others => '0');
	
	signal bpAout20f : std_logic_vector(31 downto 0) := (others => '0');
	
	signal bandpass_a0i	: std_logic_vector(31 downto 0)	:= (others => '0');
	signal bandpass_a1i	: std_logic_vector(31 downto 0)	:= (others => '0');
	signal bandpass_a2i	: std_logic_vector(31 downto 0)	:= (others => '0');
	signal bandpass_a3i	: std_logic_vector(31 downto 0)	:= (others => '0');
	
	signal bandpass_b0i	: std_logic_vector(31 downto 0)	:= (others => '0');
	signal bandpass_b1i	: std_logic_vector(31 downto 0)	:= (others => '0');
	signal bandpass_b2i	: std_logic_vector(31 downto 0)	:= (others => '0');
	signal bandpass_b3i	: std_logic_vector(31 downto 0)	:= (others => '0');
	signal bandpass_b4i	: std_logic_vector(31 downto 0)	:= (others => '0');
	
	--=================
	--Lock In Error TEMP
	--=================
	signal sin0_lpfi : std_logic_vector(31 downto 0) := (others => '0');
	signal cos0_lpfi : std_logic_vector(31 downto 0) := (others => '0');
	
	signal	sin_multd  : std_logic_vector(63 downto 0) := (others => '0');
	signal   cos_multd  : std_logic_vector(63 downto 0) := (others => '0');
	signal   errd       : std_logic_vector(63 downto 0) := (others => '0');
	signal   err_gammad : std_logic_vector(63 downto 0) := (others => '0');
	
	signal	sin_mult  : std_logic_vector(31 downto 0) := (others => '0');
	signal   cos_mult  : std_logic_vector(31 downto 0) := (others => '0');
	signal   err       : std_logic_vector(31 downto 0) := (others => '0');
	signal   err_gamma : std_logic_vector(31 downto 0) := (others => '0');
	
	signal sin_fin : std_logic_vector(31 downto 0) := (others => '0');
	signal cos_fin : std_logic_vector(31 downto 0) := (others => '0');
	
	signal sin_find : std_logic_vector(63 downto 0) := (others => '0');
	signal cos_find : std_logic_vector(63 downto 0) := (others => '0');
	
	signal lock_in_in0 : int_8_array := (others => (others => '0'));
	signal lock_in_in0d: double_8_array := (others => (others => '0'));
	
	signal lie_input : std_logic_Vector(63 downto 0) := (others => '0');
	
	
	
	--Actual outputs
	signal lock_in_output_0f : int_8_array := (others => (others => '0'));
	signal lock_in_output_1f : int_8_array := (others => (others => '0'));
	
	--========================================================================
   -- Component Definitions
   --========================================================================
	
	component float_to_double
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
	component Lock_In_Error is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		
		lock_in_input_0		  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_lpf				  : in std_logic_vector(63 downto 0)  := (others => '0');
		sine                   : in std_logic_vector(63 downto 0)  := (others => '0');
		cosine                 : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_output_0_0f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_1f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_2f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_3f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_4f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_5f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_6f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_7f    : out std_logic_vector(31 downto 0) := (others => '0');
		
		lock_in_output_1_0f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_1f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_2f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_3f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_4f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_5f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_6f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_7f    : out std_logic_vector(31 downto 0) := (others => '0');
		
		sin_mult  : out std_logic_vector(63 downto 0) := (others => '0');
	   cos_mult  : out std_logic_vector(63 downto 0) := (others => '0');
	   err       : out std_logic_vector(63 downto 0) := (others => '0');
	   err_gamma : out std_logic_vector(63 downto 0) := (others => '0');
		sin_find  : out std_logic_vector(63 downto 0) := (others => '0');
		cos_find  : out std_logic_vector(63 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);
	end component;
	
	component Lock_in_in_converter is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		input_data				  : in std_logic_vector(31 downto 0)  := (others => '0');
		input_addr				  : in std_logic_vector(7 downto 0)   := (others => '0');
		
		output_data		   	  : out std_logic_vector(63 downto 0) := (others => '0');
		output_addr				  : out std_logic_vector(7 downto 0)  := (others => '0');
		output_ready			  : out std_logic	:= '0'
	);
	end component;
	
	component Lock_in_Calculation is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		
		input_val				  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_bandpass_a_0	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_1	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_2	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_3	  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_bandpass_b_0	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_1	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_2	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_3	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_4	  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_input_val		  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_0_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_1_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_2_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_3_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bp_b_0_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_1_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_2_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_3_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_4_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		
		--Stage 2 Addition out
		s2Aout0					  : out std_logic_vector(63 downto 0) := (others => '0');
		s2Aout1					  : out std_logic_vector(63 downto 0) := (others => '0');
		s2Aout2                : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout0					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout1					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout2					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout3					  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout10					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout11					  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout20					  : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in0d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in1d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in2d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in3d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in4d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in5d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in6d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in7d           : out std_logic_vector(63 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);
	end component;
	
	component Lock_In_Model is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		const_write_data		  : in std_logic                      := '0';
		const_data_addr		  : in std_logic_vector(31 downto 0)  := (others => '0');
		const_data_val			  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		read_addr              : in integer								  := 0;
		read_pt					  : in std_logic							  := '0';
		read_ready				  : out std_logic							  := '0';
		
		sin0						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos0						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin1						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos1						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin2						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos2						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin3						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos3						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin4						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos4						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin5						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos5						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin6						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos6						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin7						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos7						  : out std_logic_vector(63 downto 0) := (others => '0')
	);
	end component;
	
	component double_to_int
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
	end component;
	
	component double_mult
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
	component int_to_double
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
	component sc_fifo
	PORT
	(
		clock		: IN STD_LOGIC ;
		data		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		rdreq		: IN STD_LOGIC ;
		sclr		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;
		empty		: OUT STD_LOGIC ;
		full		: OUT STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		usedw		: OUT STD_LOGIC_VECTOR (11 DOWNTO 0)
	);
	end component;
	
	component double_to_float
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
	end component;
	
	--========================================================================
   --Processes
   --========================================================================
begin
	
	--========================================================================
   --Component Instantiation
	--========================================================================
	
	LIE : Lock_In_Error 
	port map (
		clock_sink_clk         => clock,
		reset_sink_reset       => lie_r,

		start_calc     		  => lie_start,
		
		lock_in_input_0		  => lie_input,
		lock_in_lpf				  => lock_in_lpf_d,
		sine                   => lie_sin_input,
		cosine                 => lie_cos_input,
		
		lock_in_output_0_0f    => lock_in_output_0f(0),
		lock_in_output_0_1f    => lock_in_output_0f(1),
		lock_in_output_0_2f    => lock_in_output_0f(2),
		lock_in_output_0_3f    => lock_in_output_0f(3),
		lock_in_output_0_4f    => lock_in_output_0f(4),
		lock_in_output_0_5f    => lock_in_output_0f(5),
		lock_in_output_0_6f    => lock_in_output_0f(6),
		lock_in_output_0_7f    => lock_in_output_0f(7),
		
		lock_in_output_1_0f    => lock_in_output_1f(0),
		lock_in_output_1_1f    => lock_in_output_1f(1),
		lock_in_output_1_2f    => lock_in_output_1f(2),
		lock_in_output_1_3f    => lock_in_output_1f(3),
		lock_in_output_1_4f    => lock_in_output_1f(4),
		lock_in_output_1_5f    => lock_in_output_1f(5),
		lock_in_output_1_6f    => lock_in_output_1f(6),
		lock_in_output_1_7f    => lock_in_output_1f(7),
		
		sin_mult  => sin_multd,
	   cos_mult  => cos_multd,
	   err       => errd,
	   err_gamma => err_gammad,
		sin_find => sin_find,
		cos_find => cos_find,
		
		output_ready			  => lie_done
	);
	
	lic : Lock_in_Calculation
	port map(
		clock_sink_clk         => clock,
		reset_sink_reset       => reset,

		start_calc     		  => lic_start,
		
		input_val				  => lic_input,
		
		lock_in_bandpass_a_0	  => bandpass_a0_in,
		lock_in_bandpass_a_1	  => bandpass_a1_in,
		lock_in_bandpass_a_2	  => bandpass_a2_in,
		lock_in_bandpass_a_3	  => bandpass_a3_in,
		
		lock_in_bandpass_b_0	  => bandpass_b0_in,
		lock_in_bandpass_b_1	  => bandpass_b1_in,
		lock_in_bandpass_b_2	  => bandpass_b2_in,
		lock_in_bandpass_b_3	  => bandpass_b3_in,
		lock_in_bandpass_b_4	  => bandpass_b4_in,
		
		lock_in_input_val		  => lic_out,
		bp_a_0_out				  => bp_a_0_out,
		bp_a_1_out				  => bp_a_1_out,
		bp_a_2_out				  => bp_a_2_out,
		bp_a_3_out				  => bp_a_3_out,
		
		bp_b_0_out				  => bp_b_0_out,
		bp_b_1_out				  => bp_b_1_out,
		bp_b_2_out				  => bp_b_2_out,
		bp_b_3_out				  => bp_b_3_out,
		bp_b_4_out				  => bp_b_4_out,
	
		--Stage 2 Addition out
		s2Aout0					  => s2Aout0,
		s2Aout1					  => s2Aout1,
		s2Aout2                => s2Aout2,
		
		bpAout0					  => bpAout0,
		bpAout1					  => bpAout1,
		bpAout2					  => bpAout2,
		bpAout3					  => bpAout3,
		
		bpAout10					  => bpAout10,
		bpAout11					  => bpAout11,
		
		bpAout20					  => bpAout20,
		lock_in_in0d           => lock_in_in0d(0),
		lock_in_in1d           => lock_in_in0d(1),
		lock_in_in2d           => lock_in_in0d(2),
		lock_in_in3d           => lock_in_in0d(3),
		lock_in_in4d           => lock_in_in0d(4),
		lock_in_in5d           => lock_in_in0d(5),
		lock_in_in6d           => lock_in_in0d(6),
		lock_in_in7d           => lock_in_in0d(7),
		
		output_ready			  => lic_done
	);
	
	LIM : Lock_In_Model
	port map (
		clock_sink_clk         => clock,
		reset_sink_reset       => reset,
		
		const_write_data		  => lim_write,
		const_data_addr		  => const_addr,
		const_data_val			  => const_data,
		
		read_addr              => index,
		read_pt					  => liic_done,
		read_ready				  => lim_read_rdy,
		
		sin0						  => cos0,
		cos0						  => sin0,
		sin1						  => cos1,
		cos1						  => sin1,
		sin2						  => cos2,
		cos2						  => sin2,
		sin3						  => cos3,
		cos3						  => sin3,
		sin4						  => cos4,
		cos4						  => sin4,
		sin5						  => cos5,
		cos5						  => sin5,
		sin6						  => cos6,
		cos6						  => sin6,
		sin7						  => cos7,
		cos7						  => sin7
	);
		
	--================================
	--Sin calculation test conversions
	--================================
	double_to_int_inst0 : double_to_int PORT MAP (
		clock	 => clock,
		dataa	 => sin0,
		result	 => sin0_int
	);
	double_to_int_inst1 : double_to_int PORT MAP (
		clock	 => clock,
		dataa	 => sin1,
		result	 => sin1_int
	);
	double_to_int_inst2 : double_to_int PORT MAP (
		clock	 => clock,
		dataa	 => sin2,
		result	 => sin2_int
	);
	double_to_int_inst3 : double_to_int PORT MAP (
		clock	 => clock,
		dataa	 => sin3,
		result	 => sin3_int
	);
	double_to_int_inst4 : double_to_int PORT MAP (
		clock	 => clock,
		dataa	 => sin4,
		result	 => sin4_int
	);
	double_to_int_inst5 : double_to_int PORT MAP (
		clock	 => clock,
		dataa	 => sin5,
		result	 => sin5_int
	);
	double_to_int_inst6 : double_to_int PORT MAP (
		clock	 => clock,
		dataa	 => sin6,
		result	 => sin6_int
	);
	double_to_int_inst7 : double_to_int PORT MAP (
		clock	 => clock,
		dataa	 => sin7,
		result	 => sin7_int
	);
	
	--===============================
	--Multiplication for lock_in_norm
	--Multiplies sin/cos values with the normalization value
	--===============================
	m0 : double_mult
	PORT MAP
	(
		clock		=> clock,
		dataa		=> m0_0,
		datab		=> m0_1,
		result	=> m0_out
	);
	
	--=============================================
	--Conversion of input int to calculation double
	--=============================================
	gen_input_conv : for i in 0 to 7 generate
		f_t_d9 : int_to_double
		PORT MAP
		(
			clock		=> clock,
			dataa		=> input_data(i),
			result	=> liic_output(i)
		);
	end generate;
	
	--=============================================
	--Generates input conversions for separate read
	--=============================================
	gen_tt0 : for i in 0 to 7 generate
		double_to_int_inst12 : double_to_int PORT MAP (
		clock	 => clock,
		dataa	 => lock_in_input0_d(i),
		result	 => lock_in_input0_int(i)
	);
	end generate;
	
	--==============
	--Input handlers
	--==============
	fifo_gen : for i in 0 to 7 generate
		fifo_inst : sc_fifo
		PORT map
		(
			clock		=> clock,
			data		=> d_in(i),
			rdreq		=> read_fifo(i),
			sclr		=> reset,
			wrreq		=> write_fifo(i),
			empty		=> open,
			full		=> open,
			q			=> i_data(i),
			usedw		=> open
		);
	end generate;
	
	--================================
	--Test structure output conversion
	--================================
	d0 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bp_a_0_out,
		result	=> bp_a_0_outf
	);
	d2 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bp_a_1_out,
		result	=> bp_a_1_outf
	);
	d3 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bp_a_2_out,
		result	=> bp_a_2_outf
	);
	d4 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bp_a_3_out,
		result	=> bp_a_3_outf
	);
	d5 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bp_b_0_out,
		result	=> bp_b_0_outf
	);
	d6 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bp_b_1_out,
		result	=> bp_b_1_outf
	);
	d7 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bp_b_2_out,
		result	=> bp_b_2_outf
	);
	d8 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bp_b_3_out,
		result	=> bp_b_3_outf
	);
	d9 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bp_b_4_out,
		result	=> bp_b_4_outf
	);
	d10 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> s2Aout0,
		result	=> s2Aout0f
	);
	d11 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> s2Aout1,
		result	=> s2Aout1f
	);
	d12 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> s2Aout2,
		result	=> s2Aout2f
	);
	d13 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bpAout0,
		result	=> bpAout0f
	);
	d14 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bpAout1,
		result	=> bpAout1f
	);
	d15 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bpAout2,
		result	=> bpAout2f
	);
	d16 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bpAout3,
		result	=> bpAout3f
	);
	d17 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bpAout10,
		result	=> bpAout10f
	);
	d18 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bpAout11,
		result	=> bpAout11f
	);
	d19 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bpAout20,
		result	=> bpAout20f
	);
	
	--===================================================
	--Double to float converters for debug memory access
	--Configured for channel 0, this can be adjusted later
	--===================================================
	d20 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bandpass_a0f(0),
		result	=> bandpass_a0i
	);
	d21 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bandpass_a1f(0),
		result	=> bandpass_a1i
	);
	d22 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bandpass_a2f(0),
		result	=> bandpass_a2i
	);
	d23 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bandpass_a3f(0),
		result	=> bandpass_a3i
	);
	d24 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bandpass_b0f(0),
		result	=> bandpass_b0i
	);
	d25 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bandpass_b1f(0),
		result	=> bandpass_b1i
	);
	d26 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bandpass_b2f(0),
		result	=> bandpass_b2i
	);
	d27 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bandpass_b3f(0),
		result	=> bandpass_b3i
	);
	d28 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> bandpass_b4f(0),
		result	=> bandpass_b4i
	);
	
	--===================
	--Lock In Error Debug
	--===================
	d29 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> sin0_lpf,
		result	=> sin0_lpfi
	);
	d30 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> cos0_lpf,
		result	=> cos0_lpfi
	);
	d31 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> sin_multd,
		result	=> sin_mult
	);
	d32 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> cos_multd,
		result	=> cos_mult
	);
	d33 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> errd,
		result	=> err
	);
	d34 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> err_gammad,
		result	=> err_gamma
	);
	d35 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> sin_find,
		result	=> sin_fin
	);
	d36 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> cos_find,
		result	=> cos_fin
	);
	
	--==========================
	--Bandpass Filter Conversion
	--==========================
	bp_out_gen : for i in 0 to 7 generate
		d37 : double_to_float
		PORT MAP
		(
			clock		=> clock,
			dataa		=> lock_in_in0d(i),
			result	=> lock_in_in0(i)
		);
	end generate;
	
	--========================================================================
	--Data Support Processes
	--========================================================================
	
	--Setting the overall system enable bits
	lock_in_enable <= enable;
	
	--Generating 32 bit extensions for input data to adapt it to the scfifo
	gata_gen : for i in 0 to 7 generate
		d_in(i) <= "0000" & asi_data_in;
	end generate;
	
	--assigning input values
	--This is done when the data input is valid and has the correct channel selected
	--Each of these will be on for a total of 1 clk cycle per data input
	write_fifo(0) <= '1' when asi_valid_in = '1' and asi_channel_in = "000"  else '0';
	write_fifo(1) <= '1' when asi_valid_in = '1' and asi_channel_in = "001"  else '0';
	write_fifo(2) <= '1' when asi_valid_in = '1' and asi_channel_in = "010"  else '0';
	write_fifo(3) <= '1' when asi_valid_in = '1' and asi_channel_in = "011"  else '0';
	write_fifo(4) <= '1' when asi_valid_in = '1' and asi_channel_in = "100"  else '0';
	write_fifo(5) <= '1' when asi_valid_in = '1' and asi_channel_in = "101"  else '0';
	write_fifo(6) <= '1' when asi_valid_in = '1' and asi_channel_in = "110"  else '0';
	write_fifo(7) <= '1' when asi_valid_in = '1' and asi_channel_in = "111"  else '0';
	
	--Converting the input (which has been stored in the scfifo) into 32 bit signed value rather than 28 bit
	--This is done for all 8 channels
	clocked_input : process (clock, reset)
	begin
		if reset = '1' then
			input_data <= (others => (others => '0'));
		elsif rising_edge(clock) then
			for i in 0 to 7 loop
				input_data(i) <= std_logic_vector(resize(signed(i_data(i)(27 downto 0)), avalon_slave_readdata'length));
			end loop;
		end if;
	end process;
	
	--State Machine to handle process flow
	--This runs all of the actual data processes/control logic
	--All actions are based on the input_counter, which really should be current_cycle
	--Note: Actions are based on this and not the flags indicating input ready/done
	--Those signals simply were for debugging/simulation purposes
	data_state_machine : process (clock, reset)
	begin
		if (reset = '1') then
			null;
		elsif lie_r = '1' then
			--Setting this to act as the last state
			lic_input <= liic_output(0);
					
			--Bandpass Signals
			bandpass_a0_in <= bandpass_a0f(0);
			bandpass_a1_in <= bandpass_a1f(0);
			bandpass_a2_in <= bandpass_a2f(0);
			bandpass_a3_in <= bandpass_a3f(0);
			
			bandpass_b0_in <= bandpass_b0f(0);
			bandpass_b1_in <= bandpass_b1f(0);
			bandpass_b2_in <= bandpass_b2f(0);
			bandpass_b3_in <= bandpass_b3f(0);
			bandpass_b4_in <= bandpass_b4f(0);
			
			m0_0 <= cos7;
			m0_1 <= lock_in_norm_d(7);
			
			--Ensuring enable signals go off
			liic_done <= '0';
			lock_in_write <= '0';
			lie_start <= '0';
			
		elsif rising_edge(clock) then
			case input_counter is
				--Set for initial inputs for calculations
				when 0 =>
					lic_input <= liic_output(0);
					liic_done <= '1';
					
					--Bandpass Signals
					bandpass_a0_in <= bandpass_a0f(1);
					bandpass_a1_in <= bandpass_a1f(1);
					bandpass_a2_in <= bandpass_a2f(1);
					bandpass_a3_in <= bandpass_a3f(1);
					
					bandpass_b0_in <= bandpass_b0f(1);
					bandpass_b1_in <= bandpass_b1f(1);
					bandpass_b2_in <= bandpass_b2f(1);
					bandpass_b3_in <= bandpass_b3f(1);
					bandpass_b4_in <= bandpass_b4f(1);
				when 1 =>
					lic_input <= liic_output(0);
					liic_done <= '0';
					
					--Bandpass Signals
					bandpass_a0_in <= bandpass_a0f(2);
					bandpass_a1_in <= bandpass_a1f(2);
					bandpass_a2_in <= bandpass_a2f(2);
					bandpass_a3_in <= bandpass_a3f(2);
					
					bandpass_b0_in <= bandpass_b0f(2);
					bandpass_b1_in <= bandpass_b1f(2);
					bandpass_b2_in <= bandpass_b2f(2);
					bandpass_b3_in <= bandpass_b3f(2);
					bandpass_b4_in <= bandpass_b4f(2);

				when 2 =>
					lic_input <= liic_output(0);

					--Bandpass Signals
					bandpass_a0_in <= bandpass_a0f(3);
					bandpass_a1_in <= bandpass_a1f(3);
					bandpass_a2_in <= bandpass_a2f(3);
					bandpass_a3_in <= bandpass_a3f(3);
					
					bandpass_b0_in <= bandpass_b0f(3);
					bandpass_b1_in <= bandpass_b1f(3);
					bandpass_b2_in <= bandpass_b2f(3);
					bandpass_b3_in <= bandpass_b3f(3);
					bandpass_b4_in <= bandpass_b4f(3);
					
				when 3 =>
					lic_input <= liic_output(0);
					
					--Bandpass Signals
					bandpass_a0_in <= bandpass_a0f(4);
					bandpass_a1_in <= bandpass_a1f(4);
					bandpass_a2_in <= bandpass_a2f(4);
					bandpass_a3_in <= bandpass_a3f(4);
					
					bandpass_b0_in <= bandpass_b0f(4);
					bandpass_b1_in <= bandpass_b1f(4);
					bandpass_b2_in <= bandpass_b2f(4);
					bandpass_b3_in <= bandpass_b3f(4);
					bandpass_b4_in <= bandpass_b4f(4);

				when 4 =>
					lic_input <= liic_output(0);
					
					--Bandpass Signals
					bandpass_a0_in <= bandpass_a0f(5);
					bandpass_a1_in <= bandpass_a1f(5);
					bandpass_a2_in <= bandpass_a2f(5);
					bandpass_a3_in <= bandpass_a3f(5);
					
					bandpass_b0_in <= bandpass_b0f(5);
					bandpass_b1_in <= bandpass_b1f(5);
					bandpass_b2_in <= bandpass_b2f(5);
					bandpass_b3_in <= bandpass_b3f(5);
					bandpass_b4_in <= bandpass_b4f(5);

				when 5 =>
					lic_input <= liic_output(0);
					
					--Bandpass Signals
					bandpass_a0_in <= bandpass_a0f(6);
					bandpass_a1_in <= bandpass_a1f(6);
					bandpass_a2_in <= bandpass_a2f(6);
					bandpass_a3_in <= bandpass_a3f(6);
					
					bandpass_b0_in <= bandpass_b0f(6);
					bandpass_b1_in <= bandpass_b1f(6);
					bandpass_b2_in <= bandpass_b2f(6);
					bandpass_b3_in <= bandpass_b3f(6);
					bandpass_b4_in <= bandpass_b4f(6);
					
				when 6 =>
					lic_input <= liic_output(0);
					
					--Bandpass Signals
					bandpass_a0_in <= bandpass_a0f(7);
					bandpass_a1_in <= bandpass_a1f(7);
					bandpass_a2_in <= bandpass_a2f(7);
					bandpass_a3_in <= bandpass_a3f(7);
					
					bandpass_b0_in <= bandpass_b0f(7);
					bandpass_b1_in <= bandpass_b1f(7);
					bandpass_b2_in <= bandpass_b2f(7);
					bandpass_b3_in <= bandpass_b3f(7);
					bandpass_b4_in <= bandpass_b4f(7);
					
				when 13 => --sin0
					m0_0 <= sin0;
					m0_1 <= lock_in_norm_d(0);
					
					--Bandpass Signals
					bandpass_a0_in <= bandpass_a0f(0);
					bandpass_a1_in <= bandpass_a1f(0);
					bandpass_a2_in <= bandpass_a2f(0);
					bandpass_a3_in <= bandpass_a3f(0);
					
					bandpass_b0_in <= bandpass_b0f(0);
					bandpass_b1_in <= bandpass_b1f(0);
					bandpass_b2_in <= bandpass_b2f(0);
					bandpass_b3_in <= bandpass_b3f(0);
					bandpass_b4_in <= bandpass_b4f(0);
				when 14 => --sin1
					m0_0 <= sin1;
					m0_1 <= lock_in_norm_d(1);
				when 15 => --sin2
					m0_0 <= sin2;
					m0_1 <= lock_in_norm_d(2);
				when 16 => --sin3
					m0_0 <= sin3;
					m0_1 <= lock_in_norm_d(3);
				when 17 => --sin4
					m0_0 <= sin4;
					m0_1 <= lock_in_norm_d(4);
				when 18 => --sin5
					m0_0 <= sin5;
					m0_1 <= lock_in_norm_d(5);
				when 19 => --sin6
					m0_0 <= sin6;
					m0_1 <= lock_in_norm_d(6);
					--sin0_out
					sin0_lpf <= m0_out;
				when 20 => --sin7
					m0_0 <= sin7;
					m0_1 <= lock_in_norm_d(7);
					--sin1_out
					sin1_lpf <= m0_out;
				when 21 => --cos0
					m0_0 <= cos0;
					m0_1 <= lock_in_norm_d(0);
					--sin2_out
					sin2_lpf <= m0_out;
				when 22 => --cos1
					m0_0 <= cos1;
					m0_1 <= lock_in_norm_d(1);
					--sin3_out
					sin3_lpf <= m0_out;
				when 23 => --cos2
					m0_0 <= cos2;
					m0_1 <= lock_in_norm_d(2);
					--sin4_out
					sin4_lpf <= m0_out;
				when 24 => --cos3
					m0_0 <= cos3;
					m0_1 <= lock_in_norm_d(3);
					--sin5_out
					sin5_lpf <= m0_out;
				when 25 => --cos4
					m0_0 <= cos4;
					m0_1 <= lock_in_norm_d(4);
					--sin6_out
					sin6_lpf <= m0_out;
				when 26 => --cos5
					m0_0 <= cos5;
					m0_1 <= lock_in_norm_d(5);
					--sin7_out
					sin7_lpf <= m0_out;
				when 27 => --cos6
					m0_0 <= cos6;
					m0_1 <= lock_in_norm_d(6);
					--co0_out
					cos0_lpf <= m0_out;
				when 28 => --cos7
					m0_0 <= cos7;
					m0_1 <= lock_in_norm_d(7);
					--co1_out
					cos1_lpf <= m0_out;
				when 29 =>
					--co2_out
					cos2_lpf <= m0_out;
				when 30 =>
					--co3_out
					cos3_lpf <= m0_out;
				when 31 =>
					--co4_out
					cos4_lpf <= m0_out;
				when 32 =>
					--co5_out
					cos5_lpf <= m0_out;
				when 33 =>
					--co6_out
					cos6_lpf <= m0_out;
				when 34 =>
					--co7_out
					cos7_lpf <= m0_out;
					
				when 43 =>
					lie_start <= '1';
					lie_sin_input <= sin0_lpf;
					lie_cos_input <= cos0_lpf;
					lie_input <= lock_in_in0d(0);
					
				when 44 =>
					lie_start <= '0';
					lie_sin_input <= sin1_lpf;
					lie_cos_input <= cos1_lpf;
					lie_input <= lock_in_in0d(1);
					
					lock_in_input0_d(0) <= lic_out;
				when 45 =>
					lie_sin_input <= sin2_lpf;
					lie_cos_input <= cos2_lpf;

					lock_in_input0_d(1) <= lic_out;
					lie_input <= lock_in_in0d(2);
				when 46 =>
					lie_sin_input <= sin3_lpf;
					lie_cos_input <= cos3_lpf;

					lock_in_input0_d(2) <= lic_out;
					lie_input <= lock_in_in0d(3);
				when 47 =>
					lie_sin_input <= sin4_lpf;
					lie_cos_input <= cos4_lpf;

					lock_in_input0_d(3) <= lic_out;
					lie_input <= lock_in_in0d(4);
				when 48 =>
					lie_sin_input <= sin5_lpf;
					lie_cos_input <= cos5_lpf;

					lock_in_input0_d(4) <= lic_out;
					lie_input <= lock_in_in0d(5);
				when 49 =>
					lie_sin_input <= sin6_lpf;
					lie_cos_input <= cos6_lpf;

					lock_in_input0_d(5) <= lic_out;
					lie_input <= lock_in_in0d(6);
				when 50 =>
					lie_sin_input <= sin7_lpf;
					lie_cos_input <= cos7_lpf;

					lock_in_input0_d(6) <= lic_out;
					lie_input <= lock_in_in0d(7);
					
				when 51 =>
					lock_in_input0_d(7) <= lic_out;
					
					
				--===============
				--writing outputs
				--===============
				when 100 =>
					--CH0
					lock_in_write <= '1';
					lock_in_address <= "010000";
					lock_in_writedata <= sin0_int;
					
					read_fifo <= "11111111";
				when 101 =>
					lock_in_write <= '0';
					read_fifo <= "00000000";
					
				when 105 =>
					--CH1
					lock_in_write <= '1';
					lock_in_address <= "010001";
					lock_in_writedata <= sin1_int;
				when 106 =>
					lock_in_write <= '0';
				
				when 110 =>
					--CH2
					lock_in_write <= '1';
					lock_in_address <= "010010";
					lock_in_writedata <= sin2_int;
				when 111 =>	
					lock_in_write <= '0';
				
				when 115 =>
					--CH3
					lock_in_write <= '1';
					lock_in_address <= "010011";
					lock_in_writedata <= sin3_int;
				when 116 =>
					lock_in_write <= '0';
				
				when 120 =>
					--CH4
					lock_in_write <= '1';
					lock_in_address <= "010100";
					lock_in_writedata <= sin4_int;
				when 121 =>
					lock_in_write <= '0';
				
				when 125 =>
					--CH5
					lock_in_write <= '1';
					lock_in_address <= "010101";
					lock_in_writedata <= sin5_int;
				when 126 =>
					lock_in_write <= '0';
				
				when 130 =>
					--CH6
					lock_in_write <= '1';
					lock_in_address <= "010110";
					lock_in_writedata <= sin6_int;
				when 131 =>
					lock_in_write <= '0';
				
				when 135 =>
					--CH7
					lock_in_write <= '1';
					lock_in_address <= "010111";
					lock_in_writedata <= sin7_int;
				when 136 =>	
					lock_in_write <= '0';
					
				when 149 =>
					lic_input <= liic_output(0);
					
					--Bandpass Signals
					bandpass_a0_in <= bandpass_a0f(0);
					bandpass_a1_in <= bandpass_a1f(0);
					bandpass_a2_in <= bandpass_a2f(0);
					bandpass_a3_in <= bandpass_a3f(0);
					
					bandpass_b0_in <= bandpass_b0f(0);
					bandpass_b1_in <= bandpass_b1f(0);
					bandpass_b2_in <= bandpass_b2f(0);
					bandpass_b3_in <= bandpass_b3f(0);
					bandpass_b4_in <= bandpass_b4f(0);
				when others =>
					null;
			end case;
		end if;
	end process;
	
	
	--Counter to determine how often the C outputs should be updated
	--the sample_freq is a C input value which says how many cycles are needed before the output refreshes
	--The num_cycles_set is actively unused, but might be worth keeping track of in the future
	sample_counter_proc : process (clock,reset)
	begin
		if(reset = '1') then
			sample_counter <= 0;
		elsif lie_r = '1' then
			sample_counter <= 0;
			for i in 0 to 7 loop
				sampled_data(i) <= lock_in_output_0f(i);
				sampled_data_1(i) <= lock_in_output_1f(i);
			end loop;
			num_cycles_set <= 0;
		elsif rising_edge(clock) then
			if to_integer(unsigned(sample_freq)) * 150 <= sample_counter then --Clock frequency of 150MHz
				sample_counter <= 0;
				--Setting outputs
				for i in 0 to 7 loop
					sampled_data(i) <= lock_in_output_0f(i);
					sampled_data_1(i) <= lock_in_output_1f(i);
				end loop;
				num_cycles_set <= num_cycles_set + 1;
			else
				sample_counter <= sample_counter + 1;
			end if;
		end if;
	end process;
	
	--Limiting the input to 1MHz refresh rate
	--This is the master control clocking process
	--This also sets the lic_start bit at the start of a cycle
	input_limitor_counter_proc : process (clock,reset)
	begin
		if(reset = '1') then
			input_counter <= 0;
		elsif rising_edge(clock) then
			if asi_valid_in = '1' and asi_channel_in = "111" then
				input_counter <= 0;
				--Starting calculations
				lic_start <= '1';
			elsif input_counter < 149 then
				input_counter <= input_counter + 1;
				lic_start <= '0';
			end if;
		end if;
	end process;
	
	--================================
	--Handler for memory read location
	--================================
	counter_proc : process (clock,reset)
	begin
		if (reset = '1') then
			index <= 0;
		elsif lie_r = '1' then
			index <= 0;
		elsif rising_edge(clock) then
			--Changes at 140 out of the 150 sample clock cycles
			--Last read performed at 51 so anytime after that is fine
			if input_counter = 140 then
				if index < (buf_size-1) then
					index <= index + 1;
				else
					index <= 0;
				end if;
			else
				null;
			end if;
		end if;
	end process;
	
	
	--========================================================================
	--Avalon Processes
	--========================================================================
	
	--Handles writing the data as a constant
	lim_write_const : process (clock, reset)
	begin
		if reset = '1' then
			lim_write <= '0';
		elsif rising_edge(clock) then
			if avalon_slave_write = '1' and avalon_slave_address = cADDR_REG_CONST_DATA then
				lim_write <= '1';
			else
				lim_write <= '0';
			end if;
		end if;
	end process;
	
	--Triggers the constant input write
	--The two constants are sin and cos
	--Note: The address has to be written before the constant itself is
	avalon_trig_proc : process (clock,reset)
	begin
		if(reset = '1') then
			const_write_data <= '0';
		elsif rising_edge(clock) then
			if avalon_slave_write = '1' then
				if avalon_slave_address = "11111110" then
					const_write_data <= '1';
				else
					const_write_data <= '0';
				end if;
			else
				const_write_data <= '0';
			end if;
		end if;
	end process;
	
	--Memory mapped writing process to the registers
	reg_write_proc : process (clock,reset)
	begin
		if(reset = '1') then
			null;
		elsif rising_edge(clock) then
			if avalon_slave_write = '1' then
				case avalon_slave_address is
					when cADDR_REG_ENABLE =>
						enable <= avalon_slave_writedata(0);
					when cADDR_REG_SAMPLE_FREQ =>
						sample_freq <= avalon_slave_writedata;
					when cADDR_REG_CONST_DATA =>
						const_data(31 downto 0) <= avalon_slave_writedata;
					when cADDR_REG_CONST_ADDR =>
						const_addr <= avalon_slave_writedata;
					when cADDR_REG_BUF_SIZE =>
						buf_size <= to_integer(unsigned(avalon_slave_writedata));
					when cADDR_REG_LPF =>
						lock_in_lpf_d(31 downto 0) <= avalon_slave_writedata;
						
					--====================
					--Lower norm addresses
					--====================
					when cADDR_REG_NORM_0 =>
						lock_in_norm_d(0)(31 downto 0) <= avalon_slave_writedata;
					when cADDR_REG_NORM_1 =>
						lock_in_norm_d(1)(31 downto 0) <= avalon_slave_writedata;
					when cADDR_REG_NORM_2 =>
						lock_in_norm_d(2)(31 downto 0) <= avalon_slave_writedata;
					when cADDR_REG_NORM_3 =>
						lock_in_norm_d(3)(31 downto 0) <= avalon_slave_writedata;
					when cADDR_REG_NORM_4 =>
						lock_in_norm_d(4)(31 downto 0) <= avalon_slave_writedata;
					when cADDR_REG_NORM_5 =>
						lock_in_norm_d(5)(31 downto 0) <= avalon_slave_writedata;
					when cADDR_REG_NORM_6 =>
						lock_in_norm_d(6)(31 downto 0) <= avalon_slave_writedata;
					when cADDR_REG_NORM_7 =>
						lock_in_norm_d(7)(31 downto 0) <= avalon_slave_writedata;
						
						
					--========================
					-- Lower bandpass_a values
					--========================
					when "00110000" =>
						bandpass_a0f(0)(31 downto 0) <= avalon_slave_writedata;
					when "00110001" =>
						bandpass_a0f(1)(31 downto 0) <= avalon_slave_writedata;
					when "00110010" =>
						bandpass_a0f(2)(31 downto 0) <= avalon_slave_writedata;
					when "00110011" =>
						bandpass_a0f(3)(31 downto 0) <= avalon_slave_writedata;
					when "00110100" =>
						bandpass_a0f(4)(31 downto 0) <= avalon_slave_writedata;
					when "00110101" =>
						bandpass_a0f(5)(31 downto 0) <= avalon_slave_writedata;
					when "00110110" =>
						bandpass_a0f(6)(31 downto 0) <= avalon_slave_writedata;
					when "00110111" =>
						bandpass_a0f(7)(31 downto 0) <= avalon_slave_writedata;
						
					when "00111000" =>
						bandpass_a1f(0)(31 downto 0) <= avalon_slave_writedata;
					when "00111001" =>
						bandpass_a1f(1)(31 downto 0) <= avalon_slave_writedata;
					when "00111010" =>
						bandpass_a1f(2)(31 downto 0) <= avalon_slave_writedata;
					when "00111011" =>
						bandpass_a1f(3)(31 downto 0) <= avalon_slave_writedata;
					when "00111100" =>
						bandpass_a1f(4)(31 downto 0) <= avalon_slave_writedata;
					when "00111101" =>
						bandpass_a1f(5)(31 downto 0) <= avalon_slave_writedata;
					when "00111110" =>
						bandpass_a1f(6)(31 downto 0) <= avalon_slave_writedata;
					when "00111111" =>
						bandpass_a1f(7)(31 downto 0) <= avalon_slave_writedata;
						
					when "01000000" =>
						bandpass_a2f(0)(31 downto 0) <= avalon_slave_writedata;
					when "01000001" =>
						bandpass_a2f(1)(31 downto 0) <= avalon_slave_writedata;
					when "01000010" =>
						bandpass_a2f(2)(31 downto 0) <= avalon_slave_writedata;
					when "01000011" =>
						bandpass_a2f(3)(31 downto 0) <= avalon_slave_writedata;
					when "01000100" =>
						bandpass_a2f(4)(31 downto 0) <= avalon_slave_writedata;
					when "01000101" =>
						bandpass_a2f(5)(31 downto 0) <= avalon_slave_writedata;
					when "01000110" =>
						bandpass_a2f(6)(31 downto 0) <= avalon_slave_writedata;
					when "01000111" =>
						bandpass_a2f(7)(31 downto 0) <= avalon_slave_writedata;	
						
					when "01001000" =>
						bandpass_a3f(0)(31 downto 0) <= avalon_slave_writedata;
					when "01001001" =>
						bandpass_a3f(1)(31 downto 0) <= avalon_slave_writedata;
					when "01001010" =>
						bandpass_a3f(2)(31 downto 0) <= avalon_slave_writedata;
					when "01001011" =>
						bandpass_a3f(3)(31 downto 0) <= avalon_slave_writedata;
					when "01001100" =>
						bandpass_a3f(4)(31 downto 0) <= avalon_slave_writedata;
					when "01001101" =>
						bandpass_a3f(5)(31 downto 0) <= avalon_slave_writedata;
					when "01001110" =>
						bandpass_a3f(6)(31 downto 0) <= avalon_slave_writedata;
					when "01001111" =>
						bandpass_a3f(7)(31 downto 0) <= avalon_slave_writedata;
	
					--=======================
					--Lower bandpass_b values
					--=======================
					when "01010000" =>
						bandpass_b0f(0)(31 downto 0) <= avalon_slave_writedata;
					when "01010001" =>
						bandpass_b0f(1)(31 downto 0) <= avalon_slave_writedata;
					when "01010010" =>
						bandpass_b0f(2)(31 downto 0) <= avalon_slave_writedata;
					when "01010011" =>
						bandpass_b0f(3)(31 downto 0) <= avalon_slave_writedata;
					when "01010100" =>
						bandpass_b0f(4)(31 downto 0) <= avalon_slave_writedata;
					when "01010101" =>
						bandpass_b0f(5)(31 downto 0) <= avalon_slave_writedata;
					when "01010110" =>
						bandpass_b0f(6)(31 downto 0) <= avalon_slave_writedata;
					when "01010111" =>
						bandpass_b0f(7)(31 downto 0) <= avalon_slave_writedata;	
						
					when "01011000" =>
						bandpass_b1f(0)(31 downto 0) <= avalon_slave_writedata;
					when "01011001" =>
						bandpass_b1f(1)(31 downto 0) <= avalon_slave_writedata;
					when "01011010" =>
						bandpass_b1f(2)(31 downto 0) <= avalon_slave_writedata;
					when "01011011" =>
						bandpass_b1f(3)(31 downto 0) <= avalon_slave_writedata;
					when "01011100" =>
						bandpass_b1f(4)(31 downto 0) <= avalon_slave_writedata;
					when "01011101" =>
						bandpass_b1f(5)(31 downto 0) <= avalon_slave_writedata;
					when "01011110" =>
						bandpass_b1f(6)(31 downto 0) <= avalon_slave_writedata;
					when "01011111" =>
						bandpass_b1f(7)(31 downto 0) <= avalon_slave_writedata;	
						
					when "01100000" =>
						bandpass_b2f(0)(31 downto 0) <= avalon_slave_writedata;
					when "01100001" =>
						bandpass_b2f(1)(31 downto 0) <= avalon_slave_writedata;
					when "01100010" =>
						bandpass_b2f(2)(31 downto 0) <= avalon_slave_writedata;
					when "01100011" =>
						bandpass_b2f(3)(31 downto 0) <= avalon_slave_writedata;
					when "01100100" =>
						bandpass_b2f(4)(31 downto 0) <= avalon_slave_writedata;
					when "01100101" =>
						bandpass_b2f(5)(31 downto 0) <= avalon_slave_writedata;
					when "01100110" =>
						bandpass_b2f(6)(31 downto 0) <= avalon_slave_writedata;
					when "01100111" =>
						bandpass_b2f(7)(31 downto 0) <= avalon_slave_writedata;
					
					when "01101000" =>
						bandpass_b3f(0)(31 downto 0) <= avalon_slave_writedata;
					when "01101001" =>
						bandpass_b3f(1)(31 downto 0) <= avalon_slave_writedata;
					when "01101010" =>
						bandpass_b3f(2)(31 downto 0) <= avalon_slave_writedata;
					when "01101011" =>
						bandpass_b3f(3)(31 downto 0) <= avalon_slave_writedata;
					when "01101100" =>
						bandpass_b3f(4)(31 downto 0) <= avalon_slave_writedata;
					when "01101101" =>
						bandpass_b3f(5)(31 downto 0) <= avalon_slave_writedata;
					when "01101110" =>
						bandpass_b3f(6)(31 downto 0) <= avalon_slave_writedata;
					when "01101111" =>
						bandpass_b3f(7)(31 downto 0) <= avalon_slave_writedata;
						
					when "01110000" =>
						bandpass_b4f(0)(31 downto 0) <= avalon_slave_writedata;
					when "01110001" =>
						bandpass_b4f(1)(31 downto 0) <= avalon_slave_writedata;
					when "01110010" =>
						bandpass_b4f(2)(31 downto 0) <= avalon_slave_writedata;
					when "01110011" =>
						bandpass_b4f(3)(31 downto 0) <= avalon_slave_writedata;
					when "01110100" =>
						bandpass_b4f(4)(31 downto 0) <= avalon_slave_writedata;
					when "01110101" =>
						bandpass_b4f(5)(31 downto 0) <= avalon_slave_writedata;
					when "01110110" =>
						bandpass_b4f(6)(31 downto 0) <= avalon_slave_writedata;
					when "01110111" =>
						bandpass_b4f(7)(31 downto 0) <= avalon_slave_writedata;
						
					--=================
					--Upper norm values
					--=================
					when "10000000" =>
						lock_in_norm_d(0)(63 downto 32) <= avalon_slave_writedata;
					when "10000001" =>
						lock_in_norm_d(1)(63 downto 32) <= avalon_slave_writedata;
					when "10000010" =>
						lock_in_norm_d(2)(63 downto 32) <= avalon_slave_writedata;
					when "10000011" =>
						lock_in_norm_d(3)(63 downto 32) <= avalon_slave_writedata;
					when "10000100" =>
						lock_in_norm_d(4)(63 downto 32) <= avalon_slave_writedata;
					when "10000101" =>
						lock_in_norm_d(5)(63 downto 32) <= avalon_slave_writedata;
					when "10000110" =>
						lock_in_norm_d(6)(63 downto 32) <= avalon_slave_writedata;
					when "10000111" =>
						lock_in_norm_d(7)(63 downto 32) <= avalon_slave_writedata;
						
					--=======================
					--Upper bandpass_a values
					--=======================
					when "10110000" =>
						bandpass_a0f(0)(63 downto 32) <= avalon_slave_writedata;
					when "10110001" =>
						bandpass_a0f(1)(63 downto 32) <= avalon_slave_writedata;
					when "10110010" =>
						bandpass_a0f(2)(63 downto 32) <= avalon_slave_writedata;
					when "10110011" =>
						bandpass_a0f(3)(63 downto 32) <= avalon_slave_writedata;
					when "10110100" =>
						bandpass_a0f(4)(63 downto 32) <= avalon_slave_writedata;
					when "10110101" =>
						bandpass_a0f(5)(63 downto 32) <= avalon_slave_writedata;
					when "10110110" =>
						bandpass_a0f(6)(63 downto 32) <= avalon_slave_writedata;
					when "10110111" =>
						bandpass_a0f(7)(63 downto 32) <= avalon_slave_writedata;
						
					when "10111000" =>
						bandpass_a1f(0)(63 downto 32) <= avalon_slave_writedata;
					when "10111001" =>
						bandpass_a1f(1)(63 downto 32) <= avalon_slave_writedata;
					when "10111010" =>
						bandpass_a1f(2)(63 downto 32) <= avalon_slave_writedata;
					when "10111011" =>
						bandpass_a1f(3)(63 downto 32) <= avalon_slave_writedata;
					when "10111100" =>
						bandpass_a1f(4)(63 downto 32) <= avalon_slave_writedata;
					when "10111101" =>
						bandpass_a1f(5)(63 downto 32) <= avalon_slave_writedata;
					when "10111110" =>
						bandpass_a1f(6)(63 downto 32) <= avalon_slave_writedata;
					when "10111111" =>
						bandpass_a1f(7)(63 downto 32) <= avalon_slave_writedata;
						
					when "11000000" =>
						bandpass_a2f(0)(63 downto 32) <= avalon_slave_writedata;
					when "11000001" =>
						bandpass_a2f(1)(63 downto 32) <= avalon_slave_writedata;
					when "11000010" =>
						bandpass_a2f(2)(63 downto 32) <= avalon_slave_writedata;
					when "11000011" =>
						bandpass_a2f(3)(63 downto 32) <= avalon_slave_writedata;
					when "11000100" =>
						bandpass_a2f(4)(63 downto 32) <= avalon_slave_writedata;
					when "11000101" =>
						bandpass_a2f(5)(63 downto 32) <= avalon_slave_writedata;
					when "11000110" =>
						bandpass_a2f(6)(63 downto 32) <= avalon_slave_writedata;
					when "11000111" =>
						bandpass_a2f(7)(63 downto 32) <= avalon_slave_writedata;	
					
					when "11001000" =>
						bandpass_a3f(0)(63 downto 32) <= avalon_slave_writedata;
					when "11001001" =>
						bandpass_a3f(1)(63 downto 32) <= avalon_slave_writedata;
					when "11001010" =>
						bandpass_a3f(2)(63 downto 32) <= avalon_slave_writedata;
					when "11001011" =>
						bandpass_a3f(3)(63 downto 32) <= avalon_slave_writedata;
					when "11001100" =>
						bandpass_a3f(4)(63 downto 32) <= avalon_slave_writedata;
					when "11001101" =>
						bandpass_a3f(5)(63 downto 32) <= avalon_slave_writedata;
					when "11001110" =>
						bandpass_a3f(6)(63 downto 32) <= avalon_slave_writedata;
					when "11001111" =>
						bandpass_a3f(7)(63 downto 32) <= avalon_slave_writedata;
	
					--=======================
					--Upper bandpass_b values
					--=======================
					when "11010000" =>
						bandpass_b0f(0)(63 downto 32) <= avalon_slave_writedata;
					when "11010001" =>
						bandpass_b0f(1)(63 downto 32) <= avalon_slave_writedata;
					when "11010010" =>
						bandpass_b0f(2)(63 downto 32) <= avalon_slave_writedata;
					when "11010011" =>
						bandpass_b0f(3)(63 downto 32) <= avalon_slave_writedata;
					when "11010100" =>
						bandpass_b0f(4)(63 downto 32) <= avalon_slave_writedata;
					when "11010101" =>
						bandpass_b0f(5)(63 downto 32) <= avalon_slave_writedata;
					when "11010110" =>
						bandpass_b0f(6)(63 downto 32) <= avalon_slave_writedata;
					when "11010111" =>
						bandpass_b0f(7)(63 downto 32) <= avalon_slave_writedata;	
						
					when "11011000" =>
						bandpass_b1f(0)(63 downto 32) <= avalon_slave_writedata;
					when "11011001" =>
						bandpass_b1f(1)(63 downto 32) <= avalon_slave_writedata;
					when "11011010" =>
						bandpass_b1f(2)(63 downto 32) <= avalon_slave_writedata;
					when "11011011" =>
						bandpass_b1f(3)(63 downto 32) <= avalon_slave_writedata;
					when "11011100" =>
						bandpass_b1f(4)(63 downto 32) <= avalon_slave_writedata;
					when "11011101" =>
						bandpass_b1f(5)(63 downto 32) <= avalon_slave_writedata;
					when "11011110" =>
						bandpass_b1f(6)(63 downto 32) <= avalon_slave_writedata;
					when "11011111" =>
						bandpass_b1f(7)(63 downto 32) <= avalon_slave_writedata;	
						
					when "11100000" =>
						bandpass_b2f(0)(63 downto 32) <= avalon_slave_writedata;
					when "11100001" =>
						bandpass_b2f(1)(63 downto 32) <= avalon_slave_writedata;
					when "11100010" =>
						bandpass_b2f(2)(63 downto 32) <= avalon_slave_writedata;
					when "11100011" =>
						bandpass_b2f(3)(63 downto 32) <= avalon_slave_writedata;
					when "11100100" =>
						bandpass_b2f(4)(63 downto 32) <= avalon_slave_writedata;
					when "11100101" =>
						bandpass_b2f(5)(63 downto 32) <= avalon_slave_writedata;
					when "11100110" =>
						bandpass_b2f(6)(63 downto 32) <= avalon_slave_writedata;
					when "11100111" =>
						bandpass_b2f(7)(63 downto 32) <= avalon_slave_writedata;
					
					when "11101000" =>
						bandpass_b3f(0)(63 downto 32) <= avalon_slave_writedata;
					when "11101001" =>
						bandpass_b3f(1)(63 downto 32) <= avalon_slave_writedata;
					when "11101010" =>
						bandpass_b3f(2)(63 downto 32) <= avalon_slave_writedata;
					when "11101011" =>
						bandpass_b3f(3)(63 downto 32) <= avalon_slave_writedata;
					when "11101100" =>
						bandpass_b3f(4)(63 downto 32) <= avalon_slave_writedata;
					when "11101101" =>
						bandpass_b3f(5)(63 downto 32) <= avalon_slave_writedata;
					when "11101110" =>
						bandpass_b3f(6)(63 downto 32) <= avalon_slave_writedata;
					when "11101111" =>
						bandpass_b3f(7)(63 downto 32) <= avalon_slave_writedata;
						
					when "11110000" =>
						bandpass_b4f(0)(63 downto 32) <= avalon_slave_writedata;
					when "11110001" =>
						bandpass_b4f(1)(63 downto 32) <= avalon_slave_writedata;
					when "11110010" =>
						bandpass_b4f(2)(63 downto 32) <= avalon_slave_writedata;
					when "11110011" =>
						bandpass_b4f(3)(63 downto 32) <= avalon_slave_writedata;
					when "11110100" =>
						bandpass_b4f(4)(63 downto 32) <= avalon_slave_writedata;
					when "11110101" =>
						bandpass_b4f(5)(63 downto 32) <= avalon_slave_writedata;
					when "11110110" =>
						bandpass_b4f(6)(63 downto 32) <= avalon_slave_writedata;
					when "11110111" =>
						bandpass_b4f(7)(63 downto 32) <= avalon_slave_writedata;
						
					--Sin/Cos write value
					when "11111110" =>
						const_data(63 downto 32) <= avalon_slave_writedata;
					when "11111111" =>
						lock_in_lpf_d(63 downto 32) <= avalon_slave_writedata;
						
					when others =>
						null;
				end case;
			end if;
		end if;
	end process;
	
	--Memory mapped reading process
	reg_read_proc : process (clock,reset)
	begin
		if(reset = '1') then
			null;
		elsif rising_edge(clock) then
			if avalon_slave_read = '1' then
				case avalon_slave_address is
					when cADDR_REG_OUTPUT_0 =>
						avalon_slave_readdata <= sampled_data(0);
					when cADDR_REG_OUTPUT_1 =>
						avalon_slave_readdata <= sampled_data(1);
					when cADDR_REG_OUTPUT_2 =>
						avalon_slave_readdata <= sampled_data(2);
					when cADDR_REG_OUTPUT_3 =>
						avalon_slave_readdata <= sampled_data(3);
					when cADDR_REG_OUTPUT_4 =>
						avalon_slave_readdata <= sampled_data(4);
					when cADDR_REG_OUTPUT_5 =>
						avalon_slave_readdata <= sampled_data(5);
					when cADDR_REG_OUTPUT_6 =>
						avalon_slave_readdata <= sampled_data(6);
					when cADDR_REG_OUTPUT_7 =>
						avalon_slave_readdata <= sampled_data(7);
					when cADDR_REG_OUTPUT_1_0 =>
						avalon_slave_readdata <= sampled_data_1(0);
					when cADDR_REG_OUTPUT_1_1 =>
						avalon_slave_readdata <= sampled_data_1(1);
					when cADDR_REG_OUTPUT_1_2 =>
						avalon_slave_readdata <= sampled_data_1(2);
					when cADDR_REG_OUTPUT_1_3 =>
						avalon_slave_readdata <= sampled_data_1(3);
					when cADDR_REG_OUTPUT_1_4 =>
						avalon_slave_readdata <= sampled_data_1(4);
					when cADDR_REG_OUTPUT_1_5 =>
						avalon_slave_readdata <= sampled_data_1(5);
					when cADDR_REG_OUTPUT_1_6 =>
						avalon_slave_readdata <= sampled_data_1(6);
					when cADDR_REG_OUTPUT_1_7 =>
						avalon_slave_readdata <= sampled_data_1(7);
						
					--=============================
					--Debug output memory addresses
					--=============================
					when "01000000" =>
						avalon_slave_readdata <= bp_a_0_outf;
					when "01000001" =>
						avalon_slave_readdata <= bp_a_1_outf;
					when "01000010" =>
						avalon_slave_readdata <= bp_a_2_outf;
					when "01000011" =>
						avalon_slave_readdata <= bp_a_3_outf;
					when "01000100" =>
						avalon_slave_readdata <= bp_b_0_outf;
					when "01000101" =>
						avalon_slave_readdata <= bp_b_1_outf;
					when "01000110" =>
						avalon_slave_readdata <= bp_b_2_outf;
					when "01000111" =>
						avalon_slave_readdata <= bp_b_3_outf;
					when "01001000" =>
						avalon_slave_readdata <= bp_b_4_outf;
					when "01001001" =>
						avalon_slave_readdata <= s2Aout0f;
					when "01001010" =>
						avalon_slave_readdata <= s2Aout1f;
					when "01001011" =>
						avalon_slave_readdata <= s2Aout2f;
					when "01001100" =>
						avalon_slave_readdata <= bpAout0f;
					when "01001101" =>
						avalon_slave_readdata <= bpAout1f;
					when "01001110" =>
						avalon_slave_readdata <= bpAout2f;
					when "01001111" =>
						avalon_slave_readdata <= bpAout3f;
					when "01010000" =>
						avalon_slave_readdata <= bpAout10f;
					when "01010001" =>
						avalon_slave_readdata <= bpAout11f;
					when "01010010" =>
						avalon_slave_readdata <= bpAout20f;
					when "01010011" =>
						avalon_slave_readdata <= bandpass_a0i;
					when "01010100" =>
						avalon_slave_readdata <= bandpass_a1i;
					when "01010101" =>
						avalon_slave_readdata <= bandpass_a2i;
					when "01010110" =>
						avalon_slave_readdata <= bandpass_a3i;
					when "01010111" =>
						avalon_slave_readdata <= bandpass_b0i;
					when "01011000" =>
						avalon_slave_readdata <= bandpass_b1i;
					when "01011001" =>
						avalon_slave_readdata <= bandpass_b2i;
					when "01011010" =>
						avalon_slave_readdata <= bandpass_b3i;
					when "01011011" =>
						avalon_slave_readdata <= bandpass_b4i;
						
					when "01100000" => --96
						avalon_slave_readdata <= sin0_lpfi;
					when "01100001" => --97
						avalon_slave_readdata <= cos0_lpfi;
					when "01100010" => --98
						avalon_slave_readdata <= sin_mult;
					when "01100011" => --99
						avalon_slave_readdata <= cos_mult;
					when "01100100" => --100
						avalon_slave_readdata <= err;
					when "01100101" => --101
						avalon_slave_readdata <= err_gamma;
					when "01100110" => --102
						avalon_slave_readdata <= sin_fin;
					when "01100111" => --103
						avalon_slave_readdata <= cos_fin;
					when "01101000" => --104
						avalon_slave_readdata <= lock_in_input0_int(2);
					when "01101001" => --105
						avalon_slave_readdata <= lock_in_in0(2);
						
					when "10000000" =>
						avalon_slave_readdata <= lock_in_in0(0);
					when "10000001" =>
						avalon_slave_readdata <= lock_in_in0(1);
					when "10000010" =>
						avalon_slave_readdata <= lock_in_in0(2);
					when "10000011" =>
						avalon_slave_readdata <= lock_in_in0(3);
					when "10000100" =>
						avalon_slave_readdata <= lock_in_in0(4);
					when "10000101" =>
						avalon_slave_readdata <= lock_in_in0(5);
					when "10000110" =>
						avalon_slave_readdata <= lock_in_in0(6);
					when "10000111" =>
						avalon_slave_readdata <= lock_in_in0(7);
						
					--Default
					when others =>
						null;
				end case;
			end if;
		end if;
	end process;
	
	--==============================================
	--Handles the reset process of the lock in error
	--==============================================
	reset_proc : process (clock, reset)
	begin
		if reset = '1' then
			lie_r <= '0';
		elsif rising_edge(clock) then
			if avalon_slave_write = '1' and avalon_slave_address = "00000010" then
				lie_r <= avalon_slave_writedata(0);
			end if;
		end if;
	end process;
	
end architecture rtl;