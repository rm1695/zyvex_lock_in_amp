----------------------------------------------------------------------------
-- Date:    2019-07-26
-- File:    Lock_In_Error.vhd
-- By:      Robert Mason
--
-- Desc:    Lock in Amplifier calculation Module
--
-- Notes:   
--
--  Used to calculate the specific errors, going to need to do some piping
--
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Lock_In_Error is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		
		lock_in_input_0		  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_lpf				  : in std_logic_vector(63 downto 0)  := (others => '0');
		sine                   : in std_logic_vector(63 downto 0)  := (others => '0');
		cosine                 : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_output_0_0f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_1f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_2f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_3f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_4f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_5f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_6f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_7f    : out std_logic_vector(31 downto 0) := (others => '0');
		
		lock_in_output_1_0f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_1f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_2f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_3f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_4f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_5f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_6f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_7f    : out std_logic_vector(31 downto 0) := (others => '0');
		
		sin_mult  : out std_logic_vector(63 downto 0) := (others => '0');
	   cos_mult  : out std_logic_vector(63 downto 0) := (others => '0');
	   err       : out std_logic_vector(63 downto 0) := (others => '0');
	   err_gamma : out std_logic_vector(63 downto 0) := (others => '0');
		sin_find  : out std_logic_vector(63 downto 0) := (others => '0');
		cos_find  : out std_logic_vector(63 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);
end entity Lock_In_Error;

architecture rtl of Lock_In_Error is

   --========================================================================
   -- Constant Definitions
   --========================================================================

	
	--Constants
	constant INT32_T_SIZE					: natural 								:= 32;
	
	constant num_cycles					   : integer								:= 58; --Cycles until process completely done
	constant num_cycles_output          : integer                        := 40; --Cycles until output starts

	--========================================================================
	--Aliases
	--========================================================================

	---------
	--Types
	---------
	type double_8_array is array (0 to 7) of std_logic_vector(63 downto 0);
	type float_8_array is array (0 to 7) of std_logic_Vector(31 downto 0);

	---------
	--General
	---------
	alias clock is clock_sink_clk;
	alias reset is reset_sink_reset;
	
   --========================================================================
   -- Register Declarations
   --========================================================================
	
	--=================
	--Support Registers
	--=================
	signal sin        : double_8_array := (others => (others => '0'));
	signal cosin      : double_8_array := (others => (others => '0'));
	signal lpf        : double_8_array := (others => (others => '0'));
	signal out0       : double_8_array := (others => (others => '0'));
	signal out1       : double_8_array := (others => (others => '0'));
	signal lock_input : double_8_array := (others => (others => '0'));
	signal lock_output1: double_8_array := (others => (others => '0'));
	
	--Saving values for longer
	signal err_lpf_sin : double_8_array := (others => (others => '0'));
	signal err_lpf_cos : double_8_array := (others => (others => '0'));
	
	signal gout0 : double_8_array := (others => (others => '0'));
	signal gout1 : double_8_array := (others => (others => '0'));
	
	signal outD0 : double_8_array := (others => (others => '0'));
	signal outD1 : double_8_array := (others => (others => '0'));
	
	signal prev_out0 : double_8_array := (others => (others => '0'));
	signal prev_out1 : double_8_array := (others => (others => '0'));
	
   --========================================================================
   -- Signal Declarations
   --========================================================================
	
	signal current_cycle	: integer := 57;
	
	--Outputs of components
	signal M0o : std_logic_vector(63 downto 0) := (others => '0');
	signal M1o : std_logic_vector(63 downto 0) := (others => '0');
	signal A0o : std_logic_vector(63 downto 0) := (others => '0');
	signal S0o : std_logic_vector(63 downto 0) := (others => '0');
	signal M2o : std_logic_vector(63 downto 0) := (others => '0');
	signal M3o : std_logic_vector(63 downto 0) := (others => '0');
	signal M4o : std_logic_vector(63 downto 0) := (others => '0');
	signal A1o : std_logic_vector(63 downto 0) := (others => '0');
	signal A2o : std_logic_vector(63 downto 0) := (others => '0');
	
	--Inputs to components
	signal M0_in0 : std_logic_vector(63 downto 0) := (others => '0');
	signal M0_in1 : std_logic_vector(63 downto 0) := (others => '0');
	signal M1_in0 : std_logic_vector(63 downto 0) := (others => '0');
	signal M1_in1 : std_logic_vector(63 downto 0) := (others => '0');
	signal M4_in0 : std_logic_vector(63 downto 0) := (others => '0');
	signal M4_in1 : std_logic_vector(63 downto 0) := (others => '0');
	signal A0_in0 : std_logic_vector(63 downto 0) := (others => '0');
	signal A0_in1 : std_logic_vector(63 downto 0) := (others => '0');
	signal A2_in0 : std_logic_vector(63 downto 0) := (others => '0');
	signal A2_in1 : std_logic_vector(63 downto 0) := (others => '0');
	signal S0_in0 : std_logic_vector(63 downto 0) := (others => '0');
	signal S0_in1 : std_logic_vector(63 downto 0) := (others => '0');
	
   --========================================================================
   -- Component Definitions
   --========================================================================
	
	--Pipeline of 7 cycles
	component double_adder
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
	--Pipeline of 5 cycles
	component double_mult
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
	--Pipeline of 7 cycles
	component double_subtractor
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;

	--Worst-case 6 cycle combinational
	component double_to_float
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
	end component;
	
	--========================================================================
   --Processes
   --========================================================================
begin
	
	--========================================================================
   --Component Instantiation
	--========================================================================
	
	A0 : double_adder PORT MAP (
		clock	 => clock,
		dataa	 => A0_in0,
		datab	 => A0_in1,
		result => A0o
	);
	
	A2 : double_adder PORT MAP (
		clock	 => clock,
		dataa	 => A2_in0,
		datab	 => A2_in1,
		result => A2o
	);
	
	S0 : double_subtractor PORT MAP (
		clock	 => clock,
		dataa	 => S0_in0,
		datab	 => S0_in1,
		result => S0o
	);
	
	M0 : double_mult PORT MAP (
		clock	 => clock,
		dataa	 => M0_in0,
		datab	 => M0_in1,
		result => M0o
	);
	
	M1 : double_mult PORT MAP (
		clock	 => clock,
		dataa	 => M1_in0,
		datab	 => M1_in1,
		result => M1o
	);
	
	M4 : double_mult PORT MAP (
		clock	 => clock,
		dataa	 => M4_in0,
		datab	 => M4_in1,
		result => M4o
	);
	
	--======================================
	--Temporary Deugging breakout for output
	--======================================
	output_gen : for i in 0 to 7 generate 
		--Output 0
		Agen0 : double_adder PORT MAP (
			clock	 => clock,
			dataa	 => gout0(i),
			datab	 => err_lpf_sin(i),
			result => out0(i)
		);
		--Output 1
		Agen1 : double_adder PORT MAP (
			clock	 => clock,
			dataa	 => gout1(i),
			datab	 => err_lpf_cos(i),
			result => out1(i)
		);
		
	end generate;
	
	--Have to write double_to_float conversion blocks individually
	d0 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD0(0),
		result	=> lock_in_output_0_0f
	);
	d1 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD0(1),
		result	=> lock_in_output_0_1f
	);
	d2 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD0(2),
		result	=> lock_in_output_0_2f
	);
	d3 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD0(3),
		result	=> lock_in_output_0_3f
	);
	d4 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD0(4),
		result	=> lock_in_output_0_4f
	);
	d5 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD0(5),
		result	=> lock_in_output_0_5f
	);
	d6 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD0(6),
		result	=> lock_in_output_0_6f
	);
	d7 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD0(7),
		result	=> lock_in_output_0_7f
	);
	
	
	d10 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD1(0),
		result	=> lock_in_output_1_0f
	);
	d11 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD1(1),
		result	=> lock_in_output_1_1f
	);
	d12 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD1(2),
		result	=> lock_in_output_1_2f
	);
	d13 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD1(3),
		result	=> lock_in_output_1_3f
	);
	d14 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD1(4),
		result	=> lock_in_output_1_4f
	);
	d15 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD1(5),
		result	=> lock_in_output_1_5f
	);
	d16 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD1(6),
		result	=> lock_in_output_1_6f
	);
	d17 : double_to_float
	PORT MAP
	(
		clock		=> clock,
		dataa		=> outD1(7),
		result	=> lock_in_output_1_7f
	);
	
	--========================================================================
	--Control Logic
	--========================================================================
	
	
	--
	--This is the main control loop
	--
	counter_proc : process (clock,reset)
	begin
		if reset = '1' then
			current_cycle <= num_cycles;
		elsif rising_edge(clock) then
			if current_cycle < num_cycles and current_cycle > 0 then
				current_cycle <= current_cycle + 1;
			elsif current_cycle = 0 and start_calc = '1' then
				current_cycle <= 1;
                        elsif current_cycle = num_cycles then
				current_cycle <= 0;
			end if;
		end if;
	end process;
	
	--Output display cycle
	--This decides if the output is ready or not
	--Used specifically to let calculation know the cycle is finished
	output_ready_proc : process (clock,reset)
	begin
		if reset = '1' then
			output_ready <= '0';
		elsif rising_edge(clock) then
			if current_cycle = num_cycles_output then
				output_ready <= '1';
			else
				output_ready <= '0';
			end if;
		end if;
	end process;
	
	--========================================================================
	--Data Pipelining Support
	--========================================================================
	
	--Control logic for the state machine
	data_pipline_proc : process(clock,reset)
	begin
		if reset = '1' then
			sin <= (others => (others => '0'));
			cosin <= (others => (others => '0'));
			lpf <= (others => (others => '0'));
			lock_input <= (others => (others => '0'));
			lock_output1 <= (others => (others => '0'));
			
			M0_in0 <= (others => '0');
			M0_in1 <= (others => '0');
			M1_in0 <= (others => '0');
			M1_in1 <= (others => '0');
			M4_in0 <= (others => '0');
			M4_in1 <= (others => '0');
			A0_in0 <= (others => '0');
			A0_in1 <= (others => '0');
			A2_in0 <= (others => '0');
			A2_in1 <= (others => '0');
			S0_in0 <= (others => '0');
			S0_in1 <= (others => '0');
			
			outD1 <= (others => (others => '0'));
			outD0 <= (others => (others => '0'));
			
			gout0 <= (others => (others => '0'));
			gout1 <= (others => (others => '0'));
			
			prev_out0 <= (others => (others => '0'));
			prev_out1 <= (others => (others => '0'));
			
			err_lpf_sin <= (others => (others => '0'));
			err_lpf_cos <= (others => (others => '0'));
		elsif rising_edge(clock) then
			case current_cycle is 
				when 0 =>
					M0_in0 <= prev_out0(0);
					M0_in1 <= sine;
					sin(0) <= sine;
					M1_in0 <= prev_out1(0);
					M1_in1 <= cosine;
					cosin(0) <= cosine;
					--Input Storage
					lpf(0) <= lock_in_lpf;
					lock_input(0) <= lock_in_input_0;
					
				when 1 =>
					M0_in0 <= prev_out0(1);
					M0_in1 <= sine;
					sin(1) <= sine;
					M1_in0 <= prev_out1(1);
					M1_in1 <= cosine;
					cosin(1) <= cosine;
					--Input Storage
					lpf(1) <= lock_in_lpf;
					lock_input(1) <= lock_in_input_0;
					
				when 2 =>
					M0_in0 <= prev_out0(2);
					M0_in1 <= sine;
					sin(2) <= sine;
					M1_in0 <= prev_out1(2);
					M1_in1 <= cosine;
					cosin(2) <= cosine;
					--Input Storage
					lpf(2) <= lock_in_lpf;
					lock_input(2) <= lock_in_input_0;
					
				when 3 =>
					M0_in0 <= prev_out0(3);
					M0_in1 <= sine;
					sin(3) <= sine;
					M1_in0 <= prev_out1(3);
					M1_in1 <= cosine;
					cosin(3) <= cosine;
					--Input Storage
					lpf(3) <= lock_in_lpf;
					lock_input(3) <= lock_in_input_0;
					
				when 4 =>
					M0_in0 <= prev_out0(4);
					M0_in1 <= sine;
					sin(4) <= sine;
					M1_in0 <= prev_out1(4);
					M1_in1 <= cosine;
					cosin(4) <= cosine;
					--Input Storage
					lpf(4) <= lock_in_lpf;
					lock_input(4) <= lock_in_input_0;
					
				when 5 =>
					M0_in0 <= prev_out0(5);
					M0_in1 <= sine;
					sin(5) <= sine;
					M1_in0 <= prev_out1(5);
					M1_in1 <= cosine;
					cosin(5) <= cosine;
					--Input Storage
					lpf(5) <= lock_in_lpf;
					lock_input(5) <= lock_in_input_0;
					
				when 6 =>
					M0_in0 <= prev_out0(6);
					M0_in1 <= sine;
					sin(6) <= sine;
					M1_in0 <= prev_out1(6);
					M1_in1 <= cosine;
					cosin(6) <= cosine;
					--Input Storage
					lpf(6) <= lock_in_lpf;
					lock_input(6) <= lock_in_input_0;
					--Collecting Outputs
					A0_in0 <= M0o;
					A0_in1 <= M1o;
					
					--TEMPORARY DEBUG
					sin_mult <= m0o;
					cos_mult <= m1o;

				when 7 =>
					M0_in0 <= prev_out0(7);
					M0_in1 <= sine;
					sin(7) <= sine;
					M1_in0 <= prev_out1(7);
					M1_in1 <= cosine;
					cosin(7) <= cosine;
					--Input Storage
					lpf(7) <= lock_in_lpf;
					lock_input(7) <= lock_in_input_0;
					--Collecting Outputs
					A0_in0 <= M0o;
					A0_in1 <= M1o;
				when 8 =>
					--Collecting Outputs
					A0_in0 <= M0o;
					A0_in1 <= M1o;
					
				when 9 =>
					--Collecting Outputs
					A0_in0 <= M0o;
					A0_in1 <= M1o;
					
				when 10 =>
					--Collecting Outputs
					A0_in0 <= M0o;
					A0_in1 <= M1o;
					
				when 11 =>
					--Collecting Outputs
					A0_in0 <= M0o;
					A0_in1 <= M1o;
					
				when 12 =>
					--Collecting Outputs
					A0_in0 <= M0o;
					A0_in1 <= M1o;

				when 13 =>
					--Collecting Outputs
					A0_in0 <= M0o;
					A0_in1 <= M1o;

					
				when 14 =>
					--Setting Inputs
					S0_in0 <= lock_input(0);
					s0_in1 <= A0o;
				when 15 =>
					
					--Setting Inputs
					S0_in0 <= lock_input(1);
					s0_in1 <= A0o;
					
				when 16 =>
					
					--Setting Inputs
					S0_in0 <= lock_input(2);
					s0_in1 <= A0o;
					
				when 17 =>
					
					--Setting Inputs
					S0_in0 <= lock_input(3);
					s0_in1 <= A0o;
					
				when 18 =>
					--Setting Inputs
					S0_in0 <= lock_input(4);
					s0_in1 <= A0o;
					
				when 19 =>
					--Setting Inputs
					S0_in0 <= lock_input(5);
					s0_in1 <= A0o;
				when 20 =>
					
					--Setting Inputs
					S0_in0 <= lock_input(6);
					s0_in1 <= A0o;
					
					--TEMPORARY
					err <= S0o;
				when 21 =>
					
					--Setting Inputs
					S0_in0 <= lock_input(7);
					s0_in1 <= A0o;
				when 22 =>
					
					--Setting Outputs
					M0_in0 <= S0o;
					M0_in1 <= lpf(0);
				when 23 =>
					
					--Setting Outputs
					M0_in0 <= S0o;
					M0_in1 <= lpf(1);
					
				when 24 =>
					
					--Setting Outputs
					M0_in0 <= S0o;
					M0_in1 <= lpf(2);
				when 25 =>
					
					--Setting Outputs
					M0_in0 <= S0o;
					M0_in1 <= lpf(3);
				when 26 =>
					
					--Setting Outputs
					M0_in0 <= S0o;
					M0_in1 <= lpf(4);
					
				when 27 =>
					
					--Setting Outputs
					M0_in0 <= S0o;
					M0_in1 <= lpf(5);
					
				when 28 =>
					--Setting Outputs
					M0_in0 <= S0o;
					M0_in1 <= lpf(6);
					
					M1_in0 <= sin(0);
					M1_in1 <= M0o;
					M4_in0 <= cosin(0);
					M4_in1 <= M0o;
					
					--TEMPORARY Debug
					err_gamma <= M0o;
					
				when 29 =>
					--Setting Outputs
					M0_in0 <= S0o;
					M0_in1 <= lpf(7);
					
					--Setting Inputs
					M1_in0 <= sin(1);
					M1_in1 <= M0o;
					M4_in0 <= cosin(1);
					M4_in1 <= M0o;
					
				when 30 =>
					--Setting Inputs
					M1_in0 <= sin(2);
					M1_in1 <= M0o;
					M4_in0 <= cosin(2);
					M4_in1 <= M0o;
					
				when 31 =>
					--Setting Inputs
					M1_in0 <= sin(3);
					M1_in1 <= M0o;
					M4_in0 <= cosin(3);
					M4_in1 <= M0o;

				when 32 =>
					--Setting Inputs
					M1_in0 <= sin(4);
					M1_in1 <= M0o;
					M4_in0 <= cosin(4);
					M4_in1 <= M0o;
					
					
				when 33 =>
					--Setting Inputs
					M1_in0 <= sin(5);
					M1_in1 <= M0o;
					M4_in0 <= cosin(5);
					M4_in1 <= M0o;
					
				when 34 =>
					--Setting Inputs
					M1_in0 <= sin(6);
					M1_in1 <= M0o;
					M4_in0 <= cosin(6);
					M4_in1 <= M0o;
					--Saving intermediate calculation values
					err_lpf_sin(0) <= M1o;
					err_lpf_cos(0) <= M4o;
					
					--TEMP
					sin_find <= M1o;
					cos_find <= M4o;
					
				when 35 =>
					--Setting Inputs
					M1_in0 <= sin(7);
					M1_in1 <= M0o;
					M4_in0 <= cosin(7);
					M4_in1 <= M0o;
					
					err_lpf_sin(1) <= M1o;
					err_lpf_cos(1) <= M4o;
					
				when 36 =>
					err_lpf_sin(2) <= M1o;
					err_lpf_cos(2) <= M4o;
					
				when 37 =>
					err_lpf_sin(3) <= M1o;
					err_lpf_cos(3) <= M4o;
					
				when 38 =>
					err_lpf_sin(4) <= M1o;
					err_lpf_cos(4) <= M4o;
					
				when 39 =>
					err_lpf_sin(5) <= M1o;
					err_lpf_cos(5) <= M4o;
					
				when 40 =>
					err_lpf_sin(6) <= M1o;
					err_lpf_cos(6) <= M4o;
					
				when 41 =>
					err_lpf_sin(7) <= M1o;
					err_lpf_cos(7) <= M4o;
					
				when 42 =>
					--Sets the old output to be placed in the adder
					gout0(0) <= prev_out0(0);
					gout0(1) <= prev_out0(1);
					gout0(2) <= prev_out0(2);
					gout0(3) <= prev_out0(3);
					gout0(4) <= prev_out0(4);
					gout0(5) <= prev_out0(5);
					gout0(6) <= prev_out0(6);
					gout0(7) <= prev_out0(7);
					
					gout1(0) <= prev_out1(0);
					gout1(1) <= prev_out1(1);
					gout1(2) <= prev_out1(2);
					gout1(3) <= prev_out1(3);
					gout1(4) <= prev_out1(4);
					gout1(5) <= prev_out1(5);
					gout1(6) <= prev_out1(6);
					gout1(7) <= prev_out1(7);
					
				when 52 =>
					
					--Assigning outputs all at the same time
					--Note this clocking is arbitrary, just has to be >6 cycles after the previous cycle is set
					outD0(0) <= out0(0);
					outD0(1) <= out0(1);
					outD0(2) <= out0(2);
					outD0(3) <= out0(3);
					outD0(4) <= out0(4);
					outD0(5) <= out0(5);
					outD0(6) <= out0(6);
					outD0(7) <= out0(7);
					
					outD1(0) <= out1(0);
					outD1(1) <= out1(1);
					outD1(2) <= out1(2);
					outD1(3) <= out1(3);
					outD1(4) <= out1(4);
					outD1(5) <= out1(5);
					outD1(6) <= out1(6);
					outD1(7) <= out1(7);
					
					--Assigning output back to original
					prev_out0(0) <= out0(0);
					prev_out0(1) <= out0(1);
					prev_out0(2) <= out0(2);
					prev_out0(3) <= out0(3);
					prev_out0(4) <= out0(4);
					prev_out0(5) <= out0(5);
					prev_out0(6) <= out0(6);
					prev_out0(7) <= out0(7);
					
					prev_out1(0) <= out1(0);
					prev_out1(1) <= out1(1);
					prev_out1(2) <= out1(2);
					prev_out1(3) <= out1(3);
					prev_out1(4) <= out1(4);
					prev_out1(5) <= out1(5);
					prev_out1(6) <= out1(6);
					prev_out1(7) <= out1(7);
					
				when others =>
					null;
			end case;
		end if;
	end process;
	
	
	--========================================================================
	--Memory Support Processes
	--========================================================================
	
	
	
end architecture rtl;