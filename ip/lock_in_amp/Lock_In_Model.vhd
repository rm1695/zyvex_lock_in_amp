----------------------------------------------------------------------------
-- Date:    2019-09-01
-- File:    Lock_In_Model.vhd
-- By:      Robert Mason
--
-- Desc:    Memory Module
--
-- Notes:   
--
-- Issues:
--
--
------------------------------------------------------------------------------
--
--Revisions:
--
-- 20190901 - Robert Mason
--   Initial creation
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Lock_In_Model is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		const_write_data		  : in std_logic                      := '0';
		const_data_addr		  : in std_logic_vector(31 downto 0)  := (others => '0');
		const_data_val			  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		read_addr              : in integer								  := 0;
		read_pt					  : in std_logic							  := '0';
		read_ready				  : out std_logic							  := '0';
		
		sin0						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos0						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin1						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos1						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin2						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos2						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin3						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos3						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin4						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos4						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin5						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos5						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin6						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos6						  : out std_logic_vector(63 downto 0) := (others => '0');
		sin7						  : out std_logic_vector(63 downto 0) := (others => '0');
		cos7						  : out std_logic_vector(63 downto 0) := (others => '0')
	);
end entity Lock_In_Model;

architecture rtl of Lock_In_Model is

   --========================================================================
   -- Constant Definitions
   --========================================================================

	
	--Constants
	constant INT32_T_SIZE					: natural 								:= 32;
	
	constant num_cycles						: integer								:= 6;
	constant num_cycles_output				: integer								:= 5;
	
	type delay is array (0 to 1) of std_logic_vector(32 downto 0);

	--========================================================================
	--Aliases
	--========================================================================

	---------
	--Components
	---------
	component float_to_double
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
	component memory_2000_vals
	PORT
	(
		clock		: IN STD_LOGIC  := '1';
		data		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		rdaddress		: IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		wraddress		: IN STD_LOGIC_VECTOR (13 DOWNTO 0);
		wren		: IN STD_LOGIC  := '0';
		q		: OUT STD_LOGIC_VECTOR (255 DOWNTO 0)
	);
	END component;

	---------
	--General
	---------
	alias clock is clock_sink_clk;
	alias reset is reset_sink_reset;
	
	
   --========================================================================
   -- Signal Declarations
   --========================================================================
	
	--Temporary Input Signals
	signal input				: delay									:= (others => (others => '0'));
	signal internal_write	: std_logic								:= '0';
	signal internal_addr		: std_logic_vector(31 downto 0)	:= (others => '0');
	
	--Signals for memory blocks
	signal write01          : std_logic								:= '0';
	signal write23          : std_logic								:= '0';
	signal write45          : std_logic								:= '0';
	signal write67          : std_logic								:= '0';
	
	signal mem_01_out			: std_logic_vector(255 downto 0)	:= (others => '0');
	signal mem_23_out			: std_logic_vector(255 downto 0)	:= (others => '0');
	signal mem_45_out			: std_logic_vector(255 downto 0)	:= (others => '0');
	signal mem_67_out			: std_logic_vector(255 downto 0)	:= (others => '0');
	
	signal wr_addr_01			: std_logic_Vector(13 downto 0)	:= (others => '0');
	signal wr_addr_23			: std_logic_Vector(13 downto 0)	:= (others => '0');
	signal wr_addr_45			: std_logic_Vector(13 downto 0)	:= (others => '0');
	signal wr_addr_67			: std_logic_Vector(13 downto 0)	:= (others => '0');
	
	signal rd_addr_01			: std_logic_vector(11 downto 0)	:= (others => '0');
	signal rd_addr_23			: std_logic_vector(11 downto 0)	:= (others => '0');
	signal rd_addr_45			: std_logic_vector(11 downto 0)	:= (others => '0');
	signal rd_addr_67			: std_logic_vector(11 downto 0)	:= (others => '0');
	
	signal current_cycle		: integer								:= 10;
	
	signal conv_proc : integer := 6;
	
	--========================================================================
   --Processes
   --========================================================================
begin
	
	
	--Breaking output up into segments
	sin0 <= mem_01_out(63 downto 0);
	cos0 <= mem_01_out(127 downto 64);
	sin1 <= mem_01_out(191 downto 128);
	cos1 <= mem_01_out(255 downto 192);
	sin2 <= mem_23_out(63 downto 0);
	cos2 <= mem_23_out(127 downto 64);
	sin3 <= mem_23_out(191 downto 128);
	cos3 <= mem_23_out(255 downto 192);
	sin4 <= mem_45_out(63 downto 0);
	cos4 <= mem_45_out(127 downto 64);
	sin5 <= mem_45_out(191 downto 128);
	cos5 <= mem_45_out(255 downto 192);
	sin6 <= mem_67_out(63 downto 0);
	cos6 <= mem_67_out(127 downto 64);
	sin7 <= mem_67_out(191 downto 128);
	cos7 <= mem_67_out(255 downto 192);
	
	rd_addr_01 <= std_logic_vector(to_unsigned(read_addr,12));
	rd_addr_23 <= std_logic_vector(to_unsigned(read_addr,12));
	rd_addr_45 <= std_logic_vector(to_unsigned(read_addr,12));
	rd_addr_67 <= std_logic_vector(to_unsigned(read_addr,12));
	
	--========================================================================
	--Memory Support Processes
	--========================================================================
	
	ch_01 : memory_2000_vals
	PORT MAP
	(
		clock		=> clock,
		data		=> const_data_val,
		rdaddress		=> rd_addr_01,
		wraddress		=> wr_addr_01,
		wren	=> write01,
		q		=> mem_01_out
	);
	ch_23 : memory_2000_vals
	PORT MAP
	(
		clock		=> clock,
		data		=> const_data_val,
		rdaddress		=> rd_addr_23,
		wraddress		=> wr_addr_23,
		wren	=> write23,
		q		=> mem_23_out
	);
	ch_45 : memory_2000_vals
	PORT MAP
	(
		clock		=> clock,
		data		=> const_data_val,
		rdaddress		=> rd_addr_45,
		wraddress		=> wr_addr_45,
		wren	=> write45,
		q		=> mem_45_out
	);
	ch_67 : memory_2000_vals
	PORT MAP
	(
		clock		=> clock,
		data		=> const_data_val,
		rdaddress		=> rd_addr_67,
		wraddress		=> wr_addr_67,
		wren	=> write67,
		q		=> mem_67_out
	);
	
	
	--Process for delay timings
	--This is copied from Lock_in_in_converter
	timing_proc : process (clock, reset)
	begin
		if reset = '1' then
			internal_addr <= (others => '0');
		elsif rising_edge(clock) then
			internal_addr <= const_data_addr;
		end if;
	end process;
	
	timing_proc_2 : process (clock, reset)
	begin
		if reset = '1' then
			conv_proc <= 6;
		elsif rising_edge(clock) then
			if const_write_data = '1' then
				conv_proc <= 0;
			elsif conv_proc < 6 then
				conv_proc <= conv_proc + 1;
			end if;
		end if;
	end process;
	
	wr_proc : process (clock, reset)
	begin
		if reset = '1' then
			internal_write <= '0';
		elsif rising_edge(clock) then
			if conv_proc = 6 then
				internal_write <= '1';
			else
				internal_write <= '0';
			end if;
		end if;
	end process;
	
	--Handling writing distribution
	write_distrib_proc : process (clock, reset)
	begin
		if reset = '1' then
			write01 <= '0';
			write23 <= '0';
			write45 <= '0';
			write67 <= '0';
		elsif rising_edge(clock) then
			if internal_write = '1' then
				case internal_addr(3 downto 0) is
					when "0000" =>
						write01 <= '1';
						write23 <= '0';
						write45 <= '0';
						write67 <= '0';
						wr_addr_01 <= internal_addr(15 downto 4) & "00";
					when "0001" =>
						write01 <= '1';
						write23 <= '0';
						write45 <= '0';
						write67 <= '0';
						wr_addr_01 <= internal_addr(15 downto 4) & "01";
					when "0010" =>
						write01 <= '1';
						write23 <= '0';
						write45 <= '0';
						write67 <= '0';
						wr_addr_01 <= internal_addr(15 downto 4) & "10";
					when "0011" =>
						write01 <= '1';
						write23 <= '0';
						write45 <= '0';
						write67 <= '0';
						wr_addr_01 <= internal_addr(15 downto 4) & "11";
					when "0100" =>
						write01 <= '0';
						write23 <= '1';
						write45 <= '0';
						write67 <= '0';
						wr_addr_23 <= internal_addr(15 downto 4) & "00";
					when "0101" =>
						write01 <= '0';
						write23 <= '1';
						write45 <= '0';
						write67 <= '0';
						wr_addr_23 <= internal_addr(15 downto 4) & "01";
					when "0110" =>
						write01 <= '0';
						write23 <= '1';
						write45 <= '0';
						write67 <= '0';
						wr_addr_23 <= internal_addr(15 downto 4) & "10";
					when "0111" =>
						write01 <= '0';
						write23 <= '1';
						write45 <= '0';
						write67 <= '0';
						wr_addr_23 <= internal_addr(15 downto 4) & "11";
					when "1000" =>
						write01 <= '0';
						write23 <= '0';
						write45 <= '1';
						write67 <= '0';
						wr_addr_45 <= internal_addr(15 downto 4) & "00";
					when "1001" =>
						write01 <= '0';
						write23 <= '0';
						write45 <= '1';
						write67 <= '0';
						wr_addr_45 <= internal_addr(15 downto 4) & "01";
					when "1010" =>
						write01 <= '0';
						write23 <= '0';
						write45 <= '1';
						write67 <= '0';
						wr_addr_45 <= internal_addr(15 downto 4) & "10";
					when "1011" =>
						write01 <= '0';
						write23 <= '0';
						write45 <= '1';
						write67 <= '0';
						wr_addr_45 <= internal_addr(15 downto 4) & "11";
					when "1100" =>
						write01 <= '0';
						write23 <= '0';
						write45 <= '0';
						write67 <= '1';
						wr_addr_67 <= internal_addr(15 downto 4) & "00";
					when "1101" =>
						write01 <= '0';
						write23 <= '0';
						write45 <= '0';
						write67 <= '1';
						wr_addr_67 <= internal_addr(15 downto 4) & "01";
					when "1110" =>
						write01 <= '0';
						write23 <= '0';
						write45 <= '0';
						write67 <= '1';
						wr_addr_67 <= internal_addr(15 downto 4) & "10";
					when "1111" =>
						write01 <= '0';
						write23 <= '0';
						write45 <= '0';
						write67 <= '1';
						wr_addr_67 <= internal_addr(15 downto 4) & "11";
					when others =>
						null;
				end case;
			else
				write01 <= '0';
				write23 <= '0';
				write45 <= '0';
				write67 <= '0';
			end if;
		end if;
	end process;
	
	--Output counter process
	counter_proc : process (clock,reset)
	begin
		if reset = '1' then
			current_cycle <= num_cycles;
		elsif rising_edge(clock) then
			if current_cycle < num_cycles and current_cycle > 0 then
				current_cycle <= current_cycle + 1;
			elsif current_cycle = 0 and read_pt = '1' then
				current_cycle <= 1;
                        elsif current_cycle = num_cycles then
				current_cycle <= 0;
			end if;
		end if;
	end process;
	
	--Output display cycle
	output_ready_proc : process (clock,reset)
	begin
		if reset = '1' then
			read_ready <= '0';
		elsif rising_edge(clock) then
			if current_cycle = num_cycles_output then
				read_ready <= '1';
			else
				read_ready <= '0';
			end if;
		end if;
	end process;
	
end architecture rtl;