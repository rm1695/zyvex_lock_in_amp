----------------------------------------------------------------------------
-- Date:    2019-08-13
-- File:    Lock_in_out_converter.vhd
-- By:      Robert Mason
--
-- Desc:    Lock in Amplifier converter module
--
-- Notes:   
--
--  Used to change the output back into an integer value
--
--
-- Issues:
--
--
------------------------------------------------------------------------------
--
--Revisions:
--
-- 20190813 - Robert Mason
--   Initial creation
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Lock_in_out_converter is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		input_data				  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		output_data_0			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_2			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_3			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_4			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_5			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_6			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_7			  : out std_logic_vector(31 downto 0) := (others => '0');
		
		output_data_1_0		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_1		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_2		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_3		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_4		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_5		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_6		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_7		  : out std_logic_vector(31 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);
end entity Lock_in_out_converter;

architecture rtl of Lock_in_out_converter is

	--========================================================================
	--Aliases
	--========================================================================
	
	constant num_cycles : integer := 22;
	constant num_cycles_output : integer := 21;
	
	---------
	--General
	---------
	alias clock is clock_sink_clk;
	alias reset is reset_sink_reset;
	
   --========================================================================
   -- Register Declarations
   --========================================================================
	
	--=================
	--Support Registers
	--=================
	signal convert_out        : std_logic_vector(31 downto 0) := (others => '0');
	
	--Has to be handled separately like this because of a VHDL/Quartus quirk
	
	--Data 0
	signal data0		        : std_logic_vector(31 downto 0) := (others => '0');
	signal data1	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data2	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data3	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data4	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data5	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data6	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data7	           : std_logic_vector(31 downto 0) := (others => '0');
	
	--Data 1
	signal data10		        : std_logic_vector(31 downto 0) := (others => '0');
	signal data11	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data12	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data13	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data14	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data15	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data16	           : std_logic_vector(31 downto 0) := (others => '0');
	signal data17	           : std_logic_vector(31 downto 0) := (others => '0');
	
   --========================================================================
   -- Signal Declarations
   --========================================================================
	
	signal current_cycle	: integer := 22;
	
	--Outputs of components

	
   --========================================================================
   -- Component Definitions
   --========================================================================
	component double_to_float
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
	end component;

	
	--========================================================================
   --Processes
   --========================================================================
begin
	
	--Data mapping
	output_data_0 <= data0;
	output_data_1 <= data1;
	output_data_2 <= data2;
	output_data_3 <= data3;
	output_data_4 <= data4;
	output_data_5 <= data5;
	output_data_6 <= data6;
	output_data_7 <= data7;
	output_data_1_0 <= data10;
	output_data_1_1 <= data11;
	output_data_1_2 <= data12;
	output_data_1_3 <= data13;
	output_data_1_4 <= data14;
	output_data_1_5 <= data15;
	output_data_1_6 <= data16;
	output_data_1_7 <= data17;
	
	--========================================================================
   --Component Instantiation
	--========================================================================
	double_to_int_inst : double_to_float PORT MAP (
		clock	 => clock,
		dataa	 => input_data,
		result	 => convert_out
	);
	
	--========================================================================
	--Control Logic
	--========================================================================
	
	counter_proc : process (clock,reset)
	begin
		if reset = '1' then
			current_cycle <= num_cycles;
		elsif rising_edge(clock) then
			if current_cycle < num_cycles and current_cycle > 0 then
				current_cycle <= current_cycle + 1;
			elsif current_cycle = 0 and start_calc = '1' then
				current_cycle <= 1;
         elsif current_cycle = num_cycles then
				current_cycle <= 0;
			end if;
		end if;
	end process;
	
	--Output display cycle
	output_ready_proc : process (clock,reset)
	begin
		if reset = '1' then
			output_ready <= '0';
		elsif rising_edge(clock) then
			if current_cycle = num_cycles_output then
				output_ready <= '1';
			else
				output_ready <= '0';
			end if;
		end if;
	end process;
	
	--========================================================================
	--Data Pipelining Support
	--========================================================================
	
	data_out_proc : process (clock,reset)
	begin
		if reset = '1' then
			null;
		elsif rising_edge(clock) then
			case current_cycle is
				when 6 =>
					data0 <= convert_out;
				when 7 =>
					data1 <= convert_out;
				when 8 =>
					data2 <= convert_out;
				when 9 =>
					data3 <= convert_out;
				when 10 =>
					data4 <= convert_out;
				when 11 =>
					data5 <= convert_out;
				when 12 =>
					data6 <= convert_out;
				when 13 =>
					data7 <= convert_out;
				when 14 =>
					data10 <= convert_out;
				when 15 =>
					data11 <= convert_out;
				when 16 =>
					data12 <= convert_out;
				when 17 =>
					data13 <= convert_out;
				when 18 =>
					data14 <= convert_out;
				when 19 =>
					data15 <= convert_out;
				when 20 =>
					data16 <= convert_out;
				when 21 =>
					data17 <= convert_out;
				when others =>
					null;
			end case;
		end if;
	end process;
	
	
	
end architecture rtl;