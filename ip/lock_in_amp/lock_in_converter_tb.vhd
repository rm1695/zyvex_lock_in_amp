LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY lock_in_converter_tb IS 
END lock_in_converter_tb;

ARCHITECTURE behavior OF lock_in_converter_tb IS

   component Lock_in_out_converter is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		input_data				  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		output_data_0			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_2			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_3			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_4			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_5			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_6			  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_7			  : out std_logic_vector(31 downto 0) := (others => '0');
		
		output_data_1_0		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_1		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_2		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_3		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_4		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_5		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_6		  : out std_logic_vector(31 downto 0) := (others => '0');
		output_data_1_7		  : out std_logic_vector(31 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);
	end component;

   signal clk : std_logic := '0';
   constant clk_period : time := 1.176 ns;
	signal outR : std_logic := '0';
	signal strt : std_logic := '0';
	signal rst : std_logic := '0';
	signal inn : std_logic_vector(63 downto 0) := (others => '0');
	signal out0 : std_logic_vector(31 downto 0) := (others => '0');
	signal out1 : std_logic_vector(31 downto 0) := (others => '0');
	signal out2 : std_logic_vector(31 downto 0) := (others => '0');
	signal out3 : std_logic_vector(31 downto 0) := (others => '0');
	signal out4 : std_logic_vector(31 downto 0) := (others => '0');
	signal out5 : std_logic_vector(31 downto 0) := (others => '0');
	signal out6 : std_logic_vector(31 downto 0) := (others => '0');
	signal out7 : std_logic_vector(31 downto 0) := (others => '0');
	
	signal out10 : std_logic_vector(31 downto 0) := (others => '0');
	signal out11 : std_logic_vector(31 downto 0) := (others => '0');
	signal out12 : std_logic_vector(31 downto 0) := (others => '0');
	signal out13 : std_logic_vector(31 downto 0) := (others => '0');
	signal out14 : std_logic_vector(31 downto 0) := (others => '0');
	signal out15 : std_logic_vector(31 downto 0) := (others => '0');
	signal out16 : std_logic_vector(31 downto 0) := (others => '0');
	signal out17 : std_logic_vector(31 downto 0) := (others => '0');

BEGIN

   uut: Lock_in_out_converter
	port map (
		clock_sink_clk         => clk,
		reset_sink_reset       => rst,

		start_calc     		  => strt,
		input_data				  => inn,
		
		output_data_0			  => out0,
		output_data_1			  => out1,
		output_data_2			  => out2,
		output_data_3			  => out3,
		output_data_4			  => out4,
		output_data_5			  => out5,
		output_data_6			  => out6,
		output_data_7			  => out7,
		
		output_data_1_0		  => out10,
		output_data_1_1		  => out11,
		output_data_1_2		  => out12,
		output_data_1_3		  => out13,
		output_data_1_4		  => out14,
		output_data_1_5		  => out15,
		output_data_1_6		  => out16,
		output_data_1_7		  => out17,
		
		output_ready			  => outR
	);       

   -- Clock process definitions( clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;  --for 0.5 ns signal is '0'.
        clk <= '1';
        wait for clk_period/2;  --for next 0.5 ns signal is '1'.
   end process;
	
	test_proc : process
	begin
		wait for clk_period * 5;
		wait for clk_period * 0.5;
		strt <= '1';
		inn <= x"3ff0000000000000";
		wait for clk_period;
		inn <= x"4000000000000000";
		strt <= '0';
		wait for clk_period;
		inn <= x"4008000000000000";
		wait for clk_period;
		inn <= x"4010000000000000";
		wait for clk_period;
		inn <= x"4014000000000000";
		wait for clk_period;
		inn <= x"4018000000000000";
		wait for clk_period;
		inn <= x"401c000000000000";
		wait for clk_period;
		inn <= x"4020000000000000";
		wait for clk_period * 100;
	end process;

END;