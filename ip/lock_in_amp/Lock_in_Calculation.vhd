----------------------------------------------------------------------------
-- Date:    2019-07-21
-- File:    Lock_in_out_converter.vhd
-- By:      Robert Mason
--
-- Desc:    Lock in Amplifier Calculation Module
--
-- Notes:   
--
--  Serial lock_in output calculations
--
--
-- Issues:
--
--
------------------------------------------------------------------------------
--
--Revisions:
--
-- 20190721 - Robert Mason
--   Initial creation
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Lock_in_Calculation is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		
		input_val				  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_bandpass_a_0	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_1	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_2	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_3	  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_bandpass_b_0	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_1	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_2	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_3	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_4	  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_input_val		  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_0_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_1_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_2_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_3_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bp_b_0_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_1_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_2_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_3_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_4_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		
		--Stage 2 Addition out
		s2Aout0					  : out std_logic_vector(63 downto 0) := (others => '0');
		s2Aout1					  : out std_logic_vector(63 downto 0) := (others => '0');
		s2Aout2                : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout0					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout1					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout2					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout3					  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout10					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout11					  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout20					  : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in0d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in1d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in2d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in3d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in4d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in5d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in6d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in7d           : out std_logic_vector(63 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);
end entity Lock_in_Calculation;

architecture rtl of Lock_in_Calculation is

	--========================================================================
	--Aliases
	--========================================================================
	
	constant num_cycles : integer := 51;
	constant num_cycles_output : integer := 43;
	
	---------
	--General
	---------
	alias clock is clock_sink_clk;
	alias reset is reset_sink_reset;
	
	type buf_5_array is array (0 to 4) of std_logic_vector(63 downto 0);
	type buf_4_array is array (0 to 3) of std_logic_vector(63 downto 0);
	type buf_5_8_array is array (0 to 7) of buf_5_array;
	type buf_4_8_array is array (0 to 7) of buf_4_array;
	type buf_8_array is array (0 to 7) of std_logic_vector(63 downto 0);
	
   --========================================================================
   -- Register Declarations
   --========================================================================
	
	--=================
	--Support Registers
	--=================
	
	
   --========================================================================
   -- Signal Declarations
   --========================================================================
	
	signal current_cycle	: integer := 57;
	
	--Component Inputs
	signal m0_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal m0_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal m1_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal m1_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal m2_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal m2_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal m3_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal m3_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal m4_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal m4_1 : std_logic_vector(63 downto 0) := (others => '0');
	
	signal a0_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal a0_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal a1_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal a1_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal a2_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal a2_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal a3_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal a3_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal a4_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal a4_1 : std_logic_vector(63 downto 0) := (others => '0');
	
	--Outputs of components
	signal m0_out : std_logic_vector(63 downto 0) := (others => '0');
	signal m1_out : std_logic_vector(63 downto 0) := (others => '0');
	signal m2_out : std_logic_vector(63 downto 0) := (others => '0');
	signal m3_out : std_logic_vector(63 downto 0) := (others => '0');
	signal m4_out : std_logic_vector(63 downto 0) := (others => '0');
	
	signal a0_out : std_logic_vector(63 downto 0) := (others => '0');
	signal a1_out : std_logic_vector(63 downto 0) := (others => '0');
	signal a2_out : std_logic_vector(63 downto 0) := (others => '0');
	signal a3_out : std_logic_vector(63 downto 0) := (others => '0');
	signal a4_out : std_logic_vector(63 downto 0) := (others => '0');
	
	signal s0_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal s0_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal s0_out : std_logic_vector(63 downto 0) := (others => '0');
	
	--signal lock_in_signal: buf_5_8_array	:= (others => (others => (others => '0')));
	signal lock_in_signal_0 : buf_8_array := (others => (others => '0'));
	signal lock_in_signal_1 : buf_8_array := (others => (others => '0'));
	signal lock_in_signal_2 : buf_8_array := (others => (others => '0'));
	signal lock_in_signal_3 : buf_8_array := (others => (others => '0'));
	signal lock_in_signal_4 : buf_8_array := (others => (others => '0'));
	--signal lock_in_input : buf_5_8_array	:= (others => (others => (others => '0')));
	signal lock_in_input_0 : buf_8_array := (others => (others => '0'));
	signal lock_in_input_1 : buf_8_array := (others => (others => '0'));
	signal lock_in_input_2 : buf_8_array := (others => (others => '0'));
	signal lock_in_input_3 : buf_8_array := (others => (others => '0'));
	signal lock_in_input_4 : buf_8_array := (others => (others => '0'));
	
	signal temp : buf_8_array := (others => (others => '0'));
	signal lock_in_b_temp : buf_8_array := (others => (others => '0'));
	
	signal bandA_0 : buf_8_array := (others => (others => '0'));
	signal bandA_1 : buf_8_array := (others => (others => '0'));
	signal bandA_2 : buf_8_array := (others => (others => '0'));
	signal bandA_3 : buf_8_array := (others => (others => '0'));
	
	signal bandB_0 : buf_8_array := (others => (others => '0'));
	signal bandB_1 : buf_8_array := (others => (others => '0'));
	signal bandB_2 : buf_8_array := (others => (others => '0'));
	signal bandB_3 : buf_8_array := (others => (others => '0'));
	signal bandB_4 : buf_8_array := (others => (others => '0'));
	
   --========================================================================
   -- Component Definitions
   --========================================================================
	component double_adder
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
	component double_mult
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
	component double_subtractor
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;

	
	--========================================================================
   --Processes
   --========================================================================
begin
	
	--========================================================================
   --Component Instantiation
	--========================================================================
	m0 : double_mult
	PORT MAP
	(
		clock		=> clock,
		dataa		=> m0_0,
		datab		=> m0_1,
		result	=> m0_out
	);
	
	m1 : double_mult
	PORT MAP
	(
		clock		=> clock,
		dataa		=> m1_0,
		datab		=> m1_1,
		result	=> m1_out
	);
	
	m2 : double_mult
	PORT MAP
	(
		clock		=> clock,
		dataa		=> m2_0,
		datab		=> m2_1,
		result	=> m2_out
	);
	
	m3 : double_mult
	PORT MAP
	(
		clock		=> clock,
		dataa		=> m3_0,
		datab		=> m3_1,
		result	=> m3_out
	);
	
	m4 : double_mult
	PORT MAP
	(
		clock		=> clock,
		dataa		=> m4_0,
		datab		=> m4_1,
		result	=> m4_out
	);
	
	a0 : double_adder
	PORT MAP
	(
		clock		=> clock,
		dataa		=> a0_0,
		datab		=> a0_1,
		result	=> a0_out
	);
	
	a1 : double_adder
	PORT MAP
	(
		clock		=> clock,
		dataa		=> a1_0,
		datab		=> a1_1,
		result	=> a1_out
	);
	
	a2 : double_adder
	PORT MAP
	(
		clock		=> clock,
		dataa		=> a2_0,
		datab		=> a2_1,
		result	=> a2_out
	);
	
	a3 : double_adder
	PORT MAP
	(
		clock		=> clock,
		dataa		=> a3_0,
		datab		=> a3_1,
		result	=> a3_out
	);
	
	a4 : double_adder
	PORT MAP
	(
		clock		=> clock,
		dataa		=> a4_0,
		datab		=> a4_1,
		result	=> a4_out
	);
	
	s0 : double_subtractor
	PORT MAP
	(
		clock		=> clock,
		dataa		=> s0_0,
		datab		=> s0_1,
		result	=> s0_out
	);
	
	--========================================================================
	--Control Logic
	--========================================================================
	
	counter_proc : process (clock,reset)
	begin
		if reset = '1' then
			current_cycle <= num_cycles;
		elsif rising_edge(clock) then
			if current_cycle < num_cycles and current_cycle > 0 then
				current_cycle <= current_cycle + 1;
			elsif current_cycle = 0 and start_calc = '1' then
				current_cycle <= 1;
         elsif current_cycle >= num_cycles then
				current_cycle <= 0;
			end if;
		end if;
	end process;
	
	--Output display cycle
	output_ready_proc : process (clock,reset)
	begin
		if reset = '1' then
			output_ready <= '0';
		elsif rising_edge(clock) then
			if current_cycle = num_cycles_output then
				output_ready <= '1';
			else
				output_ready <= '0';
			end if;
		end if;
	end process;
	
	data_pipline_proc : process(clock,reset)
	begin
		if reset = '1' then
			null;
		elsif rising_edge(clock) then
			case current_cycle is 
				when 0 =>
					--Taking in input and shifting current buffers
					--Holding input until shifted
					temp(0) <= input_val;
					--ch:0
					lock_in_signal_4(0) <= lock_in_signal_3(0);
					lock_in_input_4(0) <= lock_in_input_3(0);
					bandA_0(0) <= lock_in_bandpass_a_0;
					bandA_1(0) <= lock_in_bandpass_a_1;
					bandA_2(0) <= lock_in_bandpass_a_2;
					bandA_3(0) <= lock_in_bandpass_a_3;
					
					bandB_0(0) <= lock_in_bandpass_b_0;
					bandB_1(0) <= lock_in_bandpass_b_1;
					bandB_2(0) <= lock_in_bandpass_b_2;
					bandB_3(0) <= lock_in_bandpass_b_3;
					bandB_4(0) <= lock_in_bandpass_b_4;
				when 1 =>
					temp(1) <= input_val;
					--ch:0
					lock_in_signal_3(0) <= lock_in_signal_2(0);
					lock_in_input_3(0) <= lock_in_input_2(0);
					--ch:1
					lock_in_signal_4(1) <= lock_in_signal_3(1);
					lock_in_input_4(1) <= lock_in_input_3(1);
					bandA_0(1) <= lock_in_bandpass_a_0;
					bandA_1(1) <= lock_in_bandpass_a_1;
					bandA_2(1) <= lock_in_bandpass_a_2;
					bandA_3(1) <= lock_in_bandpass_a_3;
					
					bandB_0(1) <= lock_in_bandpass_b_0;
					bandB_1(1) <= lock_in_bandpass_b_1;
					bandB_2(1) <= lock_in_bandpass_b_2;
					bandB_3(1) <= lock_in_bandpass_b_3;
					bandB_4(1) <= lock_in_bandpass_b_4;
				when 2 =>
					temp(2) <= input_val;
					--ch:0
					lock_in_signal_2(0) <= lock_in_signal_1(0);
					lock_in_input_2(0) <= lock_in_input_1(0);
					--ch:1
					lock_in_signal_3(1) <= lock_in_signal_2(1);
					lock_in_input_3(1) <= lock_in_input_2(1);
					--ch:2
					lock_in_signal_4(2) <= lock_in_signal_3(2);
					lock_in_input_4(2) <= lock_in_input_3(2);
					bandA_0(2) <= lock_in_bandpass_a_0;
					bandA_1(2) <= lock_in_bandpass_a_1;
					bandA_2(2) <= lock_in_bandpass_a_2;
					bandA_3(2) <= lock_in_bandpass_a_3;
					
					bandB_0(2) <= lock_in_bandpass_b_0;
					bandB_1(2) <= lock_in_bandpass_b_1;
					bandB_2(2) <= lock_in_bandpass_b_2;
					bandB_3(2) <= lock_in_bandpass_b_3;
					bandB_4(2) <= lock_in_bandpass_b_4;
				when 3 =>
					temp(3) <= input_val;
					--ch:0
					lock_in_signal_1(0) <= lock_in_signal_0(0);
					lock_in_input_1(0) <= lock_in_input_0(0);
					--ch:1
					lock_in_signal_2(1) <= lock_in_signal_1(1);
					lock_in_input_2(1) <= lock_in_input_1(1);
					--ch:2
					lock_in_signal_3(2) <= lock_in_signal_2(2);
					lock_in_input_3(2) <= lock_in_input_2(2);
					--ch:3
					lock_in_signal_4(3) <= lock_in_signal_3(3);
					lock_in_input_4(3) <= lock_in_input_3(3);
					bandA_0(3) <= lock_in_bandpass_a_0;
					bandA_1(3) <= lock_in_bandpass_a_1;
					bandA_2(3) <= lock_in_bandpass_a_2;
					bandA_3(3) <= lock_in_bandpass_a_3;
					
					bandB_0(3) <= lock_in_bandpass_b_0;
					bandB_1(3) <= lock_in_bandpass_b_1;
					bandB_2(3) <= lock_in_bandpass_b_2;
					bandB_3(3) <= lock_in_bandpass_b_3;
					bandB_4(3) <= lock_in_bandpass_b_4;
				when 4 =>
					temp(4) <= input_val;
					--ch:0
					lock_in_signal_0(0) <= temp(0);
					lock_in_input_0(0) <= (others => '0');
					--ch:1
					lock_in_signal_1(1) <= lock_in_signal_0(1);
					lock_in_input_1(1) <= lock_in_input_0(1);
					--ch:2
					lock_in_signal_2(2) <= lock_in_signal_1(2);
					lock_in_input_2(2) <= lock_in_input_1(2);
					--ch:3
					lock_in_signal_3(3) <= lock_in_signal_2(3);
					lock_in_input_3(3) <= lock_in_input_2(3);
					--ch:4
					lock_in_signal_4(4) <= lock_in_signal_3(4);
					lock_in_input_4(4) <= lock_in_input_3(4);
					bandA_0(4) <= lock_in_bandpass_a_0;
					bandA_1(4) <= lock_in_bandpass_a_1;
					bandA_2(4) <= lock_in_bandpass_a_2;
					bandA_3(4) <= lock_in_bandpass_a_3;
					
					bandB_0(4) <= lock_in_bandpass_b_0;
					bandB_1(4) <= lock_in_bandpass_b_1;
					bandB_2(4) <= lock_in_bandpass_b_2;
					bandB_3(4) <= lock_in_bandpass_b_3;
					bandB_4(4) <= lock_in_bandpass_b_4;
				when 5 =>
					temp(5) <= input_val;
					--ch:1
					lock_in_signal_0(1) <= temp(1);
					lock_in_input_0(1) <= (others => '0');
					--ch:2
					lock_in_signal_1(2) <= lock_in_signal_0(2);
					lock_in_input_1(2) <= lock_in_input_0(2);
					--ch:3
					lock_in_signal_2(3) <= lock_in_signal_1(3);
					lock_in_input_2(3) <= lock_in_input_1(3);
					--ch:4
					lock_in_signal_3(4) <= lock_in_signal_2(4);
					lock_in_input_3(4) <= lock_in_input_2(4);
					--ch:5
					lock_in_signal_4(5) <= lock_in_signal_3(5);
					lock_in_input_4(5) <= lock_in_input_3(5);
					
					--Starting ch:0 Calculations for band_pass_b
					m0_0 <= lock_in_signal_0(0);
					m0_1 <= bandB_0(0);
					m1_0 <= lock_in_signal_1(0);
					m1_1 <= bandB_1(0);
					m2_0 <= lock_in_signal_2(0);
					m2_1 <= bandB_2(0);
					m3_0 <= lock_in_signal_3(0);
					m3_1 <= bandB_3(0);
					m4_0 <= lock_in_signal_4(0);
					m4_1 <= bandB_4(0);
					
					bandA_0(5) <= lock_in_bandpass_a_0;
					bandA_1(5) <= lock_in_bandpass_a_1;
					bandA_2(5) <= lock_in_bandpass_a_2;
					bandA_3(5) <= lock_in_bandpass_a_3;
					
					bandB_0(5) <= lock_in_bandpass_b_0;
					bandB_1(5) <= lock_in_bandpass_b_1;
					bandB_2(5) <= lock_in_bandpass_b_2;
					bandB_3(5) <= lock_in_bandpass_b_3;
					bandB_4(5) <= lock_in_bandpass_b_4;
				when 6 =>
					temp(6) <= input_val;
					--ch:2
					lock_in_signal_0(2) <= temp(2);
					lock_in_input_0(2) <= (others => '0');
					--ch:3
					lock_in_signal_1(3) <= lock_in_signal_0(3);
					lock_in_input_1(3) <= lock_in_input_0(3);
					--ch:4
					lock_in_signal_2(4) <= lock_in_signal_1(4);
					lock_in_input_2(4) <= lock_in_input_1(4);
					--ch:5
					lock_in_signal_3(5) <= lock_in_signal_2(5);
					lock_in_input_3(5) <= lock_in_input_2(5);
					--ch:6
					lock_in_signal_4(6) <= lock_in_signal_3(6);
					lock_in_input_4(6) <= lock_in_input_3(6);
					
					--Starting ch:1 Calculations for band_pass_b
					m0_0 <= lock_in_signal_0(1);
					m0_1 <= bandB_0(1);
					m1_0 <= lock_in_signal_1(1);
					m1_1 <= bandB_1(1);
					m2_0 <= lock_in_signal_2(1);
					m2_1 <= bandB_2(1);
					m3_0 <= lock_in_signal_3(1);
					m3_1 <= bandB_3(1);
					m4_0 <= lock_in_signal_4(1);
					m4_1 <= bandB_4(1);
					
					bandA_0(6) <= lock_in_bandpass_a_0;
					bandA_1(6) <= lock_in_bandpass_a_1;
					bandA_2(6) <= lock_in_bandpass_a_2;
					bandA_3(6) <= lock_in_bandpass_a_3;
					
					bandB_0(6) <= lock_in_bandpass_b_0;
					bandB_1(6) <= lock_in_bandpass_b_1;
					bandB_2(6) <= lock_in_bandpass_b_2;
					bandB_3(6) <= lock_in_bandpass_b_3;
					bandB_4(6) <= lock_in_bandpass_b_4;
				when 7 =>
					temp(7) <= input_val;
					--ch:3
					lock_in_signal_0(3) <= temp(3);
					lock_in_input_0(3) <= (others => '0');
					--ch:4
					lock_in_signal_1(4) <= lock_in_signal_0(4);
					lock_in_input_1(4) <= lock_in_input_0(4);
					--ch:5
					lock_in_signal_2(5) <= lock_in_signal_1(5);
					lock_in_input_2(5) <= lock_in_input_1(5);
					--ch:6
					lock_in_signal_3(6) <= lock_in_signal_2(6);
					lock_in_input_3(6) <= lock_in_input_2(6);
					--ch:7
					lock_in_signal_4(7) <= lock_in_signal_3(7);
					lock_in_input_4(7) <= lock_in_input_3(7);
					
					--Starting ch:2 Calculations for band_pass_b
					m0_0 <= lock_in_signal_0(2);
					m0_1 <= bandB_0(2);
					m1_0 <= lock_in_signal_1(2);
					m1_1 <= bandB_1(2);
					m2_0 <= lock_in_signal_2(2);
					m2_1 <= bandB_2(2);
					m3_0 <= lock_in_signal_3(2);
					m3_1 <= bandB_3(2);
					m4_0 <= lock_in_signal_4(2);
					m4_1 <= bandB_4(2);
					
					bandA_0(7) <= lock_in_bandpass_a_0;
					bandA_1(7) <= lock_in_bandpass_a_1;
					bandA_2(7) <= lock_in_bandpass_a_2;
					bandA_3(7) <= lock_in_bandpass_a_3;
					
					bandB_0(7) <= lock_in_bandpass_b_0;
					bandB_1(7) <= lock_in_bandpass_b_1;
					bandB_2(7) <= lock_in_bandpass_b_2;
					bandB_3(7) <= lock_in_bandpass_b_3;
					bandB_4(7) <= lock_in_bandpass_b_4;
				when 8 =>
					--ch:4
					lock_in_signal_0(4) <= temp(4);
					lock_in_input_0(4) <= (others => '0');
					--ch:5
					lock_in_signal_1(5) <= lock_in_signal_0(5);
					lock_in_input_1(5) <= lock_in_input_0(5);
					--ch:6
					lock_in_signal_2(6) <= lock_in_signal_1(6);
					lock_in_input_2(6) <= lock_in_input_1(6);
					--ch:7
					lock_in_signal_3(7) <= lock_in_signal_2(7);
					lock_in_input_3(7) <= lock_in_input_2(7);
					
					--Starting ch:3 Calculations for band_pass_b
					m0_0 <= lock_in_signal_0(3);
					m0_1 <= bandB_0(3);
					m1_0 <= lock_in_signal_1(3);
					m1_1 <= bandB_1(3);
					m2_0 <= lock_in_signal_2(3);
					m2_1 <= bandB_2(3);
					m3_0 <= lock_in_signal_3(3);
					m3_1 <= bandB_3(3);
					m4_0 <= lock_in_signal_4(3);
					m4_1 <= bandB_4(3);
					
				when 9 =>
					--ch:5
					lock_in_signal_0(5) <= temp(5);
					lock_in_input_0(5) <= (others => '0');
					--ch:6
					lock_in_signal_1(6) <= lock_in_signal_0(6);
					lock_in_input_1(6) <= lock_in_input_0(6);
					--ch:7
					lock_in_signal_2(7) <= lock_in_signal_1(7);
					lock_in_input_2(7) <= lock_in_input_1(7);
					
					--Starting ch:4 Calculations for band_pass_b
					m0_0 <= lock_in_signal_0(4);
					m0_1 <= bandB_0(4);
					m1_0 <= lock_in_signal_1(4);
					m1_1 <= bandB_1(4);
					m2_0 <= lock_in_signal_2(4);
					m2_1 <= bandB_2(4);
					m3_0 <= lock_in_signal_3(4);
					m3_1 <= bandB_3(4);
					m4_0 <= lock_in_signal_4(4);
					m4_1 <= bandB_4(4);
					
				when 10 =>
					--ch:6
					lock_in_signal_0(6) <= temp(6);
					lock_in_input_0(6) <= (others => '0');
					--ch:7
					lock_in_signal_1(7) <= lock_in_signal_0(7);
					lock_in_input_1(7) <= lock_in_input_0(7);
					
					--Starting ch:5 Calculations for band_pass_b
					m0_0 <= lock_in_signal_0(5);
					m0_1 <= bandB_0(5);
					m1_0 <= lock_in_signal_1(5);
					m1_1 <= bandB_1(5);
					m2_0 <= lock_in_signal_2(5);
					m2_1 <= bandB_2(5);
					m3_0 <= lock_in_signal_3(5);
					m3_1 <= bandB_3(5);
					m4_0 <= lock_in_signal_4(5);
					m4_1 <= bandB_4(5);
					
				when 11 =>
					--ch:7
					lock_in_signal_0(7) <= temp(7);
					lock_in_input_0(7) <= (others => '0');
					
					--Starting ch:6 Calculations for band_pass_b
					m0_0 <= lock_in_signal_0(6);
					m0_1 <= bandB_0(6);
					m1_0 <= lock_in_signal_1(6);
					m1_1 <= bandB_1(6);
					m2_0 <= lock_in_signal_2(6);
					m2_1 <= bandB_2(6);
					m3_0 <= lock_in_signal_3(6);
					m3_1 <= bandB_3(6);
					m4_0 <= lock_in_signal_4(6);
					m4_1 <= bandB_4(6);
					
					--Starting output summation ch:0
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					lock_in_b_temp(0) <= m4_out;
				
				when 12 =>
					--Starting ch:7 Calculations for band_pass_b
					m0_0 <= lock_in_signal_0(7);
					m0_1 <= bandB_0(7);
					m1_0 <= lock_in_signal_1(7);
					m1_1 <= bandB_1(7);
					m2_0 <= lock_in_signal_2(7);
					m2_1 <= bandB_2(7);
					m3_0 <= lock_in_signal_3(7);
					m3_1 <= bandB_3(7);
					m4_0 <= lock_in_signal_4(7);
					m4_1 <= bandB_4(7);
					
					--Starting output summation ch:1
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					lock_in_b_temp(1) <= m4_out;
				when 13 =>
					--Starting output summation ch:2
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					lock_in_b_temp(2) <= m4_out;
					
					--Starting ch:0 calculations for band_pass_a
					m0_0 <= lock_in_input_1(0);
					m0_1 <= bandA_0(0);
					m1_0 <= lock_in_input_2(0);
					m1_1 <= bandA_1(0);
					m2_0 <= lock_in_input_3(0);
					m2_1 <= bandA_2(0);
					m3_0 <= lock_in_input_4(0);
					m3_1 <= bandA_3(0);
					
					
				when 14 =>
					--Starting output summation ch:3
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					lock_in_b_temp(3) <= m4_out;
					
					--Starting ch:1 calculations for band_pass_a
					m0_0 <= lock_in_input_1(1);
					m0_1 <= bandA_0(1);
					m1_0 <= lock_in_input_2(1);
					m1_1 <= bandA_1(1);
					m2_0 <= lock_in_input_3(1);
					m2_1 <= bandA_2(1);
					m3_0 <= lock_in_input_4(1);
					m3_1 <= bandA_3(1);
					
					--Temporary Debug Signals
					bp_b_0_out <= m0_out;
					bp_b_1_out <= m1_out;
					bp_b_2_out <= m2_out;
					bp_b_3_out <= m3_out;
					bp_b_4_out <= m4_out;
					
				when 15 =>
					--Starting output summation ch:4
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					lock_in_b_temp(4) <= m4_out;
					
					--Starting ch:2 calculations for band_pass_a
					m0_0 <= lock_in_input_1(2);
					m0_1 <= bandA_0(2);
					m1_0 <= lock_in_input_2(2);
					m1_1 <= bandA_1(2);
					m2_0 <= lock_in_input_3(2);
					m2_1 <= bandA_2(2);
					m3_0 <= lock_in_input_4(2);
					m3_1 <= bandA_3(2);
					
				when 16 =>
					--Starting output summation ch:5
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					lock_in_b_temp(5) <= m4_out;
					
					--Starting ch:3 calculations for band_pass_a
					m0_0 <= lock_in_input_1(3);
					m0_1 <= bandA_0(3);
					m1_0 <= lock_in_input_2(3);
					m1_1 <= bandA_1(3);
					m2_0 <= lock_in_input_3(3);
					m2_1 <= bandA_2(3);
					m3_0 <= lock_in_input_4(3);
					m3_1 <= bandA_3(3);
					
				when 17 =>
					--Starting output summation ch:6
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					lock_in_b_temp(6) <= m4_out;
					
					--Starting ch:4 calculations for band_pass_a
					m0_0 <= lock_in_input_1(4);
					m0_1 <= bandA_0(4);
					m1_0 <= lock_in_input_2(4);
					m1_1 <= bandA_1(4);
					m2_0 <= lock_in_input_3(4);
					m2_1 <= bandA_2(4);
					m3_0 <= lock_in_input_4(4);
					m3_1 <= bandA_3(4);
					
				when 18 => 
					--Starting output summation ch:7
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					lock_in_b_temp(7) <= m4_out;
					
					--Starting ch:5 calculations for band_pass_a
					m0_0 <= lock_in_input_1(5);
					m0_1 <= bandA_0(5);
					m1_0 <= lock_in_input_2(5);
					m1_1 <= bandA_1(5);
					m2_0 <= lock_in_input_3(5);
					m2_1 <= bandA_2(5);
					m3_0 <= lock_in_input_4(5);
					m3_1 <= bandA_3(5);
					
				when 19 =>
					--starting stage 2 addition ch:0
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
					--Starting ch:6 calculations for band_pass_a
					m0_0 <= lock_in_input_1(6);
					m0_1 <= bandA_0(6);
					m1_0 <= lock_in_input_2(6);
					m1_1 <= bandA_1(6);
					m2_0 <= lock_in_input_3(6);
					m2_1 <= bandA_2(6);
					m3_0 <= lock_in_input_4(6);
					m3_1 <= bandA_3(6);
					
					--starting ch:0 stage 1 additions
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					
				when 20 =>
					--starting stage 2 addition ch:1
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
					--Starting ch:7 calculations for band_pass_a
					m0_0 <= lock_in_input_1(7);
					m0_1 <= bandA_0(7);
					m1_0 <= lock_in_input_2(7);
					m1_1 <= bandA_1(7);
					m2_0 <= lock_in_input_3(7);
					m2_1 <= bandA_2(7);
					m3_0 <= lock_in_input_4(7);
					m3_1 <= bandA_3(7);
					
					--starting ch:1 stage 1 additions
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					
				when 21 =>
					--starting stage 2 addition ch:2
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
					--starting ch:2 stage 1 additions
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					
				when 22 =>
					--starting stage 2 addition ch:3
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
					--starting ch:3 stage 1 additions
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					
					--Multiple sets of debug signals here because they are processed differently.
					--temp output signals
					bpAout0 <= m0_out;
					bpAout1 <= m1_out;
					bpAout2 <= m2_out;
					bpAout3 <= m3_out;
					
					--Temporary Debug Outputs
					bp_a_0_out <= m0_out;
					bp_a_1_out <= m1_out;
					bp_a_2_out <= m2_out;
					bp_a_3_out <= m3_out;
					s2Aout0 <= a0_out;
					s2Aout1 <= a1_out;
					
					
				when 23 =>
					--starting stage 2 addition ch:4
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
					--starting ch:4 stage 1 additions
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					
				when 24 =>
					--starting stage 2 addition ch:5
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
					--starting ch:5 stage 1 additions
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					
				when 25 =>
					--starting stage 2 addition ch:6
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
					--starting ch:6 stage 1 additions
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					
				when 26 =>
					--starting stage 2 addition ch:7
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
					--starting ch:7 stage 1 additions
					a0_0 <= m0_out;
					a0_1 <= m1_out;
					a1_0 <= m2_out;
					a1_1 <= m3_out;
					
				when 27 =>
					--starting stage 3 addition ch:0
					a3_0 <= a2_out;
					a3_1 <= lock_in_b_temp(0);
					
					--starting ch:0 stage 2 additions
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
--					bpAout10 <= a0_out;
--					bpAout11 <= a1_out;
					
				when 28 =>
					--starting stage 3 addition ch:1
					a3_0 <= a2_out;
					a3_1 <= lock_in_b_temp(1);
					
					--starting ch:1 stage 2 additions
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
				when 29 =>
					--starting stage 3 addition ch:2
					a3_0 <= a2_out;
					a3_1 <= lock_in_b_temp(2);
					
					--starting ch:2 stage 2 additions
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
				when 30 =>
					--starting stage 3 addition ch:3
					a3_0 <= a2_out;
					a3_1 <= lock_in_b_temp(3);
					
					--starting ch:3 stage 2 additions
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
					bpAout10 <= a0_out;
					bpAout11 <= a1_out;
					
				when 31 =>
					--starting stage 3 addition ch:4
					a3_0 <= a2_out;
					a3_1 <= lock_in_b_temp(4);
					
					--starting ch:4 stage 2 additions
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
				when 32 =>
					--starting stage 3 addition ch:5
					a3_0 <= a2_out;
					a3_1 <= lock_in_b_temp(5);
					
					--starting ch:5 stage 2 additions
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
				when 33 =>
					--starting stage 3 addition ch:6
					a3_0 <= a2_out;
					a3_1 <= lock_in_b_temp(6);
					
					--starting ch:6 stage 2 additions
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
				when 34 =>
					--starting stage 3 addition ch:7
					a3_0 <= a2_out;
					a3_1 <= lock_in_b_temp(7);
					
					--starting ch:7 stage 2 additions
					a2_0 <= a0_out;
					a2_1 <= a1_out;
					
				when 35 =>
					--starting subtraction ch:0
					s0_0 <= a3_out;
					s0_1 <= a2_out;
					
				when 36 =>
					--starting subtraction ch:1
					s0_0 <= a3_out;
					s0_1 <= a2_out;
					
				when 37 =>
					--starting subtraction ch:2
					s0_0 <= a3_out;
					s0_1 <= a2_out;
					
				when 38 =>
					--starting subtraction ch:3
					s0_0 <= a3_out;
					s0_1 <= a2_out;
					
					--Temporary Debug Signals
					s2Aout2 <= a3_out;
					bpAout20 <= a2_out;
					
				when 39 =>
					--starting subtraction ch:4
					s0_0 <= a3_out;
					s0_1 <= a2_out;
					
				when 40 =>
					--starting subtraction ch:5
					s0_0 <= a3_out;
					s0_1 <= a2_out;
					
				when 41 =>
					--starting subtraction ch:6
					s0_0 <= a3_out;
					s0_1 <= a2_out;
					
				when 42 =>
					--starting subtraction ch:7
					s0_0 <= a3_out;
					s0_1 <= a2_out;
					
				when 43 =>
					--output ch:0
					lock_in_input_0(0) <= s0_out;
					lock_in_input_val <= s0_out;
					
					lock_in_in0d <= s0_out;
					
				when 44 =>
					--output ch:1
					lock_in_input_0(1) <= s0_out;
					lock_in_input_val <= s0_out;
					lock_in_in1d <= s0_out;
				when 45 =>
					--output ch:2
					lock_in_input_0(2) <= s0_out;
					lock_in_input_val <= s0_out;
					lock_in_in2d <= s0_out;
				when 46 =>
					--output ch:3
					lock_in_input_0(3) <= s0_out;
					lock_in_input_val <= s0_out;
					lock_in_in3d <= s0_out;
					
				when 47 =>
					--output ch:4
					lock_in_input_0(4) <= s0_out;
					lock_in_input_val <= s0_out;
					lock_in_in4d <= s0_out;
					
				when 48 =>
					--output ch:5
					lock_in_input_0(5) <= s0_out;
					lock_in_input_val <= s0_out;
					lock_in_in5d <= s0_out;
					
				when 49 =>
					--output ch:6
					lock_in_input_0(6) <= s0_out;
					lock_in_input_val <= s0_out;
					lock_in_in6d <= s0_out;
					
				when 50 =>
					--output ch:7
					lock_in_input_0(7) <= s0_out;
					lock_in_input_val <= s0_out;
					lock_in_in7d <= s0_out;
					
				when others =>
					null;
			end case;
		end if;
	end process;
	
	
end architecture rtl;