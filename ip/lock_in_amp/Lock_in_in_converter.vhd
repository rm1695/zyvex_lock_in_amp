----------------------------------------------------------------------------
-- Date:    2019-08-13
-- File:    Lock_in_in_converter.vhd
-- By:      Robert Mason
--
-- Desc:    Lock in Amplifier converter module
--
-- Notes:   
--
--  Used to change the input into an double value
--  Translates the channel input data into a usable format by the module.
--  In particular provides signal debouncing to prevent noise blips
--
--
-- Issues:
--
--
------------------------------------------------------------------------------
--
--Revisions:
--
-- 20190813 - Robert Mason
--   Initial creation
--
-- 20190818 - Robert Mason
--   Removal of unnecessary components, proper verification
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Lock_in_in_converter is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		input_data				  : in std_logic_vector(31 downto 0)  := (others => '0');
		input_addr				  : in std_logic_vector(7 downto 0)   := (others => '0');
		
		output_data		   	  : out std_logic_vector(63 downto 0) := (others => '0');
		output_addr				  : out std_logic_vector(7 downto 0)  := (others => '0');
		output_ready			  : out std_logic	:= '0'
	);
end entity Lock_in_in_converter;

architecture rtl of Lock_in_in_converter is

	--========================================================================
	--Aliases
	--========================================================================
	
	type delay is array (0 to 4) of std_logic_vector(8 downto 0);
	
	---------
	--General
	---------
	alias clock is clock_sink_clk;
	alias reset is reset_sink_reset;

   --========================================================================
   -- Signal Declarations
   --========================================================================
	
	signal current_cycle	: integer := 22;
	
	--Outputs of components
	signal shift_output	: std_logic_vector(7 downto 0) := (others => '0');
	
	--Temporary Input Signals
	signal input	: delay	:= (others => (others => '0'));

   --========================================================================
   -- Component Definitions
   --========================================================================
	component int_to_double
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
	--========================================================================
   --Processes
   --========================================================================
begin
	
	--========================================================================
   --Component Instantiation
	--========================================================================
	int_to_double_inst : int_to_double PORT MAP (
		clock	 => clock,
		dataa	 => input_data,
		result	 => output_data
	);
	
	--Process for delay timings
	timing_proc : process (clock, reset)
	begin
		if reset = '1' then
			input <= (others => (others => '0'));
			output_addr <= (others => '0');
			output_ready <= '0';
		elsif rising_edge(clock) then
			input(0) <= start_calc & input_addr;
			input(1) <= input(0);
			input(2) <= input(1);
			input(3) <= input(2);
			input(4) <= input(3);
			output_addr <= input(4)(7 downto 0);
			output_ready <= input(4)(8);
		end if;
	end process;
	
	
end architecture rtl;