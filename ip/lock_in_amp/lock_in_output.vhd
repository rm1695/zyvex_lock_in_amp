----------------------------------------------------------------------------
-- Date:    2019-08-11
-- File:    Lock_in_output.vhd
-- By:      Robert Mason
--
-- Desc:    Lock in Amplifier output calculation Module
--
-- Notes:   
--
--  Used to calculate the actual program outputs
--
--
-- Issues:
--
--
------------------------------------------------------------------------------
--
--Revisions:
--
-- 2019081 - Robert Mason
--   Initial creation
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity lock_in_output is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		
		lock_in_output_0       : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_output_1       : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_signal			  : in std_logic_vector(63 downto 0)  := (others => '0');
		sine                   : in std_logic_vector(63 downto 0)  := (others => '0');
		cosine                 : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_filtered	     : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_amp            : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_phase          : out std_logic_vector(63 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);
end entity lock_in_output;

architecture rtl of lock_in_output is

   --========================================================================
   -- Constant Definitions
   --========================================================================

	
	--Constants
	constant INT32_T_SIZE					: natural 								:= 32;
	
	constant num_cycles					   : integer								:= 28; --Cycles until process completely done
	constant num_cycles_output          : integer                        := 20; --Cycles until output starts

	--========================================================================
	--Aliases
	--========================================================================

	---------
	--Types
	---------
	type double_8_array is array (0 to 7) of std_logic_vector(63 downto 0);

	---------
	--General
	---------
	alias clock is clock_sink_clk;
	alias reset is reset_sink_reset;
	
   --========================================================================
   -- Register Declarations
   --========================================================================
	
	--=================
	--Support Registers
	--=================
	signal cosin      : double_8_array := (others => (others => '0'));
	signal out1       : double_8_array := (others => (others => '0'));
	signal sig        : double_8_array := (others => (others => '0'));
	
	signal s0_single_b: std_logic_vector(63 downto 0) := (others => '0');
	
   --========================================================================
   -- Signal Declarations
   --========================================================================
	
	signal current_cycle	: integer := 28;
	
	--Outputs of components
	signal M0o : std_logic_vector(63 downto 0) := (others => '0');
	signal S0o : std_logic_vector(63 downto 0) := (others => '0');
	
	--Inputs to components
	signal M0_in_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal M0_in_1 : std_logic_vector(63 downto 0) := (others => '0');
	signal S0_in_0 : std_logic_vector(63 downto 0) := (others => '0');
	signal S0_in_1 : std_logic_vector(63 downto 0) := (others => '0');
	
   --========================================================================
   -- Component Definitions
   --========================================================================
	
	component double_mult
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;
	
	component double_subtractor
	PORT
	(
		clock		: IN STD_LOGIC ;
		dataa		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	);
	end component;

	
	--========================================================================
   --Processes
   --========================================================================
begin
	
	--========================================================================
   --Component Instantiation
	--========================================================================
	
	S0 : double_subtractor PORT MAP (
		clock	 => clock,
		dataa	 => S0_in0,
		datab	 => S0_in1,
		result => S0o
	);
	
	M0 : double_mult PORT MAP (
		clock	 => clock,
		dataa	 => M0_in0,
		datab	 => M0_in1,
		result => M0o
	);
	
	--========================================================================
	--Control Logic
	--========================================================================
	
	counter_proc : process (clock,reset)
	begin
		if reset = '1' then
			current_cycle <= num_cycles;
		elsif rising_edge(clock) then
			if current_cycle < num_cycles and current_cycle > 0 then
				current_cycle <= current_cycle + 1;
			elsif current_cycle = 0 and start_calc = '1' then
				current_cycle <= 1;
                        elsif current_cycle = num_cycles then
				current_cycle <= 0;
			end if;
		end if;
	end process;
	
	--Output display cycle
	output_ready_proc : process (clock,reset)
	begin
		if reset = '1' then
			output_ready <= '0';
		elsif rising_edge(clock) then
			if current_cycle = num_cycles_output then
				output_ready <= '1';
			else
				output_ready <= '0';
			end if;
		end if;
	end process;
	
	--========================================================================
	--Data Pipelining Support
	--========================================================================
	
	data_proc : process (clock,reset)
	begin
		if reset = '1' then
			null;
		elsif rising_edge(clock) then
			case current_cycle is
				when 0 =>
				   --Inputs
					cosin(0) <= cosine;
					out1(0) <= lock_in_output_1;
					sig(0) <= lock_in_signal;
					M0_in_0 <= lock_in_output_0;
					M0_in_1 <= sine;
				when 1 =>
					--Inputs
					cosin(1) <= cosine;
					out1(1) <= lock_in_output_1;
					sig(1) <= lock_in_signal;
					M0_in_0 <= lock_in_output_0;
					M0_in_1 <= sine;
				when 2 =>
					--Inputs
					cosin(2) <= cosine;
					out1(2) <= lock_in_output_1;
					sig(2) <= lock_in_signal;
					M0_in_0 <= lock_in_output_0;
					M0_in_1 <= sine;
				when 3 =>
					--Inputs
					cosin(3) <= cosine;
					out1(3) <= lock_in_output_1;
					sig(3) <= lock_in_signal;
					M0_in_0 <= lock_in_output_0;
					M0_in_1 <= sine;
				when 4 =>
					--Inputs
					cosin(4) <= cosine;
					out1(4) <= lock_in_output_1;
					sig(4) <= lock_in_signal;
					M0_in_0 <= lock_in_output_0;
					M0_in_1 <= sine;
				when 5 =>
					--Inputs
					cosin(5) <= cosine;
					out1(5) <= lock_in_output_1;
					sig(5) <= lock_in_signal;
					M0_in_0 <= lock_in_output_0;
					M0_in_1 <= sine;
					--Outputs
					S0_in_0 <= sig(0);
					S0_in_1 <= M0o;
				when 6 =>
					--Inputs
					cosin(6) <= cosine;
					out1(6) <= lock_in_output_1;
					sig(6) <= lock_in_signal;
					M0_in_0 <= lock_in_output_0;
					M0_in_1 <= sine;
					--Outputs
					S0_in_0 <= sig(1);
					S0_in_1 <= M0o;
				when 7 =>
					--Inputs
					cosin(7) <= cosine;
					out1(7) <= lock_in_output_1;
					sig(7) <= lock_in_signal;
					M0_in_0 <= lock_in_output_0;
					M0_in_1 <= sine;
					--Outputs
					S0_in_0 <= sig(2);
					S0_in_1 <= M0o;
				when 8 =>
					--Outputs
					S0_in_0 <= sig(3);
					S0_in_1 <= M0o;
					--Inputs
					M0_in_0 <= out1(0);
					M0_in_1 <= cosin(0);
				when 9 =>
					--Outputs
					S0_in_0 <= sig(4);
					S0_in_1 <= M0o;
					--Inputs
					M0_in_0 <= out1(1);
					M0_in_1 <= cosin(1);
				when 10 =>
					--Outputs
					S0_in_0 <= sig(5);
					S0_in_1 <= M0o;
					--Inputs
					M0_in_0 <= out1(2);
					M0_in_1 <= cosin(2);
				when 11 =>
					--Outputs
					S0_in_0 <= sig(6);
					S0_in_1 <= M0o;
					--Inputs
					M0_in_0 <= out1(3);
					M0_in_1 <= cosin(3);
				when 12 =>
					--Outputs
					S0_in_0 <= sig(7);
					S0_in_1 <= M0o;
					s0_single_b <= S0o;
					--Inputs
					M0_in_0 <= out1(4);
					M0_in_1 <= cosin(4);
				when 13 =>
					--Inputs
					M0_in_0 <= out1(5);
					M0_in_1 <= cosin(5);
					s0_single_b <= S0o;
					S0_in_0 <= S0_single_b;
					S0_in_1 <= M0o;
				when 14 =>
					--Inputs
					M0_in_0 <= out1(6);
					M0_in_1 <= cosin(6);
					s0_single_b <= S0o;
					S0_in_0 <= S0_single_b;
					S0_in_1 <= M0o;
				when 15 =>
					--Inputs
					M0_in_0 <= out1(7);
					M0_in_1 <= cosin(7);
					s0_single_b <= S0o;
					S0_in_0 <= S0_single_b;
					S0_in_1 <= M0o;
				when 16 =>
					s0_single_b <= S0o;
					S0_in_0 <= S0_single_b;
					S0_in_1 <= M0o;
				when 17 =>
					s0_single_b <= S0o;
					S0_in_0 <= S0_single_b;
					S0_in_1 <= M0o;
				when 18 =>
					s0_single_b <= S0o;
					S0_in_0 <= S0_single_b;
					S0_in_1 <= M0o;
				when 19 =>
					s0_single_b <= S0o;
					S0_in_0 <= S0_single_b;
					S0_in_1 <= M0o;
				when 20 =>
					S0_in_0 <= S0_single_b;
					S0_in_1 <= M0o;
					lock_in_filtered <= S0o;
				when 21 =>
					lock_in_filtered <= S0o;
				when 22 =>
					lock_in_filtered <= S0o;
				when 23 =>
					lock_in_filtered <= S0o;
				when 24 =>
					lock_in_filtered <= S0o;
				when 25 =>
					lock_in_filtered <= S0o;
				when 26 =>
					lock_in_filtered <= S0o;
				when 27 =>
					lock_in_filtered <= S0o;
				when 28 =>
					
				when others =>
					
			end case;
		end if;
	end process;
	
	--========================================================================
	--Memory Support Processes
	--========================================================================
	
	
	
end architecture rtl;