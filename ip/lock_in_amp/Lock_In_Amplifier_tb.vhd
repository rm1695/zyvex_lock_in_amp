----------------------------------------------------------------------------
-- Date:    2019-XX-XX
-- File:    Lock_In_Amplifier_tb.vhd
-- By:      Robert Mason
--
-- Desc:    Testbench module for the Lock in amplifier whole module
--
-- Notes:   
--
--  Initializes the memory values according to a system default
--
--
-- Issues:
--
-- TODO: Write script to convert C code into an initialization example file
--
--
------------------------------------------------------------------------------
--
--Revisions:
--
--
------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY Lock_In_Amplifier_tb IS 
END Lock_In_Amplifier_tb;

ARCHITECTURE behavior OF Lock_In_Amplifier_tb IS

	component Lock_In_Amplifier is
	port (
		avalon_slave_address   : in  std_logic_vector(7 downto 0)  := (others => '0'); -- avalon_slave.address
		avalon_slave_read      : in  std_logic                     := '0';             --             .read
		avalon_slave_write     : in  std_logic                     := '0';             --             .write
		avalon_slave_writedata : in  std_logic_vector(31 downto 0) := (others => '0'); --             .writedata
		avalon_slave_readdata  : out std_logic_vector(31 downto 0);                    --             .readdata
		
		--for dac
		lock_in_enable		      : out std_logic:='0';
		lock_in_address		   : out std_logic_vector(5 downto 0);
		lock_in_write		      : out std_logic;
		lock_in_writedata	      : out std_logic_vector(31 downto 0);
		
		      asi_reset               : in  std_logic;  --Reset for all Avalon buses
      asi_clk                 : in  std_logic;  --Clock for all Avalon buses
		asi_sop_in              : in  std_logic;
      asi_eop_in              : in  std_logic;
      asi_valid_in            : in  std_logic;
      asi_data_in             : in  std_logic_vector(27 downto 0);  --Parallel shift register data output
      asi_channel_in          : in  std_logic_vector(2 downto 0) --Channel number output
	);
	end component;

	signal clk_150 : std_logic := '0';
	signal a_slave_address	: std_logic_vector(7 downto 0)	:= (others => '0');
	signal a_slave_read	: std_logic	 := '0';
	signal a_slave_write	: std_logic := '0';
	signal a_slave_writedata: std_logic_vector(31 downto 0)	:= (others => '0');
	signal a_slave_readdata	: std_logic_vector(31 downto 0)	:= (others => '0');
	signal lock_in_enable	: std_logic := '0';
	signal lock_in_address	: std_logic_vector(5 downto 0)	:= (others => '0');
	signal lock_in_write	: std_logic := '0';
	signal lock_in_writedata: std_logic_vector(31 downto 0)	:= (others => '0');
	signal lock_in_toggle	: std_logic := '0';
	signal lock_in_data	: std_logic_vector(255 downto 0)	:= (others => '0');
	signal lock_in_trigger	: std_logic_vector(7 downto 0)	:= (others => '0');
	signal reset_150 : std_logic := '0';
	constant clk_150_p	: time := 6.67 ns;

	signal asi_valid_in : std_logic := '0';
	signal asi_sop_in : std_logic := '0';
	signal asi_eop_in : std_logic := '0';
	signal asi_data_in : std_logic_vector(27 downto 0) := (others => '0');
	signal asi_channel_in : std_logic_vector(2 downto 0) := (others => '0');

begin

	uut : Lock_In_Amplifier
	port map(
		avalon_slave_address   => a_slave_address, -- avalon_slave.address
		avalon_slave_read      => a_slave_read,             --             .read
		avalon_slave_write     => a_slave_write,            --             .write
		avalon_slave_writedata => a_slave_writedata, --             .writedata
		avalon_slave_readdata  => a_slave_readdata,                    --             .readdata
		
		
		--for dac
		lock_in_enable		      => lock_in_enable,
		lock_in_address		   => lock_in_address,
		lock_in_write		      => lock_in_write,
		lock_in_writedata	      => lock_in_writedata,
		
		      asi_reset               => reset_150,
      asi_clk                 => clk_150,
		asi_sop_in              => asi_sop_in,
      asi_eop_in              => asi_eop_in,
      asi_valid_in            => asi_valid_in,
      asi_data_in             => asi_data_in,
      asi_channel_in          => asi_channel_in
	);

	clk_150_proc : process
	begin
		clk_150 <= '1';
		wait for clk_150_p/2;
		clk_150 <= '0';
		wait for clk_150_p/2;
	end process;

	test_proc : process
	begin
		--Starting Steady state
		wait for clk_150_p * 5;
		wait for clk_150_p * 0.5;
		
		--Enabling system
		a_slave_address <= "00000000";
		a_slave_write <= '1';
		a_slave_writedata <= x"00000001";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";

		--Writing Buffer Size
		wait for clk_150_p*5;
		a_slave_address <= "00000101";
		a_slave_write <= '1';
		a_slave_writedata <= x"00000100";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--Writing Sample Freq
		wait for clk_150_p*5;
		a_slave_address <= "00000001";
		a_slave_write <= '1';
		a_slave_writedata <= x"00000008";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--Writing LPF
		a_slave_address <= "01111111";
		a_slave_write <= '1';
		a_slave_writedata <= x"40000000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";
		

		--Writing Norm
		a_slave_address <= "00100000";
		a_slave_write <= '1';
		a_slave_writedata <= x"40000000";
		wait for clk_150_p;
		a_slave_address <= "10000000";
		a_slave_write <= '1';
		a_slave_writedata <= x"40000000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";

		wait for clk_150_p * 10;

		--Writing Bandpass A
		wait for clk_150_p;
		a_slave_address <= "00110000";
		a_slave_write <= '1';
		a_slave_writedata <= x"00000010";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";

		wait for clk_150_p;
		a_slave_address <= "10110000";
		a_slave_write <= '1';
		a_slave_writedata <= x"3ff00000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";

		wait for clk_150_p;
		a_slave_address <= "00111000";
		a_slave_write <= '1';
		a_slave_writedata <= x"40000000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";
		wait for clk_150_p;
		a_slave_address <= "01000000";
		a_slave_write <= '1';
		a_slave_writedata <= x"40080000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";
		wait for clk_150_p;
		a_slave_address <= "01001000";
		a_slave_write <= '1';
		a_slave_writedata <= x"40100000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";

		wait for clk_150_p * 10;

		--Writing Bandpass B
		wait for clk_150_p;
		a_slave_address <= "11010000";
		a_slave_write <= '1';
		a_slave_writedata <= x"3ff00000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";
		wait for clk_150_p;
		a_slave_address <= "11011000";
		a_slave_write <= '1';
		a_slave_writedata <= x"40000000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";
		wait for clk_150_p;
		a_slave_address <= "11100000";
		a_slave_write <= '1';
		a_slave_writedata <= x"40080000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";
		wait for clk_150_p;
		a_slave_address <= "11101000";
		a_slave_write <= '1';
		a_slave_writedata <= x"40100000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";
		wait for clk_150_p;
		a_slave_address <= "11110000";
		a_slave_write <= '1';
		a_slave_writedata <= x"40140000";
		wait for clk_150_p;
		a_slave_address <= "00000000";
		a_slave_write <= '0';
		a_slave_writedata <= x"00000000";

		wait for clk_150_p * 10;

		--Writing Sin and Cos
		a_slave_writedata <= x"00000000";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"3f800000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--2
		a_slave_writedata <= x"00000001";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40000000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--3
		a_slave_writedata <= x"00000002";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40400000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--4
		a_slave_writedata <= x"00000003";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40800000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--5
		a_slave_writedata <= x"00000004";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40a00000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--6
		a_slave_writedata <= x"00000005";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40c00000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--7
		a_slave_writedata <= x"00000006";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40e00000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--8
		a_slave_writedata <= x"00000007";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41000000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--9
		a_slave_writedata <= x"00000008";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41100000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--10
		a_slave_writedata <= x"00000009";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41200000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--11
		a_slave_writedata <= x"0000000A";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41300000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--12
		a_slave_writedata <= x"0000000B";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41400000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--13
		a_slave_writedata <= x"0000000C";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41500000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--14
		a_slave_writedata <= x"0000000D";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41600000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--15
		a_slave_writedata <= x"0000000E";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41700000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--16
		a_slave_writedata <= x"0000000F";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41800000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;
		
		--Second test data set

		a_slave_writedata <= x"00000010";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"3f800000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--2
		a_slave_writedata <= x"00000011";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40000000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--3
		a_slave_writedata <= x"00000012";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40400000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--4
		a_slave_writedata <= x"00000013";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40800000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--5
		a_slave_writedata <= x"00000014";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40a00000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--6
		a_slave_writedata <= x"00000015";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40c00000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--7
		a_slave_writedata <= x"00000016";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"40e00000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--8
		a_slave_writedata <= x"00000017";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41000000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--9
		a_slave_writedata <= x"00000018";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41100000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--10
		a_slave_writedata <= x"00000019";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41200000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--11
		a_slave_writedata <= x"0000001A";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41300000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--12
		a_slave_writedata <= x"0000001B";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41400000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--13
		a_slave_writedata <= x"0000001C";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41500000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--14
		a_slave_writedata <= x"0000001D";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41600000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--15
		a_slave_writedata <= x"0000001E";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41700000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--16
		a_slave_writedata <= x"0000001F";
		a_slave_write <= '1';
		a_slave_address <= "00000011";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		a_slave_writedata <= x"41800000";
		a_slave_write <= '1';
		a_slave_address <= "11111110";
		wait for clk_150_p;
		a_slave_write <= '0';
		wait for clk_150_p;

		--Generic Wait
		wait for clk_150_p * 10;

		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "000";
		asi_data_in <= x"0000001";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "001";
		asi_data_in <= x"0000002";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "010";
		asi_data_in <= x"0000002";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "011";
		asi_data_in <= x"0000003";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "100";
		asi_data_in <= x"0000004";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "101";
		asi_data_in <= x"0000005";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "110";
		asi_data_in <= x"0000006";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "111";
		asi_data_in <= x"0000007";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;

		wait for clk_150_p * 150;

				--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "000";
		asi_data_in <= x"0000001";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "001";
		asi_data_in <= x"0000002";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "010";
		asi_data_in <= x"0000002";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "011";
		asi_data_in <= x"0000003";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "100";
		asi_data_in <= x"0000004";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "101";
		asi_data_in <= x"0000005";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "110";
		asi_data_in <= x"0000006";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "111";
		asi_data_in <= x"0000007";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;

		wait for clk_150_p * 150;

		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "000";
		asi_data_in <= x"0000001";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "001";
		asi_data_in <= x"0000002";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "010";
		asi_data_in <= x"0000002";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "011";
		asi_data_in <= x"0000003";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "100";
		asi_data_in <= x"0000004";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "101";
		asi_data_in <= x"0000005";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "110";
		asi_data_in <= x"0000006";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;
		--Writing Data
		wait for clk_150_p;
		asi_valid_in <= '1';
		asi_channel_in <= "111";
		asi_data_in <= x"0000007";
		wait for clk_150_p;
		asi_valid_in <= '0';
		wait for clk_150_p * 5;

		--Generic Wait statement
		wait for clk_150_p * 1000000;
	end process;
end;