LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY lock_in_model_tb IS 
END lock_in_model_tb;

ARCHITECTURE behavior OF lock_in_model_tb IS

   component Lock_in_Calculation is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		
		input_val				  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_bandpass_a_0	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_1	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_2	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_3	  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_bandpass_b_0	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_1	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_2	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_3	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_4	  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_input_val		  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_0_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_1_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_2_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_3_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bp_b_0_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_1_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_2_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_3_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_4_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		
		--Stage 2 Addition out
		s2Aout0					  : out std_logic_vector(63 downto 0) := (others => '0');
		s2Aout1					  : out std_logic_vector(63 downto 0) := (others => '0');
		s2Aout2                : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout0					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout1					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout2					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout3					  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout10					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout11					  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout20					  : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in0d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in1d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in2d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in3d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in4d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in5d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in6d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in7d           : out std_logic_vector(63 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);
	end component;

   signal clk : std_logic := '0';
   constant clk_period : time := 1.176 ns;
	signal outR : std_logic := '0';
	signal strt : std_logic := '0';
	signal rst : std_logic := '0';
	
	signal read_ready : std_logic := '0';
	signal read_pt : std_logic := '0';
	signal read_addr : integer := 1;

	signal const_data_addr : std_logic_vector(31 downto 0) := x"00000000";
	signal const_data_val : std_logic_vector(31 downto 0) := x"00000000";
	signal const_write_data : std_logic	:= '0';
	
	signal lock_in_input_val : std_logic_vector(63 downto 0) := (others => '0');
BEGIN

   uut: Lock_In_Model
	port map (
		clock_sink_clk         => clk,
		reset_sink_reset       => rst,

		start_calc     		  => strt,
		
		input_val				  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_bandpass_a_0	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_1	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_2	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_3	  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_bandpass_b_0	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_1	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_2	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_3	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_4	  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_input_val		  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_0_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_1_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_2_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_3_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bp_b_0_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_1_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_2_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_3_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_4_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		
		--Stage 2 Addition out
		s2Aout0					  : out std_logic_vector(63 downto 0) := (others => '0');
		s2Aout1					  : out std_logic_vector(63 downto 0) := (others => '0');
		s2Aout2                : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout0					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout1					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout2					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout3					  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout10					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout11					  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout20					  : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in0d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in1d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in2d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in3d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in4d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in5d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in6d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in7d           : out std_logic_vector(63 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);      

   -- Clock process definitions( clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;  --for 0.5 ns signal is '0'.
        clk <= '1';
        wait for clk_period/2;  --for next 0.5 ns signal is '1'.
   end process;
	
	test_proc : process
	begin
		wait for clk_period * 5;
		wait for clk_period * 0.5;
		--Writing test values
		const_data_addr <= x"00000000";
		const_data_val <= x"3f800000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"00000001";
		const_data_val <= x"40000000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"00000002";
		const_data_val <= x"40400000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"00000003";
		const_data_val <= x"40800000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"00000004";
		const_data_val <= x"40a00000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"00000005";
		const_data_val <= x"40c00000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"00000006";
		const_data_val <= x"40e00000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"00000007";
		const_data_val <= x"41000000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"00000008";
		const_data_val <= x"41100000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"00000009";
		const_data_val <= x"41200000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"0000000A";
		const_data_val <= x"41300000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"0000000B";
		const_data_val <= x"41400000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"0000000C";
		const_data_val <= x"41500000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"0000000D";
		const_data_val <= x"41600000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"0000000E";
		const_data_val <= x"41700000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		const_data_addr <= x"0000000F";
		const_data_val <= x"41800000";
		const_write_data <= '1';
		wait for clk_period;
		const_write_data <= '0';
		wait for clk_period;
		
		--Reading data
		wait for clk_period * 10;
		read_addr <= 0;
		read_pt <= '1';
		wait for clk_period;
		read_pt <= '0';
		
		
		wait for clk_period * 100;
	end process;

END;