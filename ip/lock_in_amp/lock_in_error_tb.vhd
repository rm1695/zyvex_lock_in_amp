LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY lock_in_error_tb IS 
END lock_in_error_tb;

ARCHITECTURE behavior OF lock_in_error_tb IS

   component Lock_In_Error is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		
		lock_in_input_0		  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_lpf				  : in std_logic_vector(63 downto 0)  := (others => '0');
		sine                   : in std_logic_vector(63 downto 0)  := (others => '0');
		cosine                 : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_output_0_0f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_1f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_2f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_3f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_4f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_5f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_6f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_0_7f    : out std_logic_vector(31 downto 0) := (others => '0');
		
		lock_in_output_1_0f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_1f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_2f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_3f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_4f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_5f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_6f    : out std_logic_vector(31 downto 0) := (others => '0');
		lock_in_output_1_7f    : out std_logic_vector(31 downto 0) := (others => '0');
		
		sin_mult  : out std_logic_vector(63 downto 0) := (others => '0');
	   cos_mult  : out std_logic_vector(63 downto 0) := (others => '0');
	   err       : out std_logic_vector(63 downto 0) := (others => '0');
	   err_gamma : out std_logic_vector(63 downto 0) := (others => '0');
		sin_find  : out std_logic_vector(63 downto 0) := (others => '0');
		cos_find  : out std_logic_vector(63 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);
end component;

   signal clk : std_logic := '0';
   constant clk_period : time := 10 ns;
	signal outR : std_logic := '0';
	signal strt : std_logic := '0';
	signal rst : std_logic := '0';
	signal lock_in_input_0		  : std_logic_vector(63 downto 0)  := (others => '0');
	signal lock_in_lpf				  : std_logic_vector(63 downto 0)  := (others => '0');
	signal sine : std_logic_vector(63 downto 0)  := (others => '0');
	signal cosine                 : std_logic_vector(63 downto 0)  := (others => '0');
	signal lock_in_output_0_0f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_0_1f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_0_2f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_0_3f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_0_4f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_0_5f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_0_6f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_0_7f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_1_0f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_1_1f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_1_2f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_1_3f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_1_4f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_1_5f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_1_6f    : std_logic_vector(31 downto 0) := (others => '0');
	signal lock_in_output_1_7f    : std_logic_vector(31 downto 0) := (others => '0');
	signal sin_mult  : std_logic_vector(63 downto 0) := (others => '0');
	signal cos_mult  : std_logic_vector(63 downto 0) := (others => '0');
	signal err       : std_logic_vector(63 downto 0) := (others => '0');
	signal err_gamma : std_logic_vector(63 downto 0) := (others => '0');
	signal sin_find  : std_logic_vector(63 downto 0) := (others => '0');
	signal cos_find  : std_logic_vector(63 downto 0) := (others => '0');
	signal output_ready			  : std_logic	:= '0';

	signal current_cycle : integer := 0;

BEGIN

   uut: Lock_In_Error
	port map (
		clock_sink_clk         => clk,
		reset_sink_reset      => rst,

		start_calc     		  => strt,
		
		lock_in_input_0		  => lock_in_input_0,
		lock_in_lpf		=> lock_in_lpf,
		sine                   => sine,
		cosine                 => cosine,
		
		lock_in_output_0_0f    => lock_in_output_0_0f,
		lock_in_output_0_1f    => lock_in_output_0_1f,
		lock_in_output_0_2f    => lock_in_output_0_2f,
		lock_in_output_0_3f    => lock_in_output_0_3f,
		lock_in_output_0_4f    => lock_in_output_0_4f,
		lock_in_output_0_5f    => lock_in_output_0_5f,
		lock_in_output_0_6f    => lock_in_output_0_6f,
		lock_in_output_0_7f    => lock_in_output_0_7f,
		
		lock_in_output_1_0f    => lock_in_output_1_0f,
		lock_in_output_1_1f    => lock_in_output_1_1f,
		lock_in_output_1_2f    => lock_in_output_1_2f,
		lock_in_output_1_3f    => lock_in_output_1_3f,
		lock_in_output_1_4f    => lock_in_output_1_4f,
		lock_in_output_1_5f    => lock_in_output_1_5f,
		lock_in_output_1_6f    => lock_in_output_1_6f,
		lock_in_output_1_7f    => lock_in_output_1_7f,
		
		sin_mult  => sin_mult,
	   cos_mult  => cos_mult,
	   err       => err,
	   err_gamma => err_gamma,
		sin_find  => sin_find,
		cos_find  => cos_find,
		
		output_ready	=> output_ready
	);       

   -- Clock process definitions( clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '1';
        wait for clk_period/2;  --for 0.5 ns signal is '0'.
        clk <= '0';
        wait for clk_period/2;  --for next 0.5 ns signal is '1'.
   end process;
	
	test_proc : process (clk)
	begin
		if rising_edge(clk) then
			case current_cycle is
				when 1 =>
					lock_in_lpf <= x"3f50624dd2f1a9fc";
				when 43 => 
					strt <= '1';
					lock_in_input_0 <= x"3ff0000000000000";
					sine <= x"4003f5c28f5c28f6";
					cosine <= x"3fe9e353f7ced917";
				when 44 =>
					strt <= '0';
					sine <= x"4000000000000000";
					cosine <= x"4000000000000000";
					lock_in_input_0 <= x"4000000000000000";
				when 45 =>
					sine <= x"4008000000000000";
					cosine <= x"4008000000000000";
					lock_in_input_0 <= x"4008000000000000";
				when 46 =>
					sine <= x"4010000000000000";
					cosine <= x"4010000000000000";
					lock_in_input_0 <= x"4010000000000000";
				when 47 =>
					sine <= x"4014000000000000";
					cosine <= x"4014000000000000";
					lock_in_input_0 <= x"4014000000000000";
				when 48 =>
					sine <= x"4018000000000000";
					cosine <= x"4018000000000000";
					lock_in_input_0 <= x"4018000000000000";
				when 49 =>
					sine <= x"401c000000000000";
					cosine <= x"401c000000000000";
					lock_in_input_0 <= x"401c000000000000";
				when 50 =>
					sine <= x"4020000000000000";
					cosine <= x"4020000000000000";
					lock_in_input_0 <= x"4020000000000000";


				when 143 => 
					strt <= '1';
					lock_in_input_0 <= x"4000000000000000";
					sine <= x"4000000000000000";
					cosine <= x"4000000000000000";
				when 144 =>
					strt <= '0';
					sine <= x"4010000000000000";
					cosine <= x"4010000000000000";
					lock_in_input_0 <= x"4010000000000000";
				when 145 =>
					sine <= x"4018000000000000";
					cosine <= x"4018000000000000";
					lock_in_input_0 <= x"4018000000000000";
				when 146 =>
					sine <= x"4020000000000000";
					cosine <= x"4020000000000000";
					lock_in_input_0 <= x"4020000000000000";
				when 147 =>
					sine <= x"4024000000000000";
					cosine <= x"4024000000000000";
					lock_in_input_0 <= x"4024000000000000";
				when 148 =>
					sine <= x"4028000000000000";
					cosine <= x"4028000000000000";
					lock_in_input_0 <= x"4028000000000000";
				when 149 =>
					sine <= x"402c000000000000";
					cosine <= x"402c000000000000";
					lock_in_input_0 <= x"402c000000000000";
				when 150 =>
					sine <= x"4030000000000000";
					cosine <= x"4030000000000000";
					lock_in_input_0 <= x"4030000000000000";

				when others =>
					null;
			end case;
		end if;
	end process;

	c_proc : process (clk)
	begin
		if rising_edge(clk) then
			current_cycle <= current_cycle + 1;
		end if;
	end process;

END;