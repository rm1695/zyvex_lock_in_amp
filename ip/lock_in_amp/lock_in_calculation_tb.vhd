LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY lock_in_calculation_tb IS 
END lock_in_calculation_tb;

ARCHITECTURE behavior OF lock_in_calculation_tb IS

   component Lock_in_Calculation is
	port (
		clock_sink_clk         : in  std_logic                     := '0';             --   clock_sink.clk
		reset_sink_reset       : in  std_logic                     := '0';             --   reset_sink.reset

		start_calc     		  : in std_logic                      := '0';
		
		input_val				  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_bandpass_a_0	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_1	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_2	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_a_3	  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_bandpass_b_0	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_1	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_2	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_3	  : in std_logic_vector(63 downto 0)  := (others => '0');
		lock_in_bandpass_b_4	  : in std_logic_vector(63 downto 0)  := (others => '0');
		
		lock_in_input_val		  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_0_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_1_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_2_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_a_3_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bp_b_0_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_1_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_2_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_3_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		bp_b_4_out				  : out std_logic_vector(63 downto 0) := (others => '0');
		
		--Stage 2 Addition out
		s2Aout0					  : out std_logic_vector(63 downto 0) := (others => '0');
		s2Aout1					  : out std_logic_vector(63 downto 0) := (others => '0');
		s2Aout2                : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout0					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout1					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout2					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout3					  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout10					  : out std_logic_vector(63 downto 0) := (others => '0');
		bpAout11					  : out std_logic_vector(63 downto 0) := (others => '0');
		
		bpAout20					  : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in0d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in1d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in2d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in3d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in4d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in5d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in6d           : out std_logic_vector(63 downto 0) := (others => '0');
		lock_in_in7d           : out std_logic_vector(63 downto 0) := (others => '0');
		
		output_ready			  : out std_logic	:= '0'
	);
	end component;

   signal clk : std_logic := '0';
   constant clk_period : time := 10 ns;
	signal outR : std_logic := '0';
	signal strt : std_logic := '0';
	signal rst : std_logic := '0';
	signal input_val : std_logic_vector(63 downto 0) := (others => '0');

	signal lock_in_bandpass_a_0 : std_logic_vector(63 downto 0) := x"3FF0000000000000";
	signal lock_in_bandpass_a_1 : std_logic_vector(63 downto 0) := x"4000000000000000";
	signal lock_in_bandpass_a_2 : std_logic_vector(63 downto 0) := x"4008000000000000";
	signal lock_in_bandpass_a_3 : std_logic_vector(63 downto 0) := x"4010000000000000";
	signal lock_in_bandpass_b_0 : std_logic_vector(63 downto 0) := x"4014000000000000";
	signal lock_in_bandpass_b_1 : std_logic_vector(63 downto 0) := x"4018000000000000";
	signal lock_in_bandpass_b_2 : std_logic_vector(63 downto 0) := x"401c000000000000";
	signal lock_in_bandpass_b_3 : std_logic_vector(63 downto 0) := x"4020000000000000";
	signal lock_in_bandpass_b_4 : std_logic_vector(63 downto 0) := x"4022000000000000";
	
	signal lock_in_input_val : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_a_0_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_a_1_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_a_2_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_a_3_out : std_logic_vector(63 downto 0) := (others => '0');

	signal bp_b_0_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_b_1_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_b_2_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_b_3_out : std_logic_vector(63 downto 0) := (others => '0');
	signal bp_b_4_out : std_logic_vector(63 downto 0) := (others => '0');

	signal s2Aout0	: std_logic_vector(63 downto 0) := (others => '0');
	signal s2Aout1	: std_logic_vector(63 downto 0) := (others => '0');
	signal s2Aout2	: std_logic_vector(63 downto 0) := (others => '0');

	signal bpAout0 : std_logic_vector(63 downto 0) := (others => '0');
	signal bpAout1 : std_logic_vector(63 downto 0) := (others => '0');
	signal bpAout2 : std_logic_vector(63 downto 0) := (others => '0');
	signal bpAout3 : std_logic_vector(63 downto 0) := (others => '0');

	signal bpAout10	: std_logic_vector(63 downto 0) := (others => '0');
	signal bpAout11	: std_logic_vector(63 downto 0) := (others => '0');

	signal bpAout20	: std_logic_vector(63 downto 0) := (others => '0');
	signal output_ready : std_logic := '0';

	signal lock_in_in0d : std_logic_vector(63 downto 0) := (others => '0');
	signal lock_in_in1d : std_logic_vector(63 downto 0) := (others => '0');
	signal lock_in_in2d : std_logic_vector(63 downto 0) := (others => '0');
	signal lock_in_in3d : std_logic_vector(63 downto 0) := (others => '0');
	signal lock_in_in4d : std_logic_vector(63 downto 0) := (others => '0');
	signal lock_in_in5d : std_logic_vector(63 downto 0) := (others => '0');
	signal lock_in_in6d : std_logic_vector(63 downto 0) := (others => '0');
	signal lock_in_in7d : std_logic_vector(63 downto 0) := (others => '0');

	signal counter : integer := 0;

BEGIN

   uut: Lock_in_Calculation
	port map (
		clock_sink_clk         => clk,
		reset_sink_reset       => rst,

		start_calc     		  => strt,
		
		input_val		=> input_val,
		
		lock_in_bandpass_a_0	  => lock_in_bandpass_a_0,
		lock_in_bandpass_a_1	  => lock_in_bandpass_a_1,
		lock_in_bandpass_a_2	  => lock_in_bandpass_a_2,
		lock_in_bandpass_a_3	  => lock_in_bandpass_a_3,
		
		lock_in_bandpass_b_0	  => lock_in_bandpass_b_0,
		lock_in_bandpass_b_1	  => lock_in_bandpass_b_1,
		lock_in_bandpass_b_2	  => lock_in_bandpass_b_2,
		lock_in_bandpass_b_3	  => lock_in_bandpass_b_3,
		lock_in_bandpass_b_4	  => lock_in_bandpass_b_4,
		
		lock_in_input_val	=> lock_in_input_val,
		bp_a_0_out		=> bp_a_0_out,
		bp_a_1_out		=> bp_a_1_out,
		bp_a_2_out		=> bp_a_2_out,
		bp_a_3_out		=> bp_a_3_out,
		
		bp_b_0_out		=> bp_b_0_out,
		bp_b_1_out		=> bp_b_1_out,
		bp_b_2_out		=> bp_b_2_out,
		bp_b_3_out		=> bp_b_3_out,
		bp_b_4_out		=> bp_b_4_out,
		
		--Stage 2 Addition out
		s2Aout0		       => s2Aout0,
		s2Aout1		       => s2Aout1,
		s2Aout2                => s2Aout2,
		
		bpAout0			=> bpAout0,
		bpAout1			=> bpAout1,
		bpAout2			=> bpAout2,
		bpAout3			=> bpAout3,
		
		bpAout10		=> bpAout10,
		bpAout11		=> bpAout11,
		
		bpAout20		=> bpAout20,
		lock_in_in0d           => lock_in_in0d,
		lock_in_in1d           => lock_in_in1d,
		lock_in_in2d           => lock_in_in2d,
		lock_in_in3d           => lock_in_in3d,
		lock_in_in4d           => lock_in_in4d,
		lock_in_in5d           => lock_in_in5d,
		lock_in_in6d           => lock_in_in6d,
		lock_in_in7d           => lock_in_in7d,
		
		output_ready			  => output_ready
	);      

   -- Clock process definitions( clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '1';
        wait for clk_period/2;  --for 0.5 ns signal is '0'.
        clk <= '0';
        wait for clk_period/2;  --for next 0.5 ns signal is '1'.
   end process;
	
	c_proc : process (clk)
	begin
		if rising_edge(clk) then
			if counter < 100 then
				counter <= counter + 1;
			else
				counter <= 0;
			end if;
		end if;
	end process;

	test_proc : process (clk)
	begin
		if rising_edge(clk) then
			case counter is
				when 5 =>
					strt <= '1';
					input_val <= x"3ff0000000000000";
					lock_in_bandpass_a_0 <= x"3ff0000000000000";
					lock_in_bandpass_a_1 <= x"3ff0000000000000";
					lock_in_bandpass_a_2 <= x"3ff0000000000000";
					lock_in_bandpass_a_3 <= x"3ff0000000000000";
					
					lock_in_bandpass_b_0 <= x"3ff0000000000000";
					lock_in_bandpass_b_1 <= x"3ff0000000000000";
					lock_in_bandpass_b_2 <= x"3ff0000000000000";
					lock_in_bandpass_b_3 <= x"3ff0000000000000";
					lock_in_bandpass_b_4 <= x"3ff0000000000000";
					
				when 6 =>
					strt <= '0';
					input_val <= x"4000000000000000";
					lock_in_bandpass_a_0 <= x"4000000000000000";
					lock_in_bandpass_a_1 <= x"4000000000000000";
					lock_in_bandpass_a_2 <= x"4000000000000000";
					lock_in_bandpass_a_3 <= x"4000000000000000";
					
					lock_in_bandpass_b_0 <= x"4000000000000000";
					lock_in_bandpass_b_1 <= x"4000000000000000";
					lock_in_bandpass_b_2 <= x"4000000000000000";
					lock_in_bandpass_b_3 <= x"4000000000000000";
					lock_in_bandpass_b_4 <= x"4000000000000000";
					
				when 7 =>
					input_val <= x"4008000000000000";
					lock_in_bandpass_a_0 <= x"4008000000000000";
					lock_in_bandpass_a_1 <= x"4008000000000000";
					lock_in_bandpass_a_2 <= x"4008000000000000";
					lock_in_bandpass_a_3 <= x"4008000000000000";
					
					lock_in_bandpass_b_0 <= x"4008000000000000";
					lock_in_bandpass_b_1 <= x"4008000000000000";
					lock_in_bandpass_b_2 <= x"4008000000000000";
					lock_in_bandpass_b_3 <= x"4008000000000000";
					lock_in_bandpass_b_4 <= x"4008000000000000";
					
				when 8 =>
					input_val <= x"4010000000000000";
					lock_in_bandpass_a_0 <= x"4010000000000000";
					lock_in_bandpass_a_1 <= x"4010000000000000";
					lock_in_bandpass_a_2 <= x"4010000000000000";
					lock_in_bandpass_a_3 <= x"4010000000000000";
					
					lock_in_bandpass_b_0 <= x"4010000000000000";
					lock_in_bandpass_b_1 <= x"4010000000000000";
					lock_in_bandpass_b_2 <= x"4010000000000000";
					lock_in_bandpass_b_3 <= x"4010000000000000";
					lock_in_bandpass_b_4 <= x"4010000000000000";
					
				when 9 =>
					input_val <= x"4014000000000000";
					lock_in_bandpass_a_0 <= x"4014000000000000";
					lock_in_bandpass_a_1 <= x"4014000000000000";
					lock_in_bandpass_a_2 <= x"4014000000000000";
					lock_in_bandpass_a_3 <= x"4014000000000000";
					
					lock_in_bandpass_b_0 <= x"4014000000000000";
					lock_in_bandpass_b_1 <= x"4014000000000000";
					lock_in_bandpass_b_2 <= x"4014000000000000";
					lock_in_bandpass_b_3 <= x"4014000000000000";
					lock_in_bandpass_b_4 <= x"4014000000000000";
					
				when 10 =>
					input_val <= x"4018000000000000";
					lock_in_bandpass_a_0 <= x"4018000000000000";
					lock_in_bandpass_a_1 <= x"4018000000000000";
					lock_in_bandpass_a_2 <= x"4018000000000000";
					lock_in_bandpass_a_3 <= x"4018000000000000";
					
					lock_in_bandpass_b_0 <= x"4018000000000000";
					lock_in_bandpass_b_1 <= x"4018000000000000";
					lock_in_bandpass_b_2 <= x"4018000000000000";
					lock_in_bandpass_b_3 <= x"4018000000000000";
					lock_in_bandpass_b_4 <= x"4018000000000000";
					
				when 11 =>
					input_val <= x"401c000000000000";
					lock_in_bandpass_a_0 <= x"401c000000000000";
					lock_in_bandpass_a_1 <= x"401c000000000000";
					lock_in_bandpass_a_2 <= x"401c000000000000";
					lock_in_bandpass_a_3 <= x"401c000000000000";
					
					lock_in_bandpass_b_0 <= x"401c000000000000";
					lock_in_bandpass_b_1 <= x"401c000000000000";
					lock_in_bandpass_b_2 <= x"401c000000000000";
					lock_in_bandpass_b_3 <= x"401c000000000000";
					lock_in_bandpass_b_4 <= x"401c000000000000";
					
				when 12 =>
					input_val <= x"4020000000000000";
					lock_in_bandpass_a_0 <= x"4020000000000000";
					lock_in_bandpass_a_1 <= x"4020000000000000";
					lock_in_bandpass_a_2 <= x"4020000000000000";
					lock_in_bandpass_a_3 <= x"4020000000000000";
					
					lock_in_bandpass_b_0 <= x"4020000000000000";
					lock_in_bandpass_b_1 <= x"4020000000000000";
					lock_in_bandpass_b_2 <= x"4020000000000000";
					lock_in_bandpass_b_3 <= x"4020000000000000";
					lock_in_bandpass_b_4 <= x"4020000000000000";
					
				
				when others =>
					null;
			end case;
		end if;
	end process;

END;