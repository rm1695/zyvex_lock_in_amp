----------------------------------------------------------------------------
-- Date:	  2016-05-20
-- File:	  avs_channel_summer.vhd
-- By:	  Jeff Short
--         Copyright 2016 Jeffrey C. Short, All Rights Reserved.
--
-- Desc:    Avalon-Streaming Channel Summer
--
------------------------------------------------------------------------------
-- Algorithm:
------------------------------------------------------------------------------
--
-- Implements an Avalon-ST channel summer.
-- Inputs and outputs are packetized avalon streaming buses with SOP/EOP signals.
--
-- This module sums incoming data based on channel id's that can be used to average
-- incoming data readings.  Once the selected number of samples has been summed,
-- an output packet is generated.  A new sum is started with the next incoming
-- packet data.
--
-- Key points:
--    1.  Each channel can have a per-channel gain and/or offset applied.
--    2.  Summing results are rounded and divided.
--
-- An Avalon Memory mapped bus is used to control the module.
--
------------------------------------------------------------------------------
-- Registers Definitions:
--    -----------------------------------------------------------------------
--    Name               Reg# Offset   R/W   Description
--    -----------------------------------------------------------------------
--    reg_control           0     (0x00)  R/W
--       0: summing_enable                R/W      1=Enable summing, 0=Disable summing (pass-through mode)
--       1: signed_mode                   R/W      1=Data is signed, 0=Data is unsigned
--       2: offset_enable                 R/W      1=Enable per-channel offset, 0=Disable.
--       3: gain_enable                   R/W      1=Enable per-channel gain, 0=Disable.
--    
--    reg_status            1     (0x04)  R(W)
--       0: busy                          R        Module status: 1=Processing a packet, 0=Idle.
--
--    reg_sum_count         2     (0x08)  R/W      Number of samples to sum per channel.  Zero based, set to N-1.
--    reg_div_shift         3     (0x0C)  R/W      Power of 2 shift divisor (number of bits to right-shift final output)
--                                                 Note:  This only performs powers of 2 division.
--                                                        Output is rounded and shifted right by this number of bits.
--                                                 Note:  Summing and division by a non-power of 2 number
--                                                        can be achieved by setting the gain values to compensate
--                                                        for the power of 2 divisor.
--                                                 Example:  To sum 5 values and output a divided by 5 result:
--                                                          Set reg_sum_count to 4 (5-1) and set reg_div_shift to 3 (divide by 8)
--                                                          Set all gain registers to 1.600.
--                                                          Results are summed, rounded, and shifted: (sum[1.6*samples)]+4)/8
--                                                 Rounding:
--                                                          Result is rounded up for positive values, and down for
--                                                          negative values:  +10.5 rounds to +11, -10.5 rounds to -11.
--
--
--    Gain / Offset Registers (one set for each channel)
--       Gain format is:   Sign+INTEGER_BITS+FRACTIONAL_BITS
--       Offset format is: Sign+IN_BITS_PER_SYMBOL  
--    Notes:
--       Gain and Offset values are always signed, but results are clamped to 0 for unsigned mode.
--
--    reg_channel_gain[0]   0     (0x100)  R/W     Gain for channel 0
--    reg_channel_offset[0] 1     (0x104)  R/W     Offset for channel 0
--    reg_channel_gain[1]   2     (0x108)  R/W     Gain for channel 1
--    reg_channel_offset[1] 3     (0x10C)  R/W     Offset for channel 1
--    reg_channel_gain[2]   4     (0x110)  R/W     Gain for channel 2
--    reg_channel_offset[2] 5     (0x114)  R/W     Offset for channel 2
--    reg_channel_gain[3]   6     (0x118)  R/W     Gain for channel 3
--    reg_channel_offset[3] 7     (0x11C)  R/W     Offset for channel 3
--    ...
--    reg_channel_gain[N]   N     (0x100+N)  R/W   Gain for channel N
--    reg_channel_offset[N] N+1   (0x100+N+1 R/W   Offset for channel N
--
--
------------------------------------------------------------------------------
-- Data Flow Diagram
------------------------------------------------------------------------------
--
-- Data Stages:  S0, S1, S2, S3, S4
-- Count Stages:         C2, C3, C4
--
-- Pipeline
-- Stage:      S1         S2         S3         S4           S5
--          --------   --------   ---------   ---------   ---------  
--          |Per-  |   |Per-  |   |       |   |       |   |       |  
-- Data_In->|Chan  |-->|Chan  |-->|Summer |-->|Average|-->|Range  |-->Data_Out
-- Chan_In  |Offset|   |Gain  |   |       |   |Divider|   |Check  |   Chan_Out
--          --------   --------   ---------   ---------   ---------
--
--   Note: All math is performed internally as signed data.  Unsigned mode
--         only interprets the msbit of incoming data as either a sign bit
--         or as the msbit.
--         Internal pipeline stages are sized with an extra sign bit to allow
--         correct operation with both types of data.
--
--   S1:   Adds a signed per-channel offset to each incoming sample.
--         Negative results are clamped to 0 for unsigned mode if a negative
--         offset is used.  Result has 2 more bits than the input data to
--         allow for a sign bit and to preserve the offset result without
--         clamping or limiting the result.
--
--   S2:   Multiplies S1 results by a per-channel gain value.
--         Number of bits in result is the sum of the S1 bits + gain bits.
--
--   S3:   Results are summed into a signed accumulator.
--         Number of bits in accumulator is S2 bits + enough bits to hold
--         max allowed sum.  For max of 256 sums, 8 extra bits are included.
--
--   S4A:  Accumulator results are shifted right by GAIN_FRACTIONAL_BITS-1.
--         One fractional bit is left to use for rounding.
--
--   S4B:  Perform power-of-2 divide by shifting S4A results right by the value
--         in reg_div_shift.  A +1 or -1 is added to the shifted results for
--         rounding.  +1 is added to positive numbers, -1 is added to negative
--         numbers.  The lsbit is truncated from the rounding result and is
--         passed to S5.
--
--   S5:   Range check final results and limit to min/max signed or unsigned
--         values to number of output bits.
--
--         All intermediate packets are output, but only the final packet for
--         each completed sum will have the aso_valid signal asserted.
--
------------------------------------------------------------------------------
-- Revisions:
--
--    20160520 - Jeff Short
--       Initial creation.
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--use IEEE.std_logic_unsigned.all;
--use IEEE.std_logic_arith.all;

--use work.UTIL_pkg.all;

LIBRARY altera_mf;
USE altera_mf.all;

--Library UNISIM;
--use UNISIM.vcomponents.all;

entity avs_channel_summer is
   generic (
      IN_BITS_PER_SYMBOL         : natural range 4 to 24  := 18;  --Number of bits per incoming symbol
      OUT_BITS_PER_SYMBOL        : natural range 4 to 32  := 18+8;--Number of bits per output symbol. Keep <= 32. Typical = IN_BITS_PER_SYMBOL + log2_f(MAX_SUMMING_COUNT)
      NUM_CHANNELS               : natural range 8 to 8   := 8;   --Number of channels
      NUM_CHANNEL_BITS           : natural range 3 to 3   := 3;   --Number of bits for channel counter
      MAX_SUMMING_COUNT          : integer range 4 to 256 := 256; --Max number of values that can be summed
      NUM_GAIN_INTEGER_BITS      : natural range 2 to 16  := 2;   --Number of bits to use for gain value (Format: Sign bit + integer bits + fractional bits)
      NUM_GAIN_FRACTIONAL_BITS   : natural range 2 to 16  := 8;   --Number of bits to use for gain value (Format: Sign bit + integer bits + fractional bits)
      AVS_ADDRESS_BITS           : natural range 5 to 5   := 5;   --Number of address bits for AVM interface (4 + 2*Num_CHANNELS)
      AVS_DATA_BUS_WIDTH         : integer := 32                  --Data width of AVM bus
   );
   port (
      --Global Signals
      reset                      : in  std_logic;        --Global reset
      clk                        : in  std_logic;        --Reference clock

      --Avalon Streaming Sink - Uncorrected Video input stream
      asi_sop_in                 : in  std_logic;
      asi_eop_in                 : in  std_logic;
      asi_valid_in               : in  std_logic;
      asi_data_in                : in  std_logic_vector(IN_BITS_PER_SYMBOL-1 downto 0);
      asi_channel_in             : in  std_logic_vector(NUM_CHANNEL_BITS-1 downto 0);
      
      --Avalon Streaming Source - Corrected Video data output stream
      aso_sop_out                : out std_logic;
      aso_eop_out                : out std_logic;
      aso_valid_out              : out std_logic;
      aso_data_out               : out std_logic_vector(OUT_BITS_PER_SYMBOL-1 downto 0);
      aso_channel_out            : out std_logic_vector(NUM_CHANNEL_BITS-1 downto 0);

      --Avalon-MM Interface
      avs_chipselect             : in  std_logic;
      avs_read                   : in  std_logic;
      avs_write                  : in  std_logic;
      avs_address                : in  std_logic_vector(AVS_ADDRESS_BITS-1 downto 0); --Set address space to 2x Histogram memory size (no -1 needed)
      avs_byteenable             : in  std_logic_vector((AVS_DATA_BUS_WIDTH/8)-1 downto 0);
      avs_readdata               : out std_logic_vector(AVS_DATA_BUS_WIDTH-1 downto 0);
      avs_writedata              : in  std_logic_vector(AVS_DATA_BUS_WIDTH-1 downto 0);
      avs_waitrequest            : out std_logic
   );      
end avs_channel_summer;  

--===========================================================================
--===========================================================================
architecture avs_channel_summer_arch of avs_channel_summer is

   --------------------------------------------------------------------------
   -- Function Definitions
   --------------------------------------------------------------------------
   --Use to determine number of bits necessary to hold a positive value
   function log2_f(n : in natural) return natural is
   variable i : natural := 0;
   begin
      while (2**i <= n) loop
         i := i + 1;
      end loop;
      return i-1;
   end log2_f;

   --------------------------------------------------------------------------
   -- Constant Declarations
   --------------------------------------------------------------------------
   constant cPIPELINE_LENGTH              : integer := 5;   --Number of pipeline stages
   constant cNUM_OFFSET_BITS              : integer := IN_BITS_PER_SYMBOL+2;  --Set to input size
   constant cNUM_GAIN_BITS                : integer := NUM_GAIN_INTEGER_BITS + NUM_GAIN_FRACTIONAL_BITS + 1;  --Get integer, fractional, and sign bit
   
   --Lengths for data pipeline stages
   constant S1_DATA_BITS                  : integer := IN_BITS_PER_SYMBOL + 2;         --Include incoming data, one overflow bit, and a sign bit
   constant S2_DATA_MULT_BITS             : integer := S1_DATA_BITS + cNUM_GAIN_BITS;  --Include incoming data, one overflow bit, and a sign bit
   constant S3_ACCUMULATOR_BITS           : integer := S2_DATA_MULT_BITS + log2_f(MAX_SUMMING_COUNT);
   constant S4_TRUNC_DATA_BITS            : integer := S3_ACCUMULATOR_BITS - NUM_GAIN_FRACTIONAL_BITS +1; --Same as accumulator bits - all but 1 fractional bit
   constant S4_DATA_BITS                  : integer := S4_TRUNC_DATA_BITS;             --Preserve an extra sign bit for unsigned mode
   constant S5_DATA_BITS                  : integer := OUT_BITS_PER_SYMBOL;

   constant cGAIN_1p0                     : signed(cNUM_GAIN_BITS-1 downto 0) := to_signed(2**NUM_GAIN_FRACTIONAL_BITS, cNUM_GAIN_BITS);
   constant cGAIN_1n0                     : signed(cNUM_GAIN_BITS-1 downto 0) := to_signed(-(2**NUM_GAIN_FRACTIONAL_BITS), cNUM_GAIN_BITS);
   
   --------------------------------------------------------------------------
   --Register Address Definitions (First 1/4 address space)
   --------------------------------------------------------------------------
   constant cADDR_REG_CONTROL             : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned( 0, 5));  --Control register
   constant cADDR_REG_STATUS              : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned( 1, 5));  --Status register - read only, write to clear flags
   constant cADDR_REG_SUM_COUNT           : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned( 2, 5));  --Number of samples to sum
   constant cADDR_REG_DIV_SHIFT           : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned( 3, 5));  --Number of bits to shift right output
   constant cADDR_REG_CHANNEL_GAIN_0      : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned( 4, 5));  --Per Channel offset (signed)
   constant cADDR_REG_CHANNEL_OFFSET_0    : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned( 5, 5));  --Per-Channel gain, fixed + fractional (signed)
   constant cADDR_REG_CHANNEL_GAIN_1      : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned( 6, 5));  --Per Channel offset (signed)
   constant cADDR_REG_CHANNEL_OFFSET_1    : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned( 7, 5));  --Per-Channel gain, fixed + fractional (signed)
   constant cADDR_REG_CHANNEL_GAIN_2      : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned( 8, 5));  --Per Channel offset (signed)
   constant cADDR_REG_CHANNEL_OFFSET_2    : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned( 9, 5));  --Per-Channel gain, fixed + fractional (signed)
   constant cADDR_REG_CHANNEL_GAIN_3      : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(10, 5));  --Per Channel offset (signed)
   constant cADDR_REG_CHANNEL_OFFSET_3    : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(11, 5));  --Per-Channel gain, fixed + fractional (signed)
   constant cADDR_REG_CHANNEL_GAIN_4      : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(12, 5));  --Per Channel offset (signed)
   constant cADDR_REG_CHANNEL_OFFSET_4    : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(13, 5));  --Per-Channel gain, fixed + fractional (signed)
   constant cADDR_REG_CHANNEL_GAIN_5      : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(14, 5));  --Per Channel offset (signed)
   constant cADDR_REG_CHANNEL_OFFSET_5    : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(15, 5));  --Per-Channel gain, fixed + fractional (signed)
   constant cADDR_REG_CHANNEL_GAIN_6      : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(16, 5));  --Per Channel offset (signed)
   constant cADDR_REG_CHANNEL_OFFSET_6    : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(17, 5));  --Per-Channel gain, fixed + fractional (signed)
   constant cADDR_REG_CHANNEL_GAIN_7      : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(18, 5));  --Per Channel offset (signed)
   constant cADDR_REG_CHANNEL_OFFSET_7    : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(19, 5));  --Per-Channel gain, fixed + fractional (signed)
   
   --Control Register Bit Assignments
   constant cCONTROL_SUMMING_ENABLE       : integer := 0;   --Enable module, 0=Disable after current frame.
   constant cCONTROL_SIGNED_MODE          : integer := 1;   --Enable per-channel offset, 0=Disable.
   constant cCONTROL_OFFSET_ENABLE        : integer := 2;   --Enable per-channel offset, 0=Disable.
   constant cCONTROL_GAIN_ENABLE          : integer := 3;   --Enable per-channel gain, 0=Disable.
   constant cCONTROL_LAST_BIT             : integer := 4;   --Final bit in control register
   
   --Status Register Bit Assignments
   constant cSTATUS_BUSY_BIT              : integer := 0;   --Module is busy processing a packet
   constant cSTATUS_LAST_BIT              : integer := 1;   --Final bit in control register
   
   --------------------------------------------------------------------------
   -- Register Declarations
   --------------------------------------------------------------------------
   --Control Registers
   signal s_reg_control          : std_logic_vector(cCONTROL_LAST_BIT-1 downto 0);
   signal s_reg_status           : std_logic_vector(cSTATUS_LAST_BIT-1 downto 0);
   signal n_reg_sum_count        : natural range 0 to MAX_SUMMING_COUNT;
   signal n_reg_div_shift        : natural range 0 to log2_f( log2_f(MAX_SUMMING_COUNT) );   --Get the number of bits needed to perform a power of two shift equivalent to MAX_SUMMING_COUNT

   --Create record structure to hold gain offset values
   --Note:  Uses registers since there are only 8 channels, but high channel count implementations should use a RAM
   type gain_offset_record is
      record
         s_offset                : signed(cNUM_OFFSET_BITS-1 downto 0);       --Offset is signed, so include sign bit
         s_gain                  : signed(cNUM_GAIN_BITS-1 downto 0);         --Gain is signed, sign bit already included in constant
      end record;
   type gain_offset_type is array (0 to NUM_CHANNELS-1) of gain_offset_record;
   signal s_reg_gain_offset_table  : gain_offset_type;  --Gain offset register pairs for each ADC channel
   
   --Control Register Bit Aliases
   alias a_summing_enable        is s_reg_control(cCONTROL_SUMMING_ENABLE);   --Enable summing mode: 0=Pass-through, 1=Accumulate sums
   alias a_signed_mode           is s_reg_control(cCONTROL_SIGNED_MODE);      --Enable signed data: 0=Unsigned, 1=Signed
   alias a_offset_enable         is s_reg_control(cCONTROL_OFFSET_ENABLE);    --Enable global offset, 0=Disable.
   alias a_gain_enable           is s_reg_control(cCONTROL_GAIN_ENABLE);      --Enable global gain, 0=Disable.
   
   --Status Register Bits
   alias a_status_busy           is s_reg_status(cSTATUS_BUSY_BIT);           --Module is busy processing a packet (read only)
   
   --------------------------------------------------------------------------
   -- Signal Declarations
   --------------------------------------------------------------------------
   
   --Internal pipeline signals (start at 1, not 0 to match other signal names)
   signal s_sop_pipe             : std_logic_vector(cPIPELINE_LENGTH downto 1);
   signal s_eop_pipe             : std_logic_vector(cPIPELINE_LENGTH downto 1);
   signal s_valid_pipe           : std_logic_vector(cPIPELINE_LENGTH downto 1);
   type channel_array_type is array (1 to cPIPELINE_LENGTH) of natural range 0 to NUM_CHANNELS-1;
   signal n_channel_pipe         : channel_array_type;
   
   --Data pipeline signals
   --Note: Since module can run in signed/unsigned mode, all signed signals have 1 extra bit
   signal s1_data                : signed(S1_DATA_BITS-1 downto 0);
   signal s1_data_sum            : signed(S1_DATA_BITS-1 downto 0);
   signal s2_data                : signed(S2_DATA_MULT_BITS-1 downto 0);
   signal s2_data_mult           : signed(S2_DATA_MULT_BITS-1 downto 0);
   type accumulator_array_type is array (0 to NUM_CHANNELS-1) of signed(S3_ACCUMULATOR_BITS-1 downto 0);
   signal s3_accumulator_array   : accumulator_array_type;
   signal s4_trunc_data          : signed(S4_TRUNC_DATA_BITS-1 downto 0);
   signal s4_data                : signed(S4_DATA_BITS-1 downto 0);
   signal s5_data                : std_logic_vector(S5_DATA_BITS-1 downto 0);

   --packet_proc signals
   signal n1_packet_counter      : natural range 0 to MAX_SUMMING_COUNT-1 := 0;
   signal n1_packet_max_count    : natural range 0 to MAX_SUMMING_COUNT-1 := 0;
   signal s_sum_in_progress_pipe : std_logic_vector(cPIPELINE_LENGTH downto 1);
   signal s_first_packet_pipe    : std_logic_vector(cPIPELINE_LENGTH downto 1);
   signal s_final_packet_pipe    : std_logic_vector(cPIPELINE_LENGTH downto 1);
   
   
begin


   --========================================================================
   -- Component Instantiations
   --========================================================================

   --========================================================================
   -- Assign Output Signals
   --========================================================================
   aso_sop_out          <= s_sop_pipe(cPIPELINE_LENGTH);
   aso_eop_out          <= s_eop_pipe(cPIPELINE_LENGTH);
   aso_valid_out        <= s_valid_pipe(cPIPELINE_LENGTH);
   aso_data_out         <= s5_data;  --Get all relevant bits.  Signed/Unsigned data is preserved from last pipeline operation
   aso_channel_out      <= std_logic_vector(to_unsigned(n_channel_pipe(cPIPELINE_LENGTH), aso_channel_out'length));

   a_status_busy        <= '1' when a_summing_enable='1' or s_sum_in_progress_pipe /= std_logic_vector(to_unsigned(0, cPIPELINE_LENGTH)) else '0';
   
   --========================================================================
   -- Signal Pipeline
   -- Pipeline non-computational signals here
   -- Pipelines are sized as: (cPIPELINE_LENGTH downto 1) to match signal names.
   --========================================================================
   pipe_proc : process (reset, clk)
   begin
      if reset='1' then
         s_sop_pipe <= (others=>'0');
         s_eop_pipe <= (others=>'0');
         s_valid_pipe <= (others=>'0');
         for i in 1 to cPIPELINE_LENGTH loop
            n_channel_pipe(i) <= 0;
         end loop;
      elsif rising_edge(clk) then
         --Advance pipelines
         s_sop_pipe        <= s_sop_pipe(cPIPELINE_LENGTH-1 downto 1) & asi_sop_in;
         s_eop_pipe        <= s_eop_pipe(cPIPELINE_LENGTH-1 downto 1) & asi_eop_in;
         n_channel_pipe(1) <= to_integer(unsigned(asi_channel_in));
         for i in 2 to cPIPELINE_LENGTH loop
            n_channel_pipe(i) <= n_channel_pipe(i-1);
         end loop;
         
         --Final output is only valid for final accumulated packet, else all packets are valid if summing is disabled
         s_valid_pipe(cPIPELINE_LENGTH-1 downto 1) <= s_valid_pipe(cPIPELINE_LENGTH-2 downto 1) & asi_valid_in;   --All but last bit of s_valid_pipe are treated normally
         --Final bit in s_valid_pipe is gated, depending on whether summing is enabled and if it is the last packet to be accumulated
         if s_sum_in_progress_pipe(cPIPELINE_LENGTH-1)='1' then
            if s_final_packet_pipe(cPIPELINE_LENGTH-1)='1' then
               s_valid_pipe(cPIPELINE_LENGTH) <= s_valid_pipe(cPIPELINE_LENGTH-1);  --Assert valid signal since this is a summed packet
            else
               s_valid_pipe(cPIPELINE_LENGTH) <= '0'; --De-assert valid for non-summed packets
            end if;
         else
            s_valid_pipe(cPIPELINE_LENGTH) <= s_valid_pipe(cPIPELINE_LENGTH-1);  --Normal pipeline flow since summing is disabled
         end if;
         
      end if;
   end process;


   --========================================================================
   -- Packet counter
   -- Keeps track of each incoming packet and the current accumulator count
   --========================================================================
   packet_proc : process (reset, clk)
   begin
      if reset='1' then
         n1_packet_counter <= 0;
         n1_packet_max_count <= 0;
         s_sum_in_progress_pipe <= (others=>'0');
         s_first_packet_pipe <= (others=>'0');
         s_final_packet_pipe <= (others=>'0');
      elsif rising_edge(clk) then
      
         --Increment counter on sop so counter is valid for current packet
         if a_summing_enable='1' or s_sum_in_progress_pipe(1)='1' then
            if asi_sop_in='1' and asi_valid_in='1' then
               if n1_packet_counter = 0 then
                  n1_packet_max_count <= n_reg_sum_count;   --Get new terminal count at start of new accumulation cycle
               end if;
               s_sum_in_progress_pipe(1) <= '1';
            elsif asi_eop_in='1' and asi_valid_in='1' then
               --Update counter at end of every valid packet
               if n1_packet_counter < n1_packet_max_count then
                  n1_packet_counter <= n1_packet_counter + 1;  --Increment counter at start of each packet
               else
                  n1_packet_counter <= 0; --Reset for next time
                  s_sum_in_progress_pipe(1) <= '0';
               end if;
            end if;
         else
            n1_packet_counter <= 0;
            s_sum_in_progress_pipe(1) <= '0';
         end if;
         
         --Advance pipelined signals
         s_sum_in_progress_pipe(cPIPELINE_LENGTH-1 downto 2)  <= s_sum_in_progress_pipe(cPIPELINE_LENGTH-2 downto 1);  --Left shift by 1

         --Maintain first packet flag
         if (a_summing_enable='0') or ((a_summing_enable='1' or s_sum_in_progress_pipe(1)='1') and n1_packet_counter = 0) then
            if asi_sop_in='1' and asi_valid_in='1' then
               s_first_packet_pipe(1) <= '1';
            end if;
         else
            s_first_packet_pipe(1) <= '0';
         end if;
         --Advance pipeline
         s_first_packet_pipe(cPIPELINE_LENGTH downto 2)     <= s_first_packet_pipe(cPIPELINE_LENGTH-1 downto 1);  --Left shift by 1

         
         --Maintain final packet flag
         if (a_summing_enable='0') or ((a_summing_enable='1' or s_sum_in_progress_pipe(1)='1') and n1_packet_counter = n1_packet_max_count) then
            if asi_sop_in='1' and asi_valid_in='1' then
               s_final_packet_pipe(1) <= '1';
            end if;
         else
            s_final_packet_pipe(1) <= '0';
         end if;
         --Advance pipeline
         s_final_packet_pipe(cPIPELINE_LENGTH downto 2)     <= s_final_packet_pipe(cPIPELINE_LENGTH-1 downto 1);  --Left shift by 1
         
      end if;
   end process;


   --========================================================================
   -- Data Pipeline Operations
   --
   -- Note:  All math is signed, but results are limited to positive values
   --        if a_signed_mode='0'.
   --        Incoming asi_data_in is resized to add a sign bit.
   --        If a_signed_mode is set, asi_data_in is sign extended,
   --        else the new sign bit will be '0'.
   --        All subsequent math operations are signed until the final
   --        conversion to aso_data_out where a_signed_mode is used
   --        to determine the final output.
   --========================================================================
   
   s1_data_sum <= resize(signed(asi_data_in), s1_data'length) + s_reg_gain_offset_table(n_channel_pipe(1)).s_offset;
   s2_data_mult <= s1_data * s_reg_gain_offset_table(n_channel_pipe(1)).s_gain;
   s4_trunc_data <= s3_accumulator_array(n_channel_pipe(3))(S3_ACCUMULATOR_BITS-1 downto NUM_GAIN_FRACTIONAL_BITS-1); --Truncate all but msbit of fractional bits
   
   math_proc : process (reset, clk)
   begin
      if reset='1' then
         s1_data <= to_signed(0, s1_data'length);
         s2_data <= to_signed(0, s2_data'length);
         for i in 0 to NUM_CHANNELS-1 loop
            s3_accumulator_array(i) <= to_signed(0, S3_ACCUMULATOR_BITS);
         end loop;
         s4_data <= to_signed(0, s4_data'length);
         s5_data <= (others=>'0');
      elsif rising_edge(clk) then
      
         --------------------------------------------------
         --Stage 1:  Add offset to incoming data
         --          Clamp result to 0 if in unsigned mode
         --------------------------------------------------
         if a_offset_enable='1' then
            if a_signed_mode='0' and s1_data_sum < 0 then
               s1_data <= to_signed(0, s1_data'length);  --Clamp result to 0 for unsigned mode
            else
               s1_data <= s1_data_sum;
            end if;
         else
            if a_signed_mode='0' then
               s1_data <= signed("00" & asi_data_in);  --Resize unsigned data such that the sign bit is 0
            else
               s1_data <= resize(signed(asi_data_in), s1_data'length);  --Resize to sign extend incoming data
            end if;
         end if;
         
         --------------------------------------------------
         --Stage 2:  Apply gain
         --------------------------------------------------
         if s_valid_pipe(1)='1' then
            if a_gain_enable='1' then
               s2_data <= s2_data_mult;
            else
               s2_data <= s1_data * cGAIN_1p0;  --Multiply by a constant to shift bits
            end if;
         end if;

         --------------------------------------------------
         --Stage 3:  Accumulate results
         --Note:  May need to limit number of fractional bits
         --       accumulated to prevent > 32 adders
         --------------------------------------------------
         if s_valid_pipe(2)='1' then
            if s_first_packet_pipe(2) ='1' then
               --First data of new sum, so no summing this packet
               s3_accumulator_array(n_channel_pipe(2)) <= resize(s2_data, S3_ACCUMULATOR_BITS);
            else
               --Add new data to accumulator
               s3_accumulator_array(n_channel_pipe(2)) <= resize(s2_data, S3_ACCUMULATOR_BITS) + s3_accumulator_array(n_channel_pipe(2));
            end if;
         end if;

         --------------------------------------------------
         --Stage 4: Power of 2 divisor and fractional rounding.
         --         Arithmetic shift right by 'N' bits.
         --         Round final results positive or negative
         --         based on sign.
         --Note:    s4_data has one extra msbit and one
         --         fractional bit that is used for rounding.
         --         The lsbit is the highest bit of the fractional
         --         component and the msbit is used to detect
         --         an overflow/underflow after rounding.
         --------------------------------------------------
         if s_valid_pipe(3)='1' then
            if s4_trunc_data(s4_trunc_data'length-1)='0' then
               --Positive, so round up and chop final fractional bit
               s4_data <= shift_right(s4_trunc_data, n_reg_div_shift) + to_signed(1, S4_DATA_BITS);
            else
               --Negative, so round down and chop final fractional bit
               s4_data <= shift_right(s4_trunc_data, n_reg_div_shift) + to_signed(-1, S4_DATA_BITS);
            end if;
         end if;
         
         --------------------------------------------------
         --Stage 5:  Range check and clamp results
         --Note:     Truncate the lsbit of s4_data since
         --          rounding was done in Stage 4.
         --------------------------------------------------
         if s_valid_pipe(4)='1' then
            if a_signed_mode='0' then
               --Do unsigned range checking
               if s4_data(S4_DATA_BITS-1 downto 1) > to_signed(2**OUT_BITS_PER_SYMBOL-1, s4_data'length) then
                  s5_data <= std_logic_vector(to_unsigned(2**OUT_BITS_PER_SYMBOL-1, s5_data'length));   --Set to max unsigned output value with signed bit='0'
               elsif s4_data(S4_DATA_BITS-1 downto 1) < 0 then
                  s5_data <= std_logic_vector(to_signed(0, s5_data'length));  --Clamp to 0
               else
                  s5_data <= std_logic_vector(s4_data(s5_data'length downto 1)); --Drop s4_data msbit(s) for final result
               end if;
            else
               --Do signed range checking
               s5_data <= std_logic_vector(resize(s4_data(s4_data'length-1 downto 1), s5_data'length)); --Nothing to range check, just resize
            end if;
         end if;
         
      end if;
   end process;


   --========================================================================
   -- Register Read/Write Wait Request
   -- Implemented as a shift register.  The LS bit is the wait request signal.
   --========================================================================
   wait_req_proc : process (reset, clk)
   begin
      if reset='1' then
         avs_waitrequest <= '1';
      elsif rising_edge(clk) then
         if avs_chipselect = '1' then
            avs_waitrequest <= '0';
         else
            avs_waitrequest <= '1'; --Default state is asserted
         end if;
      end if;
   end process;


   --========================================================================
   -- Read registers
   --========================================================================
   reg_read_proc : process (reset, clk)
   begin
      if reset='1' then
         avs_readdata <= (others=>'0');
      elsif rising_edge(clk) then
--         if avs_chipselect='1' and avs_read='1' and avs_byteenable(1 downto 0)="11" then
         if avs_chipselect='1' and avs_read='1' then
            --Decode register addresses
            case avs_address(AVS_ADDRESS_BITS-1 downto 0) is
               when cADDR_REG_CONTROL =>
                  avs_readdata   <= std_logic_vector(resize(unsigned(s_reg_control), avs_readdata'length));
                  
               when cADDR_REG_STATUS =>
                  avs_readdata   <= std_logic_vector(resize(unsigned(s_reg_status), avs_readdata'length));

               when cADDR_REG_SUM_COUNT =>
                  avs_readdata   <= std_logic_vector(to_unsigned(n_reg_sum_count, avs_readdata'length));
                  
               when cADDR_REG_DIV_SHIFT =>
                  avs_readdata   <= std_logic_vector(to_unsigned(n_reg_div_shift, avs_readdata'length));
                  
               when cADDR_REG_CHANNEL_GAIN_0 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(0).s_gain, avs_readdata'length));
               when cADDR_REG_CHANNEL_OFFSET_0 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(0).s_offset, avs_readdata'length));

               when cADDR_REG_CHANNEL_GAIN_1 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(1).s_gain, avs_readdata'length));
               when cADDR_REG_CHANNEL_OFFSET_1 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(1).s_offset, avs_readdata'length));

               when cADDR_REG_CHANNEL_GAIN_2 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(2).s_gain, avs_readdata'length));
               when cADDR_REG_CHANNEL_OFFSET_2 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(2).s_offset, avs_readdata'length));

               when cADDR_REG_CHANNEL_GAIN_3 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(3).s_gain, avs_readdata'length));
               when cADDR_REG_CHANNEL_OFFSET_3 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(3).s_offset, avs_readdata'length));

               when cADDR_REG_CHANNEL_GAIN_4 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(4).s_gain, avs_readdata'length));
               when cADDR_REG_CHANNEL_OFFSET_4 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(4).s_offset, avs_readdata'length));

               when cADDR_REG_CHANNEL_GAIN_5 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(5).s_gain, avs_readdata'length));
               when cADDR_REG_CHANNEL_OFFSET_5 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(5).s_offset, avs_readdata'length));

               when cADDR_REG_CHANNEL_GAIN_6 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(6).s_gain, avs_readdata'length));
               when cADDR_REG_CHANNEL_OFFSET_6 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(6).s_offset, avs_readdata'length));

               when cADDR_REG_CHANNEL_GAIN_7 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(7).s_gain, avs_readdata'length));
               when cADDR_REG_CHANNEL_OFFSET_7 =>
                  avs_readdata   <= std_logic_vector(resize(s_reg_gain_offset_table(7).s_offset, avs_readdata'length));
                  
               when others =>
                  avs_readdata   <= (others=>'1');
            end case;
--         else
--            avs_readdata   <= (others=>'1');
         end if;
      end if;
   end process;   --End of reg_read_proc

   
   --========================================================================
   -- Write Registers
   --========================================================================
   reg_write_proc : process (reset, clk)
   begin
      if reset = '1' then 
         s_reg_control        <= std_logic_vector(to_unsigned(0, s_reg_control'length));
         n_reg_sum_count      <= 0;
         n_reg_div_shift      <= 0;
         for i in 0 to NUM_CHANNELS-1 loop
            s_reg_gain_offset_table(i).s_offset <= to_signed(0, cNUM_OFFSET_BITS);
            s_reg_gain_offset_table(i).s_gain   <= cGAIN_1p0; --Init to 1.0
         end loop;
         
      elsif rising_edge(clk) then
         --Decode register addresses
         if avs_chipselect = '1' and avs_write='1' then
            case avs_address(AVS_ADDRESS_BITS-1 downto 0) is
               when cADDR_REG_CONTROL =>
                  s_reg_control        <= avs_writedata(s_reg_control'length-1 downto 0);
               when cADDR_REG_STATUS =>
                  null;    --Non-Writeable register                  
               when cADDR_REG_SUM_COUNT =>
                  n_reg_sum_count      <= to_integer(unsigned(avs_writedata));
               when cADDR_REG_DIV_SHIFT =>
                  n_reg_div_shift      <= to_integer(unsigned(avs_writedata));
                  
               when cADDR_REG_CHANNEL_GAIN_0 =>
                  s_reg_gain_offset_table(0).s_gain <= signed(resize(signed(avs_writedata), cNUM_GAIN_BITS));
               when cADDR_REG_CHANNEL_OFFSET_0 =>
                  s_reg_gain_offset_table(0).s_offset <= signed(resize(signed(avs_writedata), cNUM_OFFSET_BITS));

               when cADDR_REG_CHANNEL_GAIN_1 =>
                  s_reg_gain_offset_table(1).s_gain <= signed(resize(signed(avs_writedata), cNUM_GAIN_BITS));
               when cADDR_REG_CHANNEL_OFFSET_1 =>
                  s_reg_gain_offset_table(1).s_offset <= signed(resize(signed(avs_writedata), cNUM_OFFSET_BITS));

               when cADDR_REG_CHANNEL_GAIN_2 =>
                  s_reg_gain_offset_table(2).s_gain <= signed(resize(signed(avs_writedata), cNUM_GAIN_BITS));
               when cADDR_REG_CHANNEL_OFFSET_2 =>
                  s_reg_gain_offset_table(2).s_offset <= signed(resize(signed(avs_writedata), cNUM_OFFSET_BITS));

               when cADDR_REG_CHANNEL_GAIN_3 =>
                  s_reg_gain_offset_table(3).s_gain <= signed(resize(signed(avs_writedata), cNUM_GAIN_BITS));
               when cADDR_REG_CHANNEL_OFFSET_3 =>
                  s_reg_gain_offset_table(3).s_offset <= signed(resize(signed(avs_writedata), cNUM_OFFSET_BITS));

               when cADDR_REG_CHANNEL_GAIN_4 =>
                  s_reg_gain_offset_table(4).s_gain <= signed(resize(signed(avs_writedata), cNUM_GAIN_BITS));
               when cADDR_REG_CHANNEL_OFFSET_4 =>
                  s_reg_gain_offset_table(4).s_offset <= signed(resize(signed(avs_writedata), cNUM_OFFSET_BITS));

               when cADDR_REG_CHANNEL_GAIN_5 =>
                  s_reg_gain_offset_table(5).s_gain <= signed(resize(signed(avs_writedata), cNUM_GAIN_BITS));
               when cADDR_REG_CHANNEL_OFFSET_5 =>
                  s_reg_gain_offset_table(5).s_offset <= signed(resize(signed(avs_writedata), cNUM_OFFSET_BITS));

               when cADDR_REG_CHANNEL_GAIN_6 =>
                  s_reg_gain_offset_table(6).s_gain <= signed(resize(signed(avs_writedata), cNUM_GAIN_BITS));
               when cADDR_REG_CHANNEL_OFFSET_6 =>
                  s_reg_gain_offset_table(6).s_offset <= signed(resize(signed(avs_writedata), cNUM_OFFSET_BITS));

               when cADDR_REG_CHANNEL_GAIN_7 =>
                  s_reg_gain_offset_table(7).s_gain <= signed(resize(signed(avs_writedata), cNUM_GAIN_BITS));
               when cADDR_REG_CHANNEL_OFFSET_7 =>
                  s_reg_gain_offset_table(7).s_offset <= signed(resize(signed(avs_writedata), cNUM_OFFSET_BITS));
                  
               when others =>
                  null;
            end case;
         end if;
      end if;
   end process;   --End of reg_write_proc

   
end avs_channel_summer_arch;
   
