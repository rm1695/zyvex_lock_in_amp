# +-----------------------------------
# | 
# | avs_channel_summer "AVS Channel Summer" v1.0
# | Jeff Short 2016-04-11
# | Data Acquisition
# | 
# | 
# +-----------------------------------


# +-----------------------------------
# | Module avs_channel_summer
# | 
set_module_property DESCRIPTION "Avalon-ST Channel Summer with Gain and Offset"
set_module_property NAME avs_channel_summer
set_module_property VERSION 1.0
set_module_property GROUP "Data Acquisition"
set_module_property AUTHOR "Jeff Short"
set_module_property DISPLAY_NAME "Avalon-ST Channel Summer"
set_module_property LIBRARIES {IEEE.std_logic_1164.all IEEE.numeric_std.all}
set_module_property TOP_LEVEL_HDL_FILE avs_channel_summer.vhd
set_module_property TOP_LEVEL_HDL_MODULE avs_channel_summer
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE false
#set_module_property DATASHEET_URL http://www.mywebpage.com/wiki/avs_channel_summer
set_module_property SIMULATION_MODEL_IN_VERILOG false
set_module_property SIMULATION_MODEL_IN_VHDL true
set_module_property SIMULATION_MODEL_HAS_TULIPS false
set_module_property SIMULATION_MODEL_IS_OBFUSCATED false
# | 
# +-----------------------------------


# +-----------------------------------
# | Files
# | 
add_file avs_channel_summer.vhd {SYNTHESIS SIMULATION}
# | 
# +-----------------------------------

# +-----------------------------------
# | Parameters
# | 
add_parameter IN_BITS_PER_SYMBOL NATURAL 18 "In Bits per symbol"
set_parameter_property IN_BITS_PER_SYMBOL DEFAULT_VALUE 18
set_parameter_property IN_BITS_PER_SYMBOL DISPLAY_NAME IN_BITS_PER_SYMBOL
set_parameter_property IN_BITS_PER_SYMBOL UNITS None
set_parameter_property IN_BITS_PER_SYMBOL ALLOWED_RANGES 4:24
set_parameter_property IN_BITS_PER_SYMBOL DESCRIPTION "In Bits per symbol"
set_parameter_property IN_BITS_PER_SYMBOL DISPLAY_HINT "Number of bits per input symbol"
set_parameter_property IN_BITS_PER_SYMBOL AFFECTS_GENERATION false
set_parameter_property IN_BITS_PER_SYMBOL AFFECTS_ELABORATION true
set_parameter_property IN_BITS_PER_SYMBOL HDL_PARAMETER true

add_parameter OUT_BITS_PER_SYMBOL NATURAL 28 "Out Bits per symbol"
set_parameter_property OUT_BITS_PER_SYMBOL DEFAULT_VALUE 28
set_parameter_property OUT_BITS_PER_SYMBOL DISPLAY_NAME OUT_BITS_PER_SYMBOL
set_parameter_property OUT_BITS_PER_SYMBOL UNITS None
set_parameter_property OUT_BITS_PER_SYMBOL ALLOWED_RANGES 6:32
set_parameter_property OUT_BITS_PER_SYMBOL DESCRIPTION "Out Bits per symbol"
set_parameter_property OUT_BITS_PER_SYMBOL DISPLAY_HINT "Number of bits per ouput symbol"
set_parameter_property OUT_BITS_PER_SYMBOL AFFECTS_GENERATION false
set_parameter_property OUT_BITS_PER_SYMBOL AFFECTS_ELABORATION true
set_parameter_property OUT_BITS_PER_SYMBOL HDL_PARAMETER true

add_parameter NUM_CHANNELS NATURAL 8 "Number of Channels"
set_parameter_property NUM_CHANNELS DEFAULT_VALUE 8
set_parameter_property NUM_CHANNELS DISPLAY_NAME NUM_CHANNELS
set_parameter_property NUM_CHANNELS UNITS None
set_parameter_property NUM_CHANNELS ALLOWED_RANGES 2:256
set_parameter_property NUM_CHANNELS DESCRIPTION "Number of channels"
set_parameter_property NUM_CHANNELS DISPLAY_HINT "Number of channels"
set_parameter_property NUM_CHANNELS AFFECTS_GENERATION false
set_parameter_property NUM_CHANNELS AFFECTS_ELABORATION true
set_parameter_property NUM_CHANNELS HDL_PARAMETER true

add_parameter NUM_CHANNEL_BITS NATURAL 3 "Number of Channel Bits"
set_parameter_property NUM_CHANNEL_BITS DEFAULT_VALUE 3
set_parameter_property NUM_CHANNEL_BITS DISPLAY_NAME NUM_CHANNEL_BITS
set_parameter_property NUM_CHANNEL_BITS UNITS None
set_parameter_property NUM_CHANNEL_BITS ALLOWED_RANGES 3:8
set_parameter_property NUM_CHANNEL_BITS DESCRIPTION "Number of channel bits"
set_parameter_property NUM_CHANNEL_BITS DISPLAY_HINT "Number of bits needed to hold channel count. Ex: 8 Channels = 3 bits"
set_parameter_property NUM_CHANNEL_BITS AFFECTS_GENERATION false
set_parameter_property NUM_CHANNEL_BITS AFFECTS_ELABORATION true
set_parameter_property NUM_CHANNEL_BITS HDL_PARAMETER true

add_parameter MAX_SUMMING_COUNT NATURAL 256 "Max Summing Count"
set_parameter_property MAX_SUMMING_COUNT DEFAULT_VALUE 256
set_parameter_property MAX_SUMMING_COUNT DISPLAY_NAME MAX_SUMMING_COUNT
set_parameter_property MAX_SUMMING_COUNT UNITS None
set_parameter_property MAX_SUMMING_COUNT ALLOWED_RANGES 4:1024
set_parameter_property MAX_SUMMING_COUNT DESCRIPTION "Max number of sums to accumulate"
set_parameter_property MAX_SUMMING_COUNT DISPLAY_HINT "Sets the size of the accumulator and summing counter"
set_parameter_property MAX_SUMMING_COUNT AFFECTS_GENERATION false
set_parameter_property MAX_SUMMING_COUNT AFFECTS_ELABORATION true
set_parameter_property MAX_SUMMING_COUNT HDL_PARAMETER true

add_parameter NUM_GAIN_INTEGER_BITS NATURAL 2 "Gain Integer Bits"
set_parameter_property NUM_GAIN_INTEGER_BITS DEFAULT_VALUE 2
set_parameter_property NUM_GAIN_INTEGER_BITS DISPLAY_NAME NUM_GAIN_INTEGER_BITS
set_parameter_property NUM_GAIN_INTEGER_BITS UNITS None
set_parameter_property NUM_GAIN_INTEGER_BITS ALLOWED_RANGES 1:4
set_parameter_property NUM_GAIN_INTEGER_BITS DESCRIPTION "Gain Integer Bits"
set_parameter_property NUM_GAIN_INTEGER_BITS DISPLAY_HINT "Number of bits to use for the integer portion of the gain register. Does not include the sign bit."
set_parameter_property NUM_GAIN_INTEGER_BITS AFFECTS_GENERATION false
set_parameter_property NUM_GAIN_INTEGER_BITS AFFECTS_ELABORATION true
set_parameter_property NUM_GAIN_INTEGER_BITS HDL_PARAMETER true

add_parameter NUM_GAIN_FRACTIONAL_BITS NATURAL 8 "Gain Fractional Bits"
set_parameter_property NUM_GAIN_FRACTIONAL_BITS DEFAULT_VALUE 8
set_parameter_property NUM_GAIN_FRACTIONAL_BITS DISPLAY_NAME NUM_GAIN_FRACTIONAL_BITS
set_parameter_property NUM_GAIN_FRACTIONAL_BITS UNITS None
set_parameter_property NUM_GAIN_FRACTIONAL_BITS ALLOWED_RANGES 2:12
set_parameter_property NUM_GAIN_FRACTIONAL_BITS DESCRIPTION "Gain Fractional Bits"
set_parameter_property NUM_GAIN_FRACTIONAL_BITS DISPLAY_HINT "Number of bits to use for the fractional portion of the gain register"
set_parameter_property NUM_GAIN_FRACTIONAL_BITS AFFECTS_GENERATION false
set_parameter_property NUM_GAIN_FRACTIONAL_BITS AFFECTS_ELABORATION true
set_parameter_property NUM_GAIN_FRACTIONAL_BITS HDL_PARAMETER true

add_parameter AVS_ADDRESS_BITS NATURAL 5 "Avalon Slave Address Bus Width"
set_parameter_property AVS_ADDRESS_BITS DEFAULT_VALUE 5
set_parameter_property AVS_ADDRESS_BITS DISPLAY_NAME AVS_DATA_BUS_WIDTH
set_parameter_property AVS_ADDRESS_BITS UNITS None
set_parameter_property AVS_ADDRESS_BITS ALLOWED_RANGES 5
set_parameter_property AVS_ADDRESS_BITS DESCRIPTION "Avalon Slave Address bus width in bits"
set_parameter_property AVS_ADDRESS_BITS DISPLAY_HINT "Avalon Slave Address bus width in bits"
set_parameter_property AVS_ADDRESS_BITS AFFECTS_GENERATION false
set_parameter_property AVS_ADDRESS_BITS AFFECTS_ELABORATION true
set_parameter_property AVS_ADDRESS_BITS HDL_PARAMETER true

add_parameter AVS_DATA_BUS_WIDTH NATURAL 32 "Avalon Slave Data Bus Width"
set_parameter_property AVS_DATA_BUS_WIDTH DEFAULT_VALUE 32
set_parameter_property AVS_DATA_BUS_WIDTH DISPLAY_NAME AVS_DATA_BUS_WIDTH
set_parameter_property AVS_DATA_BUS_WIDTH UNITS None
set_parameter_property AVS_DATA_BUS_WIDTH ALLOWED_RANGES 32
set_parameter_property AVS_DATA_BUS_WIDTH DESCRIPTION "Avalon Slave Data Bus Width in bits"
set_parameter_property AVS_DATA_BUS_WIDTH DISPLAY_HINT "Avalon Slave Data Bus Width in bits"
set_parameter_property AVS_DATA_BUS_WIDTH AFFECTS_GENERATION false
set_parameter_property AVS_DATA_BUS_WIDTH AFFECTS_ELABORATION true
set_parameter_property AVS_DATA_BUS_WIDTH HDL_PARAMETER true
# | 
# +-----------------------------------

# +-----------------------------------
# | Display items
# | 
add_display_item "Avalon ST Interface" IN_BITS_PER_SYMBOL PARAMETER
add_display_item "Avalon ST Interface" OUT_BITS_PER_SYMBOL PARAMETER
add_display_item "Avalon ST Interface" NUM_CHANNELS PARAMETER
add_display_item "Avalon ST Interface" NUM_CHANNEL_BITS PARAMETER
add_display_item "Accumulator Settings" MAX_SUMMING_COUNT PARAMETER
add_display_item "Accumulator Settings" NUM_GAIN_INTEGER_BITS PARAMETER
add_display_item "Accumulator Settings" NUM_GAIN_FRACTIONAL_BITS  PARAMETER
# | 
# +-----------------------------------


# +-----------------------------------
# | Connection point asi_clk
# | 
add_interface asi_clock clock end
set_interface_property asi_clock ENABLED true
add_interface_port asi_clock clk clk Input 1
# | 
# +-----------------------------------

# +-----------------------------------
# | Connection point asi_reset
# | 
add_interface asi_reset reset end
set_interface_property asi_reset associatedClock asi_clock
set_interface_property asi_reset ENABLED true
set_interface_property asi_reset synchronousEdges DEASSERT
add_interface_port asi_reset reset reset Input 1
# | 
# +-----------------------------------


set_module_property ELABORATION_CALLBACK decoder_elaboration_callback
proc decoder_elaboration_callback {} {
	set ibps [get_parameter_value IN_BITS_PER_SYMBOL]
	set obps [get_parameter_value OUT_BITS_PER_SYMBOL]
	set in_data_width [expr $ibps]
	set out_data_width [expr $obps]
	set the_channel_width [get_parameter_value NUM_CHANNEL_BITS]
	set the_avs_data_bus_width [get_parameter_value AVS_DATA_BUS_WIDTH]
   set the_avs_address_bits [get_parameter_value AVS_ADDRESS_BITS]
   set the_byteenablewidth [expr {$the_avs_data_bus_width / 8} ]

   # Output defines to system.h
   set_module_assignment embeddedsw.CMacro.IN_BITS_PER_SYMBOL        $ibps
   set_module_assignment embeddedsw.CMacro.OUT_SYMBOLS_PER_BEAT      $obps
   set_module_assignment embeddedsw.CMacro.NUM_CHANNELS              [get_parameter_value NUM_CHANNELS]
   set_module_assignment embeddedsw.CMacro.NUM_CHANNEL_BITS          $the_channel_width
   set_module_assignment embeddedsw.CMacro.MAX_SUMMING_COUNT         [get_parameter_value MAX_SUMMING_COUNT]
   set_module_assignment embeddedsw.CMacro.NUM_GAIN_INTEGER_BITS     [get_parameter_value NUM_GAIN_INTEGER_BITS]
   set_module_assignment embeddedsw.CMacro.NUM_GAIN_FRACTIONAL_BITS  [get_parameter_value NUM_GAIN_FRACTIONAL_BITS]
   
   # +-----------------------------------
   # | Connection point din
   # | 
   add_interface din avalon_streaming end
   set_interface_property din dataBitsPerSymbol $ibps
   set_interface_property din errorDescriptor ""
   set_interface_property din maxChannel 0
   set_interface_property din associatedClock asi_clock
   set_interface_property din associatedReset asi_reset
   set_interface_property din ENABLED true

   add_interface_port din asi_sop_in startofpacket Input 1
   add_interface_port din asi_eop_in endofpacket Input 1
   add_interface_port din asi_valid_in valid Input 1
   add_interface_port din asi_data_in data Input $ibps
   add_interface_port din asi_channel_in channel Input $the_channel_width
   # | 
   # +-----------------------------------
   
   
   # +-----------------------------------
   # | Connection point dout
   # | 
   add_interface dout avalon_streaming source
   set_interface_property dout dataBitsPerSymbol $obps
   set_interface_property dout errorDescriptor ""
   set_interface_property dout maxChannel 0
   set_interface_property dout associatedClock asi_clock
   set_interface_property dout associatedReset asi_reset
   set_interface_property dout ENABLED true

   add_interface_port dout aso_sop_out startofpacket Output 1
   add_interface_port dout aso_eop_out endofpacket Output 1
   add_interface_port dout aso_valid_out valid Output 1
   add_interface_port dout aso_data_out data Output $obps
   add_interface_port dout aso_channel_out channel Output $the_channel_width
   # | 
   # +-----------------------------------

   
   # +-----------------------------------
   # | Avalon-MM Slave Interface
   # | connection point av_mm
   # | 
   add_interface control avalon slave
   set_interface_property control addressAlignment DYNAMIC
#   set_interface_property control addressSpan $the_address_span
   set_interface_property control bridgesToMaster ""
   set_interface_property control burstOnBurstBoundariesOnly false
   set_interface_property control holdTime 0
   set_interface_property control isMemoryDevice false
   set_interface_property control isNonVolatileStorage false
   set_interface_property control linewrapBursts false
   set_interface_property control maximumPendingReadTransactions 0
   set_interface_property control minimumUninterruptedRunLength 1
   set_interface_property control printableDevice false
   set_interface_property control readLatency 0
   set_interface_property control readWaitTime 0
   set_interface_property control setupTime 0
   set_interface_property control timingUnits Cycles
   set_interface_property control writeWaitTime 0
   set_interface_property control associatedClock asi_clock
   set_interface_property control associatedReset asi_reset
   set_interface_property control ENABLED true

   add_interface_port control avs_chipselect chipselect Input 1
   add_interface_port control avs_read read Input 1
   add_interface_port control avs_write write Input 1
   add_interface_port control avs_address address Input $the_avs_address_bits
   add_interface_port control avs_readdata readdata Output $the_avs_data_bus_width
   add_interface_port control avs_writedata writedata Input $the_avs_data_bus_width
   add_interface_port control avs_waitrequest waitrequest Output 1
   add_interface_port control avs_byteenable byteenable Input $the_byteenablewidth
   # | 
   # +-----------------------------------

}
