# +-----------------------------------
# |
# | ad5791_dac "ad5791_dac" v1.0
# |
# +-----------------------------------

# +-----------------------------------
# | request TCL package from ACDS 9.1
# |
#package require -exact sopc 10.0
# |
# +-----------------------------------

# +-----------------------------------
# | module ad5791_dac
# |
set_module_property DESCRIPTION "AD5791 20-bit DAC Interface"
set_module_property NAME ad5791_dac
set_module_property VERSION 1.0
set_module_property GROUP "Data Acquisition"
set_module_property AUTHOR "Jeff Short"
set_module_property DISPLAY_NAME "AD5791 DAC Interface"
#set_module_property LIBRARIES {ieee.std_logic_1164.all ieee.std_logic_unsigned.all ieee.std_logic_arith.all std.standard.all}
set_module_property TOP_LEVEL_HDL_FILE ad5791_dac.vhd
set_module_property TOP_LEVEL_HDL_MODULE ad5791_dac
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
#set_module_property DATASHEET_URL http://www.mywebpage.com/wiki/ad5791_dac
set_module_property SIMULATION_MODEL_IN_VERILOG false
set_module_property SIMULATION_MODEL_IN_VHDL true
set_module_property SIMULATION_MODEL_HAS_TULIPS false
set_module_property SIMULATION_MODEL_IS_OBFUSCATED false
# |
# +-----------------------------------

# +-----------------------------------
# | files
# |
add_file ad5791_dac.vhd {SYNTHESIS SIMULATION}
#add_file ddr_out1.vhd {SYNTHESIS SIMULATION}
# |
# +-----------------------------------


# +-----------------------------------
# | parameters
# |
add_parameter clock_freq INTEGER
set_parameter_property clock_freq SYSTEM_INFO {CLOCK_RATE "avs_clk"}


add_parameter BITS_PER_SYMBOL NATURAL 20 "Bits per symbol"
set_parameter_property BITS_PER_SYMBOL DEFAULT_VALUE 20
set_parameter_property BITS_PER_SYMBOL DISPLAY_NAME BITS_PER_SYMBOL
set_parameter_property BITS_PER_SYMBOL UNITS None
set_parameter_property BITS_PER_SYMBOL ALLOWED_RANGES 8:24
set_parameter_property BITS_PER_SYMBOL DESCRIPTION "DAC bits per sample"
set_parameter_property BITS_PER_SYMBOL DISPLAY_HINT "Number of bits per sample"
set_parameter_property BITS_PER_SYMBOL AFFECTS_GENERATION false
set_parameter_property BITS_PER_SYMBOL HDL_PARAMETER true

add_parameter NUM_DAC_CHANNELS NATURAL 8 "Number of DAC channels"
set_parameter_property NUM_DAC_CHANNELS DEFAULT_VALUE 8
set_parameter_property NUM_DAC_CHANNELS DISPLAY_NAME NUM_DAC_CHANNELS
set_parameter_property NUM_DAC_CHANNELS UNITS None
set_parameter_property NUM_DAC_CHANNELS ALLOWED_RANGES 8:8
set_parameter_property NUM_DAC_CHANNELS DESCRIPTION "Number of DAC channels"
set_parameter_property NUM_DAC_CHANNELS DISPLAY_HINT "Number of DAC channels to create"
set_parameter_property NUM_DAC_CHANNELS AFFECTS_GENERATION false
set_parameter_property NUM_DAC_CHANNELS HDL_PARAMETER true

add_parameter DEFAULT_INT_CLK_DIV NATURAL 1 "DAC Reference clock divider"
set_parameter_property DEFAULT_INT_CLK_DIV DEFAULT_VALUE 1
set_parameter_property DEFAULT_INT_CLK_DIV DISPLAY_NAME DEFAULT_INT_CLK_DIV
set_parameter_property DEFAULT_INT_CLK_DIV UNITS None
set_parameter_property DEFAULT_INT_CLK_DIV ALLOWED_RANGES 0:1023
set_parameter_property DEFAULT_INT_CLK_DIV DESCRIPTION "DAC Reference clock divider"
set_parameter_property DEFAULT_INT_CLK_DIV DISPLAY_HINT "Divides input clock to an internal 2X shift clock rate. Set to N-1."
set_parameter_property DEFAULT_INT_CLK_DIV AFFECTS_GENERATION false
set_parameter_property DEFAULT_INT_CLK_DIV HDL_PARAMETER true

add_parameter DEFAULT_SAMPLE_RATE_DIV NATURAL 49 "DAC sample rate divider"
set_parameter_property DEFAULT_SAMPLE_RATE_DIV DEFAULT_VALUE 49
set_parameter_property DEFAULT_SAMPLE_RATE_DIV DISPLAY_NAME DEFAULT_SAMPLE_RATE_DIV
set_parameter_property DEFAULT_SAMPLE_RATE_DIV UNITS None
set_parameter_property DEFAULT_SAMPLE_RATE_DIV ALLOWED_RANGES 49:1023
set_parameter_property DEFAULT_SAMPLE_RATE_DIV DESCRIPTION "DAC sample rate divider"
set_parameter_property DEFAULT_SAMPLE_RATE_DIV DISPLAY_HINT "Number of INT_CLK's between sending output words to DAC.  Set to N-1.  Remember that INT_CLK is a 2X clock, so set this to 2X desired shift clocks."
set_parameter_property DEFAULT_SAMPLE_RATE_DIV AFFECTS_GENERATION false
set_parameter_property DEFAULT_SAMPLE_RATE_DIV HDL_PARAMETER true

add_parameter AVALON_MM_BUS_WIDTH NATURAL 32 "Avalon MM Data Bus Width"
set_parameter_property AVALON_MM_BUS_WIDTH DEFAULT_VALUE 32
set_parameter_property AVALON_MM_BUS_WIDTH DISPLAY_NAME AVALON_MM_BUS_WIDTH
set_parameter_property AVALON_MM_BUS_WIDTH UNITS None
set_parameter_property AVALON_MM_BUS_WIDTH ALLOWED_RANGES 32
set_parameter_property AVALON_MM_BUS_WIDTH DESCRIPTION "Avalon MM Data Bus Width in bits"
set_parameter_property AVALON_MM_BUS_WIDTH DISPLAY_HINT "Avalon MM Data Bus Width in bits"
set_parameter_property AVALON_MM_BUS_WIDTH AFFECTS_GENERATION false
set_parameter_property AVALON_MM_BUS_WIDTH AFFECTS_ELABORATION true
set_parameter_property AVALON_MM_BUS_WIDTH HDL_PARAMETER true
# |
# +-----------------------------------

# +-----------------------------------
# | display items
# |
add_display_item "DAC Parameters" clock_freq PARAMETER
add_display_item "DAC Parameters" BITS_PER_SYMBOL PARAMETER
add_display_item "DAC Parameters" NUM_DAC_CHANNELS PARAMETER
add_display_item "DAC Parameters" DEFAULT_INT_CLK_DIV PARAMETER
add_display_item "DAC Parameters" DEFAULT_SAMPLE_RATE_DIV PARAMETER

add_display_item "Avalon MM Interface" AVALON_MM_BUS_WIDTH PARAMETER
# |
# +-----------------------------------

# +-----------------------------------
# | connection point avs_clk
# |
add_interface avs_clk clock end
set_interface_property avs_clk ENABLED true
add_interface_port avs_clk avs_clk clk Input 1
# |
# +-----------------------------------

# +-----------------------------------
# | connection point avs_reset
# |
add_interface avs_reset reset end
set_interface_property avs_reset associatedClock avs_clk
set_interface_property avs_reset ENABLED true
set_interface_property avs_reset synchronousEdges DEASSERT
add_interface_port avs_reset avs_reset reset Input 1
# |
# +-----------------------------------

# +-----------------------------------
# | connection point control_irq
# |
add_interface control_irq interrupt end
set_interface_property control_irq associatedAddressablePoint control
set_interface_property control_irq ASSOCIATED_CLOCK avs_clk
set_interface_property control_irq ENABLED true
add_interface_port control_irq avs_irq irq Output 1
# |
# +-----------------------------------



set_module_property ELABORATION_CALLBACK decoder_elaboration_callback
proc decoder_elaboration_callback {} {
   set the_avalon_mm_bus_width [get_parameter_value AVALON_MM_BUS_WIDTH]
   set the_byteenablewidth [expr {$the_avalon_mm_bus_width / 8} ]
   set the_clock_freq  [get_parameter_value "clock_freq"]
   set the_num_dac_channels [get_parameter_value NUM_DAC_CHANNELS]

   # Output values to system.h
   set_module_assignment embeddedsw.CMacro.clock_freqUENCY          $the_clock_freq
   set_module_assignment embeddedsw.CMacro.BITS_PER_SYMBOL              [get_parameter_value BITS_PER_SYMBOL]
   set_module_assignment embeddedsw.CMacro.NUM_DAC_CHANNELS             [get_parameter_value NUM_DAC_CHANNELS]
   set_module_assignment embeddedsw.CMacro.DEFAULT_INT_CLK_DIV          [get_parameter_value DEFAULT_INT_CLK_DIV]
   set_module_assignment embeddedsw.CMacro.DEFAULT_SAMPLE_RATE_DIV      [get_parameter_value DEFAULT_SAMPLE_RATE_DIV]


   # +-----------------------------------
   # | connection point ext_dac_if
   # |
   add_interface ext_dac_if conduit source
   set_interface_property ext_dac_if ENABLED true
   add_interface_port ext_dac_if dac_clr_out_n         export Output 1
   add_interface_port ext_dac_if dac_reset_out_n       export Output 1
   add_interface_port ext_dac_if dac_sclk_out          export Output 1
   add_interface_port ext_dac_if dac_sync_out_n        export Output 1
   add_interface_port ext_dac_if dac_ldac_out_n        export Output 1
   add_interface_port ext_dac_if dac_sdin_to_dac       export Output $the_num_dac_channels
   add_interface_port ext_dac_if dac_sdo_from_dac      export Input $the_num_dac_channels
   # |
   # +-----------------------------------


   # +-----------------------------------
   # | Avalon-MM Slave Interface
   # | connection point av_mm
   # |
   add_interface control avalon slave
   set_interface_property control addressAlignment DYNAMIC
#   set_interface_property control addressSpan $the_address_span
   set_interface_property control bridgesToMaster ""
   set_interface_property control burstOnBurstBoundariesOnly false
   set_interface_property control holdTime 0
   set_interface_property control isMemoryDevice false
   set_interface_property control isNonVolatileStorage false
   set_interface_property control linewrapBursts false
   set_interface_property control maximumPendingReadTransactions 0
   set_interface_property control minimumUninterruptedRunLength 1
   set_interface_property control printableDevice false
   set_interface_property control readLatency 1
   set_interface_property control readWaitTime 0
   set_interface_property control setupTime 0
   set_interface_property control timingUnits Cycles
   set_interface_property control writeWaitTime 0
   set_interface_property control associatedClock avs_clk
   set_interface_property control associatedReset avs_reset
   set_interface_property control ENABLED true

   add_interface_port control avs_chipselect chipselect Input 1
   add_interface_port control avs_read read Input 1
   add_interface_port control avs_write write Input 1
   add_interface_port control avs_address address Input 6
   add_interface_port control avs_readdata readdata Output $the_avalon_mm_bus_width
   add_interface_port control avs_writedata writedata Input $the_avalon_mm_bus_width
   add_interface_port control avs_waitrequest waitrequest Output 1
   add_interface_port control avs_byteenable byteenable Input $the_byteenablewidth
   # |
   # +-----------------------------------

   # +-----------------------------------
   # | connection point adc_fifo
   # |
   add_interface adc_fifo conduit source
   set_interface_property adc_fifo associatedClock avs_clk
   set_interface_property adc_fifo associatedReset avs_reset
   add_interface_port adc_fifo fifo_start trigger Input 1
   # |
   # +-----------------------------------

   # +-----------------------------------
   # | connection point lock_in
   # |
   add_interface lock_in conduit source
   set_interface_property lock_in associatedClock avs_clk
   set_interface_property lock_in associatedReset avs_reset
   add_interface_port lock_in lock_in_enable control Input 1
   add_interface_port lock_in lock_in_address address Input 6
   add_interface_port lock_in lock_in_write write Input 1
   add_interface_port lock_in lock_in_writedata writedata Input 32
   # |
   # +-----------------------------------
}
