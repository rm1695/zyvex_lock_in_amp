----------------------------------------------------------------------------
-- Date:    2016-02-02
-- File:    ad5791_dac.vhd
-- By:      Jeff Short
--
-- Desc:    AD5791 serial DAC output control and timing generator
--
-- Notes:   This file REQUIRES the use of VHDL-2008 for the "case?" keyword!!!
--
--Algorithm:
--
-- Supports 'N' AD5791 DAC's that share common control signals.
-- Supports full read and writes to DAC registers.
-- Includes a stand-alone timing generator and clock divider.
--
--
------------------------------------------------------------------------------
-- Register Definitions:
-- -----------------------------------------------------------------------
-- Name               Reg# Offset   R/W   Description
-- -----------------------------------------------------------------------
-- reg_control           0 (0x00)   R/W
--    [0]: Global enable            R/W   Global enable bit: 1=Enable, 0=Disable after current frame completes.
--    [1]: Output mode              R/W   0=Software write triggered output mode, 1=Fixed-Rate output mode
--    [2]: DAC clear request        W     1=Request all DAC's to be cleared.  Bit auto clears to 0.
--    [3]: DAC reset request        W     1=Request all DAC's to be reset.  Bit auto clears to 0.
--    [4]: Interrupt enable         R/W   1=Enable data ready interrupts (interrupt when wr data regs are empty and at end of read operation)
--
--    [5]: Continuous test mode     R/W   1=Enable continuous output test mode.
--                                           Output Mode must be set to '1' (Fixed Rate)
--                                           Data in the dout registers is continuously shifted out even if the dac_rw_ctl register is not written to.
-- reg_status            1 (0x04)   R(W)
--    [0]: DAC data out full        R     Data output register(s) are full.  This register is double buffered,
--                                           so it is okay to write next data while the shifter is busy.
--                                           Cleared when transfers is started and data is transferred to the shift registers.
--    [1]: DAC shift register busy  R     DAC shift register is busy.
--                                           Cleared at end of current cycle unless another output is pending
--    [2]: Interrupt flag           R/WC  Pending interrupt.  Write a '1' to clear this bit.
--
-- reg_sample_rate_div   2 (0x08)   R/W   DAC output rate divider.  Ranges from 1KHz to 800KHz.
--   [9:0]                                Set to N-1 where N is the desired divider denominator.
--                                           Numerator is the fixed DAC_INT_CLK_IN.
--                                           Max allowed output rate is ~800KHz with 2.5V DAC I/O interface.
--                                           Default divider = REF_CLK_FREQ / SAMPLE_RATE_FREQ -1
--                                           Example: 1Msps: REF_CLK_FREQ=100MHz, SAMPLE_RATE_FREQ=1MHz
--                                                 Divider = (100e6 / 1e6) -1 = 99
--
-- reg_dac_ref_clk_div   3 (0x0C)   R/W   DAC reference clock rate divider.  Divides bus clock down to 2X shift rate clock
--   [9:0]                                Set to N-1 where N is the desired divider denominator.
--                                           The output is the DAC reference clock rate divided by N.
--                                           Max allowed shift clock rate is 25MHz (40ns) with 2.5V DAC I/O interface.
--
-- reg_dac_enable_mask   4 (0x10)   R/W   DAC channel enable mask.  Each bit enables corresponding DAC channel.
-- NOTE:  THIS REGISTER IS NOT CURRENTLY IMPLEMENTED DUE TO ALL DAC'S SHARING HARDWARE CONTROL PINS
--   [NUM_DAC_CHANNELS-1:0]               Setting each bit enables the corresponding DAC to be updated.
--
-- reg_dac_din(0 to N-1) 8 (0x20)   R/W   DAC data input register array of 'N' registers, where N is the number of DAC's
--   [23:0]                               20-bits to shift.  Shifted out MSB first.
--
-- reg_dac_dout(0 to N-1) 16(0x40)  R/W   DAC data output register array of 'N' registers, where N is the number of DAC's
--   [19:0]                               20-bits to shift.  Shifted out MSB first.
--
--
-- reg_dac_rw_ctl        24 (0x60)  R/W   DAC read/write control.  Write to this register to start transfers to all DAC's.
--   [3:0]                                Bits[3:0] are sent to all dac's as bits [23:20].
--                                        Bit[3] (aka DAC Bit[23]) = R/W flag.  1=Read, 0=Write
--                                        Bits[2:0] (aka DAC Bits[22:20]) = Register address ("001" for DAC output).
--
-- reg_dac_sigma			31 (0x70)  W	  Sigma Delta control. Write to this register to enable (1) or disable (0) sigma delta 
--
--
------------------------------------------------------------------------------
--
--Revisions:
--
-- 20160301 - Jeff Short
--    Adapted for use with AD5791 D/A Converter.
--
-- 20160324 - Jeff Short
--    Changed register address map so that the reg_dac_rw_ctl follows reg_dac_dout(7)
--    so software can perform a burst write to update the DAC outputs.
--
-- 20170625 - Robert Mason
--		Adapted for 24 bit input
--		Added Sigma Delta modulation, set up using a control regISTER
--		  -This register is only one bit and is written as either allowed or disallowed
--		  -Uses separate data path, delayed by 1 clock cycle
--
-- 20171003 - Robert Mason
--		Added data_flow connections
--
--
-- 20190728 - Robert Mason
--		Repurposed for use with Lock_In_Amplifier
--
------------------------------------------------------------------------------

--Include this line to use VHDL 2008 since the "case?" is used in this design
-- altera vhdl_input_version vhdl_2008
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--use ieee.math_real.all;

LIBRARY altera_mf;
USE altera_mf.all;


entity ad5791_dac is
   generic (
      DAC_REF_CLK_FREQ_HZ     : natural := 50;                --Frequency of incoming reference clock, in Hz
      BITS_PER_SYMBOL         : natural range 4 to 24  := 20;  --Number of bits per symbol
      NUM_DAC_CHANNELS        : natural range 1 to 32  := 8;   --Number of analog to digital converter chips in parallel
      MAX_INT_CLK_DIV         : integer := 1023;               --Max count of dac int clock divider (Must be >= 29*SAMPLE_RATE_DIV, since need 29 clocks per output sample)
      DEFAULT_INT_CLK_DIV     : integer := (50/(25*2))-1;     --2X shift clock frequency.  Max rate is 2*25MHz. With 100MHz bus clock, set to 2-1=1 to get 50MHz with 25MHz shift rate.
      MAX_SAMPLE_RATE_DIV     : integer := 1023;               --Max count of DAC output rate divider
      DEFAULT_SAMPLE_RATE_DIV : integer := 2*25-1;             --Divide by N-1. With 50MHz int clock, 1MHz SCLK = (25MHz/1MHz-1 = 25-1 = 24
      AVALON_MM_BUS_WIDTH     : integer := 32                  --Number of data bits to use for Avalon Memory-Mapped Slave bus
   );
   port
   (
--      ref_sync_in             : in  std_logic;  --Reference input to sync adc samples with an external signal

      --DAC Signals
      dac_clr_out_n           : out std_logic := '1';  --Clear output to dac, active low
      dac_reset_out_n         : out std_logic := '1';  --Reset output to dac, active low
      dac_sclk_out            : out std_logic := '1';  --Serial shift clock output to dac
      dac_sync_out_n          : out std_logic := '1';  --Sync (chip select) output to dac
      dac_ldac_out_n          : out std_logic := '1';  --DAC load output to dac
      dac_sdin_to_dac         : out std_logic_vector(NUM_DAC_CHANNELS-1 downto 0); --Output data from DAC's
      dac_sdo_from_dac        : in  std_logic_vector(NUM_DAC_CHANNELS-1 downto 0); --Read data input from DAC's
      
      --Avalon-MM Slave Interface
      avs_reset               : in  std_logic;
      avs_clk                 : in  std_logic;  --Continuous reference clock used to generate timing for adc clock
      avs_irq                 : out std_logic;
      avs_chipselect          : in  std_logic;
      avs_read                : in  std_logic;
      avs_write               : in  std_logic;
      avs_address             : in  std_logic_vector(5 downto 0); --Set large enough to handle control registers + 32 DAC channel registers
      avs_byteenable          : in  std_logic_vector((AVALON_MM_BUS_WIDTH/8)-1 downto 0);
      avs_readdata            : out std_logic_vector(AVALON_MM_BUS_WIDTH-1 downto 0);
      avs_writedata           : in  std_logic_vector(AVALON_MM_BUS_WIDTH-1 downto 0);
      avs_waitrequest         : out std_logic;
		
		lock_in_enable		      : in std_logic:='0';
		lock_in_address		   : in std_logic_vector(5 downto 0);
		lock_in_write		      : in std_logic;
		lock_in_writedata	      : in std_logic_vector(31 downto 0);
		
		fifo_start				   : in std_logic:='0'
   );
end ad5791_dac;

--===========================================================================
--===========================================================================
architecture ad5791_dac_arch of ad5791_dac is

   --------------------------------------------------------------------------
   -- Constant Definitions
   --------------------------------------------------------------------------
   --Timing delay constants
   constant c_DAC_CLEAR_RESET_DLY      : natural := 10;  --Need max of:  /CLEAR >=50ns, /RESET >=35ns
   constant c_DAC_WR_TO_RD_DLY         : natural := 4;   --Assuming that /SYNC must be high >=48ns between write and read phases w/25MHz clock
   constant c_MAX_DELAY_COUNT          : natural := 31;	--Note: must be the greater of c_DAC_CLEAR_RESET_DLY and c_DAC_WR_TO_RD_DLY
   
   constant NUM_CONTROL_BITS           : natural := 4;   --Number of control bits per DAC
   constant NUM_SHIFT_BITS             : natural := NUM_CONTROL_BITS + BITS_PER_SYMBOL;
   
   --AD5791 DAC commands
   constant cWR_DAC_CMD                : std_logic_vector(3 downto 0) := "0001";    --AD5791 DAC register write command
   
   --Register Address Definitions
   constant cADDR_REG_CONTROL          : std_logic_vector(5 downto 0) := "000000";  --Control register
   constant cADDR_REG_STATUS           : std_logic_vector(5 downto 0) := "000001";  --Status register - read only, write to clear flags
   constant cADDR_REG_SAMPLE_RATE_DIV  : std_logic_vector(5 downto 0) := "000010";  --ADC sample rate divider
   constant cADDR_REG_DAC_SHIFT_CLK_DIV: std_logic_vector(5 downto 0) := "000011";  --DAC shift clock rate divider
   constant cADDR_REG_DAC_ENABLE_MASK  : std_logic_vector(5 downto 0) := "000100";  --DAC read/write control and DAC output bits[23:20]
   constant cADDR_REG_DAC_DIN_BASE     : std_logic_vector(5 downto 0) := "001000";  --DAC read back data
   constant cADDR_REG_DAC_DIN_BASE_XXX : std_logic_vector(5 downto 0) := "001---";  --DAC read back data
   constant cADDR_REG_DAC_DIN_0        : std_logic_vector(5 downto 0) := "001000";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DIN_1        : std_logic_vector(5 downto 0) := "001001";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DIN_2        : std_logic_vector(5 downto 0) := "001010";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DIN_3        : std_logic_vector(5 downto 0) := "001011";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DIN_4        : std_logic_vector(5 downto 0) := "001100";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DIN_5        : std_logic_vector(5 downto 0) := "001101";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DIN_6        : std_logic_vector(5 downto 0) := "001110";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DIN_7        : std_logic_vector(5 downto 0) := "001111";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DOUT_BASE    : std_logic_vector(5 downto 0) := "001000";  --First DAC output data bits[19:0]
   constant cADDR_REG_DAC_DOUT_BASE_XXX: std_logic_vector(5 downto 0) := "010---";  --First DAC output data bits[19:0]
   constant cADDR_REG_DAC_DOUT_0       : std_logic_vector(5 downto 0) := "010000";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DOUT_1       : std_logic_vector(5 downto 0) := "010001";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DOUT_2       : std_logic_vector(5 downto 0) := "010010";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DOUT_3       : std_logic_vector(5 downto 0) := "010011";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DOUT_4       : std_logic_vector(5 downto 0) := "010100";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DOUT_5       : std_logic_vector(5 downto 0) := "010101";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DOUT_6       : std_logic_vector(5 downto 0) := "010110";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_DOUT_7       : std_logic_vector(5 downto 0) := "010111";  --DAC output data bits[19:0]
   constant cADDR_REG_DAC_RW_CTL       : std_logic_vector(5 downto 0) := "011000";  --DAC read/write control and DAC output bits[23:20]
	
	constant cADDR_REG_DAC_RESET			: std_logic_vector (5 downto 0):= "011010";
	
	constant cADDR_REG_DAC_SIGMA			: std_logic_vector(5 downto 0) := "011111";  --Sigma Delta enable
	constant cADDR_REG_FIFO_ENABLE		: std_logic_vector(5 downto 0) := "111110";
	constant cADDR_REG_FIFO_TIME			: std_logic_vector(5 downto 0) := "111111"; 	--Fifo time, in us
	
   constant cADDR_REG_DAC_AUTOMATIC		: std_logic_vector(5 downto 0) := "111110";
	constant cADDR_REG_DAC_AUTO_SET		: std_logic_vector(5 downto 0) := "111111";
   --Note:  Subsequent DAC's occupy the remaining addresses as needed
   
	constant cADDR_REG_UNDERFLOW_0		: std_logic_vector(5 downto 0) := "100000";
	constant cADDR_REG_UNDERFLOW_1		: std_logic_vector(5 downto 0) := "100001";
	constant cADDR_REG_UNDERFLOW_2		: std_logic_vector(5 downto 0) := "100010";
	constant cADDR_REG_UNDERFLOW_3		: std_logic_vector(5 downto 0) := "100011";
	constant cADDR_REG_UNDERFLOW_4		: std_logic_vector(5 downto 0) := "100100";
	constant cADDR_REG_UNDERFLOW_5		: std_logic_vector(5 downto 0) := "100101";
	constant cADDR_REG_UNDERFLOW_6		: std_logic_vector(5 downto 0) := "100110";
	constant cADDR_REG_UNDERFLOW_7		: std_logic_vector(5 downto 0) := "100111";
   
   --------------------------------------------------------------------------
   -- Register Declarations
   --------------------------------------------------------------------------
   --Control Registers
   signal s_reg_control                : std_logic_vector(5 downto 0) := (others=>'0');
   signal s_reg_status                 : std_logic_vector(2 downto 0) := (others=>'0');
   signal n_reg_sample_rate_div        : natural range 0 to MAX_SAMPLE_RATE_DIV;
   signal n_reg_int_clk_div            : natural range 0 to MAX_INT_CLK_DIV;
   signal s_reg_dac_enable_mask        : std_logic_vector(NUM_DAC_CHANNELS-1 downto 0);
   signal s_reg_dac_rw_ctl             : std_logic_vector(3 downto 0);
   
   --Data Register Array
   type   dac_dout_array             is array (0 to NUM_DAC_CHANNELS-1) of std_logic_vector(BITS_PER_SYMBOL-1 downto 0);
   signal s_reg_dac_dout               : dac_dout_array := (others => (others => '0'));
	signal s_reg_dac_dout_sigma			: dac_dout_array := (others => (others => '0'));
	signal s_reg_dac_dout_sigma_in		: dac_dout_array := (others => (others => '0'));
   
   --Shift Register Array (Note: includes s_reg_dac_rw_ctl + s_reg_dac_dout(i))
   type   shift_data_array           is array (0 to NUM_DAC_CHANNELS-1) of std_logic_vector(NUM_SHIFT_BITS-1 downto 0);
   signal s_shift_data                 : shift_data_array := (others => (others => '0'));
   
   --Control Register Bit Aliases
   alias a_global_enable               is s_reg_control(0); --Enable module, 0=Disable after current frame.
   alias a_global_output_mode          is s_reg_control(1); --0=Output DAC data when s_reg_dac_rw_ctl is written. 1=Output data at a fixed rate.
   alias a_global_dac_clear_req        is s_reg_control(2); --Write a 1 to request that the DAC /CLR signal be pulsed low.
   alias a_global_dac_reset_req        is s_reg_control(3); --Write a 1 to request that the DAC /RESET signal be pulsed low.
   alias a_global_irq_enable           is s_reg_control(4); --Enable interrupts.
   alias a_global_continuous_test_mode is s_reg_control(5); --1=continuously output dout data for testing, 0=normal operation only out when new data is written
   
   --Status Register Bits
   alias a_status_dac_data_full        is s_reg_status(0);  --DAC data registers contain data that has not been transferred to the output shift registers
   alias a_status_dac_shift_busy       is s_reg_status(1);  --DAC shift register is busy.  Is cleared at end of current cycle unless another output is pending
   alias a_status_irq_flag             is s_reg_status(2);  --Interrupt flag.  Asserted whenever the dac data registers are empty
   
   --------------------------------------------------------------------------
   -- Signal Declarations
   --------------------------------------------------------------------------
	signal sigma_delta_enable		: std_logic := '0';
	
   signal s_dac_clr_out_n        : std_logic := '1';
   signal s_dac_reset_out_n      : std_logic := '1';
   signal s_dac_sync_out_n       : std_logic := '1';

   signal s_dac_clk_en           : std_logic;
   signal s_dac_int_clk_en       : std_logic := '1';
   signal n_dac_int_clk_counter  : natural range 0 to MAX_INT_CLK_DIV;
   
   signal n_cycle_counter        : natural range 0 to MAX_SAMPLE_RATE_DIV := 0; --ADC sample interval tCYCLE counter
   signal s_start_dac_pulse      : std_logic := '0';

   signal s_sm_delay_ctr_en      : std_logic := '0';
   signal n_sm_delay_ctr         : natural range 0 to c_MAX_DELAY_COUNT;
   signal s_read_mode            : std_logic := '0';
   
   signal s_dac_rw_ctl_req       : std_logic := '0';
   signal s_set_rw_ctl_req       : std_logic := '0';
   signal s_clr_rw_ctl_req       : std_logic := '0';
   
   signal s_clr_dac_data_full    : std_logic := '0';
   signal s_set_dac_data_full    : std_logic := '0';
   
   signal s_dac_rd_data_valid    : std_logic := '0';
   signal s_clr_rd_data_valid    : std_logic := '0';
   signal s_set_rd_data_valid    : std_logic := '0';
   
   signal s_clr_irq_flag         : std_logic := '0';
   signal s_set_irq_flag         : std_logic := '0';
	
	signal lock_in_previous		: std_logic_vector(7 downto 0)	:= (others => '0');
	
	signal fifo_enable				: std_logic	:= '0';
	signal fifo_enable_request		: std_logic	:= '0';
	signal fifo_cycles				: integer	:= 0;
	signal fifo_counter				: integer	:= 10000;
	signal fifo_shift_out			: std_logic	:= '0';
	
	----------
	--Flagging
   ----------
	
	signal data_this_cycle	: std_logic_vector(7 downto 0)	:= (others => '0');
	type int_8_array			is array (0 to 7) of integer;
	signal underflow			: int_8_array							:= (others => 0);
	
   --------------------------------------------------------------------------
   -- State Machine Definitions
   --------------------------------------------------------------------------
   --Sample Enable State Type
   type STATE_TYPE is
   (
      STATE_IDLE,
      STATE_DAC_CLEAR_RESET_WAIT,
      STATE_DAC_CLEAR_RESET_DONE,
      STATE_SHIFT_WR_DATA,
      STATE_WR_CLK_LOW,
      STATE_SHIFT_RD_WAIT,
      STATE_SHIFT_RD_DATA,
      STATE_RD_CLK_HTOL_WAIT,
      STATE_RD_CLK_LOW,
      STATE_RD_CLK_LTOH_WAIT
   );
   signal sm_current_state: STATE_TYPE := STATE_IDLE;      --Current state	  

   --========================================================================
   -- Component Definitions
   --========================================================================
--	component sigma_delta_improved is
--    generic (
--			BITS_PER_SYMBOL	: natural := 24
--    );
--    port (
--			input					: in  std_logic_vector(BITS_PER_SYMBOL - 1 downto 0);
--			output				: out std_logic_vector(19 downto 0);
--			clock					: in std_logic;
--			enable				: in std_logic;
--			reset					: in std_logic
--    );
--	end component;
--	
--	component sigma_delta is
--		port(
--				clk      : in STD_LOGIC;							--Clock (avs_clk)
--				enable   : in STD_LOGIC;							--Enable
--				data_in  : in STD_LOGIC_VECTOR(23 downto 0); --Data in line
--				data_out : out STD_LOGIC_VECTOR(19 downto 0)); --Data out line
--		
--	end component;
--===========================================================================
--===========================================================================
begin

   --========================================================================
   -- Output pin mapping
   --========================================================================
   dout_gen: for i in 0 to NUM_DAC_CHANNELS-1 generate
      dac_sdin_to_dac(i) <= s_shift_data(i)(NUM_SHIFT_BITS-1); --Map shift register MSB's to data out pins
   end generate;

   dac_clr_out_n     <= s_dac_clr_out_n;
   dac_reset_out_n   <= s_dac_reset_out_n;
	
	--========================================================================
	--Component instantiation
	--========================================================================
--	sigma_delta_gen : for i in 0 to NUM_DAC_CHANNELS - 1 generate
--		sigma_delta_inst0 : sigma_delta
--		port map (
--				clk => avs_clk,													--clock signal
--				enable => sigma_delta_enable,									--from: input
--				data_in => s_reg_dac_dout_sigma_in(i), --& "0000",		--from: Avalon
--				data_out =>s_reg_dac_dout_sigma(i)(19 downto 0)			--to  : DAC
--		);
--   end generate;
	
	--Improved sigma delta
--	sigma_delta_improved_gen: for i in 0 to NUM_DAC_CHANNELS-1 generate
--		sigma_delta_imp_inst0 : sigma_delta_improved
--		generic map (
--				BITS_PER_SYMBOL	=> BITS_PER_SYMBOL
--		)
--		port map(
--				input					=> s_reg_dac_dout_sigma_in(i),				--from: Avalon
--				output				=> s_reg_dac_dout_sigma(i)(19 downto 0),	--to  : DAC
--				clock					=> avs_clk,											--clock signal
--				enable				=> sigma_delta_enable,							--enable signal
--				reset					=> '0'												--reset signal
--		);
--	end generate;
	
   --========================================================================
   -- Generate divided DAC reference clock.
   -- Set to run at 2X desired SCLK.
   -- s_dac_int_clk_en is used to run the DAC output state machine
   --========================================================================
   ref_clk_proc : process (avs_reset, avs_clk)
   begin
      if avs_reset='1' then
         s_dac_clk_en <= '0';
         s_dac_int_clk_en <= '1';
         n_dac_int_clk_counter <= DEFAULT_INT_CLK_DIV;
      elsif rising_edge(avs_clk) then
      
         --Control ref clock divider enable based on global enable bit and the in-progress status bits
         if a_global_enable='0' and a_status_dac_data_full='0' and a_status_dac_shift_busy='0' then
            s_dac_clk_en <= '0';
         elsif a_global_enable='1' then
            s_dac_clk_en <= '1';
         end if;

         --DAC reference clock divider - output is a single clock wide enable pulse
         if s_dac_clk_en='0' then
            s_dac_int_clk_en <= '0';
            n_dac_int_clk_counter <= n_reg_int_clk_div;            
         else
            if n_dac_int_clk_counter /= 0 then
               n_dac_int_clk_counter <= n_dac_int_clk_counter - 1;
               s_dac_int_clk_en <= '0';
            else
               s_dac_int_clk_en <= '1';
               n_dac_int_clk_counter <= n_reg_int_clk_div;
            end if;
         end if;
         
      end if;
   end process;
   

   --========================================================================
   -- Generate DAC Start of conversion pulse
   -- Notes:
   --    Conversion rate is set by clk_in frequency and by n_reg_sample_rate_div.
   --========================================================================
   trig_proc : process (avs_reset, avs_clk)
   begin
      if avs_reset='1' then
         s_clr_rw_ctl_req <= '0';
         s_start_dac_pulse <= '0';
         n_cycle_counter <= 0;
      elsif rising_edge(avs_clk) then

         --Only run once per dac_ref_clk cycle
         if s_dac_int_clk_en='1' and fifo_enable = '0' then
            --Count ref clocks down to 0 and reload cycle timer for next output.
            --Software should be careful not to set this below 25 since write cycles take >=25 cycles to complete
            s_clr_rw_ctl_req <= '0';
            --Check for continuous output mode, but only if the current s_reg_dac_rw_ctl is a data update command and NOT a control command
            if a_global_output_mode='1' and s_reg_dac_rw_ctl = cWR_DAC_CMD then
               --Periodic output mode, so output pulse to start next DAC output
               if n_cycle_counter = 0 then
                  n_cycle_counter <= n_reg_sample_rate_div; --Reload for next time
                  --Only start a new output cycle if new data has been written and it is a DAC output update, not a control register update
                  if (s_dac_rw_ctl_req='1' or a_global_continuous_test_mode='1') then -- and fifo_enable = '0'=============================================================================================
                     s_start_dac_pulse <= '1';
                     s_clr_rw_ctl_req <= '1';
                  end if;
               else
                  s_start_dac_pulse <= '0';
                  n_cycle_counter <= n_cycle_counter - 1;
               end if;
            else
               --Software triggered output mode
               if s_dac_rw_ctl_req='1' and s_start_dac_pulse <= '0' and sm_current_state=STATE_IDLE then
                  s_clr_rw_ctl_req <= '1';
                  --detected falling edge of s_dac_rw_ctl_req, so start a software triggered dac output cycle
                  s_start_dac_pulse <= '1';
               else
                  s_start_dac_pulse <= '0';
               end if;
               n_cycle_counter <= n_reg_sample_rate_div; --Keep counter in reset
            end if;
         end if;
      end if;
   end process;

   --Gate interrupt output with enable bit
   avs_irq <= '1' when a_global_irq_enable='1' and a_status_irq_flag='1' else '0';
   
   --========================================================================
   -- Set/Clear Interrupt flags
   --========================================================================
   irq_proc : process (avs_reset, avs_clk)
   begin
      if avs_reset='1' then
         a_status_irq_flag <= '0';
      elsif rising_edge(avs_clk) then
         --Set/Clear IRQ flag
         if s_set_irq_flag='1' then
            a_status_irq_flag <= '1';
         elsif s_clr_irq_flag='1' then
            a_status_irq_flag <= '0';
         end if;
      end if;
   end process;
   
   --========================================================================
   -- Set/Clear Status flags
   --========================================================================
   flags_proc : process (avs_reset, avs_clk)
   begin
      if avs_reset='1' then
         s_dac_rw_ctl_req <= '0';
         a_status_dac_data_full <= '0';
         s_dac_rd_data_valid <= '0';
      elsif rising_edge(avs_clk) then
      
         --Set/Clear rd/wr control request (used to start a read or write transfer)
         if s_set_rw_ctl_req='1' then
            s_dac_rw_ctl_req <= '1';
         elsif s_clr_rw_ctl_req='1' then
            s_dac_rw_ctl_req <= '0';
         end if;
      
         --Set/Clear data reg full flag
         if s_set_dac_data_full='1' then
            a_status_dac_data_full <= '1';
         elsif s_clr_dac_data_full='1' then
            a_status_dac_data_full <= '0';
         end if;

         --Set/Clear read data valid flag
         if s_set_rd_data_valid='1' then
            s_dac_rd_data_valid <= '1';
         elsif s_clr_rd_data_valid='1' then
            s_dac_rd_data_valid <= '0';
         end if;

      end if;
   end process;

   
   --========================================================================
   -- State Machine Delay Counter
   -- Used by state machine for misc delays
   -- Counter increments when s_sm_delay_ctr_en is '1', else counter is reset to 0.
   --========================================================================
   dly_proc : process (avs_reset, avs_clk)
   begin
      if avs_reset='1' then
         n_sm_delay_ctr <= 0;
      elsif rising_edge(avs_clk) then
         --Run once on every pre-divider clock
         if s_dac_int_clk_en='1' then
            if s_sm_delay_ctr_en='0' then
               n_sm_delay_ctr <= 0;    --Hold in reset until enables
            elsif n_sm_delay_ctr /= c_MAX_DELAY_COUNT then
               n_sm_delay_ctr <= n_sm_delay_ctr + 1;  --Increment counter 
            end if;
         end if;
      end if;
   end process;
   
   
   --------------------------------------------------------------------------
   -- Serial Shift State Machine
   --
   -- Operates the serial shift output registers to the DAC's.
   -- The Synchronous update mode is used where /LDAC is held low and
   -- the /SYNC signal is used to transfer the DAC data from the input
   -- shift register to the analog output.
   --
   -- Notes:
   --    For writes, the SCLK pin has a minimum period of 40ns (25MHz) at < 3.3V.
   --    For reads, the SCLK pin has a minimum period of 62.5ns (16MHz) at < 3.3V.
   --    The /SYNC pin has a minimum high time of 48ns when VccIO < 3.3V.
   --
   --------------------------------------------------------------------------
   sr_main_state : process (avs_reset, avs_clk)
      variable n_shift_ctr : natural range 0 to NUM_SHIFT_BITS := NUM_SHIFT_BITS;
   begin
      
      if avs_reset='1' then
         s_dac_clr_out_n   <= '0';
         s_dac_reset_out_n <= '0';
         dac_sclk_out      <= '0';
         dac_sync_out_n    <= '1';
         s_dac_sync_out_n  <= '1';
         dac_ldac_out_n    <= '0';
         for i in 0 to NUM_DAC_CHANNELS-1 loop
            s_shift_data(i) <= (others=>'0');
         end loop;               
         sm_current_state        <= STATE_IDLE;                --Start at beginning of state machine
         s_sm_delay_ctr_en       <= '0';
         s_set_rd_data_valid     <= '0';
         s_clr_dac_data_full     <= '0';
         a_status_dac_shift_busy <= '0';  --Mark shifters as idle
         s_set_irq_flag          <= '0';
         
      elsif rising_edge(avs_clk) then
         dac_ldac_out_n <= '0';  --Asserted low to run DAC in synchronous output mode (s_dac_sync_out_n controls output)
         dac_sync_out_n <= s_dac_sync_out_n; --Delay /SYNC output by 1 system clock to meet setup=5ns and hold=2ns times relative to SCLK

         s_set_irq_flag <= '0';
         s_clr_dac_data_full <= '0';
         s_set_rd_data_valid <= '0';
         --Only run state machine once every N reference clock cycles
         if s_dac_int_clk_en='1' then
            --State machine
            case sm_current_state is
               when STATE_IDLE =>
                  if a_global_dac_clear_req='1' then
                     s_dac_clr_out_n <= '0'; --Need >= 50ns
                     sm_current_state <= STATE_DAC_CLEAR_RESET_WAIT; --k
                  elsif a_global_dac_reset_req='1' then
                     s_dac_reset_out_n <= '0'; --Need >= 35ns
                     sm_current_state <= STATE_DAC_CLEAR_RESET_WAIT; 	
						--NORMAL OPERATION	
                  elsif (a_global_enable='1' and s_start_dac_pulse = '1') or (fifo_enable = '1' and fifo_shift_out = '1') then 
                     --Load read/write control word and data into shift registers.  Note that the DAC output data registers are double buffered.
                     for i in 0 to NUM_DAC_CHANNELS-1 loop
								if sigma_delta_enable = '0' then
									s_shift_data(i)  <= s_reg_dac_rw_ctl & s_reg_dac_dout(i);   --Combine dac_rw_ctl register with data register for each DAC
								else
									s_shift_data(i) <=  s_reg_dac_rw_ctl & s_reg_dac_dout_sigma(i); --Combine dac_rw_ctl register with sigma data register for each DAC
								end if;
							end loop;
                     s_read_mode <= s_reg_dac_rw_ctl(3); --Save read mode flag
                     if s_reg_Dac_rw_ctl(3)='0' then
                        s_set_irq_flag <= '1';     --Write cycle and DAC data regs are now empty
                     end if;
                     dac_sclk_out <= '1';          --Send first rising clock edge
                     s_dac_sync_out_n <= '0';      --Assert /SYNC output
                     n_shift_ctr := 0;             --Load and enable shift counter
                     s_clr_dac_data_full <= '1';   --Data has been transferred to the shift registers, so okay for S/W to write new data
                     a_status_dac_shift_busy <= '1';  --Mark shifters as busy
                     sm_current_state <= STATE_WR_CLK_LOW;
                  else
                     --Do Idle condition here
                     s_dac_clr_out_n     <= '1';
                     s_dac_reset_out_n   <= '1';
                     dac_sclk_out      <= '0';
                     s_dac_sync_out_n  <= '1';     --Note this state serves to make sure that the /SYNC output is high for > 48ns between outputs
                     s_sm_delay_ctr_en <= '0';     --Reset delay counter
                     a_status_dac_shift_busy <= '0';  --Mark shifters as idle
							fifo_enable <= fifo_enable_request;
                  end if;

               --------------------------------------------
               -- DAC Clear / Reset States
               --------------------------------------------
               when STATE_DAC_CLEAR_RESET_WAIT =>
                  --Start clear / reset delay timer
                  s_sm_delay_ctr_en <= '1';   --Start delay counter
                  sm_current_state <= STATE_DAC_CLEAR_RESET_DONE;--and go shift the new data out
                  
               when STATE_DAC_CLEAR_RESET_DONE =>
                  --Wait for delay timer to reach final count (>= 50ns)
                  if n_sm_delay_ctr >= c_DAC_CLEAR_RESET_DLY then
                     s_sm_delay_ctr_en <= '0';  --Disable delay counter
                     s_dac_clr_out_n <= '1';
                     s_dac_reset_out_n <= '1';
                     sm_current_state <= STATE_IDLE;
                  end if;
               
               --------------------------------------------
               -- DAC Write States
               --------------------------------------------
               --Shift data out until counter reaches 0
               when STATE_SHIFT_WR_DATA =>
                  if n_shift_ctr /= NUM_SHIFT_BITS-1 then
                     n_shift_ctr := n_shift_ctr + 1;
                     dac_sclk_out <= '1';
                     for i in 0 to NUM_DAC_CHANNELS-1 loop
                        s_shift_data(i)  <= s_shift_data(i)(NUM_SHIFT_BITS-2 downto 0) & '0';   --Shift data out MSB first and fill with '0's for NOP in case this is a read cycle
                     end loop;
                     sm_current_state <= STATE_WR_CLK_LOW;
                  elsif s_read_mode = '1' then
                     --Read operation, so go read data input at 1/2 clock speed
                     s_dac_sync_out_n <= '1';
                     s_sm_delay_ctr_en <= '1';   --Start delay counter
                     sm_current_state <= STATE_SHIFT_RD_WAIT;
                  else
                     --End of write operation
                     s_dac_sync_out_n <= '1';
                     sm_current_state <= STATE_IDLE;
                  end if;
                  
               --Pulse clock out to external shift register
               when STATE_WR_CLK_LOW =>
                  --DAC samples data on falling edge
                  dac_sclk_out <= '0';
                  if n_shift_ctr = NUM_SHIFT_BITS-1 then
                     --Deassert /SYNC output 1/2 clock early in order to get 1MHz output using 25 pulses of a 25MHz clock (25*40ns clocks)
                     s_dac_sync_out_n <= '1';
                  end if;
                  sm_current_state <= STATE_SHIFT_WR_DATA;      --Go shift next data bit
               
               --------------------------------------------
               -- DAC Read States
               --------------------------------------------
               when STATE_SHIFT_RD_WAIT =>
                  --Wait > 48ns while /SYNC is high between write and read phases
                  n_shift_ctr := 0;
                  if n_sm_delay_ctr >= c_DAC_WR_TO_RD_DLY then
                     s_sm_delay_ctr_en <= '0';  --Reset counter
                     s_dac_sync_out_n <= '0';     --Assert /SYNC output
                     sm_current_state <= STATE_SHIFT_RD_DATA;--and go shift the new data out
                  end if;
               
               when STATE_SHIFT_RD_DATA =>
                  --Note:  Read data runs at 1/2 write speed due to chip limitations (Fmax <= 16MHz)
                  if n_shift_ctr /= NUM_SHIFT_BITS-1 then
                     n_shift_ctr := n_shift_ctr + 1;
                     dac_sclk_out <= '1';
                     sm_current_state <= STATE_RD_CLK_HTOL_WAIT;
                  else
                     --Read done, transfer data from shift registers into output registers
                     s_dac_sync_out_n <= '1';  --Assert /SYNC output
                     s_set_rd_data_valid <= '1';
                     s_set_irq_flag <= '1';  --Read cycle and DAC shift data contains valid readback data
                     sm_current_state <= STATE_IDLE;          --Back to whence we have come
                  end if;

               when STATE_RD_CLK_HTOL_WAIT =>
                  sm_current_state <= STATE_RD_CLK_LOW;
                  
               when STATE_RD_CLK_LOW =>
                  --Set SCLK low and sample read data on falling edge
                  dac_sclk_out <= '0';
                  for i in 0 to NUM_DAC_CHANNELS-1 loop
                     s_shift_data(i)  <= s_shift_data(i)(NUM_SHIFT_BITS-2 downto 0) & dac_sdo_from_dac(i);   --Register read data from dac on falling edge of SCLK
                  end loop;
                  sm_current_state <= STATE_RD_CLK_LTOH_WAIT;

               when STATE_RD_CLK_LTOH_WAIT =>
                  --Wait 1 extra clock for reads since Fmax <= 16MHz
                  sm_current_state <= STATE_SHIFT_RD_DATA;

               when others =>
                  sm_current_state <= STATE_IDLE;
                  
            end case;

         end if;
      end if;
   end process;

   --************************************************************************
   -- Avalon Memory-Mapped Register Read/Write Logic
   --************************************************************************
   --========================================================================
   -- Register Read/Write Wait Request
   --========================================================================
   wait_req_proc : process (avs_reset, avs_clk)
   begin
      if avs_reset='1' then
         avs_waitrequest <= '1';
      elsif rising_edge(avs_clk) then
         if avs_chipselect = '1' then
            avs_waitrequest <= '0';
         else
            avs_waitrequest <= '1'; --Default state is asserted
         end if;
      end if;
   end process;


   --========================================================================
   -- Read registers
   --========================================================================
   reg_read_proc : process (avs_reset, avs_clk)
   begin
      if avs_reset='1' then
         avs_readdata <= (others=>'0');
      elsif rising_edge(avs_clk) then
--Removed to make timing faster
--         if avs_chipselect='1' and avs_read='1' and avs_byteenable(3 downto 0)="1111" then
         if avs_chipselect='1' and avs_read='1' then
            --Decode register addresses (note REQUIRES VHDL 2008 for the case? statement to work with "don't care" inputs
            case? avs_address is
               when cADDR_REG_CONTROL =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_control), avs_readdata'length));
               when cADDR_REG_STATUS =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_status), avs_readdata'length));
               when cADDR_REG_SAMPLE_RATE_DIV =>
                  avs_readdata      <= std_logic_vector(to_unsigned(n_reg_sample_rate_div, avs_readdata'length));
               when cADDR_REG_DAC_SHIFT_CLK_DIV =>
                  avs_readdata      <= std_logic_vector(to_unsigned(n_reg_int_clk_div, avs_readdata'length));
               when cADDR_REG_DAC_ENABLE_MASK =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_enable_mask), avs_readdata'length));
               when cADDR_REG_DAC_RW_CTL =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_rw_ctl), avs_readdata'length));
--               when cADDR_REG_DAC_DOUT_BASE_XXX =>
--                  --Do all dac data out registers here
--                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_dout(to_integer(unsigned(avs_address))-to_integer(unsigned(cADDR_REG_DAC_DOUT_BASE)))),avs_readdata'length));
               when cADDR_REG_DAC_DOUT_0 =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_dout(0)), avs_readdata'length));
               when cADDR_REG_DAC_DOUT_1 =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_dout(1)), avs_readdata'length));
               when cADDR_REG_DAC_DOUT_2 =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_dout(2)), avs_readdata'length));
               when cADDR_REG_DAC_DOUT_3 =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_dout(3)), avs_readdata'length));
               when cADDR_REG_DAC_DOUT_4 =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_dout(4)), avs_readdata'length));
               when cADDR_REG_DAC_DOUT_5 =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_dout(5)), avs_readdata'length));
               when cADDR_REG_DAC_DOUT_6 =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_dout(6)), avs_readdata'length));
               when cADDR_REG_DAC_DOUT_7 =>
                  avs_readdata      <= std_logic_vector(resize(unsigned(s_reg_dac_dout(7)), avs_readdata'length));
--20160314, JCS: The general case method doesn't seem to work in Quartus, but does in ModelSim                  
--               when cADDR_REG_DAC_DIN_BASE_XXX =>
--                  --Do all dac data in registers here
--                  if s_dac_rd_data_valid='1' then
--                     --If previous access was a DAC read operation, return the DAC read data results that are in the shift registers
--                     avs_readdata   <= std_logic_vector(resize(unsigned(s_shift_data(to_integer(unsigned(avs_address))-to_integer(unsigned(cADDR_REG_DAC_DIN_BASE)))),avs_readdata'length));
--                  else
--                     avs_readdata   <= (others=>'1'); --Else return invalid data
--                  end if;
               when cADDR_REG_DAC_DIN_0 =>
                  if s_dac_rd_data_valid='1' then
                     --If previous access was a DAC read operation, return the DAC read data results that are in the shift registers
                     avs_readdata   <= std_logic_vector(resize(unsigned(s_shift_data(0)),avs_readdata'length));
                  else
                     avs_readdata   <= (others=>'1'); --Else return invalid data
                  end if;
               when cADDR_REG_DAC_DIN_1 =>
                  if s_dac_rd_data_valid='1' then
                     --If previous access was a DAC read operation, return the DAC read data results that are in the shift registers
                     avs_readdata   <= std_logic_vector(resize(unsigned(s_shift_data(1)),avs_readdata'length));
                  else
                     avs_readdata   <= (others=>'1'); --Else return invalid data
                  end if;
               when cADDR_REG_DAC_DIN_2 =>
                  if s_dac_rd_data_valid='1' then
                     --If previous access was a DAC read operation, return the DAC read data results that are in the shift registers
                     avs_readdata   <= std_logic_vector(resize(unsigned(s_shift_data(2)),avs_readdata'length));
                  else
                     avs_readdata   <= (others=>'1'); --Else return invalid data
                  end if;
               when cADDR_REG_DAC_DIN_3 =>
                  if s_dac_rd_data_valid='1' then
                     --If previous access was a DAC read operation, return the DAC read data results that are in the shift registers
                     avs_readdata   <= std_logic_vector(resize(unsigned(s_shift_data(3)),avs_readdata'length));
                  else
                     avs_readdata   <= (others=>'1'); --Else return invalid data
                  end if;
               when cADDR_REG_DAC_DIN_4 =>
                  if s_dac_rd_data_valid='1' then
                     --If previous access was a DAC read operation, return the DAC read data results that are in the shift registers
                     avs_readdata   <= std_logic_vector(resize(unsigned(s_shift_data(4)),avs_readdata'length));
                  else
                     avs_readdata   <= (others=>'1'); --Else return invalid data
                  end if;
               when cADDR_REG_DAC_DIN_5 =>
                  if s_dac_rd_data_valid='1' then
                     --If previous access was a DAC read operation, return the DAC read data results that are in the shift registers
                     avs_readdata   <= std_logic_vector(resize(unsigned(s_shift_data(5)),avs_readdata'length));
                  else
                     avs_readdata   <= (others=>'1'); --Else return invalid data
                  end if;
               when cADDR_REG_DAC_DIN_6 =>
                  if s_dac_rd_data_valid='1' then
                     --If previous access was a DAC read operation, return the DAC read data results that are in the shift registers
                     avs_readdata   <= std_logic_vector(resize(unsigned(s_shift_data(6)),avs_readdata'length));
                  else
                     avs_readdata   <= (others=>'1'); --Else return invalid data
                  end if;
               when cADDR_REG_DAC_DIN_7 =>
                  if s_dac_rd_data_valid='1' then
                     --If previous access was a DAC read operation, return the DAC read data results that are in the shift registers
                     avs_readdata   <= std_logic_vector(resize(unsigned(s_shift_data(7)),avs_readdata'length));
                  else
                     avs_readdata   <= (others=>'1'); --Else return invalid data
                  end if;
						
					when cADDR_REG_UNDERFLOW_0 =>
						avs_readdata <= std_logic_vector(to_unsigned(underflow(0),32));
					when cADDR_REG_UNDERFLOW_1 =>
						avs_readdata <= std_logic_vector(to_unsigned(underflow(1),32));
					when cADDR_REG_UNDERFLOW_2 =>
						avs_readdata <= std_logic_vector(to_unsigned(underflow(2),32));
					when cADDR_REG_UNDERFLOW_3 =>
						avs_readdata <= std_logic_vector(to_unsigned(underflow(3),32));
					when cADDR_REG_UNDERFLOW_4 =>
						avs_readdata <= std_logic_vector(to_unsigned(underflow(4),32));
					when cADDR_REG_UNDERFLOW_5 =>
						avs_readdata <= std_logic_vector(to_unsigned(underflow(5),32));
					when cADDR_REG_UNDERFLOW_6 =>
						avs_readdata <= std_logic_vector(to_unsigned(underflow(6),32));
					when cADDR_REG_UNDERFLOW_7 =>
						avs_readdata <= std_logic_vector(to_unsigned(underflow(7),32));
					
					when others =>
                  avs_readdata      <= (others=>'1');
            end case?;
--         else
--            avs_readdata   <= (others=>'1');
         end if;
      end if;
   end process;   --End of reg_read_proc

   
   --========================================================================
   -- Write Registers
   --========================================================================
   reg_write_proc : process (avs_reset, avs_clk)
   begin
      if avs_reset='1' then 
         s_reg_control           <= std_logic_vector(to_unsigned(0, s_reg_control'length));
--         s_reg_status            <= (others=>'0');
         n_reg_sample_rate_div   <= DEFAULT_SAMPLE_RATE_DIV;
         n_reg_int_clk_div       <= DEFAULT_INT_CLK_DIV;
         s_reg_dac_enable_mask   <= (others=>'1');
         s_reg_dac_rw_ctl        <= (others=>'0');
         for i in 0 to NUM_DAC_CHANNELS-1 loop
            s_reg_dac_dout(i)    <= (others=>'0');
         end loop;
         s_set_rw_ctl_req        <= '0';
         s_set_dac_data_full     <= '0';
         s_clr_rd_data_valid     <= '0';
         s_clr_irq_flag          <= '0';

         
--         s_reg_dac_dout_0        <= (others=>'0');
--         s_reg_dac_dout_1        <= (others=>'0');
--         s_reg_dac_dout_2        <= (others=>'0');
--         s_reg_dac_dout_3        <= (others=>'0');
--         s_reg_dac_dout_4        <= (others=>'0');
--         s_reg_dac_dout_5        <= (others=>'0');
--         s_reg_dac_dout_6        <= (others=>'0');
--         s_reg_dac_dout_7        <= (others=>'0');
         
      elsif rising_edge(avs_clk) then
--Removed to make timing faster
--         if avs_chipselect = '1' and avs_write='1' and avs_byteenable(3 downto 0)="1111" and avs_address(AVS_ADDRESS_BITS-1 downto AVS_ADDRESS_BITS-2)="00" then
         if avs_chipselect = '1' and avs_write='1' then
            --Decode register addresses (note REQUIRES VHDL 2008 for the case? statement to work with "don't care" inputs
            case? avs_address is
               when cADDR_REG_CONTROL =>
                  s_reg_control           <= avs_writedata(s_reg_control'length-1 downto 0);
               when cADDR_REG_STATUS =>
                  if avs_writedata(2)='1' then
                     s_clr_irq_flag <= '1';  --Clear irq flag immediately to prevent cpu from getting interrupted again on early exit from irq routine.
                  end if;
--                  null; --Non-Writeable register
               when cADDR_REG_SAMPLE_RATE_DIV =>
                  n_reg_sample_rate_div   <= to_integer(unsigned(avs_writedata));
               when cADDR_REG_DAC_SHIFT_CLK_DIV =>
                  n_reg_int_clk_div       <= to_integer(unsigned(avs_writedata));
               when cADDR_REG_DAC_ENABLE_MASK =>
                  s_reg_dac_enable_mask   <= avs_writedata(s_reg_dac_enable_mask'length-1 downto 0);
               when cADDR_REG_DAC_RW_CTL =>
                  s_reg_dac_rw_ctl        <= avs_writedata(s_reg_dac_rw_ctl'length-1 downto 0);
                  s_set_rw_ctl_req <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software has requested a new DAC rd/wr operation
--20160314, JCS: The general case method doesn't seem to work in Quartus, but does in ModelSim                  
--               when cADDR_REG_DAC_DOUT_BASE_XXX =>
--                  --Do all dac dout registers here.  WARNING:  DO NOT WRITE TO THESE REGISTERS WHILE A SERIAL DAC READ OPERATION IS ACTIVE.
--                  s_reg_dac_dout(to_integer(unsigned(avs_address))-to_integer(unsigned(cADDR_REG_DAC_DOUT_BASE))) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
--                  s_set_dac_data_full <= '1';
--                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_0 =>
						if sigma_delta_enable = '0' then
                  s_reg_dac_dout(0) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(0) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
						end if;
						s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_1 =>
                  if sigma_delta_enable = '0' then
                  s_reg_dac_dout(1) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(1) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_2 =>
                  if sigma_delta_enable = '0' then
							s_reg_dac_dout(2) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(2) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_3 =>
                  if sigma_delta_enable = '0' then
							s_reg_dac_dout(3) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(3) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_4 =>
                  if sigma_delta_enable = '0' then
							s_reg_dac_dout(4) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(4) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_5 =>
                  if sigma_delta_enable = '0' then
							s_reg_dac_dout(5) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(5) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_6 =>
                  if sigma_delta_enable = '0' then
							s_reg_dac_dout(6) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(6) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_7 =>
                  if sigma_delta_enable = '0' then
							s_reg_dac_dout(7) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(7) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
					when others =>
                  null;
            end case?;	
         elsif lock_in_enable = '1' and lock_in_write = '1' then
				case? lock_in_address is
--20160314, JCS: The general case method doesn't seem to work in Quartus, but does in ModelSim                  
--               when cADDR_REG_DAC_DOUT_BASE_XXX =>
--                  --Do all dac dout registers here.  WARNING:  DO NOT WRITE TO THESE REGISTERS WHILE A SERIAL DAC READ OPERATION IS ACTIVE.
--                  s_reg_dac_dout(to_integer(unsigned(avs_address))-to_integer(unsigned(cADDR_REG_DAC_DOUT_BASE))) <= std_logic_vector(resize(unsigned(avs_writedata),BITS_PER_SYMBOL));
--                  s_set_dac_data_full <= '1';
--                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_0 =>
						if sigma_delta_enable = '0' and lock_in_previous(0) = '0' then
							s_reg_dac_dout(0) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(0) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
						end if;
						s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_1 =>
                  if sigma_delta_enable = '0' and lock_in_previous(1) = '0' then
                  s_reg_dac_dout(1) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(1) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_2 =>
                  if sigma_delta_enable = '0' and lock_in_previous(2) = '0' then
							s_reg_dac_dout(2) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(2) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_3 =>
                  if sigma_delta_enable = '0' and lock_in_previous(3) = '0' then
							s_reg_dac_dout(3) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(3) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_4 =>
                  if sigma_delta_enable = '0' and lock_in_previous(4) = '0' then
							s_reg_dac_dout(4) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(4) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_5 =>
                  if sigma_delta_enable = '0' and lock_in_previous(5) = '0' then
							s_reg_dac_dout(5) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(5) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_6 =>
                  if sigma_delta_enable = '0' and lock_in_previous(6) = '0' then
							s_reg_dac_dout(6) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(6) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
               when cADDR_REG_DAC_DOUT_7 =>
                  if sigma_delta_enable = '0' and lock_in_previous(7) = '0' then
							s_reg_dac_dout(7) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
                  else 
							s_reg_dac_dout_sigma_in(7) <= std_logic_vector(resize(unsigned(lock_in_writedata),BITS_PER_SYMBOL));
						end if;
                  s_set_dac_data_full <= '1';
                  s_clr_rd_data_valid <= '1';  --Indicate DAC read data is now invalid since software wrote to the output registers
					when others =>
                  null;
            end case?;
			else
            --Auto clear bits / registers here
            if s_dac_clr_out_n='0' then
               a_global_dac_clear_req <= '0';   --Auto clear this global bit once the state machine is asserting the output
            end if;
            if s_dac_reset_out_n='0' then
               a_global_dac_reset_req <= '0';   --Auto clear this global bit once the state machine is asserting the output
            end if;
            
            --Clear flag set/clr request bits here
            s_set_rw_ctl_req <= '0';     --Clear command write request
            s_set_dac_data_full <= '0';
            s_clr_rd_data_valid <= '0';
            s_clr_irq_flag <= '0';
         end if;
      end if;
   end process;   --End of reg_write_proc

   
	--clock conversion process
	lock_in_clock_proc : process (avs_clk,avs_reset)
	begin
		if avs_reset = '1' then
			lock_in_previous <= (others => '0');
		elsif rising_edge(avs_clk) then
			if lock_in_enable = '1' and lock_in_write = '1' then
				case lock_in_address is
					when cADDR_REG_DAC_DOUT_0 =>
						lock_in_previous <= "00000001";
					when cADDR_REG_DAC_DOUT_1 =>
						lock_in_previous <= "00000010";
					when cADDR_REG_DAC_DOUT_2 =>
						lock_in_previous <= "00000100";
					when cADDR_REG_DAC_DOUT_3 =>
						lock_in_previous <= "00001000";
					when cADDR_REG_DAC_DOUT_4 =>
						lock_in_previous <= "00010000";
					when cADDR_REG_DAC_DOUT_5 =>
						lock_in_previous <= "00100000";
					when cADDR_REG_DAC_DOUT_6 =>
						lock_in_previous <= "01000000";
					when cADDR_REG_DAC_DOUT_7 =>
						lock_in_previous <= "10000000";
					when others =>
						lock_in_previous <= (others => '0');
				end case;
			else
				lock_in_previous <= (others => '0');
			end if;
		end if;
	end process;
	
	fifo_count_proc : process (avs_clk,avs_reset)
	begin
		if avs_reset = '1' then
			fifo_counter <= 0;
		elsif rising_edge(avs_clk) then
			if fifo_counter < fifo_cycles then
				fifo_counter <= fifo_counter + 1;
			elsif fifo_counter >= fifo_cycles and fifo_start = '1' then
				fifo_counter <= 0;
			else
				fifo_counter <= fifo_counter;
			end if;
		end if;
	end process;
	
	fifo_start_proc : process (avs_clk,avs_reset)
	begin
		if avs_reset = '1' then
			fifo_shift_out <= '0';
		elsif rising_edge(avs_clk) then
			if fifo_counter = fifo_cycles - 1 then
				fifo_shift_out <= '1';
			else
				fifo_shift_out <= '0';
			end if;
		end if;
	end process;
	
	--underflow flags
	underflow_proc : process (avs_clk,avs_reset)
	begin
		if avs_reset = '1' then
			underflow <= (others => 0);
		elsif rising_edge(avs_clk) then
			if avs_write = '1' and avs_address = cADDR_REG_DAC_RESET then
				underflow <= (others => 0);
			elsif sm_current_state = STATE_IDLE then
				if (a_global_enable='1' and s_start_dac_pulse = '1' and fifo_enable = '0') or (fifo_enable = '1' and fifo_shift_out = '1') then
					for i in 0 to 7 loop
						if data_this_cycle(i) = '0' then
							underflow(i) <= underflow(i) + 1;
						end if;
					end loop;
				end if;
			end if;
		end if;
	end process;
	
	--dac_automatic
	dac_automatic_proc : process (avs_clk,avs_reset)
	begin
		if avs_reset = '1' then
			fifo_enable_request <= '0';
			fifo_cycles <= 1000;
		elsif rising_edge(avs_clk) then
			if avs_write = '1' and avs_address = cADDR_REG_DAC_AUTOMATIC then  --Write 1 to enable, 0 to disable
				fifo_enable_request <= avs_writedata(0);
			elsif avs_write = '1' and avs_address = cADDR_REG_DAC_AUTO_SET then
				fifo_cycles <= 150 * to_integer(unsigned(avs_writedata))+ 2;
			end if;	
		end if;
	end process;
	
	--data_this_cycle process
	data_this_cycle_proc : process (avs_clk,avs_reset)
	begin
		if avs_reset = '1' then
			data_this_cycle <= (others => '0');
		elsif rising_edge(avs_clk) then
			if sm_current_state = STATE_IDLE and ((a_global_enable='1' and s_start_dac_pulse = '1' and fifo_enable = '0') or 
															  (fifo_enable = '1' and fifo_shift_out = '1')) then
				data_this_cycle <= (others => '0');
			elsif lock_in_enable = '1' and lock_in_write = '1' then
				case lock_in_address(2 downto 0) is
					when "000" => data_this_cycle(0) <= '1';
					when "001" => data_this_cycle(1) <= '1';
					when "010" => data_this_cycle(2) <= '1';
					when "011" => data_this_cycle(3) <= '1';
					when "100" => data_this_cycle(4) <= '1';
					when "101" => data_this_cycle(5) <= '1';
					when "110" => data_this_cycle(6) <= '1';
					when others=> data_this_cycle(7) <= '1';
				end case;
			elsif avs_write = '1' then
				case avs_address is
					when cADDR_REG_DAC_DOUT_0 => data_this_cycle(0) <= '1';
					when cADDR_REG_DAC_DOUT_1 => data_this_cycle(1) <= '1';
					when cADDR_REG_DAC_DOUT_2 => data_this_cycle(2) <= '1';
					when cADDR_REG_DAC_DOUT_3 => data_this_cycle(3) <= '1';
					when cADDR_REG_DAC_DOUT_4 => data_this_cycle(4) <= '1';
					when cADDR_REG_DAC_DOUT_5 => data_this_cycle(5) <= '1';
					when cADDR_REG_DAC_DOUT_6 => data_this_cycle(6) <= '1';
					when cADDR_REG_DAC_DOUT_7 => data_this_cycle(7) <= '1';
					when others => null;
				end case;	
			end if;
		end if;
	end process;
	
end ad5791_dac_arch;