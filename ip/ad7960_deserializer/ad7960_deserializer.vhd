----------------------------------------------------------------------------
-- Date:    2016-02-02
-- File:    ad7960_deserializer.vhd
-- By:      Jeff Short
--
-- Desc:    Direct LVDS to parallel converter front end
--
--Algorithm:
--
-- Includes a master timing generator for start of conversion and a gated
-- shift clock output to the ADC's.
--
-- Implements a direct LVDS to parallel converter for each ADC that runs from
-- the LVDS input clock without needing a PLL.
--
--
--============================================================================ 
-- Register Definitions:
-- -----------------------------------------------------------------------
-- Name               Reg# Offset   R/W   Description
-- -----------------------------------------------------------------------
-- reg_control           0 (0x00)   R/W
--    [0]: global_enable            R/W   Global Go Bit: 1=Enable, 0=Disable after current frame completes.
--    [1]: conversion_req           W     1=Request single-shot conversion (auto clears to 0, only use when global_enable=0)
--    [3]:                                Reserved
--    [7:4]                               ADC_EN[3:0] = AD7960 mode select control bits (affects ALL ADC's).
--                                        Normal operation, bits should be set to 0x5 or 0x2
--                                        ADC_EN[3] = 0, The EN3 pins on all ADC's are tied to 0 on the PCB on Rev A Boards (fixed on Rev B).
--
-- reg_status            1 (0x04)   R(W)
--    0: busy                       R     Module status: 1=Running, 0=Idle
--
-- reg_chan_en_mask      2 (0x08)   R/W   ADC channel enable bit mask.  Each bit enables one ADC channel.
--   [NUM_CHANNELS-1:0]                      0=Disable, 1=Enabled
-- 
-- reg_sample_rate_div   3 (0x0C)   R/W   Sample rate divider.  Set to N-1 where:
--   [11:0]                               N is the desired divider denominator.
--                                           Numerator is the fixed ADC_REF_CLK_IN.
--                                           Default divider = REF_CLK_FREQ / SAMPLE_RATE_FREQ -1
--                                              5Msps: REF_CLK_FREQ=300MHz, SAMPLE_RATE_FREQ=5MHz
--                                                 Divider = (300e6 / 5e6) -1 = 59
--                                              1Msps: REF_CLK_FREQ=300MHz, SAMPLE_RATE_FREQ=1MHz
--                                                 Divider = (300e6 / 1e6) -1 = 299
--
-- reg_dac_clk_div      4 (0x10)    R/W   Clock divider for external DAC ref clock.  Set to N-1 where:
--   [9:0]                                  N is the desired divider denominator.
--                                        The output is the ADC ref clock divided by N.
--
-- reg_dac_sync_div     5 (0x14)    R/W   External Sync Output rate divider.  Set to N-1 where:
--   [9:0]                                  N is the desired divider denominator.
--                                        This value takes the ADC 
--
-- reg_dac_sync_phase   6 (0x18)    R/W   External Sync Phase adjust.
--   [9:0]                                Number of clocks to delay the external sync from the
--                                        ADC convert reference pulse.
--
--============================================================================ 
--
--Revisions:
--
-- 20160202 - Jeff Short
--    Adapted for use with AD7960 A/D Converter.
--
-- 20160720 - Jeff Short
--    Added +1 to ADC_CONV_PULSE_WIDTH for rounding to guarantee 10ns+ CONV_START pulse width.
--
-- 20161007 - Jeff Short
--    Increased the ADC_EN[2:0] to ADC_EN[3:0] for Rev B Analog Boards.
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--use ieee.math_real.all;

LIBRARY altera_mf;
USE altera_mf.all;


entity ad7960_deserializer is
   generic (
      ADC_REF_CLK_FREQ_MHZ    : natural range 1 to 300 := 300; --Frequency of ADC reference clock, in MHz
      BITS_PER_SYMBOL         : natural range 4 to 24  := 18;  --Number of bits per symbol
      NUM_ADC_CHANNELS        : natural range 1 to 32  := 8;   --Number of analog to digital converter chips in parallel
      NUM_CHANNEL_BITS        : natural range 1 to 8   := 3;   --Number of bits for channel counter
      MAX_SAMPLE_RATE_DIV     : integer := 4095;               --Max count of ADC sample rate count divider
      DEFAULT_SAMPLE_RATE_DIV : integer := 59;                 --Divide by N-1. For 5MHz: (60-1=59)
      MAX_DAC_CLK_DIV         : integer := 1023;               --Max count of external sync divider
      DEFAULT_DAC_CLK_DIV     : integer := 50-1;               --Divider for DAC ref clock from ADC ref clock
      MAX_DAC_SYNC_RATE_DIV   : integer := 1023;               --Max count of external sync divider
      DEFAULT_DAC_SYNC_DIV    : integer := 50-1;               --Divide by N-1. For 100KHz: (50-1=49)
      AVALON_MM_BUS_WIDTH     : integer := 32                  --Number of data bits to use for Avalon Memory-Mapped Slave bus
   );
   port
   (
      adc_reset               : in  std_logic;
      adc_ref_clk_in          : in  std_logic;  --Continuous reference clock used to generate timing for adc clock
--      ref_sync_in             : in  std_logic;  --Reference input to sync adc samples with an external signal

      --Outputs to ADC
      adc_clk_out             : out std_logic;  --Gated ref clock output to adc
      adc_conv_start_out      : out std_logic;  --Conversion start pulse to adc
      adc_en_bits             : out std_logic_vector(3 downto 0); --Mode control bits to ADC's
      
      --Inputs from ADC
      adc_rx_clk_in           : in  std_logic_vector(NUM_ADC_CHANNELS-1 downto 0);  --Echoed clock from adc's
      adc_rx_data_in          : in  std_logic_vector(NUM_ADC_CHANNELS-1 downto 0);  --Serial data from adc's

      --External Sync Outputs
      dac_reset_out           : out std_logic;  --Reset for dac_clk_out domain
      dac_ref_clk_out         : out std_logic;  --Reference clock to DAC module
      dac_sync_out            : out std_logic;  --Sync output to DAC module

      --Avalon Streaming ADC data out
      aso_reset               : in  std_logic;  --Reset for all Avalon busses
      aso_clk                 : in  std_logic;  --Clock for all Avalon busses
      aso_sop_out             : out std_logic;
      aso_eop_out             : out std_logic;
      aso_data_out            : out std_logic_vector(BITS_PER_SYMBOL-1 downto 0);  --Parallel shift register data output
      aso_valid_out           : out std_logic;
      aso_channel_out         : out std_logic_vector(NUM_CHANNEL_BITS-1 downto 0); --Channel number output
      
      --Avalon-MM Slave Interface (uses aso_clk)
      avs_chipselect          : in  std_logic;
      avs_read                : in  std_logic;
      avs_write               : in  std_logic;
      avs_address             : in  std_logic_vector(2 downto 0); --Set address space to 2x Histogram memory size (no -1 needed)
      avs_byteenable          : in  std_logic_vector((AVALON_MM_BUS_WIDTH/8)-1 downto 0);
      avs_readdata            : out std_logic_vector(AVALON_MM_BUS_WIDTH-1 downto 0);
      avs_writedata           : in  std_logic_vector(AVALON_MM_BUS_WIDTH-1 downto 0);
      avs_waitrequest         : out std_logic
   );
end ad7960_deserializer;

--===========================================================================
--===========================================================================
architecture ad7960_deserializer_arch of ad7960_deserializer is

   --------------------------------------------------------------------------
   -- Constant Definitions
   --------------------------------------------------------------------------
   constant NUM_RESYNC_PIPE_BITS  : natural := 5;   --Number of clocks to delay between receiving serial data and converting it to parallel data
   --Get minimum number of clock cycles for the ADC conversion start pulse based on the AD7960 tCNVH>=10ns
   constant ADC_CONV_PULSE_WIDTH  : natural := 10 / (1*1000/ADC_REF_CLK_FREQ_MHZ) + 1;  --Min width of ADC conversion start pulse (in clock cycles)

   --Num clocks to stretch a_global_conversion_req so it can be sampled correctly by adc_ref_clk if it is slower than the asi_clk_in
   constant cGLOBAL_CONVERSION_REQ_PULSE_WIDTH : natural := 3;

   
   --Register Address Definitions
   constant cADDR_REG_CONTROL          : std_logic_vector(2 downto 0) := "000";  --Control register
   constant cADDR_REG_STATUS           : std_logic_vector(2 downto 0) := "001";  --Status register - read only, write to clear flags
   constant cADDR_REG_CHAN_EN_MASK     : std_logic_vector(2 downto 0) := "010";  --ADC Enable mask register (1 bit per channel, 1=Enabled, 0=Disabled)
   constant cADDR_REG_SAMPLE_RATE_DIV  : std_logic_vector(2 downto 0) := "011";  --ADC sample rate divider
   constant cADDR_REG_DAC_CLK_DIV      : std_logic_vector(2 downto 0) := "100";  --External clock divider
   constant cADDR_REG_DAC_SYNC_DIV     : std_logic_vector(2 downto 0) := "101";  --External sync divider
   constant cADDR_REG_DAC_SYNC_PHASE   : std_logic_vector(2 downto 0) := "110";  --External sync phase delay

   --------------------------------------------------------------------------
   -- Register Declarations
   --------------------------------------------------------------------------
   --Control Registers
   signal s_reg_control                : std_logic_vector(7 downto 0);
   signal s_reg_status                 : std_logic_vector(0 downto 0);
   signal s_reg_chan_en_mask           : std_logic_vector(NUM_ADC_CHANNELS-1 downto 0);
   signal n_reg_sample_rate_div        : natural range 0 to MAX_SAMPLE_RATE_DIV;
   signal n_reg_dac_ref_clk_div        : natural range 0 to MAX_DAC_CLK_DIV;
   signal n_reg_dac_sync_div           : natural range 0 to MAX_DAC_SYNC_RATE_DIV;
   signal n_reg_dac_sync_phase         : natural range 0 to MAX_DAC_SYNC_RATE_DIV;

   --Control Register Bit Aliases
   alias a_global_adc_enable           is s_reg_control(0); --Enable module, 0=Disable after current frame.
   alias a_global_conversion_req       is s_reg_control(1); --Request single-shot conversion (a_global_adc_enable must be 0 to work properly)
   alias a_global_en_bits              is s_reg_control(7 downto 4); --Enable per-channel offset, 0=Disable.
   
   --Status Register Bits
   alias a_status_busy                 is s_reg_status(0);  --Module is busy processing a packet (read only)
   
   --------------------------------------------------------------------------
   -- Signal Declarations
   --------------------------------------------------------------------------
   signal s_aso_sop_pipe         : std_logic := '0';
   signal s_aso_eop_pipe         : std_logic := '0';
   signal s_aso_valid_pipe       : std_logic := '0';
   signal s_aso_data_pipe        : std_logic_vector(BITS_PER_SYMBOL-1 downto 0) := (others=>'0');
   signal s_aso_channel_pipe     : std_logic_vector(NUM_CHANNEL_BITS-1 downto 0) := (others=>'0');
   
   signal s_adc_en_dly1          : std_logic := '0';
   signal s_adc_en_dly2          : std_logic := '0';
   signal s_conversion_req_dly1  : std_logic := '0';
   signal s_conversion_req_dly2  : std_logic := '0';
   signal s_conversion_start     : std_logic := '0';
   signal n_cycle_counter        : natural range 0 to MAX_SAMPLE_RATE_DIV := 0; --ADC sample interval tCYCLE counter
   signal n_bit_counter          : natural range 0 to BITS_PER_SYMBOL := 0; --Bit counter for ADC shift clock output
   signal s_adc_conv_start_out   : std_logic := '0';
   signal s_adc_clk_en           : std_logic := '0';
   
   type data_array         is array (0 to NUM_ADC_CHANNELS-1) of std_logic_vector(BITS_PER_SYMBOL-1 downto 0);
   signal s_shift_data           : data_array := (others => (others=>'0'));
   signal s_data_out             : data_array := (others => (others=>'0'));
--   signal s_ser_to_par_dly       : std_logic_vector(SER_TO_PAR_DLY_BITS-1 downto 0);
   signal s_resync_pipe          : std_logic_vector(NUM_RESYNC_PIPE_BITS-1 downto 0);
   signal s_ser_to_par_en        : std_logic := '0';
   signal s_parallel_data_valid  : std_logic := '0';

   signal s_packet_busy          : std_logic := '0';
   signal s_valid_packet         : std_logic := '0';
   signal s_channel_mask         : std_logic_vector(NUM_ADC_CHANNELS-1 downto 0);
   signal n_channel_ctr          : natural range 0 to NUM_ADC_CHANNELS-1;
   
   signal n_global_conversion_req_ctr : natural range 0 to cGLOBAL_CONVERSION_REQ_PULSE_WIDTH;
   
   --========================================================================
   -- Component Definitions
   --========================================================================

   --------------------------------------------------------------------------
   -- DDR Output - 1 bit
   --------------------------------------------------------------------------
   component ddr_out1 is
   port
   (
      signal datain_h		: in std_logic_vector(0 downto 0);
      signal datain_l		: in std_logic_vector(0 downto 0);
      signal outclock		: in std_logic;
      signal dataout       : out std_logic_vector(0 downto 0)
   );
   end component ddr_out1;

   
--===========================================================================
--===========================================================================
begin

   --========================================================================
   -- Output pin mapping
   --========================================================================
   adc_en_bits <= a_global_en_bits;   --Output mode control bits to ADC's

   --For future use
   dac_reset_out     <= '0';
   dac_ref_clk_out   <= '0';
   dac_sync_out      <= '0';

   
   --========================================================================
   -- Generate ADC Start of Conversion pulse
   -- Notes:
   --    Conversion rate is set by adc_ref_clk_in frequency and by n_reg_sample_rate_div.
   --    Pulse output is minimum of 4 clocks wide to meet tCNVH min 10ns with
   --    a 300MHZ ref clock.
   --========================================================================
   conv_proc : process (adc_reset, adc_ref_clk_in)
   begin
      if adc_reset='1' then
         s_adc_en_dly1 <= '0';
         s_adc_en_dly2 <= '0';
         s_conversion_req_dly1 <= '0';
         s_conversion_req_dly2 <= '0';
         s_conversion_start <= '0';
         s_adc_conv_start_out <= '0';
         adc_conv_start_out <= '0';
         n_cycle_counter <= 0;
         a_status_busy <= '0';   --Idle
      elsif rising_edge(adc_ref_clk_in) then
      
         --Resync a_global_adc_enable to adc clock domain
         s_adc_en_dly1    <= a_global_adc_enable;
         s_adc_en_dly2    <= s_adc_en_dly1;
      
         --Resync single-shot conversion request to adc clock domain
         s_conversion_req_dly1 <= a_global_conversion_req;
         s_conversion_req_dly2 <= s_conversion_req_dly1;
         
         if s_adc_en_dly2='1' or (s_conversion_req_dly2='0' and s_conversion_req_dly1='1') then
            s_conversion_start <= '1';  --Rising edge detected, start conversion
         else
            s_conversion_start <= '0';
         end if;
      
         --Cycle divider counter
         if n_cycle_counter /= 0 then
            n_cycle_counter <= n_cycle_counter - 1;
         else
            --Check if need to do another conversion or be idle
            if s_conversion_start='1' then
               n_cycle_counter <= n_reg_sample_rate_div;
               a_status_busy <= '1';   --Running
            else
               a_status_busy <= '0';   --Idle
            end if;
         end if;

         --Generate external conversion start pulse to ADC's
         --Need >= 10ns pulse width with 300MHz ref clock
         --Conversion start pulse width must be >= 10ns min pulse width using the adc_ref_clk_in (300MHz max)
         if n_cycle_counter = ADC_CONV_PULSE_WIDTH-1 then
            s_adc_conv_start_out <= '1';
         elsif n_cycle_counter = 0 then
            s_adc_conv_start_out <= '0';
         end if;

         --Delay adc_conv_start_out by one clock for internal pipeline delay in FPGA
         adc_conv_start_out <= s_adc_conv_start_out;
         
      end if;
   end process;


   --========================================================================
   -- Generate ADC ref clock
   -- Notes:
   --    The AD7960 requires a gated clock output.
   --    s_adc_clk_en is generated as an 18 clock wide strobe that
   --    is fed into a DDR IOB output where it is converted into a clock
   --    by toggling between the strobe and a '0'.
   --========================================================================
   ref_clk_proc : process (adc_reset, adc_ref_clk_in)
   begin
      if adc_reset='1' then
         n_bit_counter <= 0;
         s_adc_clk_en <= '0';
      elsif rising_edge(adc_ref_clk_in) then
      
         --Generate clock enable that is BITS_PER_SYMBOL clocks wide
         if s_adc_conv_start_out='1' and s_adc_clk_en='0' then
            s_adc_clk_en <= '1';
         elsif s_adc_clk_en='1' and n_bit_counter /= BITS_PER_SYMBOL-1 then
            n_bit_counter <= n_bit_counter + 1;
         else
            s_adc_clk_en <= '0';
            n_bit_counter <= 0;
         end if;
         
      end if;
   end process;

   --========================================================================
   --Convert s_adc_clk_en strobe into an output clock by DDR IOB
   --========================================================================
   ddr_clk_inst : ddr_out1 PORT MAP (
      datain_h(0)    => s_adc_clk_en,
      datain_l(0)    => '0',
      outclock	      => adc_ref_clk_in,
      dataout(0)     => adc_clk_out
   );

   
   --========================================================================
   -- ADC Input Data Shift Register
   -- Use incoming adc clock's to sample adc data into shift registers
   -- No bit counter is needed since the output clock is generated with 18
   -- pulses per sample.
   --========================================================================
   shift_data_proc: for i in 0 to NUM_ADC_CHANNELS-1 generate
      shift_proc : process (adc_rx_clk_in)
      begin
         --Clock incoming data from ADC into shift register on rising edge of incoming clock
--20160318, JCS: Changed to a common clock for ALL 8 ADC's to improve timing.  Channel 1 has the best timing.
--         if rising_edge(adc_rx_clk_in(i)) then
--Clock all channels with Channel 1 clock
         if rising_edge(adc_rx_clk_in(1)) then
            s_shift_data(i) <= s_shift_data(i)(BITS_PER_SYMBOL-2 downto 0) & adc_rx_data_in(i); --Shift MSB in first
         end if;
      end process;
   end generate;
   
   
   --========================================================================
   -- Generate serial to parallel conversion strobe
   --
   -- Uses the aso_clk domain to clock the received serial shift register into
   -- parallel data without using a fifo.
   --
   -- Note:  The falling edge of s_adc_clk_en strobe is used to indicate when
   --        all serial data bits have been shifted, it must account for 
   --        the following delays:
   --          tDCO     (5ns)         = Time delay of ADC from input clock to echoed clock.
   --          1/2*tCLK (0.5*3.333ns) = Output delay of DDR reg clock to ADC.
   --          1*tCLK   (1  *3.333ns) = Input clock to serial shift register.
   --          ------------------
   --          Total =   > 10ns       = Min time that NUM_RESYNC_PIPE_BITS must delay.
   --
   --========================================================================
   resync_proc : process (aso_reset, aso_clk)
   begin
      if aso_reset='1' then
         s_resync_pipe <= (others=>'0');
         s_ser_to_par_en <= '0';
      elsif rising_edge(aso_clk) then

         --Run adc clock enable strobe into a shift register on another clock domain
         s_resync_pipe <= s_resync_pipe(NUM_RESYNC_PIPE_BITS-2 downto 0) & s_adc_clk_en;

         --Look for falling edge on delayed s_adc_clk_en to determine when to convert serial data into parallel data using aso_clk
         if s_resync_pipe(NUM_RESYNC_PIPE_BITS-1)='1' and s_resync_pipe(NUM_RESYNC_PIPE_BITS-2)='0' then
            s_ser_to_par_en <= '1';
         else
            s_ser_to_par_en <= '0';
         end if;
      end if;
   end process;

--   --Use ms bit for serial to parallel conversion enable
--   s_ser_to_par_en <= s_ser_to_par_dly(SER_TO_PAR_DLY_BITS-1);
   
   --========================================================================
   -- Serial to Parallel Conversion
   -- Converts shift register into parallel data for final output
   --========================================================================
   ser2par_proc : process (aso_reset, aso_clk)
   begin
      --Once for each incoming lane
      if aso_reset='1' then
         for i in 0 to NUM_ADC_CHANNELS-1 loop
            s_data_out(i) <= (others=>'0');
         end loop;
         s_parallel_data_valid <= '0';
      elsif rising_edge(aso_clk) then
         if s_ser_to_par_en='1' then
            for i in 0 to NUM_ADC_CHANNELS-1 loop
               s_data_out(i) <= s_shift_data(i);
            end loop;
            s_parallel_data_valid <= '1';
         else
            s_parallel_data_valid <= '0';
         end if;
      end if;
   end process;

   --========================================================================
   -- ASO Pipeline stage to help with timing in large FPGA
   --========================================================================
   pipe_proc : process (aso_reset, aso_clk)
   begin
      if aso_reset='1' then
         aso_sop_out       <= '0';
         aso_eop_out       <= '0';
         aso_valid_out     <= '0';
         aso_data_out      <= (others=>'0');
         aso_channel_out   <= (others=>'0');
      elsif rising_edge(aso_clk) then
         --Add an extra pipeline stage to ASO outputs
         aso_sop_out       <= s_aso_sop_pipe;
         aso_eop_out       <= s_aso_eop_pipe;
         aso_valid_out     <= s_aso_valid_pipe;
         aso_data_out      <= s_aso_data_pipe;
         aso_channel_out   <= s_aso_channel_pipe;
      end if;
   end process;
   
   --========================================================================
   -- Convert parallel data array into serial aso output stream
   --========================================================================
   dout_proc : process (aso_reset, aso_clk)
   begin
      if aso_reset='1' then
         s_packet_busy        <= '0';
         s_valid_packet       <= '0';
         n_channel_ctr        <= 0;
         s_channel_mask       <= (others=>'0');
         s_aso_sop_pipe       <= '0';
         s_aso_eop_pipe       <= '0';
         s_aso_valid_pipe     <= '0';
         s_aso_data_pipe      <= (others=>'0');
         s_aso_channel_pipe   <= (others=>'0');
      elsif rising_edge(aso_clk) then

         if s_parallel_data_valid='1' and s_packet_busy='0' then
            s_packet_busy <= '1';
            s_valid_packet <= '0';
            n_channel_ctr <= 0;   --Reset channel counter to 0
            s_channel_mask <= s_reg_chan_en_mask;
            s_aso_sop_pipe <= '0';
            s_aso_eop_pipe <= '0';
            s_aso_valid_pipe <= '0';
            s_aso_channel_pipe <= (others=>'0');
         elsif s_packet_busy='1' then
            --Output valid data on ASO streaming bus
--            s_aso_data_pipe <= s_shift_data(n_channel_ctr);
            s_aso_data_pipe <= s_data_out(n_channel_ctr);
            s_aso_channel_pipe <= std_logic_vector(to_unsigned(n_channel_ctr, s_aso_channel_pipe'length));
            s_aso_valid_pipe <= s_reg_chan_en_mask(n_channel_ctr);
            if n_channel_ctr /= NUM_ADC_CHANNELS-1 then
               n_channel_ctr <= n_channel_ctr + 1; --Decrement channel counter
            else
               s_packet_busy <= '0';   --Final channel has been output, go wait for next sequence
            end if;
            
            -----------------------------------------------
            -- Packet Support
            -----------------------------------------------
            --Output SOP on first channel, even if it is not enabled
--            if n_channel_ctr=0 and s_channel_mask/=std_logic_vector(to_unsigned(2**(NUM_ADC_CHANNELS-2), s_channel_mask'length))then
            if n_channel_ctr=0 and s_channel_mask/=std_logic_vector(to_unsigned(0, s_channel_mask'length)) then
               s_aso_sop_pipe <= '1';  --Assert with first channel count, but keep asserted until first enabled channel
               s_valid_packet <= '1';
            elsif s_aso_valid_pipe='1' then
               s_aso_sop_pipe <= '0';  --Deassert after first enabled channel
            end if;
            
            s_channel_mask <= '0' & s_channel_mask(NUM_ADC_CHANNELS-1 downto 1); --Right shift enable mask so EOP can be output on last active channel
            --If more than 1 channel is enabled, output EOP on last enabled channel
            if s_valid_packet='1' and s_channel_mask=std_logic_vector(to_unsigned(1, s_channel_mask'length)) then  --Check if only LS bit is set
               s_aso_eop_pipe <= '1';
            else
               s_aso_eop_pipe <= '0';
            end if;
         else
            s_aso_valid_pipe <= '0';
            s_valid_packet <= '0';
            s_aso_sop_pipe <= '0';
            s_aso_eop_pipe <= '0';
         end if;

      end if;
   end process;

   
   --************************************************************************
   -- Avalon Memory-Mapped Register Read/Write Logic
   --************************************************************************
   --========================================================================
   -- Register Read/Write Wait Request
   --========================================================================
   wait_req_proc : process (aso_reset, aso_clk)
   begin
      if aso_reset='1' then
         avs_waitrequest <= '1';
      elsif rising_edge(aso_clk) then
         if avs_chipselect = '1' then
            avs_waitrequest <= '0';
         else
            avs_waitrequest <= '1'; --Default state is asserted
         end if;
      end if;
   end process;


   --========================================================================
   -- Read registers
   --========================================================================
   reg_read_proc : process (aso_reset, aso_clk)
   begin
      if aso_reset='1' then
         avs_readdata <= (others=>'0');
      elsif rising_edge(aso_clk) then
--Removed to make timing faster
--         if avs_chipselect='1' and avs_read='1' and avs_byteenable(3 downto 0)="1111" then
         if avs_chipselect='1' and avs_read='1' then
            --Decode register addresses
            case avs_address is
               when cADDR_REG_CONTROL =>
                  avs_readdata   <= std_logic_vector(resize(unsigned(s_reg_control), avs_readdata'length));
               when cADDR_REG_STATUS =>
                  avs_readdata   <= std_logic_vector(resize(unsigned(s_reg_status), avs_readdata'length));
               when cADDR_REG_CHAN_EN_MASK =>
                  avs_readdata   <= std_logic_vector(resize(unsigned(s_reg_chan_en_mask), avs_readdata'length));
               when cADDR_REG_SAMPLE_RATE_DIV =>
                  avs_readdata   <= std_logic_vector(to_unsigned(n_reg_sample_rate_div, avs_readdata'length));
               when cADDR_REG_DAC_CLK_DIV =>
                  avs_readdata   <= std_logic_vector(to_unsigned(n_reg_dac_ref_clk_div, avs_readdata'length));
               when cADDR_REG_DAC_SYNC_DIV =>
                  avs_readdata   <= std_logic_vector(to_unsigned(n_reg_dac_sync_div, avs_readdata'length));
               when cADDR_REG_DAC_SYNC_PHASE =>
                  avs_readdata   <= std_logic_vector(to_unsigned(n_reg_dac_sync_phase, avs_readdata'length));
               when others =>
                  avs_readdata   <= (others=>'1');
            end case;
--         else
--            avs_readdata   <= (others=>'1');
         end if;
      end if;
   end process;   --End of reg_read_proc

   
   --========================================================================
   -- Write Registers
   --========================================================================
   reg_write_proc : process (aso_reset, aso_clk)
   begin
      if aso_reset = '1' then 
         s_reg_control              <= std_logic_vector(to_unsigned(0, s_reg_control'length));  --Disable module by default
         s_reg_chan_en_mask         <= std_logic_vector(to_unsigned(0, s_reg_chan_en_mask'length)); --Disable all ADC's by default
         n_reg_sample_rate_div      <= DEFAULT_SAMPLE_RATE_DIV;
         n_reg_dac_ref_clk_div      <= DEFAULT_DAC_CLK_DIV;
         n_reg_dac_sync_div         <= DEFAULT_DAC_SYNC_DIV;
         n_reg_dac_sync_phase       <= 0;
         n_global_conversion_req_ctr<= 0;
      elsif rising_edge(aso_clk) then
--Removed to make timing faster
--         if avs_chipselect = '1' and avs_write='1' and avs_byteenable(3 downto 0)="1111" and avs_address(AVS_ADDRESS_BITS-1 downto AVS_ADDRESS_BITS-2)="00" then
         if avs_chipselect = '1' and avs_write='1' then
            --Decode register addresses
            case avs_address is
               when cADDR_REG_CONTROL =>
                  s_reg_control           <= avs_writedata(s_reg_control'length-1 downto 0);
                  if avs_writedata(1)='1' then
                     n_global_conversion_req_ctr <= cGLOBAL_CONVERSION_REQ_PULSE_WIDTH;
                  end if;
               when cADDR_REG_STATUS =>
                  null; --Non-Writeable register
               when cADDR_REG_CHAN_EN_MASK =>
                  s_reg_chan_en_mask      <= avs_writedata(s_reg_chan_en_mask'length-1 downto 0);
               when cADDR_REG_SAMPLE_RATE_DIV =>
                  n_reg_sample_rate_div   <= to_integer(unsigned(avs_writedata));
               when cADDR_REG_DAC_CLK_DIV =>
                  n_reg_dac_ref_clk_div   <= to_integer(unsigned(avs_writedata));
               when cADDR_REG_DAC_SYNC_DIV =>
                  n_reg_dac_sync_div      <= to_integer(unsigned(avs_writedata));
               when cADDR_REG_DAC_SYNC_PHASE =>
                  n_reg_dac_sync_phase    <= to_integer(unsigned(avs_writedata));
               when others =>
                  null;
            end case;
         else
            --Auto clear bits / registers here

            --Start pulse-stretch counter so the a_global_conversion_req is long enough to be sampled by a slower adc_ref_clk
            if n_global_conversion_req_ctr /= 0 then
               n_global_conversion_req_ctr <= n_global_conversion_req_ctr - 1;
            else
               a_global_conversion_req <= '0';  --Clear single-shot conversion request            
            end if;
         end if;
      end if;
   end process;   --End of reg_write_proc

   
end ad7960_deserializer_arch;