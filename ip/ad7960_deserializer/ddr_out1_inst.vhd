ddr_out1_inst : ddr_out1 PORT MAP (
		datain_h	 => datain_h_sig,
		datain_l	 => datain_l_sig,
		outclock	 => outclock_sig,
		dataout	 => dataout_sig
	);
