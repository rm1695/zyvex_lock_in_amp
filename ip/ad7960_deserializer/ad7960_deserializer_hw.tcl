# +-----------------------------------
# | 
# | ad7960_deserializer "ad7960_deserializer" v1.0
# | 
# +-----------------------------------

# +-----------------------------------
# | request TCL package from ACDS 9.1
# | 
#package require -exact sopc 10.0
# | 
# +-----------------------------------

# +-----------------------------------
# | module ad7960_deserializer
# | 
set_module_property DESCRIPTION "ADC LVDS Interface for AD7960"
set_module_property NAME ad7960_deserializer
set_module_property VERSION 1.0
set_module_property GROUP "Data Acquisition"
set_module_property AUTHOR "Jeff Short"
set_module_property DISPLAY_NAME "AD7960 ADC LVDS Interface"
#set_module_property LIBRARIES {ieee.std_logic_1164.all ieee.std_logic_unsigned.all ieee.std_logic_arith.all std.standard.all}
set_module_property TOP_LEVEL_HDL_FILE ad7960_deserializer.vhd
set_module_property TOP_LEVEL_HDL_MODULE ad7960_deserializer
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
#set_module_property DATASHEET_URL http://www.mywebpage.com/wiki/ad7960_deserializer
set_module_property SIMULATION_MODEL_IN_VERILOG false
set_module_property SIMULATION_MODEL_IN_VHDL true
set_module_property SIMULATION_MODEL_HAS_TULIPS false
set_module_property SIMULATION_MODEL_IS_OBFUSCATED false
# | 
# +-----------------------------------

# +-----------------------------------
# | files
# | 
add_file ad7960_deserializer.vhd {SYNTHESIS SIMULATION}
add_file ddr_out1.vhd {SYNTHESIS SIMULATION}
# | 
# +-----------------------------------


# +-----------------------------------
# | parameters
# |
add_parameter adc_clock_freq INTEGER
set_parameter_property adc_clock_freq SYSTEM_INFO {CLOCK_RATE "adc_ref_clk_in"}

add_parameter aso_clock_freq INTEGER
set_parameter_property aso_clock_freq SYSTEM_INFO {CLOCK_RATE "aso_clock"}

add_parameter BITS_PER_SYMBOL NATURAL 18 "Bits per symbol"
set_parameter_property BITS_PER_SYMBOL DEFAULT_VALUE 18
set_parameter_property BITS_PER_SYMBOL DISPLAY_NAME BITS_PER_SYMBOL
set_parameter_property BITS_PER_SYMBOL UNITS None
set_parameter_property BITS_PER_SYMBOL ALLOWED_RANGES 8:24
set_parameter_property BITS_PER_SYMBOL DESCRIPTION "ADC bits per sample"
set_parameter_property BITS_PER_SYMBOL DISPLAY_HINT "Number of bits per sample"
set_parameter_property BITS_PER_SYMBOL AFFECTS_GENERATION false
set_parameter_property BITS_PER_SYMBOL HDL_PARAMETER true

add_parameter NUM_ADC_CHANNELS NATURAL 8 "Number of ADC channels"
set_parameter_property NUM_ADC_CHANNELS DEFAULT_VALUE 8
set_parameter_property NUM_ADC_CHANNELS DISPLAY_NAME NUM_ADC_CHANNELS
set_parameter_property NUM_ADC_CHANNELS UNITS None
set_parameter_property NUM_ADC_CHANNELS ALLOWED_RANGES 1:16
set_parameter_property NUM_ADC_CHANNELS DESCRIPTION "Number of ADC channels"
set_parameter_property NUM_ADC_CHANNELS DISPLAY_HINT "Number of ADC channels to create"
set_parameter_property NUM_ADC_CHANNELS AFFECTS_GENERATION false
set_parameter_property NUM_ADC_CHANNELS HDL_PARAMETER true

add_parameter NUM_CHANNEL_BITS NATURAL 3 "Number of bits needed to hold adc channel count"
set_parameter_property NUM_CHANNEL_BITS DEFAULT_VALUE 3
set_parameter_property NUM_CHANNEL_BITS DISPLAY_NAME NUM_CHANNEL_BITS
set_parameter_property NUM_CHANNEL_BITS UNITS None
set_parameter_property NUM_CHANNEL_BITS ALLOWED_RANGES 1:5
set_parameter_property NUM_CHANNEL_BITS DESCRIPTION "Number of bits for adc channel count"
set_parameter_property NUM_CHANNEL_BITS DISPLAY_HINT "Number of bits needed to hold max adc channel number"
set_parameter_property NUM_CHANNEL_BITS AFFECTS_GENERATION false
set_parameter_property NUM_CHANNEL_BITS HDL_PARAMETER true

add_parameter MAX_SAMPLE_RATE_DIV NATURAL 4095 "Max count of ADC sample rate count divider"
set_parameter_property MAX_SAMPLE_RATE_DIV DEFAULT_VALUE 4095
set_parameter_property MAX_SAMPLE_RATE_DIV DISPLAY_NAME MAX_SAMPLE_RATE_DIV
set_parameter_property MAX_SAMPLE_RATE_DIV UNITS None
set_parameter_property MAX_SAMPLE_RATE_DIV ALLOWED_RANGES 32:16383
set_parameter_property MAX_SAMPLE_RATE_DIV DESCRIPTION "Max count of ADC sample rate count divider"
set_parameter_property MAX_SAMPLE_RATE_DIV DISPLAY_HINT "Sets size of ADC sample rate divider counter"
set_parameter_property MAX_SAMPLE_RATE_DIV AFFECTS_GENERATION false
set_parameter_property MAX_SAMPLE_RATE_DIV HDL_PARAMETER true

add_parameter DEFAULT_SAMPLE_RATE_DIV NATURAL 59 "Default adc sample rate divider"
set_parameter_property DEFAULT_SAMPLE_RATE_DIV DEFAULT_VALUE 59
set_parameter_property DEFAULT_SAMPLE_RATE_DIV DISPLAY_NAME DEFAULT_SAMPLE_RATE_DIV
set_parameter_property DEFAULT_SAMPLE_RATE_DIV UNITS None
set_parameter_property DEFAULT_SAMPLE_RATE_DIV ALLOWED_RANGES 0:16383
set_parameter_property DEFAULT_SAMPLE_RATE_DIV DESCRIPTION "Default adc sample rate divider"
set_parameter_property DEFAULT_SAMPLE_RATE_DIV DISPLAY_HINT "Sets N-1 divider count for adc sample rate"
set_parameter_property DEFAULT_SAMPLE_RATE_DIV AFFECTS_GENERATION false
set_parameter_property DEFAULT_SAMPLE_RATE_DIV HDL_PARAMETER true


add_parameter MAX_DAC_CLK_DIV NATURAL 1023 "Max count of ref clock output to DAC"
set_parameter_property MAX_DAC_CLK_DIV DEFAULT_VALUE 1023
set_parameter_property MAX_DAC_CLK_DIV DISPLAY_NAME MAX_DAC_CLK_DIV
set_parameter_property MAX_DAC_CLK_DIV UNITS None
set_parameter_property MAX_DAC_CLK_DIV ALLOWED_RANGES 0:1023
set_parameter_property MAX_DAC_CLK_DIV DESCRIPTION "Max count of ref clock output to DAC"
set_parameter_property MAX_DAC_CLK_DIV DISPLAY_HINT "Constrains the clock divider register size for an output ref clock to the DAC module."
set_parameter_property MAX_DAC_CLK_DIV AFFECTS_GENERATION false
set_parameter_property MAX_DAC_CLK_DIV HDL_PARAMETER true

add_parameter DEFAULT_DAC_CLK_DIV NATURAL 50 "Divider for DAC ref clock from ADC ref clock"
set_parameter_property DEFAULT_DAC_CLK_DIV DEFAULT_VALUE 50
set_parameter_property DEFAULT_DAC_CLK_DIV DISPLAY_NAME DEFAULT_DAC_CLK_DIV
set_parameter_property DEFAULT_DAC_CLK_DIV UNITS None
set_parameter_property DEFAULT_DAC_CLK_DIV ALLOWED_RANGES 0:1023
set_parameter_property DEFAULT_DAC_CLK_DIV DESCRIPTION "Divider for DAC ref clock from ADC ref clock"
set_parameter_property DEFAULT_DAC_CLK_DIV DISPLAY_HINT "Sets N-1 divider count for ref clock to DAC"
set_parameter_property DEFAULT_DAC_CLK_DIV AFFECTS_GENERATION false
set_parameter_property DEFAULT_DAC_CLK_DIV HDL_PARAMETER true

add_parameter MAX_DAC_SYNC_RATE_DIV NATURAL 1023 "Max count of external sync divider"
set_parameter_property MAX_DAC_SYNC_RATE_DIV DEFAULT_VALUE 1023
set_parameter_property MAX_DAC_SYNC_RATE_DIV DISPLAY_NAME MAX_DAC_SYNC_RATE_DIV
set_parameter_property MAX_DAC_SYNC_RATE_DIV UNITS None
set_parameter_property MAX_DAC_SYNC_RATE_DIV ALLOWED_RANGES 0:1023
set_parameter_property MAX_DAC_SYNC_RATE_DIV DESCRIPTION "Max count of external sync divider"
set_parameter_property MAX_DAC_SYNC_RATE_DIV DISPLAY_HINT "Constrains the clock divider register size."
set_parameter_property MAX_DAC_SYNC_RATE_DIV AFFECTS_GENERATION false
set_parameter_property MAX_DAC_SYNC_RATE_DIV HDL_PARAMETER true

add_parameter DEFAULT_DAC_SYNC_DIV NATURAL 50 "Divider for DAC sync pulse output"
set_parameter_property DEFAULT_DAC_SYNC_DIV DEFAULT_VALUE 50
set_parameter_property DEFAULT_DAC_SYNC_DIV DISPLAY_NAME DEFAULT_DAC_SYNC_DIV
set_parameter_property DEFAULT_DAC_SYNC_DIV UNITS None
set_parameter_property DEFAULT_DAC_SYNC_DIV ALLOWED_RANGES 0:1023
set_parameter_property DEFAULT_DAC_SYNC_DIV DESCRIPTION "Divider for DAC sync pulse output"
set_parameter_property DEFAULT_DAC_SYNC_DIV DISPLAY_HINT "Sets N-1 divider count for sync output to DAC module"
set_parameter_property DEFAULT_DAC_SYNC_DIV AFFECTS_GENERATION false
set_parameter_property DEFAULT_DAC_SYNC_DIV HDL_PARAMETER true
     
add_parameter AVALON_MM_BUS_WIDTH NATURAL 32 "Avalon MM Data Bus Width"
set_parameter_property AVALON_MM_BUS_WIDTH DEFAULT_VALUE 32
set_parameter_property AVALON_MM_BUS_WIDTH DISPLAY_NAME AVALON_MM_BUS_WIDTH
set_parameter_property AVALON_MM_BUS_WIDTH UNITS None
set_parameter_property AVALON_MM_BUS_WIDTH ALLOWED_RANGES 32
set_parameter_property AVALON_MM_BUS_WIDTH DESCRIPTION "Avalon MM Data Bus Width in bits"
set_parameter_property AVALON_MM_BUS_WIDTH DISPLAY_HINT "Avalon MM Data Bus Width in bits"
set_parameter_property AVALON_MM_BUS_WIDTH AFFECTS_GENERATION false
set_parameter_property AVALON_MM_BUS_WIDTH AFFECTS_ELABORATION true
set_parameter_property AVALON_MM_BUS_WIDTH HDL_PARAMETER true
# | 
# +-----------------------------------

# +-----------------------------------
# | display items
# | 
add_display_item "ADC Parameters" adc_ref_clk_in PARAMETER
add_display_item "ADC Parameters" MAX_SAMPLE_RATE_DIV PARAMETER
add_display_item "ADC Parameters" DEFAULT_SAMPLE_RATE_DIV PARAMETER

add_display_item "Avalon ST Interface" aso_clk_freq PARAMETER
add_display_item "Avalon ST Interface" BITS_PER_SYMBOL PARAMETER
add_display_item "Avalon ST Interface" NUM_ADC_CHANNELS PARAMETER
add_display_item "Avalon ST Interface" NUM_CHANNEL_BITS PARAMETER

add_display_item "DAC Clock Parameters" MAX_DAC_CLK_DIV PARAMETER
add_display_item "DAC Clock Parameters" DEFAULT_DAC_CLK_DIV PARAMETER
add_display_item "DAC Clock Parameters" MAX_DAC_SYNC_RATE_DIV PARAMETER
add_display_item "DAC Clock Parameters" DEFAULT_DAC_SYNC_DIV PARAMETER

add_display_item "Avalon MM Interface" AVALON_MM_BUS_WIDTH PARAMETER
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point aso_clock
# | 
add_interface aso_clock clock end
set_interface_property aso_clock ENABLED true
add_interface_port aso_clock aso_clk clk Input 1
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point aso_reset
# | 
add_interface aso_reset reset end
set_interface_property aso_reset associatedClock aso_clock
set_interface_property aso_reset ENABLED true
set_interface_property aso_reset synchronousEdges DEASSERT
add_interface_port aso_reset aso_reset reset Input 1
# | 
# +-----------------------------------


# +-----------------------------------
# | connection point adc_ref_clk_in
# | 
add_interface adc_ref_clk clock end
set_interface_property adc_ref_clk ENABLED true
add_interface_port adc_ref_clk adc_ref_clk_in clk Input 1
# | 
# +-----------------------------------

# +-----------------------------------
# | connection point adc_reset
# | 
add_interface adc_reset reset end
set_interface_property adc_reset associatedClock aso_clock
set_interface_property adc_reset ENABLED true
set_interface_property adc_reset synchronousEdges DEASSERT
add_interface_port adc_reset adc_reset reset Input 1
# | 
# +-----------------------------------


# +-----------------------------------
# | connection point ext_dac_ref
# | 
add_interface ext_dac_ref conduit source
set_interface_property ext_dac_ref ENABLED true
add_interface_port ext_dac_ref dac_reset_out    export Output 1
add_interface_port ext_dac_ref dac_ref_clk_out  export Output 1
add_interface_port ext_dac_ref dac_sync_out     export Output 1
# | 
# +-----------------------------------


set_module_property ELABORATION_CALLBACK decoder_elaboration_callback
proc decoder_elaboration_callback {} {
   set obps [get_parameter_value BITS_PER_SYMBOL]
#   set ospb [get_parameter_value SYMBOLS_PER_BEAT]
   set out_data_width [expr $obps]
   set the_num_adc_channels [get_parameter_value NUM_ADC_CHANNELS]
   set the_channel_width [get_parameter_value NUM_CHANNEL_BITS]
   set the_avalon_mm_bus_width [get_parameter_value AVALON_MM_BUS_WIDTH]
   set the_byteenablewidth [expr {$the_avalon_mm_bus_width / 8} ]
   set the_adc_clock_freq  [get_parameter_value "adc_clock_freq"]
   set the_aso_clock_freq  [get_parameter_value "aso_clock_freq"]
   
   # Output values to system.h
   set_module_assignment embeddedsw.CMacro.ADC_CLOCK_FREQUENCY          $the_adc_clock_freq
   set_module_assignment embeddedsw.CMacro.ASO_CLOCK_FREQUENCY          $the_aso_clock_freq
   set_module_assignment embeddedsw.CMacro.BITS_PER_SYMBOL              [get_parameter_value BITS_PER_SYMBOL]
   set_module_assignment embeddedsw.CMacro.NUM_ADC_CHANNELS             [get_parameter_value NUM_ADC_CHANNELS]
   set_module_assignment embeddedsw.CMacro.NUM_ADC_CHANNELS             [get_parameter_value NUM_CHANNEL_BITS]
   set_module_assignment embeddedsw.CMacro.MAX_SAMPLE_RATE_DIV          [get_parameter_value MAX_SAMPLE_RATE_DIV]
   set_module_assignment embeddedsw.CMacro.MAX_DAC_SYNC_RATE_DIV        [get_parameter_value MAX_DAC_SYNC_RATE_DIV]

   # +-----------------------------------
   # | connection point ext_adc_input
   # | 
   add_interface ext_adc_input conduit source
   set_interface_property ext_adc_input ENABLED true
   add_interface_port ext_adc_input adc_clk_out          export Output 1
   add_interface_port ext_adc_input adc_conv_start_out   export Output 1
   add_interface_port ext_adc_input adc_en_bits          export Output 4
   add_interface_port ext_adc_input adc_rx_clk_in        export Input $the_num_adc_channels
   add_interface_port ext_adc_input adc_rx_data_in       export Input $the_num_adc_channels
   # | 
   # +-----------------------------------

   # +-----------------------------------
   # | connection point dout
   # | 
   add_interface dout avalon_streaming start
   set_interface_property dout dataBitsPerSymbol $obps
   set_interface_property dout errorDescriptor ""
   set_interface_property dout maxChannel 0
## set_interface_property dout readyLatency 1
## set_interface_property dout readyLatency 0
   set_interface_property dout associatedClock aso_clock
   set_interface_property dout associatedReset aso_reset
   set_interface_property dout ENABLED true

## add_interface_port dout aso_ready_in ready Input 1
   add_interface_port dout aso_sop_out startofpacket Output 1
   add_interface_port dout aso_eop_out endofpacket Output 1
   add_interface_port dout aso_valid_out valid Output 1
   add_interface_port dout aso_data_out data Output $out_data_width
   add_interface_port dout aso_channel_out channel Output $the_channel_width
##   add_interface_port dout aso_empty_out empty Output 1
   # | 
   # +-----------------------------------

   # +-----------------------------------
   # | Avalon-MM Slave Interface
   # | connection point av_mm
   # | 
   add_interface control avalon slave
#   add_interface control avalon end
   set_interface_property control addressAlignment DYNAMIC
#   set_interface_property control addressSpan $the_address_span
   set_interface_property control bridgesToMaster ""
   set_interface_property control burstOnBurstBoundariesOnly false
   set_interface_property control holdTime 0
   set_interface_property control isMemoryDevice false
   set_interface_property control isNonVolatileStorage false
   set_interface_property control linewrapBursts false
   set_interface_property control maximumPendingReadTransactions 0
   set_interface_property control minimumUninterruptedRunLength 1
   set_interface_property control printableDevice false
   set_interface_property control readLatency 0
   set_interface_property control readWaitTime 0
   set_interface_property control setupTime 0
   set_interface_property control timingUnits Cycles
   set_interface_property control writeWaitTime 0
   set_interface_property control associatedClock aso_clock
   set_interface_property control associatedReset aso_reset
   set_interface_property control ENABLED true

   add_interface_port control avs_chipselect chipselect Input 1
   add_interface_port control avs_read read Input 1
   add_interface_port control avs_write write Input 1
   add_interface_port control avs_address address Input 3
   add_interface_port control avs_readdata readdata Output $the_avalon_mm_bus_width
   add_interface_port control avs_writedata writedata Input $the_avalon_mm_bus_width
   add_interface_port control avs_waitrequest waitrequest Output 1
   add_interface_port control avs_byteenable byteenable Input $the_byteenablewidth
   # | 
   # +-----------------------------------
}
