----------------------------------------------------------------------------
-- Date:    2017-07-21
-- File:    avs_fifo.vhd
-- By:      Robert Mason
--
-- Desc:    8 Fifo model
--
-- Notes:   
--
-- Algorithm:
--
--
--
-- Known bugs:
--		Unable to use word count
--
------------------------------------------------------------------------------
-- Register Definitions:
-- -----------------------------------------------------------------------
-- Name                       Reg# Offset   R/W   Description
-- -----------------------------------------------------------------------
-- reg_control                 0 (0x00)     R/W
--    [0-2]: Unused								 R/W
--		[3] a_global_sign_ext_en				 R    Whether sign extension should be carried to 32 bits
--
--
-- reg_word_count					5 (0x14)		 R		
--		[31:0]										 R		Returns 8 if fifo has data, 1 otherwise
--
-- reg_status                  7 (01Cx)     R/W
--    [0]: Data Done			                R	Current Data complete: 1=Finished, 0=Unfinished
--
--
--
------------------------------------------------------------------------------
--
--Revisions:
--
-- 20170721 - Robert Mason
--		Initial document creation, with integration into system to read and write
--
-- 20190728 - Robert Mason
--		Adaptation for use in lock_in_amplifier
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity avs_fifo is
   generic (
      BITS_PER_SYMBOL         : natural range 4 to 32  := 28;  --Number of bits per symbol
      NUM_CHANNELS            : natural range 1 to 32  := 8;   --Number of channels
      NUM_CHANNEL_BITS        : natural range 1 to 8   := 3;   --Number of bits for channel counter
      FIFO_DEPTH              : integer := 4;                --Number of words in fifo
      AVALON_MM_BUS_WIDTH     : integer := 32                  --Number of data bits to use for Avalon Memory-Mapped Slave bus
   );
   port
   (
      --Avalon Streaming data in
      asi_reset               : in  std_logic;  --Reset for all Avalon buses
      asi_clk                 : in  std_logic;  --Clock for all Avalon buses
      asi_sop_in              : in  std_logic;
      asi_eop_in              : in  std_logic;
      asi_valid_in            : in  std_logic;
      asi_data_in             : in  std_logic_vector(BITS_PER_SYMBOL-1 downto 0);  --Parallel shift register data output
      asi_channel_in          : in  std_logic_vector(NUM_CHANNEL_BITS-1 downto 0); --Channel number output
      
      --Avalon-MM Slave Interface
      avs_read                : in  std_logic;
      avs_write               : in  std_logic;
      avs_address             : in  std_logic_vector(3 downto 0); --Set large enough to handle control registers + 32 DAC channel registers
      avs_readdata            : out std_logic_vector(AVALON_MM_BUS_WIDTH-1 downto 0);
      avs_writedata           : in  std_logic_vector(AVALON_MM_BUS_WIDTH-1 downto 0);
		
		fifo_start			: out std_logic
   );
end avs_fifo;

architecture rtl of avs_fifo is

   --========================================================================
   -- Constant Definitions
   --========================================================================

	--Control Registers
	constant cADDR_REG_FIFO_CONTROL		: std_logic_vector(3 downto 0) 	:= "0000";  --Control register
	constant cADDR_WORD_COUNT				: std_logic_vector(3 downto 0)	:= "0101";
	constant cADDR_REG_FIFO_STATUS		: std_logic_vector(3 downto 0)	:= "0110";  --Status register
	constant cADDR_FILL_LEVEL				: std_logic_vector(3 downto 0)	:= "0001";
	constant cADDR_FIFO_CLEAR				: std_logic_vector(3 downto 0)	:= "0111";
	
	--Constants Registers
	constant cADDR_REG_CHANNEL_0			: std_logic_vector(3 downto 0)	:= "1000";
	constant cADDR_REG_CHANNEL_1			: std_logic_vector(3 downto 0)	:= "1001";
	constant cADDR_REG_CHANNEL_2			: std_logic_vector(3 downto 0)	:= "1010";
	constant cADDR_REG_CHANNEL_3			: std_logic_vector(3 downto 0)	:= "1011";
	constant cADDR_REG_CHANNEL_4			: std_logic_vector(3 downto 0)	:= "1100";
	constant cADDR_REG_CHANNEL_5			: std_logic_vector(3 downto 0)	:= "1101";
	constant cADDR_REG_CHANNEL_6			: std_logic_vector(3 downto 0)	:= "1110";
	constant cADDR_REG_CHANNEL_7			: std_logic_vector(3 downto 0)	:= "1111";
	
   --========================================================================
   -- Register Declarations
   --========================================================================
	
	signal control									: std_logic_vector(3 downto 0) := "0000";
	signal status									: std_logic_vector(3 downto 0) := "0000";
	
		
   --========================================================================
   -- Signal Declarations
   --========================================================================
	
	--Data Signals
	signal read_fifo								: std_logic_vector(NUM_CHANNELS - 1 downto 0) := (others => '0');
	signal write_fifo								: std_logic_vector(NUM_CHANNELS - 1 downto 0) := (others => '0');
	
	signal clear									: std_logic 						:= '0';
	
	--data handling
	type data_array								is array (0 to NUM_CHANNELS - 1) of std_logic_vector(AVALON_MM_BUS_WIDTH - 1 downto 0);
	signal data_in									: data_array						:= (others => (others => '0'));
	signal data_out								: data_array						:= (others => (others => '0'));
	signal data_ext								: data_array						:= (others => (others => '0'));
	
	signal fifo_fill_level						: integer							:= 1;
	signal lis										: std_logic_vector(7 downto 0) := (others => '0');
	
	--Flags
	signal full										: std_logic_vector(NUM_CHANNELS - 1 downto 0) := (others => '0');
	signal empty									: std_logic_vector(NUM_CHANNELS - 1 downto 0) := (others => '0');
	type usedw_array								is array (0 to NUM_CHANNELS - 1) of std_logic_vector(11 downto 0);
	signal usedw									: usedw_array											 := (others => (others => '0'));
	
	alias a_control_0								is control(0);
	alias a_control_1								is control(1);
	alias a_control_2								is control(2);
	alias a_global_sign_ext_en					is control(3);
	
	alias a_status_0								is status(0);
	alias a_status_1								is status(1);
	alias a_status_2								is status(2);
	alias a_status_3								is status(3);
	
   --========================================================================
   -- Component Definitions
   --========================================================================

	component sc_fifo
	PORT
	(
		clock		: IN STD_LOGIC ;
		data		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		rdreq		: IN STD_LOGIC ;
		sclr		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;
		empty		: OUT STD_LOGIC ;
		full		: OUT STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		usedw		: OUT STD_LOGIC_VECTOR (11 DOWNTO 0)
	);
	end component;
	
begin

   --========================================================================
   -- Output Pin Mapping
   --========================================================================
	--taking ASI data in
	gata_gen : for i in 0 to NUM_CHANNELS - 1 generate
		data_in(i) <= "0000" & asi_data_in;
	end generate;
	
	--assigning input
	write_fifo(0) <= '1' when asi_valid_in = '1' and asi_channel_in = "000" and to_integer(unsigned(usedw(0))) < fifo_fill_level else '0';
	write_fifo(1) <= '1' when asi_valid_in = '1' and asi_channel_in = "001" and to_integer(unsigned(usedw(1))) < fifo_fill_level else '0';
	write_fifo(2) <= '1' when asi_valid_in = '1' and asi_channel_in = "010" and to_integer(unsigned(usedw(2))) < fifo_fill_level else '0';
	write_fifo(3) <= '1' when asi_valid_in = '1' and asi_channel_in = "011" and to_integer(unsigned(usedw(3))) < fifo_fill_level else '0';
	write_fifo(4) <= '1' when asi_valid_in = '1' and asi_channel_in = "100" and to_integer(unsigned(usedw(4))) < fifo_fill_level else '0';
	write_fifo(5) <= '1' when asi_valid_in = '1' and asi_channel_in = "101" and to_integer(unsigned(usedw(5))) < fifo_fill_level else '0';
	write_fifo(6) <= '1' when asi_valid_in = '1' and asi_channel_in = "110" and to_integer(unsigned(usedw(6))) < fifo_fill_level else '0';
	write_fifo(7) <= '1' when asi_valid_in = '1' and asi_channel_in = "111" and to_integer(unsigned(usedw(7))) < fifo_fill_level else '0';
	
	--reading fifos
	read_fifo(0) <= '1' when avs_read = '1' and avs_address = "1000" else '0';
	read_fifo(1) <= '1' when avs_read = '1' and avs_address = "1001" else '0';
	read_fifo(2) <= '1' when avs_read = '1' and avs_address = "1010" else '0';
	read_fifo(3) <= '1' when avs_read = '1' and avs_address = "1011" else '0';
	read_fifo(4) <= '1' when avs_read = '1' and avs_address = "1100" else '0';
	read_fifo(5) <= '1' when avs_read = '1' and avs_address = "1101" else '0';
	read_fifo(6) <= '1' when avs_read = '1' and avs_address = "1110" else '0';
	read_fifo(7) <= '1' when avs_read = '1' and avs_address = "1111" else '0';
	
   --========================================================================
   --Component Instantiation
	--========================================================================

	--Generating the actual models
	fifo_gen : for i in 0 to NUM_CHANNELS - 1 generate
		fifo_inst : sc_fifo
		PORT map
		(
			clock		=>asi_clk,
			data		=>data_in(i),
			rdreq		=>read_fifo(i),
			sclr		=> clear,
			wrreq		=>write_fifo(i),
			empty		=>empty(i),
			full		=>full(i),
			q			=>data_out(i),
			usedw		=> usedw(i)
		);
	end generate;
		
	--========================================================================
	--Avalon Processes
	--========================================================================
	
	------------------------------------------------------------
	--Read Process
	------------------------------------------------------------
	--Reading from registers process
	reg_read_proc : process (asi_clk,asi_reset)
	begin
		if(asi_reset = '1') then
		
		elsif rising_edge(asi_clk) then
			if avs_read = '1' then
				case avs_address is 
					when cADDR_REG_FIFO_CONTROL => 
						avs_readdata <= std_logic_vector(resize(unsigned(control), avs_readdata'length));
					when cADDR_WORD_COUNT=> --If none are empty there must be 1 piece of data in each register
--						if empty = "00000000" then
--							avs_readdata <= "00000000000000000000000000001000";
--						--elsif empty = "00000000" and (usedw(0) = "10" or usedw(1) = "10" or usedw(2) = "10" or 
--						--										usedw(3) = "10" or usedw(4) = "10" or usedw(5) = "10" or 
--						--										usedw(6) = "10" or usedw(7) = "10") then
--						--	avs_readdata <= "00000000000000000000000000001001";
--						else
--							avs_readdata <= "00000000000000000000000000000001";
--						end if;
----						avs_readdata <= "00000000000000000000000000001001";
						avs_readdata <= "00000000000000000000" & usedw(0);
					when cADDR_REG_FIFO_STATUS=> 
						avs_readdata <= std_logic_vector(resize(unsigned(status), avs_readdata'length));
					when cADDR_REG_CHANNEL_0=> 
						if a_global_sign_ext_en='1' then --Might be an issue
							--Sign extend data to 32 bits
							avs_readdata      <= std_logic_vector(resize(signed(data_out(0)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						else
							--No sign extension
							avs_readdata      <= std_logic_vector(resize(unsigned(data_out(0)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						end if;
					when cADDR_REG_CHANNEL_1=>
						if a_global_sign_ext_en='1' then --Might be an issue
							--Sign extend data to 32 bits
							avs_readdata      <= std_logic_vector(resize(signed(data_out(1)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						else
							--No sign extension
							avs_readdata      <= std_logic_vector(resize(unsigned(data_out(1)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						end if;
					when cADDR_REG_CHANNEL_2=>
						if a_global_sign_ext_en='1' then --Might be an issue
							--Sign extend data to 32 bits
							avs_readdata      <= std_logic_vector(resize(signed(data_out(2)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						else
							--No sign extension
							avs_readdata      <= std_logic_vector(resize(unsigned(data_out(2)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						end if;
					when cADDR_REG_CHANNEL_3=>
						if a_global_sign_ext_en='1' then --Might be an issue
							--Sign extend data to 32 bits
							avs_readdata      <= std_logic_vector(resize(signed(data_out(3)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						else
							--No sign extension
							avs_readdata      <= std_logic_vector(resize(unsigned(data_out(3)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						end if;
					when cADDR_REG_CHANNEL_4=>
						if a_global_sign_ext_en='1' then --Might be an issue
							--Sign extend data to 32 bits
							avs_readdata      <= std_logic_vector(resize(signed(data_out(4)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						else
							--No sign extension
							avs_readdata      <= std_logic_vector(resize(unsigned(data_out(4)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						end if;
					when cADDR_REG_CHANNEL_5=>
						if a_global_sign_ext_en='1' then --Might be an issue
							--Sign extend data to 32 bits
							avs_readdata      <= std_logic_vector(resize(signed(data_out(5)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						else
							--No sign extension
							avs_readdata      <= std_logic_vector(resize(unsigned(data_out(5)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						end if;
					when cADDR_REG_CHANNEL_6=>
						if a_global_sign_ext_en='1' then --Might be an issue
							--Sign extend data to 32 bits
							avs_readdata      <= std_logic_vector(resize(signed(data_out(6)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						else
							--No sign extension
							avs_readdata      <= std_logic_vector(resize(unsigned(data_out(6)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						end if;
					when cADDR_REG_CHANNEL_7=>
						if a_global_sign_ext_en='1' then --Might be an issue
							--Sign extend data to 32 bits
							avs_readdata      <= std_logic_vector(resize(signed(data_out(7)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						else
							--No sign extension
							avs_readdata      <= std_logic_vector(resize(unsigned(data_out(7)(BITS_PER_SYMBOL - 1 downto 0)), avs_readdata'length));
						end if;
					when others =>
						avs_readdata <= (others => '1');
				end case;
			end if;
		end if;
	end process;
	
	------------------------------------------------------------
	--Write Process
	------------------------------------------------------------
	--Handles the write to register functions
	--Based around case statement that selects where the input value is being written
	--As of right now, has limited functionality but will be expanded when other registers are used
	reg_write_proc : process (asi_reset,asi_clk)
	begin
		if(asi_reset = '1') then
		elsif rising_edge(asi_clk) then
			if avs_write = '1' then		
				case avs_address is 
				
					--Control registers
					when cADDR_REG_FIFO_CONTROL =>
						control <= avs_writedata(3 downto 0);
					when cADDR_REG_FIFO_STATUS =>
						status <= avs_writedata(3 downto 0);
					when cADDR_FILL_LEVEL =>
						fifo_fill_level <= to_integer(unsigned(avs_writedata));
					---------------------
					--Read only register
					---------------------
					when cADDR_WORD_COUNT =>
						null;
					when "1---" => --should work, might need to expand
						null;
					when others =>
						null;
				end case;
			end if;
		end if;
	end process;
	
	--reset process
	reset_proc : process (asi_reset,asi_clk)
	begin
		if asi_reset = '1' then
			clear <= '1';
		elsif rising_edge(asi_clk) then
			if avs_write = '1' and avs_address = cADDR_FIFO_CLEAR then
				clear <= '1';
			else
				clear <= '0';
			end if;	
		end if;
	end process;
	
	dac_proc : process (asi_reset,asi_clk)
	begin
		if asi_reset = '1' then
			fifo_start <= '0';
		elsif rising_edge(asi_clk) then
			if asi_valid_in = '1' and asi_channel_in = "000" then
				fifo_start <= '1';
			else
				fifo_start <= '0';
			end if;
		end if;
	end process;
	
	
end architecture rtl; -- of fifo