LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY avs_fifo_tb IS 
END avs_fifo_tb;

ARCHITECTURE behavior OF avs_fifo_tb IS

   component avs_fifo is
   generic (
      BITS_PER_SYMBOL         : natural range 4 to 32  := 28;  --Number of bits per symbol
      NUM_CHANNELS            : natural range 1 to 32  := 8;   --Number of channels
      NUM_CHANNEL_BITS        : natural range 1 to 8   := 3;   --Number of bits for channel counter
      FIFO_DEPTH              : integer := 4;                --Number of words in fifo
      AVALON_MM_BUS_WIDTH     : integer := 32                  --Number of data bits to use for Avalon Memory-Mapped Slave bus
   );
   port
   (
      --Avalon Streaming data in
      asi_reset               : in  std_logic;  --Reset for all Avalon buses
      asi_clk                 : in  std_logic;  --Clock for all Avalon buses
      asi_sop_in              : in  std_logic;
      asi_eop_in              : in  std_logic;
      asi_valid_in            : in  std_logic;
      asi_data_in             : in  std_logic_vector(BITS_PER_SYMBOL-1 downto 0);  --Parallel shift register data output
      asi_channel_in          : in  std_logic_vector(NUM_CHANNEL_BITS-1 downto 0); --Channel number output
      
      --Avalon-MM Slave Interface
      avs_read                : in  std_logic;
      avs_write               : in  std_logic;
      avs_address             : in  std_logic_vector(3 downto 0); --Set large enough to handle control registers + 32 DAC channel registers
      avs_readdata            : out std_logic_vector(AVALON_MM_BUS_WIDTH-1 downto 0);
      avs_writedata           : in  std_logic_vector(AVALON_MM_BUS_WIDTH-1 downto 0);
		
		lock_in_toggle		: in std_logic;	
		lock_in_data		: out std_logic_vector(255 downto 0);
		lock_in_trigger	: out std_logic_vector(7 downto 0);
		
		fifo_start			: out std_logic
   );
	end component;

   signal clk : std_logic := '0';
   constant clk_period : time := 1.176 ns;
	signal outR : std_logic := '0';
	signal strt : std_logic := '0';
	signal rst : std_logic := '0';
	signal inn : std_logic_vector(63 downto 0) := (others => '0');
	
	signal read_ready : std_logic := '0';
	signal read_pt : std_logic := '0';
	signal read_addr : integer := 1;

	signal lock_in_input_val : std_logic_vector(63 downto 0) := (others => '0');
	signal asi_valid_in : std_logic := '0';
	signal asi_data_in : std_logic_vector(27 downto 0) := (others => '0');
	signal asi_channel_in : std_logic_vector(2 downto 0) := (others => '0');
	
	signal lock_in_toggle : std_logic := '0';
	signal lock_in_data : std_logic_vector(255 downto 0) := (others => '0');
	signal lock_in_trigger : std_logic_vector(7 downto 0) := (others => '0');
	
	signal fifo_start : std_logic := '0';
	signal asi_sop_in : std_logic := '0';
	signal asi_eop_in : std_logic := '0';
	
	signal avs_read : std_logic := '0';
	signal avs_write : std_logic := '0';
	signal avs_address : std_logic_vector(3 downto 0) := "0000";
	signal avs_writedata : std_logic_vector(31 downto 0) := (others => '0');
	signal avs_readdata : std_logic_vector(31 downto 0) := (others => '0');
	
BEGIN

   aa1:  avs_fifo
   port map
   (
      --Avalon Streaming data in
      asi_reset               => rst,
      asi_clk                 => clk,
      asi_sop_in              => asi_sop_in,
      asi_eop_in              => asi_eop_in,
      asi_valid_in            => asi_valid_in,
      asi_data_in             => asi_data_in,
      asi_channel_in          => asi_channel_in,
      
      --Avalon-MM Slave Interface
      avs_read                => avs_read,
      avs_write               => avs_write,
      avs_address             => avs_address,
      avs_readdata            => avs_readdata,
      avs_writedata           => avs_writedata,
		
		lock_in_toggle		=> lock_in_toggle,
		lock_in_data		=> lock_in_data,
		lock_in_trigger	=> lock_in_trigger,
		
		fifo_start			=> fifo_start
   );   

   -- Clock process definitions( clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;  --for 0.5 ns signal is '0'.
        clk <= '1';
        wait for clk_period/2;  --for next 0.5 ns signal is '1'.
   end process;
	
	test_proc : process
	begin
		wait for clk_period * 5;
		wait for clk_period * 0.5;
		--Writing test values
		lock_in_toggle <= '1';
		wait for clk_period;
		asi_channel_in <= "000";
		asi_valid_in <= '1';
		asi_data_in <= x"0000005";
		wait for clk_period;
		asi_data_in <= x"0000000";
		asi_valid_in <= '0';
		wait for clk_period;
		--1
		wait for clk_period;
		asi_channel_in <= "001";
		asi_valid_in <= '1';
		asi_data_in <= x"0000006";
		wait for clk_period;
		asi_data_in <= x"0000000";
		asi_valid_in <= '0';
		wait for clk_period;
		--2
		wait for clk_period;
		asi_channel_in <= "010";
		asi_valid_in <= '1';
		asi_data_in <= x"0000007";
		wait for clk_period;
		asi_data_in <= x"0000000";
		asi_valid_in <= '0';
		wait for clk_period;
		--3
		wait for clk_period;
		asi_channel_in <= "011";
		asi_valid_in <= '1';
		asi_data_in <= x"0000008";
		wait for clk_period;
		asi_data_in <= x"0000000";
		asi_valid_in <= '0';
		wait for clk_period;
		
		
		wait for clk_period * 100;
	end process;

END;